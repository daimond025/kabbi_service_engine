"use strict";
const lib = require("../../services/lib");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const config = lib.getConfig();
const redis = require("redis");
const retryStrategy = require("./redisRetryStrategy");


/**
 * Async answer manager
 * @param {Number} port
 * @param  {String} ip
 * @returns {RedisAsyncAnswerManager}
 */
function RedisAsyncAnswerManager(port, ip) {
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db12");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db12");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db12:" + err);
    });

    this.dbNumber = 12;
}

/**
 * Set answer of async request to redis with TTL
 * @param {String} reqId
 * @param {Object} asyncAnswerObj
 * @param {Number} messageTTL  Time to live at seconds
 * @param {Function} callback
 */
RedisAsyncAnswerManager.prototype.setAsyncAnswer = function (reqId, asyncAnswerObj, messageTTL, callback) {
    const self = this;
    if (!messageTTL) {
        messageTTL = 30;
    }
    try {
        asyncAnswerObj = JSON.stringify(asyncAnswerObj);
    } catch (err) {
        return callback(err);
    }
    self.redisClientDb.select(self.dbNumber, () => {
        const setResult = self.redisClientDb.set(reqId, asyncAnswerObj, redis.print);
        if (setResult || setResult === 0) {
            self.redisClientDb.expire(reqId, messageTTL);
            return callback(null, 1);
        }
        callback(new Error('Error - set bad result. Set result:' + setResult));
    });
};

/**
 * Get answer of async request
 * @param {String} reqId
 * @param {Function} callback
 */
RedisAsyncAnswerManager.prototype.getAsyncAnswer = function (reqId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(reqId, (err, result) => {
            if (err) {
                return callback(err);
            }
            try {
                result = JSON.parse(result);
                callback(null, result);
            } catch (err) {
                callback(err);
            }
        });
    });

};

module.exports = exports = new RedisAsyncAnswerManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);