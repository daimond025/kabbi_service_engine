"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const redis = require("redis");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const retryStrategy = require("./redisRetryStrategy");


/**
 * Parking Manager
 * @param {Number} port
 * @param {String} ip
 * @returns {RedisParkingManager}
 */
function RedisParkingManager(port, ip) {

    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});
    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db8");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db8");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db8:" + err);
    });

    this.dbNumber = 8;

}

/**
 * Get worker rating on parking
 * @param  {Number} parkingId
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
RedisParkingManager.prototype.getWorkerRaitingAtParking = function (parkingId, workerCallsign, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, function () {
        self.redisClientDb.hget([parkingId, workerCallsign], (err, rating) => {
            if (err) {
                return callback(err);
            }
            callback(null, rating);
        });
    });
};

/**
 *  Delete worker from parking
 * @param  {Number} parkingId
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
RedisParkingManager.prototype.deleteWorkerFromParking = function (parkingId, workerCallsign, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, function () {
        self.redisClientDb.hdel([parkingId, workerCallsign], (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

/**
 * Get workers callsign on parking
 * @param  {Number} parkingId
 * @param  {Function} callback
 */
RedisParkingManager.prototype.getAllWorkersOnParking = function (parkingId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, function () {
        self.redisClientDb.hkeys(parkingId, (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

module.exports = exports = new RedisParkingManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);