const config = require("../../services/lib").getConfig();
const nodeId = config.MY_ID;
const uuid = require('uuid/v4');
const orderEventSenderService = require('./orderEventSenderService');

/**
 * Create worker AMQP message on order event
 * @param {Object} options
 * @param {string} options.command
 * @param {number} options.tenant_id
 * @param {string} options.tenant_login
 * @param {string} options.worker_callsign
 * @param {object} options.params
 * @return {string} message
 */

module.exports = (options) => {
    return JSON.stringify({
        uuid: uuid(),
        type: 'order_event',
        timestamp: Math.floor(Date.now() / 1000),
        sender_service: orderEventSenderService.engineService,
        command: options.command,
        tenant_id: options.tenant_id,
        tenant_login: options.tenant_login,
        worker_callsign: options.worker_callsign,
        change_sender_id: nodeId,
        params: options.params
    });
};