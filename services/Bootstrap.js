'use strict';
const mongoDB = require('../database/mongo/MongoDB');
const consulServiceWorker = require("../services/consul/consulServiceWorker");
const mainLogger = require("../services/logger").mainLogger;

const getOrderAfterRestart = require("../services/serviceRestartProcess/getOrderAfterRestart");
const getWorkerAfterRestart = require("../services/serviceRestartProcess/getWorkerAfterRestart");
class Bootstrap {

    /**
     * Start
     * @returns {Promise<any>}
     */
    start() {
        return new Promise((resolve, reject) => {

            const dbConnections = [
                mongoDB.connect(),
            ];
            Promise.all(dbConnections)
                .then(dbConnectionsResult => {
                    mainLogger("info", `bootstrap->start->dbConnections result: ${dbConnectionsResult}`);
                    return consulServiceWorker.run();
                })
                .then(consulResult => {
                    mainLogger("info", `bootstrap->start->consulServiceWorker.run result: ${consulResult}`);
                    //return resolve(true);
                    return getOrderAfterRestart.run();
                }).then(ordersRezult => {
                    mainLogger("info", `bootstrap->start->getOrderAfterRestart.run result: ${ordersRezult}`);
                    //return resolve(true);
                    return getWorkerAfterRestart.run();
                })
                .then(workerRezult => {
                    mainLogger("info", `bootstrap->start->consulServiceWorker.run result: ${workerRezult}`);
                    return resolve(true);
                })
                .catch(err => {
                    console.log(err);
                    return reject(err);
                });
        });

    }

    /**
     * Exit
     * @param code
     */
    exit(code = 0) {
        if (code === 0) {
            Promise.all([
                mongoDB.close(),
                consulServiceWorker.deregister(),
            ])
                .then(result => {
                    mainLogger("info", `bootstrap->exit->result: ${result}`);
                })
                .catch(err => {
                    mainLogger("error", `bootstrap->exit->error: ${err.message}`);
                })
                .then(() => {
                    mainLogger("info", `bootstrap->process exit with code=${code}`);
                    return process.exit(code);
                })
        } else {
            mainLogger("info", `bootstrap->process exit with code=${code}`);
            return process.exit(code);
        }
    }
}

module.exports = exports = new Bootstrap();
