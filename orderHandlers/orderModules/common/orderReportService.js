'use strict';


const serviceApi = require("../../../services/externalWebServices/serviceApi");
const moment = require('moment');
const sqlManager = require('../../../database/sqlDB/sqlManager');
const PHPUnserialize = require('php-unserialize');
const OrderDetailCostService = require('./orderDetailCostService');

/**
 * @class OrderReportService
 */
class OrderReportService {

    /**
     * @constructor
     */
    constructor() {

    }

    /**
     * Send order report to client email
     * @param {object} orderData
     * @return {Promise<any>}
     */
    sendOrderReportToClientEmail(orderData) {
        return new Promise((resolve, reject) => {
            try {
                const tenantId = parseInt(orderData.tenant_id);
                const type = 'client_order_report';
                const to = orderData.client.email;
                const lang = orderData.client.lang ? orderData.client.lang : 'en';
                const cityId = orderData.city_id;
                let emailParams = {
                    order_number: orderData.order_number ? orderData.order_number : null,
                    order_date: OrderReportService._orderTime(orderData.order_time, lang),
                    address_points: OrderReportService._addressPoints(orderData.address),
                    tariff_name: orderData.tariff ? (orderData.tariff.name ? orderData.tariff.name : null) : null,
                    company_name: orderData.tenant ? (orderData.tenant.company_name ? orderData.tenant.company_name : null) : null,
                    company_phone: orderData.tenant ? (orderData.tenant.contact_phone ? orderData.tenant.contact_phone : null) : null,
                    company_email: orderData.tenant ? (orderData.tenant.contact_email ? orderData.tenant.contact_email : null) : null,
                    company_site: orderData.tenant ? (orderData.tenant.site ? orderData.tenant.site : null) : null,
                    worker_name: orderData.worker ? `${orderData.worker.last_name} ${orderData.worker.name} ${orderData.worker.second_name}` : null,
                    worker_phone: orderData.worker ? (orderData.worker.phone ? orderData.worker.phone : null) : null,
                    car_model: orderData.car ? (orderData.car.model ? orderData.car.model : null) : null,
                    car_brand: orderData.car ? (orderData.car.brand ? orderData.car.brand : null) : null,
                    car_color: orderData.car ? (orderData.car.color ? orderData.car.color : null) : null,
                    car_gos_number: orderData.car ? (orderData.car.gos_number ? orderData.car.gos_number : null) : null,
                };
                sqlManager.getCurrencyById(orderData.currency_id)
                    .then(currency => {
                        if (currency) {
                            Object.assign(emailParams, {currency_symbol: currency.symbol});
                        }
                        return sqlManager.getOrderDetailCost(orderData.order_id)
                    })
                    .then(orderDetailCost => {
                        if (orderDetailCost) {
                            const orderDetailCostFormatted = OrderReportService._formatOrderDetailCost(orderDetailCost);
                            Object.assign(emailParams, orderDetailCostFormatted);
                        }
                        return serviceApi.sendEmail(tenantId, type, to, cityId, lang, emailParams);
                    })
                    .then(result => {
                        return resolve(result);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            } catch (err) {
                return reject(err);
            }
        })
    }


    /**
     * format order detail cost
     * @param orderDetailCost
     * @returns {{order_time: *, order_cost: *, supply_cost: *, included_supply_time: *, included_supply_distance: *, included_supply_cost: number, supply_outside_city_tariff: *, supply_outside_city_meter: *, supply_outside_city_cost: *, before_boarding_tariff: *, before_boarding_meter: *, before_boarding_cost: *, city_distance_tariff: *, city_distance_meter: *, city_distance_cost: *, city_time_tariff: *, city_time_meter: *, city_time_cost: *, outside_city_distance_tariff: *, outside_city_distance_meter: *, outside_city_distance_cost: *, outside_city_time_tariff: *, outside_city_time_meter: *, outside_city_time_cost: *, waiting_city_tariff: *, waiting_city_meter: *, waiting_city_cost: *, waiting_outside_city_tariff: *, waiting_outside_city_meter: *, waiting_outside_city_cost: *, additional_cost: *, summary_distance: *, summary_time: *, bonus_replenishment: *, promo_bonus_replenishment: *, summary_cost_no_discount: *, summary_cost: *}}
     * @private
     */
    static _formatOrderDetailCost(orderDetailCost) {
        return new OrderDetailCostService(orderDetailCost).getDetailingOrderReport();
    }

    /**
     * Get address points as string, .e.g:
     *  фронотовая 4; советская 22; парк кирова фронтовая 22;
     * @param addressRaw
     * @return {string|null}
     * @private
     */
    static _addressPoints(addressRaw) {
        let addressFull;
        try {
            addressFull = PHPUnserialize.unserialize(addressRaw);
        } catch (err) {
            return null;
        }
        let addressPoints = [];
        for (let point in addressFull) {
            if (addressFull.hasOwnProperty(point)) {
                let addressInfo = addressFull[point];
                let address = '';
                if (addressInfo.street) {
                    address += addressInfo.street;
                }
                if (addressInfo.house) {
                    address += ', ' + addressInfo.house;
                }
                address = address.replace(';', '');
                addressPoints.push(address);
            }
        }
        return addressPoints.join(';');
    }

    /**
     * Get formatted order time as string
     * @param orderTimeRaw
     * @param lang
     * @return {string|null}
     * @private
     */
    static _orderTime(orderTimeRaw, lang) {
        moment.locale(lang);
        return moment.unix(orderTimeRaw).format("LLL");
    }
}

module.exports = exports = new OrderReportService();