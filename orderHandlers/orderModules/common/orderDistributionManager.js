'use strict';

const logger = require('../../../services/logger');
const orderLogger = logger.orderLogger;
const geoLib = require("geolib");
const redisWorkerManager = require('../../../database/redisDB/redisWorkerManager');
const redisParkingManager = require('../../../database/redisDB/redisParkingManager');
const rabbitmqHttpService = require('../../../services/rabbitmqHttpService');
const orderActionManager = require('./orderActionManager');
const sqlManager = require("../../../database/sqlDB/sqlManager");
const async = require("async");
const pushApi = require("../../../services/externalWebServices/pushApi");
const redisOrderManager = require("../../../database/redisDB/redisOrderManager");
const amqpWorkerMessageSender = require("../../../services/amqpWorkerMessageSender");
const amqpOrderMessageSender = require("../../../services/amqpOrderMessageSender");
const orderEventOrderMessage = require('../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../orderEvent/orderEventWorkerCommand');
const statusInfo = require('../../../services/orderStatusManager').statusInfo;
const PHPUnserialize = require('php-unserialize');
const workerStatusManager = require("../../../services/workerStatusManager");

const serviceApi = require('../../../services/externalWebServices/serviceApi');
const lib = require('../../../services/lib');

const redisWorkerTimeManager = require('../../../database/redisDB/redisTimeWorkerManager');
const orderType = require("../../orderEvent/orderDevice");

/**
 * @class OrderDistributionManager
 */
class OrderDistributionManager {

    /**
     *@constructor
     */
    constructor() {
    }

    get distanceType() {
        return 1;
    }

    get parkingType() {
        return 2;
    }

    get inRotationMode() {
        return 'queue';
    }

    get multipleMode() {
        return 'batch';
    }

    /**
     * Run/Continue order distribution process
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     */
    runDistributionProcess(orderInstance, orderData) {
        orderLogger(orderInstance.orderId, 'info', 'orderDistributionManager->runDistributionProcess->Called!');
        if (orderInstance.isFinished) {
            orderLogger(orderInstance.orderId, 'info', 'orderDistributionManager->runDistributionProcess->order already finished..');
            return;
        }

        if (!orderInstance.isForAllWorkers) {
            orderLogger(orderInstance.orderId, 'info', 'orderDistributionManager->runDistributionProcess->Order is not for all workers!');
            orderInstance.isForAllWorkers = true;
            orderActionManager.sendOrderToFreeStatus(orderData, false)
                .then(result => {
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runDistributionProcess->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                });
        } else {
            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->currentDistributionCircle: ${orderInstance.currentDistributionCircle}`);
            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->currentDistributionCircleRepeat: ${orderInstance.currentDistributionCircleRepeat}`);
            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->offeredInCircleRepeatWorkersList: ${Array.from(orderInstance.offeredInCircleRepeatWorkersList)}`);
            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->offeredInCircleWorkersList: ${Array.from(orderInstance.offeredInCircleWorkersList)}`);
            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->offeredInAllTimeWorkersList: ${Array.from(orderInstance.offeredInAllTimeWorkersList)}`);


            switch (orderInstance.settings.TYPE_OF_DISTRIBUTION_ORDER) {
                case this.distanceType:
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->maxDistanceDistributionCircle: ${orderInstance.maxDistanceDistributionCircle}`);
                    this._runDistanceDistributionType(orderInstance, orderData);
                    break;
                case this.parkingType:
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runDistributionProcess->maxParkingDistributionRepeat: ${orderInstance.settings.CIRCLES_DISTRIBUTION_ORDER}`);
                    this._runParkingDistributionType(orderInstance, orderData);
                    break;
                default:
                    orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runDistributionProcess->Order has unsupported distribution type : ${orderInstance.settings.TYPE_OF_DISTRIBUTION_ORDER}`);
                    break;
            }

        }
    }

    /**
     * Run personal offer order for worker
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @param {Number} workerCallsign
     */
    runPersonalOffer(orderInstance, orderData, workerCallsign) {
        orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->Need send personal offer order№${orderInstance.orderId} for worker ${workerCallsign} `);
        orderInstance.isForAllWorkers = false;
        redisWorkerManager.getWorker(orderInstance.tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                orderLogger(orderInstance.orderId, 'error', 'orderDistributionManager->runPersonalOffer->getWorker->Error');
                orderLogger(orderInstance.orderId, "error", err);
                return;
            }
            this.isGoodWorkerForOrder(orderData, workerData, {
                distanceForSearch: Number.MAX_SAFE_INTEGER,
                needCalcDistance: false,
                needCheckSocket: false,
                needCheckStatus: false,
                exceptCarModels: orderInstance.exceptCarModels,
                dislikeWorkerIds: orderInstance.dislikeWorkerIds,

            }).then(workerInfo => {
                rabbitmqHttpService.getQueueConsumersCount(`${orderInstance.tenantId}_worker_${workerCallsign}`, null, (err, result) => {
                    if (err) {
                        orderLogger(orderInstance.orderId, 'error', `orderDistributionManager-runPersonalOffer->getQueueConsumersCount error`);
                        orderLogger(orderInstance.orderId, 'error', err.message);
                        orderActionManager.sendOrderToFreeStatus(orderData, false)
                            .then(result => {
                                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                            });
                    } else {
                        orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->is socket connected of ${workerCallsign}? result: ${result}`);
                        if (result >= 1) {
                            this._sendOfferOrder({
                                tenantId: orderInstance.tenantId,
                                tenantLogin: orderData.tenant_login,
                                workerInfo,
                                orderId: orderInstance.orderId,
                                orderCurrencyId: orderData.currency_id,
                                offerSec: orderInstance.settings.ORDER_OFFER_SEC
                            })
                                .then(result => {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->Send offer order to worker ${workerCallsign}! `);
                                })
                                .catch(err => {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->sendOfferOrder->Can't send offer order to  ${workerCallsign}! Reason: ${err.message}`);
                                    orderActionManager.sendOrderToFreeStatus(orderData, false)
                                        .then(result => {
                                            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                                        })
                                        .catch(err => {
                                            orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                                        });
                                });

                        } else {
                            orderActionManager.sendOrderToFreeStatus(orderData, false)
                                .then(result => {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                                })
                                .catch(err => {
                                    orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                                });
                        }
                    }
                });
            }).catch(err => {
                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->runPersonalOffer->isGoodWorkerForOrder->${err.message}`);
                orderActionManager.sendOrderToFreeStatus(orderData, false)
                    .then(result => {
                        orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->runPersonalOffer->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                    });
            });

        });

    }

    /**
     * Run/Continue distance distribution process
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @private
     */
    _runDistanceDistributionType(orderInstance, orderData) {

        this._distributeOrderByDistance(orderInstance, orderData)
            .then(result => {
                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->_distributeOrderByDistance->Result ${result}`);
            })
            .catch(err => {
                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->_distributeOrderByDistance-> ${err.message}`);
                if (orderInstance.isFinished) {
                    orderLogger(orderInstance.orderId, 'info', 'orderDistributionManager->_runDistanceDistributionType->order already finished..');
                    return;
                }
                let sendToFree = false;

                //Если это последний повтор в круге
                if (orderInstance.currentDistributionCircleRepeat >= orderInstance.settings[`CIRCLE_REPEAT_${orderInstance.currentDistributionCircle}`]) {
                    //Если это не последний круг
                    if (orderInstance.currentDistributionCircle < orderInstance.maxDistanceDistributionCircle) {
                        //Увеличиваем круг
                        orderInstance.currentDistributionCircle++;
                        //Выставляем повтор в новом круге
                        orderInstance.currentDistributionCircleRepeat = 1;
                        //Чистим список водителей, которым предлагался заказ в повторе
                        orderInstance.offeredInCircleRepeatWorkersList.clear();
                        //Копириуем всех водителей, котороым предлагался заказ в круге в глобальный список
                        orderInstance.offeredInCircleWorkersList.forEach(workerCallsign => {
                            orderInstance.offeredInAllTimeWorkersList.add(workerCallsign);
                        });
                        //Чистим список водителей, которым предлагался заказ в круге
                        orderInstance.offeredInCircleWorkersList.clear();
                    } else {
                        sendToFree = true;
                    }
                } else {
                    orderInstance.offeredInCircleRepeatWorkersList.clear();
                    orderInstance.currentDistributionCircleRepeat++;
                }

                if (sendToFree) {
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->_distributeOrderByDistance->Maximum number of order distribution cycles reached. Need send order to free`);

                    orderActionManager.sendOrderToFreeStatus(orderData, false)
                        .then(result => {
                            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runDistanceDistributionType->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                        });
                } else {
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->New order distribution cycle will be call after ${orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER} seconds`);
                    if (orderInstance && orderInstance.orderTimer) {
                        orderInstance.orderTimer.clearAll();
                    }
                    if (orderInstance.settings.FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES) {
                        orderActionManager.sendOrderToFreeStatus(orderData, true)
                            .then(result => {
                                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runDistanceDistributionType->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                            })
                            .then(() => {
                                if (orderInstance && orderInstance.orderTimer) {
                                    orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel, () => {
                                        orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->New order distribution circle repeat.Value:${orderInstance.currentDistributionCircleRepeat}`);
                                        this.runDistributionProcess(orderInstance, orderData);
                                    }, orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER * 1000);
                                }
                            });
                    } else {
                        orderData.status_id = statusInfo.new_order.status_id;
                        orderData.status.status_id = statusInfo.new_order.status_id;
                        orderData.status.name = statusInfo.new_order.name;
                        orderData.status.status_group = statusInfo.new_order.status_group;
                        orderData.worker_id = null;
                        orderData.car_id = null;
                        orderData.car = null;
                        orderData.worker = null;
                        const updateTime = Math.floor(Date.now() / 1000);
                        orderData.update_time = updateTime;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runDistanceDistributionType->saveOrder->Error: ${err.message}`);
                            }
                            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->saveOrder->Result: ${result}`);
                            sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.new_order.status_id, null, null, updateTime, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runDistanceDistributionType->updateOrderFromMysql->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->updateOrderFromMysql->Result: ${result}`);
                                }

                            });
                            if (orderInstance && orderInstance.orderTimer) {
                                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel, () => {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runDistanceDistributionType->New order distribution circle repeat.Value:${orderInstance.currentDistributionCircleRepeat}`);
                                    this.runDistributionProcess(orderInstance, orderData);
                                }, orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER * 1000);
                            }
                        });
                    }
                }
            });
    }


    /**
     * Run/Continue parking distribution process
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @private
     */
    _runParkingDistributionType(orderInstance, orderData) {
        this._distributeOrderByParking(orderInstance, orderData)
            .then(result => {
                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->_distributeOrderByParking->Result ${result}`);
            })
            .catch(err => {
                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->_distributeOrderByParking-> ${err.message}`);
                if (orderInstance.isFinished) {
                    orderLogger(orderInstance.orderId, 'info', 'orderDistributionManager->_runParkingDistributionType->order already finished..');
                    return;
                }
                let sendToFree = false;
                if (orderInstance.currentDistributionCircleRepeat >= orderInstance.settings.CIRCLES_DISTRIBUTION_ORDER) {
                    sendToFree = true;
                } else {
                    orderInstance.offeredInCircleRepeatWorkersList.clear();
                    orderInstance.currentDistributionCircleRepeat++;
                }
                if (sendToFree) {
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->_distributeOrderByParking->Maximum number of order distribution circle repeat reached. Need send order to free`);
                    orderActionManager.sendOrderToFreeStatus(orderData, false)
                        .then(result => {
                            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runParkingDistributionType->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                        });
                } else {
                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->New order distribution circle repeat will be call after ${orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER} seconds`);
                    if (orderInstance && orderInstance.orderTimer) {
                        orderInstance.orderTimer.clearAll();
                    }
                    if (orderInstance.settings.FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES) {
                        orderActionManager.sendOrderToFreeStatus(orderData, true)
                            .then(result => {
                                orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->orderActionManager.sendOrderToFreeStatus->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runParkingDistributionType->orderActionManager.sendOrderToFreeStatus->Error: ${err.message}`);
                            })
                            .then(() => {
                                if (orderInstance && orderInstance.orderTimer) {
                                    orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel, () => {
                                        orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->New order distribution circle repeat. Value:${orderInstance.currentDistributionCircleRepeat}`);
                                        this.runDistributionProcess(orderInstance, orderData);
                                    }, orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER * 1000);
                                }
                            });
                    } else {
                        orderData.status_id = statusInfo.new_order.status_id;
                        orderData.status.status_id = statusInfo.new_order.status_id;
                        orderData.status.name = statusInfo.new_order.name;
                        orderData.status.status_group = statusInfo.new_order.status_group;
                        orderData.worker_id = null;
                        orderData.car_id = null;
                        orderData.car = null;
                        orderData.worker = null;
                        const updateTime = Math.floor(Date.now() / 1000);
                        orderData.update_time = updateTime;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runParkingDistributionType->saveOrder->Error: ${err.message}`);
                            }
                            orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->saveOrder->Result: ${result}`);
                            sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.new_order.status_id, null, null, updateTime, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_runParkingDistributionType->updateOrderFromMysql->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->updateOrderFromMysql->Result: ${result}`);
                                }

                            });
                            if (orderInstance && orderInstance.orderTimer) {
                                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel, () => {
                                    orderLogger(orderInstance.orderId, 'info', `orderDistributionManager->_runParkingDistributionType->New order distribution circle repeat. Value:${orderInstance.currentDistributionCircleRepeat}`);
                                    this.runDistributionProcess(orderInstance, orderData);
                                }, orderInstance.settings.DELAY_CIRCLES_DISTRIBUTION_ORDER * 1000);
                            }
                        });
                    }

                }
            });
    }

    /**
     * Find best workers for distance distribution and send offer order
     * @param {BaseGootaxOrder} orderInstance
     * @param {object} orderData
     * @returns {Promise}
     * @private
     */
    _distributeOrderByDistance(orderInstance, orderData) {
        const self = this;
        const offerOrderMode = orderInstance.settings.OFFER_ORDER_TYPE ? orderInstance.settings.OFFER_ORDER_TYPE : this.inRotationMode;
        const offerOrderWorkersCount = offerOrderMode === this.inRotationMode ? 1 : orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT ? orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT : 2;

        return new Promise((resolve, reject) => {
            this._getRelevantWorkersListByDistance(orderInstance, orderData, offerOrderMode, offerOrderWorkersCount)
                .then((relevantWorkersList) => {
                    function offerSender(relevantWorkersList) {
                        if (relevantWorkersList.length === 0) {
                            return reject(new Error("No relevant workers"));
                        }

                        redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, "info", "orderDistributionManager->_distributeOrderByDistance->Don't need distribute order");
                                orderLogger(orderInstance.orderId, "error", `orderDistributionManager->_distributeOrderByDistance->getOrder->Error: ${err.message}`);
                                return resolve(1);
                            }
                            if (self._getRelevantOfferOrderStatuses().indexOf(parseInt(orderData.status.status_id)) === -1) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_distributeOrderForDistance->Don't need distribute order. Current order status id: ${orderData.status.status_id}`);
                                return resolve(1);
                            }

                            if (!orderInstance.settings.FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES) {
                                if ([statusInfo.free_order.status_id, statusInfo.outdated_order.status_id].indexOf(parseInt(orderData.status_id)) !== -1) {
                                    orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_distributeOrderForDistance->Don't need distribute order. Current order status: ${orderData.status_id}`);
                                    return resolve(1);
                                }
                            }


                            if (offerOrderMode === self.multipleMode) {

                                self._sendBatchOfferOrder({
                                    tenantId: orderInstance.tenantId,
                                    tenantLogin: orderData.tenant_login,
                                    workers: relevantWorkersList,
                                    orderId: orderInstance.orderId,
                                    orderCurrencyId: orderData.currency_id,
                                    offerSec: orderInstance.settings.ORDER_OFFER_SEC
                                })
                                    .then(result => {
                                        for (let worker of relevantWorkersList) {
                                            orderInstance.offeredInCircleRepeatWorkersList.add(worker.worker_callsign);
                                            orderInstance.offeredInCircleWorkersList.add(worker.worker_callsign);
                                        }

                                        orderInstance.settingNewOrderAssign = 0;
                                        return resolve(1);
                                    })
                                    .catch(err => {
                                        for (let worker of relevantWorkersList) {
                                            orderInstance.offeredInCircleRepeatWorkersList.add(worker.worker_callsign);
                                            orderInstance.offeredInCircleWorkersList.add(worker.worker_callsign);
                                        }
                                        offerSender(relevantWorkersList);
                                    });
                            } else {
                                const topWorker = relevantWorkersList.shift();
                                self._sendOfferOrder({
                                    tenantId: orderInstance.tenantId,
                                    tenantLogin: orderData.tenant_login,
                                    workerInfo: topWorker,
                                    orderId: orderInstance.orderId,
                                    orderCurrencyId: orderData.currency_id,
                                    offerSec: orderInstance.settings.ORDER_OFFER_SEC
                                })
                                    .then(result => {
                                        orderInstance.offeredInCircleRepeatWorkersList.add(topWorker.worker_callsign);
                                        orderInstance.offeredInCircleWorkersList.add(topWorker.worker_callsign);

                                        orderInstance.settingNewOrderAssign = 0;
                                        return resolve(1);
                                    })
                                    .catch(err => {
                                        orderInstance.offeredInCircleRepeatWorkersList.add(topWorker.worker_callsign);
                                        orderInstance.offeredInCircleWorkersList.add(topWorker.worker_callsign);
                                        offerSender(relevantWorkersList);
                                    });
                            }
                        });
                    }

                    offerSender(relevantWorkersList);
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    /**
     * Find best worker for parking distribution and send offer order
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @returns {Promise}
     * @private
     */
    _distributeOrderByParking(orderInstance, orderData) {
        const self = this;
        const offerOrderMode = orderInstance.settings.OFFER_ORDER_TYPE ? orderInstance.settings.OFFER_ORDER_TYPE : this.inRotationMode;
        const offerOrderWorkersCount = offerOrderMode === this.inRotationMode ? 1 : orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT ? orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT : 2;
        return new Promise((resolve, reject) => {
            this._getRelevantWorkersListByParking(orderInstance, orderData, offerOrderMode, offerOrderWorkersCount)
                .then((workersList) => {
                    function offerSender(relevantWorkersList) {
                        if (relevantWorkersList.length === 0) {
                            return reject(new Error("No relevant workers"));
                        }
                        redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, "info", "orderDistributionManager->_distributeOrderByParking->Don't need distribute order");
                                orderLogger(orderInstance.orderId, "error", `orderDistributionManager->_distributeOrderByParking->Error: ${err.message}`);
                                return resolve(1);
                            }
                            if (self._getRelevantOfferOrderStatuses().indexOf(parseInt(orderData.status.status_id)) === -1) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_distributeOrderByParking->Don't need distribute order. Current order status id: ${orderData.status.status_id}`);
                                return resolve(1);
                            }
                            if (!orderInstance.settings.FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES) {
                                if ([statusInfo.free_order.status_id, statusInfo.outdated_order.status_id].indexOf(parseInt(orderData.status_id)) !== -1) {
                                    orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_distributeOrderByParking->Don't need distribute order. Current order status: ${orderData.status_id}`);
                                    return resolve(1);
                                }
                            }
                            if (offerOrderMode === self.multipleMode) {
                                self._sendBatchOfferOrder({
                                    tenantId: orderInstance.tenantId,
                                    tenantLogin: orderData.tenant_login,
                                    workers: relevantWorkersList,
                                    orderId: orderInstance.orderId,
                                    orderCurrencyId: orderData.currency_id,
                                    offerSec: orderInstance.settings.ORDER_OFFER_SEC
                                })
                                    .then(result => {
                                        for (let worker of relevantWorkersList) {
                                            orderInstance.offeredInCircleRepeatWorkersList.add(worker.worker_callsign);
                                            orderInstance.offeredInCircleWorkersList.add(worker.worker_callsign);
                                        }
                                        orderInstance.settingNewOrderAssign = 0;
                                        return resolve(1);
                                    })
                                    .catch(err => {
                                        for (let worker of relevantWorkersList) {
                                            orderInstance.offeredInCircleRepeatWorkersList.add(worker.worker_callsign);
                                            orderInstance.offeredInCircleWorkersList.add(worker.worker_callsign);
                                        }
                                        offerSender(relevantWorkersList);
                                    });
                            } else {
                                const topWorker = relevantWorkersList.shift();
                                self._sendOfferOrder({
                                    tenantId: orderInstance.tenantId,
                                    tenantLogin: orderData.tenant_login,
                                    workerInfo: topWorker,
                                    orderId: orderInstance.orderId,
                                    orderCurrencyId: orderData.currency_id,
                                    offerSec: orderInstance.settings.ORDER_OFFER_SEC
                                })
                                    .then(result => {
                                        orderInstance.offeredInCircleRepeatWorkersList.add(topWorker.worker_callsign);
                                        orderInstance.offeredInCircleWorkersList.add(topWorker.worker_callsign);

                                        orderInstance.settingNewOrderAssign = 0;
                                        return resolve(1);
                                    })
                                    .catch(err => {
                                        orderInstance.offeredInCircleRepeatWorkersList.add(topWorker.worker_callsign);
                                        orderInstance.offeredInCircleWorkersList.add(topWorker.worker_callsign);
                                        offerSender(relevantWorkersList);
                                    });
                            }

                        });
                    }

                    offerSender(workersList);
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }


    /**
     * Sort relevant workers by distance
     * @param {number} orderId
     * @param {array} relevantWorkers
     * @param {object} options
     * @param {string} options.offerOrderMode
     * @param {number} options.offerWorkersCount
     * @param {boolean} options.checkWorkerRating
     * @param {number} options.promoWorkerId
     * @returns {array}
     */

    sortRelevantWorkersByDistance(orderId, relevantWorkers, options) {
        let promoWorkerIndex = null;
        let promoWorker = null;

        if (options.promoWorkerId) {
            orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByDistance->order has promo worker id: ${options.promoWorkerId}`);
            relevantWorkers.forEach(function (item, i, arr) {
                if (item.worker_id === options.promoWorkerId) {
                    orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByDistance->promo worker is in filteredWorkers list`);
                    promoWorkerIndex = i;
                    promoWorker = relevantWorkers[promoWorkerIndex];
                }
            });
            if (promoWorkerIndex) {
                relevantWorkers.splice(promoWorkerIndex + 1, 1);
            }
        }


        relevantWorkers.sort((prevWorker, nextWorker) => {
            if (prevWorker.worker_group_priority > nextWorker.worker_group_priority) {
                return -1;
            }
            if (prevWorker.worker_group_priority < nextWorker.worker_group_priority) {
                return 1;
            }
            if (prevWorker.worker_group_priority === nextWorker.worker_group_priority) {
                if (options.checkWorkerRating) {
                    if (prevWorker.worker_personal_rating > nextWorker.worker_personal_rating) {
                        return -1;
                    }
                    if (prevWorker.worker_personal_rating < nextWorker.worker_personal_rating) {
                        return 1;
                    }
                    if (prevWorker.worker_personal_rating === nextWorker.worker_personal_rating) {
                        if (prevWorker.worker_distance_to_order > nextWorker.worker_distance_to_order) {
                            return 1;
                        } else if (prevWorker.worker_distance_to_order < nextWorker.worker_distance_to_order) {
                            return -1;
                        } else if (prevWorker.worker_distance_to_order === nextWorker.worker_distance_to_order) {
                            return 0;
                        }
                    }
                } else {
                    if (prevWorker.worker_distance_to_order > nextWorker.worker_distance_to_order) {
                        return 1;
                    } else if (prevWorker.worker_distance_to_order < nextWorker.worker_distance_to_order) {
                        return -1;
                    } else if (prevWorker.worker_distance_to_order === nextWorker.worker_distance_to_order) {
                        return 0;
                    }
                }
            }
        });
        if (promoWorker) {
            relevantWorkers.unshift(promoWorker);
        }

        if (options.offerOrderMode === this.multipleMode) {
            relevantWorkers = relevantWorkers.slice(0, options.offerWorkersCount);
        }

        orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByDistance->sorted workers: ${JSON.stringify(relevantWorkers, (key, value) => {
            if (key === "worker_worker" || key === "worker_car") {
                return '';
            }
            return value;
        }, 4)}`);
        return relevantWorkers;
    }

    /**
     * Sort relevant workers by parking
     * @param {number} orderId
     * @param {array} relevantWorkers
     * @param {object} options
     * @param {string} options.offerOrderMode
     * @param {number} options.offerWorkersCount
     * @param {boolean} options.checkWorkerRating
     * @param {number} options.promoWorkerId
     * @returns {array}
     */
    sortRelevantWorkersByParking(orderId, relevantWorkers, options) {
        let promoWorkerIndex = null;
        let promoWorker = null;

        if (options.promoWorkerId) {
            orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByParking->order has promo worker id: ${options.promoWorkerId}`);
            relevantWorkers.forEach(function (item, i, arr) {
                if (item.worker_id === options.promoWorkerId) {
                    orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByParking->promo worker is in filteredWorkers list`);
                    promoWorkerIndex = i;
                    promoWorker = relevantWorkers[promoWorkerIndex];
                }
            });
            if (promoWorkerIndex) {
                relevantWorkers.splice(promoWorkerIndex + 1, 1);
            }
        }

        if (options.checkWorkerRating) {
            relevantWorkers.sort((a, b) => {
                return b.worker_personal_rating - a.worker_personal_rating;
            });
        } else {
            relevantWorkers.sort((a, b) => {
                return a.worker_parking_index - b.worker_parking_index;
            });
        }

        if (promoWorker) {
            relevantWorkers.unshift(promoWorker);
        }

        if (options.offerOrderMode === this.multipleMode) {
            relevantWorkers = relevantWorkers.slice(0, options.offerWorkersCount);
        }
        orderLogger(orderId, "info", `distributeOrderForDistance->sortRelevantWorkersByParking->Filtered workers: ${JSON.stringify(relevantWorkers, (key, value) => {
            if (key === "worker_worker" || key === "worker_car") {
                return '';
            }
            return value;
        }, 4)}`);

        return relevantWorkers;
    }


    /**
     * Get relevant workers list by distance
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @param {String} offerOrderMode
     * @param {Number} workersCount
     * @return {Promise}
     * @private
     */
     _getRelevantWorkersListByDistance(orderInstance, orderData, offerOrderMode, workersCount) {
        return new Promise((resolve, reject) => {
            redisWorkerManager.getAllWorkers(orderInstance.tenantId, (err, workers) => {
                if (err) {
                    orderLogger(orderInstance.orderId, "error", `orderDistributionManager->_getRelevantWorkersListByDistance->redisWorkerManager.getAllWorkers->Error: ${err.message}`);
                    return reject(err);
                }

                let filteredWorkers = [];
                let lastOfferedWorkerCallsign;
                orderLogger(orderInstance.orderId, "info", "orderDistributionManager->_getRelevantWorkersListByDistance->Start filtering workers ...");
                if (offerOrderMode === this.inRotationMode) {
                    lastOfferedWorkerCallsign = Array.from(orderInstance.offeredInCircleRepeatWorkersList).pop();
                }

                let distanceForSearch;
                switch (orderInstance.currentDistributionCircle) {
                    case 1:
                        distanceForSearch = orderInstance.settings.CIRCLE_DISTANCE_1;
                        break;
                    case 2:
                        distanceForSearch = orderInstance.settings.CIRCLE_DISTANCE_2;
                        break;
                    case 3:
                        distanceForSearch = orderInstance.settings.CIRCLE_DISTANCE_3;
                        break;
                    case 4:
                        distanceForSearch = orderInstance.settings.CIRCLE_DISTANCE_4;
                        break;
                    case 5:
                        distanceForSearch = orderInstance.settings.CIRCLE_DISTANCE_5;
                        break;
                    default:
                        return reject(new Error(`Unsupported currentDistributionCircle value: ${orderInstance.currentDistributionCircle}`));
                }

                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByDistance->distance for search worker: ${distanceForSearch} m`);
                async.each(workers,
                    (workerItem, callback) => {
                        try {
                            const workerCallsign = parseInt(workerItem.worker.callsign);
                            if (offerOrderMode === this.inRotationMode && lastOfferedWorkerCallsign && lastOfferedWorkerCallsign === workerCallsign) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByDistance->Worker ${workerCallsign} was last offered worker at previous distribution circle repeat`);
                                return callback();
                            }


                            if (orderInstance.offeredInCircleRepeatWorkersList.has(workerCallsign)) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByDistance->Worker ${workerCallsign} is already was offered at this distribution circle repeat`);
                                return callback();
                            }

                            if (orderInstance.offeredInAllTimeWorkersList.has(workerCallsign)) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByDistance->Worker ${workerCallsign} is already was offered at some of previous distribution circles`);
                                return callback();
                            }

                            this.isGoodWorkerForOrder(orderData, workerItem, {
                                distanceForSearch: distanceForSearch,
                                needCalcDistance: true,
                                needCheckSocket: true,
                                needCheckStatus: true,
                                exceptCarModels: orderInstance.exceptCarModels,
                                dislikeWorkerIds: orderInstance.dislikeWorkerIds,

                            })
                                .then(filteredWorker => {
                                    orderLogger(orderInstance.orderId, "info", `distributeOrderForDistance->_getRelevantWorkersListByDistance->Worker ${workerCallsign} is good for this order!`);
                                    filteredWorkers.push(filteredWorker);
                                    return callback();
                                })
                                .catch(err => {
                                    orderLogger(orderInstance.orderId, "info", `distributeOrderForDistance->_getRelevantWorkersListByDistance->Worker ${workerCallsign} is NOT good for this order. Reason: ${err}`);
                                    return callback();
                                });
                        } catch (err) {
                            orderLogger(orderInstance.orderId, "error", `distributeOrderForDistance->_getRelevantWorkersListByDistance->Bad worker data ${workerItem}. Error: ${err.message}`);
                            return callback();
                        }
                    }, (err) => {
                        if (err) {
                            orderLogger(orderInstance.orderId, "error", `distributeOrderForDistance->_getRelevantWorkersListByDistance->Error: ${err.message}`);
                            return reject(err);
                        }

                        let sortedWorkers = this.sortRelevantWorkersByDistance(orderInstance.orderId, filteredWorkers, {
                            offerOrderMode: offerOrderMode,
                            offerWorkersCount: workersCount,
                            checkWorkerRating: orderInstance.settings.CHECK_WORKER_RATING_TO_OFFER_ORDER,
                            promoWorkerId: orderInstance.promoWorkerId
                        });



                        // реализация метода вычисления времени поездки всех водителей до  заказа
                        if(parseInt(orderInstance.settings.WORKER_TIME_CALCULATE) === 1 && (sortedWorkers.length > 0)) {

                             this._SetIntervalForDriver(orderInstance, orderData, sortedWorkers)
                                .then(result => {
                                    orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->Return: 1 `);
                                    resolve(result);
                                })
                                .catch(err => {
                                    orderLogger(orderInstance.orderId, 'error', `distributeOrderForDistance->_getRelevantWorkersListByDistance->defineTimeFromDriverToOrder->Error: ${err.message}`);
                                    resolve(sortedWorkers);
                                })


                        }else {
                            return resolve(sortedWorkers);
                        }
                });
            });
        })
    }



    /** calculate  time series for driver
     * * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @param {array} sortedWorkers
     * @return {Promise}
     */
     _SetIntervalForDriver(orderInstance, orderData, sortedWorkers){
        let orderId  = orderData.order_id;
        let tenantId  = orderData.tenant_id;

        return new  Promise((resolve, reject) => {
            redisWorkerTimeManager.getWorkersTime(tenantId, orderId, (err, workerTime) => {
                orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->Success: 1 `);
                return resolve( workerTime);
            })
        }).then(function (result) {

            let workerCallSings = '';
            let WorkersNeedCalculate = [];
            sortedWorkers.forEach(function (item, i) {
                let worker_callsign = item.worker_callsign;
                let find_cache = false;


                for(let index in result) {
                    let save_item = result[index];
                    let worker_callsign_cache = save_item.worker_callsign;
                    let worker_arrival = (typeof save_item.worker_arrival === "object") ? Object.values(save_item.worker_arrival) : [];

                    // запись данных в данные из кеша
                    if(parseInt(worker_callsign) === parseInt(worker_callsign_cache) && (worker_arrival.length > 0)){
                        find_cache = true;
                        sortedWorkers[i].worker_arrival = worker_arrival;
                        break
                    }
                }

                if(!find_cache){
                    workerCallSings += ", " + worker_callsign;
                    WorkersNeedCalculate.push(item);
                }
            });

            orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->NeedCalculate: ${workerCallSings}`);
            let rezultObjct = {};
            rezultObjct.worker_need = WorkersNeedCalculate;
            return  rezultObjct;
        }).then(function (result) {
            let WorkersNeedCalculate = result.worker_need;

            let rezultObjct = {};
            rezultObjct.worker_need_calculate = [];
            if(WorkersNeedCalculate.length > 0) {

                let options = {};
                options.city_id = (orderData.city_id);
                options.tenant_id = (orderData.tenant_id);
                options.tenant_login = (orderData.tenant_login);

                const orderAddress = PHPUnserialize.unserialize(orderData.address);
                let orderFromLat = !isNaN(parseFloat(orderAddress.A.lat)) ? parseFloat(orderAddress.A.lat) : null;
                let orderFromLon = !isNaN(parseFloat(orderAddress.A.lon)) ? parseFloat(orderAddress.A.lon) : null;

                options.origins = [];
                options.origins.push([parseFloat(orderFromLat), parseFloat(orderFromLon)]);

                let workerCallSings = '';
                options.destinations = [];
                WorkersNeedCalculate.forEach(function (item, i, sortedWorkers) {
                    options.destinations.push([item.worker_lat, item.worker_lon]);
                    workerCallSings += ', ' + item.worker_callsign;
                });

                return serviceApi.defineTimeFromDriverToOrder(options)
                    .then(function(data){
                        return OrderDistributionManager._setRangeOfTimeWorkerToOrder(orderInstance, WorkersNeedCalculate, data).then(function (result_2) {
                            rezultObjct.worker_need_calculate = result_2;
                            orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->RangeOfTime Success: 1`);
                            return rezultObjct;
                        });
                    })
                    .catch(err => {
                        orderLogger(orderInstance.orderId, 'error', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->ErrorCalculate Error: ${err.message}`);
                        return rezultObjct;
                    });
            }else {
                return rezultObjct;
            }
        }).then(function (result) {
            let WorkersNeedCalculate = (typeof result.worker_need_calculate !== "undefined") ?   result.worker_need_calculate : [];

            sortedWorkers.forEach(function (item, i) {
                let worker_callsign = item.worker_callsign;

                WorkersNeedCalculate.forEach(function (item_calculate, y) {
                    let worker_callsign_cache = item_calculate.worker_callsign;
                    if(parseInt(worker_callsign) === parseInt(worker_callsign_cache)){
                        sortedWorkers[i].worker_arrival =  (typeof item_calculate.worker_arrival !== "undefined") ? item_calculate.worker_arrival : [];
                    }
                });
            });

            orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->AddRangeOfTime Success: ${WorkersNeedCalculate.length}`);
            return sortedWorkers;
        }).then(function (rezult) {
            return redisWorkerTimeManager.saveWorkerTime(tenantId, orderId, rezult)
                .then(function(data){
                    return data;
                })
                .catch(err => {
                    return rezult;
                });
        });
    }

    /** ad
     * @return {object}
     */
      static _setRangeOfTimeWorkerToOrder(orderInstance, sortedWorkers, timeToOrder){

        return new  Promise((resolve, reject) => {
            let n = 6;
            let count = (typeof( orderInstance.settings.WORKER_TIME_CALCULATE_STEP_COUNT) !== "undefined" ) ? orderInstance.settings.WORKER_TIME_CALCULATE_STEP_COUNT : 5;
            let step_time = (typeof( orderInstance.settings.WORKER_TIME_CALCULATE_STEP) !== "undefined" ) ? orderInstance.settings.WORKER_TIME_CALCULATE_STEP : 10;
            sortedWorkers.forEach(function (item_worker, i) {
                let current_worker = item_worker;
                let current_worker_lat = lib.roundNumberMore(current_worker.worker_lat, n);
                let current_worker_lon = lib.roundNumberMore(current_worker.worker_lon, n);

                // determ range of time
                let time_range = [];

                for (let i in  timeToOrder) {
                    let item_time_order = timeToOrder[i];
                    // nothing  to determ time to order
                    if(item_time_order.latitude.toString() === "" ||  item_time_order.longitude.toString()  === "" ){
                        break;
                    }

                    let current_time_order_lat = lib.roundNumberMore((item_time_order.latitude), n);
                    let current_time_order_lon = lib.roundNumberMore((item_time_order.longitude),n);

                    if((current_worker_lat === current_time_order_lat) && (current_worker_lon === current_time_order_lon)){
                        let current_time_order_lon = Math.ceil( parseFloat(item_time_order.duration)/60);
                        time_range.push(current_time_order_lon);
                        for(let step = 0; step < count; step++){
                            current_time_order_lon += step_time;
                            time_range.push(current_time_order_lon);
                        }
                        break;
                    }
                }
                sortedWorkers[i].worker_arrival = time_range;

                let worker_login = sortedWorkers[i].worker_callsign +  "/" + time_range.join(",");
                orderLogger(orderInstance.orderId, 'info', `distributeOrderForDistance->_getRelevantWorkersListByDistance->_SetIntervalForDriver->_setRangeOfTimeWorkerToOrder: ${worker_login}`);
            });
            return resolve(sortedWorkers);
        });
    }

    /**
     * Get relevant workers list by parking
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     * @param {String} offerOrderMode
     * @param {Number} workersCount
     * @return {Promise}
     * @private
     */
    _getRelevantWorkersListByParking(orderInstance, orderData, offerOrderMode, workersCount) {
        return new Promise((resolve, reject) => {
            const parkingId = orderData.parking_id;
            if (!parkingId) {
                return reject(new Error('Order have not parking area'));
            }
            orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByParking->order parking id: ${parkingId}`);
            redisParkingManager.getAllWorkersOnParking(parkingId, (err, workersAtParking) => {
                if (err) {
                    orderLogger(orderInstance.orderId, "error", `orderDistributionManager->_getRelevantWorkersListByParking->redisWorkerManager.getAllWorkersOnParking->Error: ${err.message}`);
                    return reject(err);
                }
                if (Array.isArray(workersAtParking) && workersAtParking.length < 1) {
                    return reject(new Error("No workers at parking"));
                }
                let filteredWorkers = [];
                let lastOfferedWorkerCallsign;
                orderLogger(orderInstance.orderId, "info", "orderDistributionManager->_getRelevantWorkersListByDistance->Start filtering workers ...");
                if (offerOrderMode === this.inRotationMode) {
                    lastOfferedWorkerCallsign = Array.from(orderInstance.offeredInCircleRepeatWorkersList).pop();
                }
                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByParking->last offered worker: ${lastOfferedWorkerCallsign}`);
                async.each(workersAtParking, (workerCallsign, workerCb) => {
                    workerCallsign = parseInt(workerCallsign);
                    redisWorkerManager.getWorker(orderInstance.tenantId, workerCallsign, (err, workerItem) => {
                        if (err) {
                            orderLogger(orderInstance.orderId, 'error', `orderDistributionManager->_getRelevantWorkersListByParking->redisWorkerManager.getWorker->Error: ${err.message}`);
                            return workerCb(null, 1);
                        } else {
                            if (offerOrderMode === this.inRotationMode && lastOfferedWorkerCallsign && lastOfferedWorkerCallsign === workerCallsign) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByParking->Worker ${workerCallsign} was last offered worker at previous distribution circle repeat`);
                                return workerCb(null, 1);
                            }
                            if (orderInstance.offeredInCircleRepeatWorkersList.has(workerCallsign)) {
                                orderLogger(orderInstance.orderId, "info", `orderDistributionManager->_getRelevantWorkersListByParking->Worker ${workerCallsign} is already was offered at this distribution circle repeat`);
                                return workerCb(null, 1);
                            }
                            const index = workersAtParking.indexOf(workerCallsign.toString());
                            if (index !== -1) {
                                workerItem.parking_index = index;
                            }
                            this.isGoodWorkerForOrder(orderData, workerItem, {
                                distanceForSearch: null,
                                needCalcDistance: false,
                                needCheckSocket: true,
                                needCheckStatus: true,
                                exceptCarModels: orderInstance.exceptCarModels,
                                dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                            })
                                .then(filteredWorker => {
                                    orderLogger(orderInstance.orderId, "info", `distributeOrderForDistance->_getRelevantWorkersListByParking->Worker ${workerCallsign} is good for this order!`);
                                    filteredWorkers.push(filteredWorker);
                                    return workerCb(null, 1);
                                })
                                .catch(err => {
                                    orderLogger(orderInstance.orderId, "info", `distributeOrderForDistance->_getRelevantWorkersListByParking->Worker ${workerCallsign} is NOT good for this order. Reason: ${err}`);
                                    return workerCb(null, 1);
                                });
                        }
                    })
                }, (err) => {
                    if (err) {
                        orderLogger(orderInstance.orderId, "error", `distributeOrderForDistance->_getRelevantWorkersListByParking->Error: ${err.message}`);
                        return reject(err);
                    }

                    const sortedWorkers = this.sortRelevantWorkersByParking(orderInstance.orderId, filteredWorkers, {
                        offerOrderMode: offerOrderMode,
                        offerWorkersCount: workersCount,
                        checkWorkerRating: orderInstance.settings.CHECK_WORKER_RATING_TO_OFFER_ORDER,
                        promoWorkerId: orderInstance.promoWorkerId
                    });


                    return resolve(sortedWorkers);
                })
            })
        })
    }


    /**
     * Send offer order to many workers at the same time
     * @param {Object} options
     * @param {Number} options.tenantId
     * @param {String} options.tenantLogin
     * @param {Object} options.workers
     * @param {Number} options.orderId
     * @param {Number} options.orderCurrencyId
     * @param {Number} options.offerSec
     * @return {Promise}
     * @private
     */
    _sendBatchOfferOrder(options) {
        return new Promise((resolve, reject) => {
            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendBatchOfferOrder->Need send offer order for ${JSON.stringify(options.workers)}`);
            redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeOrderStatus->getOrder->Error: ${err.message}`);
                    return reject(new Error("Error of get order from redis"));
                }
                orderData.status_id = statusInfo.offer_order.status_id;
                orderData.status.status_id = statusInfo.offer_order.status_id;
                orderData.status.name = statusInfo.offer_order.name;
                orderData.status.status_group = statusInfo.offer_order.status_group;
                orderData.workers_offered = options.workers.map(worker => {
                    return worker.worker_callsign;
                });


                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                orderData.status_time = updateTime;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeOrderStatus->saveOrder->Error: ${err.message}`);
                        return reject(new Error("Error of saving order to redis"));
                    }
                    orderLogger(options.orderId, "info", `orderDistributionManager->_sendBatchOfferOrder->changeOrderStatus->saveOrder->Result: ${result}`);
                    sqlManager.updateOrderFromMysql(options.orderId, statusInfo.offer_order.status_id, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeOrderStatus->updateOrderFromMysql->Error: ${err.message}`);
                        } else {
                            orderLogger(options.orderId, "info", `orderDistributionManager->_sendBatchOfferOrder->changeOrderStatus->updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                        }
                    });
                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: options.tenantId,
                        order_id: options.orderId,
                        params: {}
                    });


                    amqpOrderMessageSender.sendMessage(options.orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendBatchOfferOrder->published command: ${orderEventOrderCommand.updateOrderData} to order: ${options.orderId}. Result: ${result}`);
                        }
                    });

                    async.each(options.workers,
                        (workerInfo, eachCb) => {
                            async.waterfall([
                                //Check money balance
                                wfCb => {
                                    sqlManager.gerWorkerMoneyBalanse(options.tenantId, workerInfo.worker_id, options.orderCurrencyId, (err, result) => {
                                        if (err) {
                                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->Error: ${err.message}`);
                                            return wfCb(new Error("get worker balance error"));
                                        } else {
                                            return wfCb(null, 1);
                                        }
                                    })
                                },
                                //Update worker status
                                (prevResult, wfCb) => {
                                    redisWorkerManager.getWorker(options.tenantId, workerInfo.worker_callsign, (err, workerDataTemp) => {
                                        if (err) {
                                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeWorkerStatus->getWorker->Error: ${err.message}`);
                                            return wfCb(err);
                                        }
                                        if (workerDataTemp && workerDataTemp.worker && workerDataTemp.worker.status) {
                                            workerDataTemp.worker.status = workerStatusManager.offerOrder;
                                            workerDataTemp.worker.last_offered_order_id = options.orderId;
                                            redisWorkerManager.saveWorker(workerDataTemp, (err, result) => {
                                                if (err) {
                                                    orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeWorkerStatus->saveWorker->Error: ${err.message}`);
                                                    return wfCb(err);
                                                }
                                                sqlManager.logOrderChangeData(options.tenantId, options.orderId, 'status', workerDataTemp.worker.callsign, "worker", workerStatusManager.offerOrder, "system", (err, result) => {
                                                    if (err) {
                                                        orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->changeWorkerStatus->logOrderChangeData->Error: ${err.message}`);
                                                    } else {
                                                        orderLogger(options.orderId, "info", `orderDistributionManager->_sendBatchOfferOrder->changeWorkerStatus->logOrderChangeData->Result: ${result}`);
                                                    }
                                                });
                                                return wfCb(null, 1);
                                            })
                                        } else {
                                            return wfCb(new Error('Bad worker data'));
                                        }
                                    })
                                },
                                //Send push to offer order
                                (prevResult, wfCb) => {
                                    pushApi.sendNewOrderToWorker(options.tenantId, options.orderId, workerInfo.worker_callsign, (err, result) => {
                                        if (err) {
                                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->pushApi.sendNewOrderToWorker->Error: ${err.message}`);
                                        }
                                        orderLogger(options.orderId, "info", `orderDistributionManager->_sendBatchOfferOrder->pushApi.sendNewOrderToWorker->Result: ${result}`);
                                        return wfCb(null, 1);
                                    })
                                },
                                //Send socket event to offer order
                                (prevResult, wfCb) => {
                                    let worker_arrival_time = (typeof workerInfo.worker_arrival !== "undefined" ) ? workerInfo.worker_arrival : [];
                                    const workerMessageString = orderEventWorkerMessage({
                                        command: orderEventWorkerCommand.offerOrder,
                                        tenant_id: options.tenantId,
                                        tenant_login: options.tenantLogin,
                                        worker_callsign: workerInfo.worker_callsign,
                                        params: {
                                            offer_sec: options.offerSec,
                                            order_id: options.orderId,
                                            worker_arrival: JSON.stringify(worker_arrival_time)
                                        }
                                    });

                                  /*  console.log("ОТПРАВЛЕННО ВОДИТЕЛЮ");
                                    console.log(workerMessageString);*/

                                    const workerCallsign = workerInfo.worker_callsign;
                                    amqpWorkerMessageSender.sendMessage(options.tenantId, workerCallsign, workerMessageString, options.offerSec, (err, result) => {
                                        if (err) {
                                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                        } else {
                                            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendBatchOfferOrder->published command: ${orderEventWorkerCommand.offerOrder} to worker: ${workerCallsign}. Result: ${result} `);
                                        }
                                        return wfCb(null, 1);
                                    });
                                },
                            ], (err, result) => {
                                if (err) {
                                    orderLogger(options.orderId, "error", `orderDistributionManager->_sendBatchOfferOrder->error of sending offer order for worker ${workerInfo.worker_callsign} : ${err.message}`);
                                } else {
                                    orderLogger(options.orderId, "info", `orderDistributionManager->_sendBatchOfferOrder->offer order sent to worker ${workerInfo.worker_callsign}`);
                                }
                                return eachCb(null, 1);
                            })
                        }, (err) => {
                            if (err) {
                                orderLogger(options.orderId, 'info', `orderDistributionManager->_sendBatchOfferOrder->Error: ${err.message}`);
                                return reject(err);
                            } else {
                                return resolve();
                            }
                        });
                })

            });
        });
    }

    /**
     * Send offer order to worker
     * @param {Object} options
     * @param {Number} options.tenantId
     * @param {String} options.tenantLogin
     * @param {Object} options.workerInfo
     * @param {Number} options.orderId
     * @param {Number} options.orderCurrencyId
     * @param {Number} options.offerSec
     * @return {Promise}
     * @private
     */
    _sendOfferOrder(options) {
        return new Promise((resolve, reject) => {
            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendOfferOrder->Need send offer order for ${JSON.stringify(options.workerInfo)}`);
            sqlManager.gerWorkerMoneyBalanse(options.tenantId, options.workerInfo.worker_id, options.orderCurrencyId, (err, result) => {
                if (err) {
                    orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->Error: ${err.message}`);
                    return reject(new Error("get worker balance error"));
                }
                async.parallel({
                    sendPushNewOrder: callback => {
                        pushApi.sendNewOrderToWorker(options.tenantId, options.orderId, options.workerInfo.worker_callsign, (err, result) => {
                            if (err) {
                                orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->pushApi.sendNewOrderToWorker->Error: ${err.message}`);
                            }
                            orderLogger(options.orderId, "info", `orderDistributionManager->_sendOfferOrder->pushApi.sendNewOrderToWorker->Result: ${result}`);
                            return callback(null, 1);
                        })
                    },
                    changeOrderStatus: callback => {
                        redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                            if (err) {
                                orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeOrderStatus->getOrder->Error: ${err.message}`);
                                return callback(new Error("Error of get order from redis"));
                            }
                            orderData.status_id = statusInfo.offer_order.status_id;
                            orderData.status.status_id = statusInfo.offer_order.status_id;
                            orderData.status.name = statusInfo.offer_order.name;
                            orderData.status.status_group = statusInfo.offer_order.status_group;
                            orderData.worker_id = options.workerInfo.worker_id;
                            orderData.worker = options.workerInfo.worker_worker;
                            orderData.workers_offered = [options.workerInfo.worker_callsign];
                            let carId = null;
                            if (options.workerInfo.car && options.workerInfo.car.car_id) {
                                orderData.car_id = options.workerInfo.car.car_id;
                                orderData.car = options.workerInfo.car;
                                carId = orderData.car_id
                            }
                            const updateTime = Math.floor(Date.now() / 1000);
                            orderData.update_time = updateTime;
                            orderData.status_time = updateTime;
                            redisOrderManager.saveOrder(orderData, (err, result) => {
                                if (err) {
                                    orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeOrderStatus->saveOrder->Error: ${err.message}`);
                                    return callback(new Error("Error of saving order to redis"));
                                }
                                orderLogger(options.orderId, "info", `orderDistributionManager->_sendOfferOrder->changeOrderStatus->saveOrder->Result: ${result}`);
                                sqlManager.updateOrderFromMysql(options.orderId, statusInfo.offer_order.status_id, options.workerInfo.worker_id, carId, updateTime, (err, result) => {
                                    if (err) {
                                        orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeOrderStatus->updateOrderFromMysql->Error: ${err.message}`);
                                    } else {
                                        orderLogger(options.orderId, "info", `orderDistributionManager->_sendOfferOrder->changeOrderStatus->updateOrderFromMysql->Result: ${result}`);
                                    }
                                });
                                return callback(null, 1);
                            })

                        })
                    },
                    changeWorkerStatus: callback => {
                        redisWorkerManager.getWorker(options.tenantId, options.workerInfo.worker_callsign, (err, workerDataTemp) => {
                            if (err) {
                                orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeWorkerStatus->getWorker->Error: ${err.message}`);
                                return callback(null, 1);
                            }
                            if (workerDataTemp && workerDataTemp.worker && workerDataTemp.worker.status) {
                                workerDataTemp.worker.status = workerStatusManager.offerOrder;
                                workerDataTemp.worker.last_offered_order_id = options.orderId;
                                redisWorkerManager.saveWorker(workerDataTemp, (err, result) => {
                                    if (err) {
                                        orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeWorkerStatus->saveWorker->Error: ${err.message}`);
                                    }
                                    sqlManager.logOrderChangeData(options.tenantId, options.orderId, 'status', workerDataTemp.worker.callsign, "worker", workerStatusManager.offerOrder, "system", (err, result) => {
                                        if (err) {
                                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->changeWorkerStatus->logOrderChangeData->Error: ${err.message}`);
                                        } else {
                                            orderLogger(options.orderId, "info", `orderDistributionManager->_sendOfferOrder->changeWorkerStatus->logOrderChangeData->Result: ${result}`);
                                        }
                                    });
                                    return callback(null, 1);
                                })
                            } else {
                                orderLogger(options.orderId, "error", "orderDistributionManager->_sendOfferOrder->changeWorkerStatus->getWorker->Bad worker data");
                                orderLogger(options.orderId, "error", workerDataTemp);
                                return callback(null, 1);
                            }
                        })
                    }

                }, (err, result) => {
                    if (err) {
                        orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->Error: ${err.message}`);
                        return reject(err);
                    }
                    const workerMessageString = orderEventWorkerMessage({
                        command: orderEventWorkerCommand.offerOrder,
                        tenant_id: options.tenantId,
                        tenant_login: options.tenantLogin,
                        worker_callsign: options.workerInfo.worker_callsign,
                        params: {
                            offer_sec: options.offerSec,
                            order_id: options.orderId
                        }
                    });
                    const workerCallsign = options.workerInfo.worker_callsign;
                    amqpWorkerMessageSender.sendMessage(options.tenantId, workerCallsign, workerMessageString, options.offerSec, (err, result) => {
                        if (err) {
                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendOfferOrder->published command: ${orderEventWorkerCommand.offerOrder} to worker: ${workerCallsign}. Result: ${result} `);
                        }
                    });
                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: options.tenantId,
                        order_id: options.orderId,
                        params: {}
                    });
                    amqpOrderMessageSender.sendMessage(options.orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(options.orderId, "error", `orderDistributionManager->_sendOfferOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(options.orderId, 'info', `orderDistributionManager->_sendOfferOrder->published command: ${orderEventOrderCommand.updateOrderData} to order: ${options.orderId}. Result: ${result}`);
                        }
                    });
                    return resolve(1);
                })
            })
        });
    }


    /**
     * Is this worker good for order
     * @param {Object} orderItem
     * @param {Object} workerItem
     * @param {Object} options
     * @param {Boolean} options.needCalcDistance
     * @param {Number|null}  options.distanceForSearch
     * @param {Boolean} options.needCheckSocket
     * @param {Boolean} options.needCheckStatus
     * @param {Array|null} options.dislikeWorkerIds
     * @param {Array|null} options.exceptCarModels
     * @return {Promise<object>}
     */
    isGoodWorkerForOrder(orderItem, workerItem, options) {
        return new Promise((resolve, reject) => {
            try {
                //order data
                const orderCityId = parseInt(orderItem.city_id);
                const orderPositionId = parseInt(orderItem.tariff.position_id);
                const orderTariffClassId = parseInt(orderItem.tariff.class_id);
                const orderOptionsArr = [];
                const orderOptionsRaw = orderItem.options;
                const typeOrder = orderItem.device.toString(); // источник заказа
                if (typeof orderOptionsRaw === "object" && orderOptionsRaw !== null) {
                    const orderOptionsRawArr = Object.keys(orderOptionsRaw).map(k => orderOptionsRaw[k]);
                    if (orderOptionsRawArr.length > 0) {
                        for (let optionData of orderOptionsRawArr) {
                            if (optionData.option_id) {
                                orderOptionsArr.push(parseInt(optionData.option_id));
                            }
                        }
                    }
                }

                const workerId = parseInt(workerItem.worker.worker_id);
                const workerCallsign = parseInt(workerItem.worker.callsign);
                const workerTenantId = parseInt(workerItem.worker.tenant_id);
                const workerCityId = parseInt(workerItem.worker.city_id);
                const workerCityArr = workerItem.worker.cities;
                const workerStatus = workerItem.worker.status;
                const workerPositionId = parseInt(workerItem.position.position_id);
                const workerPositionHasCar = parseInt(workerItem.position.has_car);


                //car data
                const car = workerItem.car;
                if (car) {
                    const carModelId = parseInt(car.model_id);
                    //Check except car model list
                    if (Array.isArray(options.exceptCarModels) && options.exceptCarModels.length > 0) {
                        if (options.exceptCarModels.includes(carModelId)) {
                            return reject(new Error(`Workers car model in except car models list`));
                        }
                    }
                }

                //Check client dislike list
                if (Array.isArray(options.dislikeWorkerIds) && options.dislikeWorkerIds.length > 0) {
                    if (options.dislikeWorkerIds.includes(workerId)) {
                        return reject(new Error(`Worker in client dislike list`));
                    }
                }

                //Check worker city
                if (typeof workerCityArr === "object") {
                    let isIn = false;
                    Object.keys(workerCityArr).forEach(key => {
                        let workerCity = workerCityArr[key];
                        if (parseInt(workerCity.city_id) === orderCityId) {
                            isIn = true;
                        }
                    });

                    Object.keys(workerCityArr).forEach(key => {
                        let workerCity = workerCityArr[key];

                        if (parseInt(workerCity) === orderCityId) {
                            isIn = true;
                        }
                    });

                    if (!isIn) {
                        return reject(new Error(`orderCityId not in workerCityArr`));
                    }
                } else {
                    if (orderCityId !== workerCityId) {
                        return reject(new Error(`Bad worker cityId ${workerCityId}`));
                    }
                }



                if (workerPositionHasCar) {
                    //Check worker position
                    if (orderPositionId !== workerPositionId) {
                        return reject(new Error(`Bad worker position id ${workerPositionId}`));
                    }

                    let workerTariffClasses = workerItem.position.classes;
                    if (typeof workerTariffClasses === "object" && workerTariffClasses !== null) {
                        workerTariffClasses = Object.keys(workerTariffClasses).map(k => workerTariffClasses[k]);
                    }

                    if (Array.isArray(workerTariffClasses)) {
                        if(typeOrder != orderType.HOSPITAL){
                            if(!workerTariffClasses.find((workerTariffClass, index, array) =>
                                parseInt(workerTariffClass) === orderTariffClassId)){
                                return reject(new Error(`Bad worker tariff classes: ${JSON.stringify(workerTariffClasses)}`));
                            }
                        }
                    } else {
                        return reject(new Error(`Bad worker tariff classes: ${JSON.stringify(workerTariffClasses)}`));
                    }
                } else {
                    let workerPositions = workerItem.position.positions;
                    if (typeof workerPositions === "object" && workerPositions !== null) {
                        workerPositions = Object.keys(workerPositions).map(k => workerPositions[k]);
                    }

                    if (Array.isArray(workerPositions)) {
                        if (!workerPositions.find((workerPosition, index, array) =>
                            parseInt(workerPosition) === orderPositionId)) {
                            return reject(new Error(`Bad worker position classes: ${workerPositions}`));
                        }
                    } else {
                        return reject(new Error(`Bad worker position classes: ${workerPositions}`));
                    }
                }


                // Check worker car options
                if (workerPositionHasCar && workerItem.car && Array.isArray(orderOptionsArr) && orderOptionsArr.length > 0) {
                    const carOptions = [];
                    let carOptionsRaw = workerItem.car.carHasOptions;
                    let isGoodCarOption = true;
                    if (typeof carOptionsRaw === "object" && carOptionsRaw !== null) {
                        carOptionsRaw = Object.keys(carOptionsRaw).map(k => carOptionsRaw[k]);
                        if (Array.isArray(carOptionsRaw) && carOptionsRaw.length > 0) {
                            for (let carOptionData of carOptionsRaw) {
                                if (carOptionData.option_id) {
                                    carOptions.push(parseInt(carOptionData.option_id));
                                }
                            }
                        }
                        for (let orderOptionId of orderOptionsArr) {
                            if (!carOptions.find((carOptionId, index, array) => carOptionId === orderOptionId)) {
                                isGoodCarOption = false;
                                break;
                            }
                        }
                    } else {
                        isGoodCarOption = false;
                    }
                    if (!isGoodCarOption) {
                        return reject(new Error(`Bad worker car options: ${carOptions}`));
                    }
                }


                // Check worker status  daimond
                if (options.needCheckStatus) {
                    if (workerStatus !== workerStatusManager.free) {
                        return reject(new Error(`Bad worker status: ${workerStatus}`));
                    }
                }

                let workerDistanceToOrder;
                //Check worker distance to order
                if (options.needCalcDistance) {
                    try {
                        workerDistanceToOrder = this.distanceFromWorkerToOrder(orderItem, workerItem);
                        workerItem.distance_to_order = workerDistanceToOrder;
                    } catch (err) {
                        return reject(err);
                    }
                    if (workerDistanceToOrder > options.distanceForSearch) {
                        return reject(new Error(`Bad worker distance to order. Distance: ${workerDistanceToOrder} m.`));
                    }
                }


                //Check worker connection to socket server
                if (options.needCheckSocket) {
                    rabbitmqHttpService.getQueueConsumersCount(`${workerTenantId}_worker_${workerCallsign}`, null, (err, result) => {
                        if (err) {
                            return reject(new Error(`getQueueConsumersCount error:${err.message}`));
                        } else {
                            if (result >= 1) {
                                return resolve(this.getWorkerInfo(workerItem))
                            } else {
                                return reject(new Error(`Worker is not connected to web-socket server`));
                            }
                        }
                    });
                } else {
                    return resolve(this.getWorkerInfo(workerItem))
                }
            } catch (err) {
                return reject(err);
            }

        });
    }


    /**
     * Get worker info
     * @param workerItem
     * @returns {{worker_id: any, worker_callsign: any, worker_distance_to_order: number, worker_parking_index: any, worker_personal_rating: number, worker_group_priority: number, worker_worker: *, worker_car: *}}
     */
    getWorkerInfo(workerItem) {
        return {
            "worker_id": workerItem.worker ? parseInt(workerItem.worker.worker_id) : null,
            "worker_callsign": workerItem.worker ? parseInt(workerItem.worker.callsign) : null,
            "worker_distance_to_order": workerItem.distance_to_order ? workerItem.distance_to_order : 0,
            "worker_parking_index": workerItem.parking_index ? workerItem.parking_index : 0,
            "worker_personal_rating": workerItem.position ? (workerItem.position.rating ? parseFloat(workerItem.position.rating) : 0) : 0,
            "worker_group_priority": workerItem.position ? (workerItem.position.group_priority ? parseFloat(workerItem.position.group_priority) : 0) : 0,
            "worker_worker": workerItem.worker ? workerItem.worker : null,
            "worker_car": workerItem.car ? workerItem.car : null,


            "worker_lat": workerItem.geo.lat ? workerItem.geo.lat : null,
            "worker_lon": workerItem.geo.lon ? workerItem.geo.lon : null,

            "worker_arrival" : []


        };
    }

    /**
     * Distance from worker to order first address
     * @param orderItem
     * @param workerItem
     * @returns {number}
     */
    distanceFromWorkerToOrder(orderItem, workerItem) {
        let orderFromLat, orderFromLon, workerLat, workerLon;
        try {
            const orderAddress = PHPUnserialize.unserialize(orderItem.address);
            orderFromLat = !isNaN(parseFloat(orderAddress.A.lat)) ? parseFloat(orderAddress.A.lat) : null;
            orderFromLon = !isNaN(parseFloat(orderAddress.A.lon)) ? parseFloat(orderAddress.A.lon) : null;
            workerLat = !isNaN(parseFloat(workerItem.geo.lat)) ? parseFloat(workerItem.geo.lat) : null;
            workerLon = !isNaN(parseFloat(workerItem.geo.lon)) ? parseFloat(workerItem.geo.lon) : null;
        } catch (err) {

        }

        if (!orderFromLat || !orderFromLon) {
            throw new Error(`Bad order coordinates. lat:${orderFromLat};lon:${orderFromLon}`);
        }
        if (!workerLat || !workerLon) {
            throw new Error(`Bad worker coordinates. lat:${workerLat};lon:${workerLon}`);
        }
        const workerCoords = {
            'latitude': workerLat,
            'longitude': workerLon
        };
        const orderCoords = {
            'latitude': orderFromLat,
            'longitude': orderFromLon
        };

        return geoLib.getDistance(
            workerCoords,
            orderCoords
        )
    }

    /**
     * Can offer order for worker
     * @param {number} tenantId
     * @param {number} orderId
     * @param {number} workerCallsign
     * @returns {Promise<object>}
     */
    canOfferOrderForWorker(tenantId, orderId, workerCallsign,  positionId,  orderCity) {
        orderLogger(orderId, 'info', `orderDistributionManager->canOfferOrderForWorker called. tenantId:${tenantId}; orderId:${orderId}; workerCallsign:${workerCallsign}`);

        return new Promise((resolve, reject) => {
            async.parallel({
                orderData: callback => {
                    redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                        if (err) {
                            callback(err);
                        } else {
                            callback(null, orderData);
                        }
                    })
                },
                workerData: callback => {
                    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                        // нет данных в редисе
                        if(workerData === undefined){
                           return  callback(null, null);
                        }
                        if (err) {
                            return  callback(err);
                        } else {
                            return callback(null, workerData);
                        }
                    })
                },
                workerDataBD: callback => {
                    sqlManager.getInfoAsignWorkerForOrder(tenantId, workerCallsign, positionId, orderCity)
                        .then(result => {
                            return sqlManager.getWorkerCarLastShift(result);
                        })
                        .then(result_car => {
                            return callback(null, result_car);
                        })
                        .catch(err => {
                            return callback(err, null);
                        });
                }
            }, (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderDistributionManager->canOfferOrderForWorker>Error: ${err.message}`);
                    reject(err);
                } else {
                    let orderData = result.orderData;
                    let workerData =  result.workerData;
                    if(workerData === null){
                        workerData =  result.workerDataBD;
                    }

                    this.isGoodWorkerForOrder(orderData, workerData, {
                        needCalcDistance: false,
                        distanceForSearch: null,
                        needCheckSocket: false,
                        needCheckStatus: false
                    })
                        .then(result => {
                            orderLogger(orderId, 'info', `orderDistributionManager->canOfferOrderForWorker>Result: ${JSON.stringify(result)}`);
                            resolve(result);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderDistributionManager->canOfferOrderForWorker>Error: ${err.message}`);
                            reject(err);
                        })
                }
            })
        });
    }

    /**
     * Get order statuses, that relevant for order offering
     * @returns {*[]}
     * @private
     */
    _getRelevantOfferOrderStatuses() {
        return [
            statusInfo.new_order.status_id,
            statusInfo.new_order_no_parking.status_id,
            statusInfo.free_order.status_id,
            statusInfo.worker_refused_order.status_id,
            statusInfo.worker_refused_order_assign.status_id,
            statusInfo.worker_ignored_offer_order.status_id

        ]
    }

    /**
     * Get relevant workers callsign list to offer order
     * @param {Number} tenantId
     * @param {Number} orderId
     * @returns {Promise}
     */
    getRelevantWorkersList(tenantId, orderId) {
        return new Promise((resolve, reject) => {
            let relevantWorkers = [];
            redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderId, "error", `orderDistributionManager->getRelevantWorkersList->redisOrderManager.getOrder->Error: ${err.message}`);
                    reject(err);
                } else {
                    redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
                        if (err) {
                            orderLogger(orderId, "error", `orderDistributionManager->getRelevantWorkersList->redisWorkerManager.getAllWorkers->Error: ${err.message}`);
                            return reject(err);
                        }
                        async.each(workers, (workerItem, callback) => {
                            try {
                                const workerCallsign = parseInt(workerItem.worker.callsign);
                                this.isGoodWorkerForOrder(orderData, workerItem, {
                                    distanceForSearch: null,
                                    needCalcDistance: false,
                                    needCheckSocket: true,
                                    needCheckStatus: true
                                })
                                    .then(filteredWorker => {
                                        orderLogger(orderId, "info", `orderDistributionManager->getRelevantWorkersList->Worker ${workerCallsign} is good for this order!`);
                                        relevantWorkers.push(filteredWorker.worker_callsign);
                                        return callback();
                                    })
                                    .catch(err => {
                                        orderLogger(orderId, "info", `orderDistributionManager->getRelevantWorkersList->Worker ${workerCallsign} is NOT good for this order. Reason: ${err.message}`);
                                        return callback();
                                    });
                            } catch (err) {
                                orderLogger(orderId, "error", `orderDistributionManager->getRelevantWorkersList->Bad worker data ${workerItem}. Error: ${err.message}`);
                                return callback();
                            }
                        }, (err) => {
                            if (err) {
                                orderLogger(orderId, "error", `orderDistributionManager->getRelevantWorkersList->Error: ${err.message}`);
                                return reject(err);
                            }
                            return resolve(relevantWorkers);
                        })
                    })
                }
            })
        })
    }
}

module.exports = exports = new OrderDistributionManager();
