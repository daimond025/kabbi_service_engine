'use strict';

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");


/**
 * Worker late at order timeout
 * @param {BaseGootaxOrder}  orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'WorkerLateTimeOut Called!');
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderInstance.orderId, 'error', `workerLateTimeout->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                const currentStatusId = parseInt(orderData.status_id);
                const validOrderStatuses = [
                    statusInfo.worker_accepted_order.status_id,
                    statusInfo.pre_order_executing.status_id
                ];
                if (validOrderStatuses.includes(currentStatusId)) {
                    orderData.status_id = statusInfo.worker_coming_later.status_id;
                    orderData.status.status_id = statusInfo.worker_coming_later.status_id;
                    orderData.status.name = statusInfo.worker_coming_later.name;
                    const updateTime = Math.floor(Date.now() / 1000);
                    orderData.update_time = updateTime;
                    orderData.status_time = updateTime;
                    orderInstance.workerLateTimestamp = updateTime;
                    redisOrderManager.saveOrder(orderData, (err, result) => {
                        if (err) {
                            orderLogger(orderInstance.orderId, 'error', `workerLateTimeout->redisOrderManager.saveOrder->Error: ${err.message}`);
                            return reject(err);
                        }
                        sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.worker_coming_later.status_id, orderData.worker_id, orderData.car_id, updateTime, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, "error", `workerLateTimeout->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                            } else {
                                orderLogger(orderInstance.orderId, "info", `workerLateTimeout->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                            }
                        });

                        const messageString = orderEventOrderMessage({
                            command: orderEventOrderCommand.updateOrderData,
                            tenant_id: orderInstance.tenantId,
                            order_id: orderInstance.orderId,
                            params: {}
                        });
                        amqpOrderMessageSender.sendMessage(orderInstance.orderId, messageString, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, "error", `workerLateTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                                return reject(err);
                            } else {
                                orderLogger(orderInstance.orderId, 'info', `workerLateTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderInstance.orderId}.Result: ${result}`);
                                return resolve(true);
                            }
                        });
                    })
                } else {
                    orderLogger(orderInstance.orderId, 'info', `workerLateTimeout->Can't set worker late status. Order status_id: ${currentStatusId}`);
                    return resolve(false);
                }
            })
        } catch (err) {
            return reject(err);
        }
    });
};
