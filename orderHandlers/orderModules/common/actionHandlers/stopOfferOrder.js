"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const orderActionManager = require('../orderActionManager');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;


/**
 * Stop offer order action
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'stopOfferOrderEvent called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel);
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel);
            redisOrderManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    orderLogger(orderId, 'error', `stopOfferOrderEvent->getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                const currStatusId = parseInt(orderItem.status_id);
                const validOrderStatuses = [
                    statusInfo.offer_order.status_id,
                    statusInfo.new_order.status_id,
                    statusInfo.new_order_no_parking.status_id,
                    statusInfo.worker_refused_order.status_id
                ];
                if (validOrderStatuses.includes(currStatusId)) {
                    orderActionManager.sendOrderToManualStatus(orderItem, false)
                        .then(result => {
                            orderLogger(orderId, 'info', `stopOfferOrderEvent->orderActionManager.sendOrderToManualStatus->Result: ${result}`);
                            return resolve(true);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `stopOfferOrderEvent->orderActionManager.sendOrderToManualStatus->Error: ${err.message}`);
                            return reject(err);
                        });
                } else {
                    orderLogger(orderId, 'info', `stopOfferOrderEvent->Cant stop offer order. Current order statusId: ${currStatusId}`);
                    return resolve(true);
                }
            });
        } catch (err) {
            return reject(err);
        }
    })
};
