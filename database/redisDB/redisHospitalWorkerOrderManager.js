"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const PHPUnserialize = require('php-unserialize');
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const redis = require("redis");
const retryStrategy = require("./redisRetryStrategy");

const redisWorkerReservedOrderListManager = require('./redisWorkerReservedOrderListManager');

/**
 * Worker reserved order list manager
 * @param {Number} port
 * @param {String} ipredisCompanyOrderManager
 * @returns {redisWorkerReservedOrderListManager}
 */
function redisHospitalWorkerOrderManager(port, ip) {
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db17");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db17");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db17:" + err);
    });

    this.dbNumber = 17;

}

/* TODO serialise array order OrderTime
redisHospitalWorkerOrderManager.prototype.getOrderAssign = function (tenantId, workerCallsign, orderId, callback) {
    const self = this;
    let key = tenantId.toString() + "_" + workerCallsign.toString();
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(key, (err, assignOrder) => {
            if (err) {
                return callback(err);
            }
            try {
                assignOrder = JSON.parse(assignOrder);
                if (assignOrder === null || typeof(assignOrder) === "undefined") {
                    callback(null, {});
                }
                callback(null, assignOrder);
            } catch (err) {
                return callback(err);
            }
        });

    });
};
redisHospitalWorkerOrderManager.prototype.sortByOrderTime = function(assignOrders){
    return assignOrders.sort(function(a , b){

        if(parseInt(a.order_time) < parseInt(b.order_time) ) {
            return -1
        }

        if(parseInt(a.order_time) > parseInt(b.order_time) ) {
            return 1
        }

        return 0;
        
    });

};

redisHospitalWorkerOrderManager.prototype.getOrder = function (tenantId, workerCallsign, orderData, callback) {
    const self = this;
    let key = tenantId.toString() + "_" + workerCallsign.toString();

    let orderId =  parseInt(orderData.orderId);
    let order_time = parseInt (orderData.orderTime);

    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(key, (err, assignOrdes) => {
            if (err) {
                return callback(err);
            }
            try {
                assignOrdes = PHPUnserialize(assignOrder);
                if (assignOrdes === null || typeof(assignOrdes) === "undefined") {
                    assignOrdes = [];
                }

                let offer_order = null;
                for(let i = 0 ; i < assignOrdes.length; i++){
                    if(typeof assignOrdes[i].order_id !=   "undefined"){
                        offer_order =  assignOrdes[i]; break;
                    }

                }
                callback(null, offer_order );

            } catch (err) {
                return callback(err);
            }
        });

    });

};
redisHospitalWorkerOrderManager.prototype.deleteOrderAssign = function (tenantId, workerCallsign, orderData, callback) {
    const self = this;
    let key = tenantId.toString() + "_" + workerCallsign.toString();

    let orderId =  parseInt(orderData.orderId);
    let order_time = parseInt (orderData.orderTime);

    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(key, (err, assignOrdes) => {
            if (err) {
                return callback(err);
            }
            try {
                assignOrdes = PHPUnserialize(assignOrder);
                if (assignOrdes === null || typeof(assignOrdes) === "undefined") {
                    assignOrdes = [];
                }

                let index = -1;
                assignOrdes.forEach(function (item, i) {
                    if(typeof item.order_id != "undefined"  && item.order_id === orderId){
                        index = i;
                    }
                });

                if(index >= 0){
                    assignOrdes.splice(index, 1);
                }
                assignOrdes = self.sortByOrderTime(assignOrdes);

                let jsonStr = lib.serialize(assignOrdes);
                self.saveRedis(tenantId, workerCallsign, jsonStr, (err, rezult_save) => {
                    if(rezult_save){
                        callback(null, assignOrdes )
                    }
                    callback(null, null )
                });

            } catch (err) {
                return callback(err);
            }
        });

    });

};
redisHospitalWorkerOrderManager.prototype.addOrderAssign = function (tenantId, workerCallsign, orderData, callback) {
    const self = this;
    let key = tenantId.toString() + "_" + workerCallsign.toString();

    let orderId =  parseInt(orderData.orderId);
    let order_time = parseInt (orderData.orderTime);

    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(key, (err, assignOrdes) => {
            if (err) {
                return callback(err);
            }
            try {
                assignOrdes = JSON.parse(assignOrder);
                if (assignOrdes === null || typeof(assignOrdes) === "undefined") {
                    assignOrdes = [];
                }

                let order_object = {
                    order_id: parseInt( orderId),
                    order_time: parseInt(order_time)

                };

                let index = -1;
                assignOrdes.forEach(function (item, i) {
                    if(typeof item.order_id != "undefined"  && item.order_id === orderId){
                        index = i;
                    }
                });

                if(index >= 0){
                    assignOrdes[index] = order_object;
                }else {
                    assignOrdes.push(order_object);
                }
                assignOrdes = self.sortByOrderTime(assignOrdes);

                let jsonStr = JSON.stringify(assignOrdes);
                self.saveRedis(tenantId, workerCallsign, jsonStr, (err, rezult_save) => {
                    if(rezult_save){
                        callback(null, assignOrdes )
                    }
                    callback(null, null )
                });

            } catch (err) {
                return callback(err);
            }
        });

    });
};
redisHospitalWorkerOrderManager.prototype.saveRedis = function(tenantId, workerCallsign, value, callback ) {
    const self = this;
    let key = tenantId.toString() + "_" + workerCallsign.toString();

    self.redisClientDb.select(self.dbNumber, () => {
        const setResult = self.redisClientDb.set(key, value, redis.print);
        if(setResult){
            callback(null, setResult);
        }else{
            callback(null, null);
        }
    });
};


*/


/**
 * Add order to worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisHospitalWorkerOrderManager.prototype.addOrder = function (tenantId, workerCallsign, orderId, callback) {
    try{
        const self = this;
        const key = `${tenantId}_${workerCallsign}`;

        self.getAll(tenantId, workerCallsign, function(err, orders_list){
            if(err){
                callback(err);
            }

            if(!orders_list.includes(parseInt(orderId))){

                self.redisClientDb.select(self.dbNumber, () => {
                    self.redisClientDb.sadd(key, orderId, (err, result) => {
                        if(err){
                            return callback(err);
                        }
                        callback(null, result);
                    });

                });

            }else{
                callback(null, 1);
            }
        });
    }catch(err){
        return callback(err);
    }
};

/**
 * Delete order form worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisHospitalWorkerOrderManager.prototype.delOrder = function (tenantId, workerCallsign, orderId, callback) {
    try{
        const self = this;
        const key = `${tenantId}_${workerCallsign}`;

        self.getAll(tenantId, workerCallsign, function(err, orders_list){
            if(err){
                callback(err);
            }

            // синхронизация
            redisWorkerReservedOrderListManager.getAll(tenantId, workerCallsign,function(errRedisWorker, worker_orders){
                if(errRedisWorker){

                }
                let key_delete = []; key_delete.push(orderId);
                orders_list.forEach(function(item, i){
                    if(!worker_orders.includes(item)){
                        key_delete.push(item);
                    }
                });

                self.redisClientDb.select(self.dbNumber, () => {
                    self.redisClientDb.srem(key, key_delete, (err, result) => {
                        if(err){
                            return callback(err);
                        }
                        callback(null, result);
                    });

                });
            });
        });
    }catch(err){
        return callback(err);
    }
};


/**
 * Get all orders form worker active order list
 * @param {Number} tenantIdredisHospitalWorkerOrderManager
 * @param {Number} workerCallsign
 * @param {Function} callback
 */
redisHospitalWorkerOrderManager.prototype.getAll = function (tenantId, workerCallsign, callback) {
    try{
        const self = this;
        const key = `${tenantId}_${workerCallsign}`;

        self.redisClientDb.select(self.dbNumber, () => {
            self.redisClientDb.smembers(key, function(err, result){
                if(err){
                    return callback(err);
                }
                let orders_list = result.map(function(order){
                    return parseInt(order);
                });

                orders_list.sort();
                callback(null, orders_list);
            });
        });
    }catch(err){
        return callback(err);
    }
};


module.exports = exports = new redisHospitalWorkerOrderManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);