'use strict';
const async = require("async");
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderNotificationManager = require('../orderNotificationManager');
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const orderActionManager = require('../orderActionManager');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");

const phoneApi = require("../../../../services/externalWebServices/phoneApi");
const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');


function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
/**
 * Order status free hospital order for all
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {*}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusFreeHospitalForCompany called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;

            orderInstance.orderTimer.clearAll();

            const nowTime = parseInt((new Date()).getTime() / 1000);
            const orderExpiredTime = !isNaN(parseFloat(orderData.order_time)) ? parseFloat(orderData.order_time) : 0;
            const timeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
            const timeDiff = parseInt(orderExpiredTime) - (parseInt(nowTime) + parseInt(timeOffset));
            orderLogger(orderId, 'info', `orderStatusFreeHospitalForCompany->OrderLateTimeout will bew called after ${timeDiff / 60} min`);


            if (orderData.worker && orderData.worker.callsign) {
                const workerCallsign = orderData.worker.callsign;
                const workerAssignedNewStatuses = [
                    statusInfo.worker_assigned_at_order_soft.status_id,
                    statusInfo.worker_assigned_at_order_hard.status_id
                ];
                const workerAssignedPreStatuses = [
                    statusInfo.worker_assigned_at_preorder_soft.status_id,
                    statusInfo.worker_assigned_at_preorder_hard.status_id,
                    statusInfo.worker_accepted_preorder.status_id
                ];
                let orderWasAssigned = false;
                let penultimateStatus;
                let isPreOrder = false;
                if (orderInstance.statusArr.length >= 2 && orderInstance.statusArr[orderInstance.statusArr.length - 2]) {
                    penultimateStatus = orderInstance.statusArr[orderInstance.statusArr.length - 2];
                    //Заказ был назначен
                    if (workerAssignedNewStatuses.indexOf(penultimateStatus) !== -1 || workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                    }
                    if (workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                        isPreOrder = true;
                    }
                }
                if (orderWasAssigned) {
                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder: isPreOrder,
                        isWorkerEventOwner: false,
                    });
                } else{
                    orderActionManager.deleteWorkersFromOrder(orderData)
                    .then(result => {
                        orderLogger(orderId, "info", `orderStatusFreeOrder->deleteWorkersFromOrder->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderStatusFreeOrder->deleteWorkersFromOrder->Error: ${err.message}`);
                    });
                }
                if (orderInstance.workerLateTimestamp !== null && (orderInstance.statusArr[orderInstance.statusArr.length - 2] === statusInfo.worker_coming_later.status_id)) {
                    try {
                        const lateTime = Math.floor(Date.now() / 1000) - orderInstance.workerLateTimestamp;
                        orderInstance.workerLateTimestamp = null;
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                orderLogger(orderId, "error", `orderStatusFreeOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
                            } else {
                                const workerShiftId = workerData.worker.worker_shift_id;
                                sqlManager.logWorkerLateTime(workerShiftId, lateTime, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Result: ${result}`);
                                    }
                                });
                            }
                        })
                    } catch (err) {
                        orderLogger(orderId, "error", `orderStatusFreeOrder->Error: ${err.message}`);
                    }
                }
            }

            // note fore all - free status

            let company_id = (typeof orderData.worker_company.tenant_company_id != undefined ) ? orderData.worker_company.tenant_company_id : null;
            let company_call = 0;
            if(typeof orderData.settings.company_call  != "undefined"){
                company_call = parseInt(orderData.settings.company_call);
            }

            orderLogger(orderId, 'info', `orderStatusFreeHospitalForCompany->company_call: ${company_call}`);
            if(company_id != null && company_call === 0){

                phoneApi.sendCallNotificationCompany(orderId, tenantId, company_id, (err, result) => {
                    if(err){
                        orderLogger(orderId, 'error', `orderStatusFreeHospitalForCompany->sendCallNotificationCompany->Error: ${err.message}`);
                    }else{
                        orderLogger(orderId, 'info', `orderStatusFreeHospitalForCompany->sendCallNotificationCompany->Result: ${result}`);

                        orderData.settings.company_call = 1;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if(err){
                                orderLogger(this.orderId, 'error', `orderStatusFreeHospitalForCompany->_updateOrderData->saveOrder->Error: ${err.message}`);
                            }
                            orderLogger(orderId, 'info', `orderStatusFreeHospitalForCompany->redisOrderManager.saveOrder->Result: ${result}`);
                        });
                    }
                });


            }
            console.log("СВОБОДНЫЙ больничный заказ компании");

            let tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000); // -- tomorrow
            let hour = randomInteger(0,2);
            let minute = randomInteger(1,50);
            tomorrow.setHours(hour); tomorrow.setMinutes(minute);

            let tenantNowTime = parseInt((new Date()).getTime() / 1000);
            let rejectTime = parseInt(tomorrow.getTime() / 1000);

            let startTime = parseInt(rejectTime) - parseInt(tenantNowTime);
            if (startTime <= 0) {
                startTime = 1;
            }

            orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.rejectOrderByTimeoutLabel, () => {
                orderLogger(orderId, 'info', 'orderActionManager->rejectOrderByTimeout->rejectOrderByTimeout called!');

                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.orderRejectTimeout,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.orderRejectTimeout} to order: ${orderId}. Result: ${result}`);
                    }
                });
            }, startTime * 1000, tenantId, orderId);
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};

