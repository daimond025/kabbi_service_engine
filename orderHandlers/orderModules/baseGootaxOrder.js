"use strict";

const orderActionManager = require("./common/orderActionManager");
const orderNotificationManager = require("./common/orderNotificationManager");
const redisOrderManager = require('../../database/redisDB/redisOrderManager');
const async = require("async");
const logger = require('../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../services/orderStatusManager').statusInfo;
const OrderTimer = require("../../services/serviceTimer");
const amqpWorkerMessageSender = require("../../services/amqpWorkerMessageSender");
const pushApi = require('../../services/externalWebServices/pushApi');
const serviceApi = require('../../services/externalWebServices/serviceApi');
const sqlManager = require("../../database/sqlDB/sqlManager");
const orderEventWorkerMessage = require('../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../orderEvent/orderEventWorkerCommand');
const orderEventSenderService = require('../orderEvent/orderEventSenderService');
const orderSettingsManager = require('./common/orderSettingsManager');
const redisWorkerReservedOrderListManager = require('../../database/redisDB/redisWorkerReservedOrderListManager');
const redisActiveOrderPhoneManager = require('../../database/redisDB/redisActiveOrderPhoneManager');
const redisWorkerManager = require("../../database/redisDB/redisWorkerManager");
const redisParkingManager = require("../../database/redisDB/redisParkingManager");
const orderStatusRejected = require('./common/statusHandlers/orderStatusRejected');
const orderStatusCompleted = require("./common/statusHandlers/orderStatusCompleted");
const orderStatusExecuting = require("./common/statusHandlers/orderStatusExecuting");
const orderStatusWorkerAtPlace = require("./common/statusHandlers/orderStatusWorkerAtPlace");
const orderStatusWorkerAssigned = require("./common/statusHandlers/orderStatusWorkerAssigned");
const preOrderStatusWorkerAssigned = require("./common/statusHandlers/preOrderStatusWorkerAssigned");
const preOrderStatusWorkerRefusedOrderAssignment = require("./common/statusHandlers/preOrderStatusWorkerRefusedOrderAssignment");
const preOrderStatusNew = require("./common/statusHandlers/preOrderStatusNew");
const orderStatusNewOrder = require("./common/statusHandlers/orderStatusNewOrder");
const orderStatusWorkerRefusedOrderAssignment = require('./common/statusHandlers/orderStatusWorkerRefusedOrderAssignment');
const orderStatusWorkerAssignedAtOrder = require('./common/statusHandlers/orderStatusWorkerAssignedAtOrder');
const orderStatusManualMode = require('./common/statusHandlers/orderStatusManualMode');
const orderStatusFreeOrder = require('./common/statusHandlers/orderStatusFreeOrder');
const orderStatusOutdated = require('./common/statusHandlers/orderStatusOutdated');
const orderStatusFreeHospitalForAll = require('./common/statusHandlers/orderStatusFreeHospitalForAll');
const orderStatusFreeHospitalForCompany = require('./common/statusHandlers/orderStatusFreeHospitalForCompany');
const preOrderStatusHospitalForCompany = require('./common/statusHandlers/preOrderStatusHospitalForCompany');

const orderStatusOfferOrder = require('./common/statusHandlers/orderStatusOfferOrder');
const orderStatusRefuseOfferOrder = require('./common/statusHandlers/orderStatusRefuseOfferOrder');
const preOrderStatusWaitingForConfirmation = require('./common/statusHandlers/preOrderStatusWaitingForConfirmation');
const preOrderStatusRefuseConfirmation = require('./common/statusHandlers/preOrderStatusRefuseConfirmation');
const preOrderStatusAcceptedConfirmation = require('./common/statusHandlers/preOrderStatusAcceptedConfirmation');
const stopOfferOrder = require('./common/actionHandlers/stopOfferOrder');
const sendOrderToExchange = require('./common/actionHandlers/sendOrderToExchange');
const refuseOfferedOrderMultiple = require('./common/actionHandlers/refuseOfferedOrderMultiple');
const getOfferedOrderMultiple = require('./common/actionHandlers/getOfferedOrderMultiple');
const clientLateTimeOut = require('./common/timerHandlers/clientLateTimeOut');
const orderLateTimeOut = require('./common/timerHandlers/orderLateTimeOut');
const orderOfferTimeOut = require('./common/timerHandlers/orderOfferTimeOut');
const orderRejectTimeOut = require('./common/timerHandlers/orderRejectTimeOut');
const workerLateTimeOut = require('./common/timerHandlers/workerLateTimeOut');
const preOrderConfirmTimeOut = require('./common/timerHandlers/preOrderConfirmTimeOut');
const orderDistributionManager = require('./common/orderDistributionManager');

const removeWorker = require('./common/actionHandlers/removeWorker');
const removeCompany = require('./common/actionHandlers/removeCompany');

/**
 * Base Gootax order
 * @class BaseGootaxOrder
 */
class BaseGootaxOrder {

    /**
     * @constructor
     * @param {Number} tenantId
     * @param {Number} orderId
     * @param {Number} isFromLoader - (1/0) Was it order add after restart node process(isFromLoader = 1), or from external services
     */
    constructor(tenantId, orderId, isFromLoader) {

        orderLogger(orderId, 'info', `baseGootaxOrder->new BaseGootaxOrder(${tenantId},${orderId},${isFromLoader}) called!`);

        this.orderId = orderId;

        this.tenantId = tenantId;

        this.isFromLoader = isFromLoader;

        this.settings = {};

        // System delay for offer order timer in seconds
        this.offerSecSystemIncrease = 2;

        this.settingNewOrderAssign = 0;

        this.currentDistributionCircle = 1;

        this.currentDistributionCircleRepeat = 1;

        this.maxDistanceDistributionCircle = 1;

        this.offeredInCircleRepeatWorkersList = new Set();

        this.offeredInCircleWorkersList = new Set();

        this.offeredInAllTimeWorkersList = new Set();

        // Service to manage order timers events
        this.orderTimer = new OrderTimer();

        //Array of sequence status ids changes
        this.statusArr = [];

        //Array of sequence status groups changes
        this.statusGroupArr = [];

        //Status changed after update order
        this.statusChanged = false;

        //New order status already was
        this.newStatusAlreadyWas = false;

        //Is this order for all workers or for special one
        this.isForAllWorkers = true;

        this.workerLateTimestamp = null;

        this.startWaitingForPaymentTimestamp = null;

        this._inited = false;

        this.promoWorkerId = null;

        this.dislikeWorkerIds = [];

        this.exceptCarModels = [];

        this.pointRecipientsNotifiedAboutOrderExecution = false;

        this._finished = false;

        // setting for order
        this.currentDistributionCircleRepeatForFree = 1;

        this._initOrder();
    }


    /**
     * In order finished
     * @returns {boolean}
     */
    get isFinished() {
        return this._finished;
    }

    /**
     * Set order as finished
     */
     setFinished() {
        this._finished = true;
    }

    /**
     * Do order event
     * @param event
     * @returns {Promise<boolean>}doEvent
     */
    async doEvent(event) {
        orderLogger(this.orderId, 'info', `baseGootaxOrder->doEvent called!`);
        orderLogger(this.orderId, 'info', `baseGootaxOrder->event data: ${JSON.stringify(event)}`);
        let result = false;

       /* console.log('Обрботка сообещния' );
        console.log(event.command );
        console.log('\n' );*/

        switch (event.command) {
            case orderEventOrderCommand.updateOrderData:
                const updateParams = typeof event.params === 'object' ? event.params : {};
                this._updateOrderData(event.sender_service, updateParams);
                result = true;
                break;
            case orderEventOrderCommand.refuseOfferedOrderMultiple:
                try {
                    result = await refuseOfferedOrderMultiple(this, event);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->refuseOfferedOrderMultiple->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.getOfferedOrderMultiple:
                try {
                    result = await getOfferedOrderMultiple(this, event);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->getOfferedOrderMultiple->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.stopOffer:
                try {
                    result = await stopOfferOrder(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->stopOfferOrder->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.sendOrderToExchange:
                try {
                    result = await sendOrderToExchange(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->sendOrderToExchange->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.clientLateTimeout:
                try {
                    result = await clientLateTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->clientLateTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.orderLateTimeout:
                try {
                    result = await orderLateTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->orderLateTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.orderOfferTimeout:
                try {
                    result = await orderOfferTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->orderOfferTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.orderRejectTimeout:
                try {
                    result = await orderRejectTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->orderRejectTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.workerLateTimeout:
                try {
                    result = await workerLateTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->workerLateTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.preOrderConfirmTimeout:
                try {
                    result = await preOrderConfirmTimeOut(this);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->preOrderConfirmTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.removeWorker:
                try {
                    result = await removeWorker(this, event);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->preOrderConfirmTimeOut->Error: ${err.message}`);
                }
                break;
            case orderEventOrderCommand.removeCompany:
                try {
                    result = await removeCompany(this, event);
                } catch (err) {
                    throw new Error(`baseGootaxOrder->doEvent->preOrderConfirmTimeOut->Error: ${err.message}`);
                }
                break;
            default:
                throw new Error(`baseGootaxOrder->doEvent->unsupported order event`);
        }
        return result;
    };


    /**
     * Modify order event
     * @param {Object} event
     * @return {*}
     */
    modifyEvent(event) {

        if (event.sender_service === orderEventSenderService.workerService && event.command === orderEventOrderCommand.updateOrderData) {
            //If worker sent refuse offer order and offer order type was 'batch' (sending offer order for many workers)
            //then need change event command to 'refuse_offer_order_multiply', because in this case we should not change order status
            if (event.params && parseInt(event.params.status_id) === statusInfo.worker_refused_order.status_id) {
                if (this.settings.OFFER_ORDER_TYPE === orderDistributionManager.multipleMode) {
                    event.command = orderEventOrderCommand.refuseOfferedOrderMultiple;
                }
            }
            //If worker sent assign at order and offer order type was 'batch' (sending offer order for many workers)
            //then need change event command to 'get_offered_order_multiple', because in this case we should not change order status
            if (event.params && parseInt(event.params.status_id) === statusInfo.worker_accepted_order.status_id) {
                //order is offering now
                if (this.statusArr[this.statusArr.length - 1] === statusInfo.offer_order.status_id) {
                    if (this.settings.OFFER_ORDER_TYPE === orderDistributionManager.multipleMode) {
                        event.command = orderEventOrderCommand.getOfferedOrderMultiple;
                    }
                }

            }
        }
        return event;
    }


    /**
     * Init order
     * @private
     */
    _initOrder() {
        if (this._inited) {
            return;
        }
        orderLogger(this.orderId, 'info', 'baseGootaxOrder->_initOrder called!');
        redisOrderManager.getOrder(this.tenantId, this.orderId, (err, orderData) => {
            if (err) {
                orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->Error of getting order from redis: ${err.message}. ${this.tenantId} ${this.orderId}`);
                return;
            }
            if (this.isFromLoader == 1) {
                this._initOrderFromLoader(orderData);
                return;
            }
            //Order sent from external services
            async.waterfall([
                // TODO формирование доп настроек  + плюс к заказу в редис
                //Persist order setting to to redis and write to object property
                callback => {
                    orderSettingsManager.persistSettings(this.tenantId, this.orderId)
                        .then(orderSettings => {

                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->orderSettingsManager.persistSettings->Result:`);
                            orderLogger(this.orderId, 'info', orderSettings);
                            //Set settings
                            this.settings = orderSettings;

                            let circleDistanceSettings = [
                                orderSettings.CIRCLE_DISTANCE_5,
                                orderSettings.CIRCLE_DISTANCE_4,
                                orderSettings.CIRCLE_DISTANCE_3,
                                orderSettings.CIRCLE_DISTANCE_2,
                                orderSettings.CIRCLE_DISTANCE_1,
                            ];
                            let lastNotEmptyDistanceSetting = circleDistanceSettings.find(element => {
                                return (!isNaN(parseFloat(element)) && element !== 0);
                            });


                            if (!lastNotEmptyDistanceSetting) {
                                if (orderSettings.CIRCLE_DISTANCE_1) {
                                    this.maxDistanceDistributionCircle = 5
                                } else {
                                    this.maxDistanceDistributionCircle = 0;
                                }
                            } else {
                                this.maxDistanceDistributionCircle = circleDistanceSettings.reverse().indexOf(lastNotEmptyDistanceSetting) + 1;
                            }
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->orderSettingsManager.persistSettings->Error: ${err.message}`);
                            return callback(err);
                        })
                },
                // TODO свойсво последенее состояние закза
                //Save last order status to redis
                (prevResult, callback) => {
                    this._persistLastOrderStatus(this.tenantId, this.orderId)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->_persistLastOrderStatus->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->_persistLastOrderStatus->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                // todo если предзаказ
                //If pre-order, then set pre-order attribute
                (prevResult, callback) => {

                    const newPreOrdersStatuses = [statusInfo.new_pre_order.status_id, statusInfo.new_pre_order_no_parking.status_id];
                    const orderStatusId = parseInt(orderData.status.status_id);

                    if (newPreOrdersStatuses.indexOf(orderStatusId) === -1) {
                        return callback(null, 1);
                    }

                    this._setPreOrderAttribute(this.tenantId, this.orderId)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->_setPreOrderAttribute->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->_setPreOrderAttribute->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                // todo массив данных по телефону
                //Save client phone to redis
                (prevResult, callback) => {
                    this._persistOrderPhone(this.tenantId, this.orderId, orderData.phone)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->_persistOrderPhone->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->_persistOrderPhone->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                // todo статистика
                //Send order to statistic
                (prevResult, callback) => {
                    serviceApi.sendOrderToStatistic(this.tenantId, this.orderId, (err, result) => {
                        if (err) {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->sendOrderToStatistic->Error: ${err.message}`);
                            return callback(null, 1);
                        } else {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->sendOrderToStatistic->Result: ${result}`);
                            return callback(null, 1);
                        }
                    })
                },
                //if order from border, init it
                (prevResult, callback) => {
                    if (orderData.device === "WORKER") {
                        orderLogger(this.orderId, 'info', "baseGootaxOrder->initOrder->order from border. Need init it.");

                        this._initBorderOrder(orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->_initBorderOrder->Result: ${result}`);
                                return callback(null, 1);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->_initBorderOrder->Error: ${err.message}`);
                                return callback(null, 1);
                            });
                    } else {
                        return callback(null, 1);
                    }
                },
                //Check worker promo code
                (prevResult, callback) => {
                    if (!orderData.promo_code_id) {
                        return callback(null, 1);
                    }
                    //order has promocode
                    orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->order has promocode with id=${orderData.promo_code_id}`);
                    let promoCodeId = orderData.promo_code_id;
                    sqlManager.getPromoCodeWorkerId(promoCodeId, (err, workerId) => {
                        if (err) {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrder->sqlManager.getPromoCodeWorkerId->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                        if (!workerId) {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->sqlManager.getPromoCodeWorkerId->promo code have not worker`);
                            return callback(null, 1);
                        }
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrder->sqlManager.getPromoCodeWorkerId->promoWorkerId=${workerId}`);
                        this.promoWorkerId = parseInt(workerId);
                        return callback(null, 1);
                    })
                },
                //Set order except car models
                (prevResult, callback) => {
                    sqlManager.getOrderExceptCarModels(this.orderId)
                        .then(carModels => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->sqlManager.getOrderExceptCarModels->Result: ${JSON.stringify(carModels)}`);
                            if (carModels) {
                                this.exceptCarModels = carModels.map(model => {
                                    return parseInt(model.car_model_id);
                                });
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->exceptCarModels->${JSON.stringify(this.exceptCarModels)}`);
                            }
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->sqlManager.getOrderExceptCarModels->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                //Set client dislike workers
                (prevResult, callback) => {
                    const clientId = parseInt(orderData.client_id);
                    const positionId = parseInt(orderData.position_id);

                    sqlManager.getClientDislikeWorkers(clientId, positionId)
                        .then(dislikeWorkers => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->sqlManager.getClientDislikeWorkers->Result: ${JSON.stringify(dislikeWorkers)}`);
                            if (dislikeWorkers) {
                                this.dislikeWorkerIds = dislikeWorkers.map(worker => {
                                    return parseInt(worker.worker_id)
                                });
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->dislikeWorkerIds->${JSON.stringify(this.exceptCarModels)}`);
                            }
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->sqlManager.getClientDislikeWorkers->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                //Save system settings(server_id,process_id...)
                (prevResult, callback) => {
                    orderSettingsManager.setSystemSettings(this.tenantId, this.orderId)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->orderSettingsManager.setSystemSettings->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->orderSettingsManager.setSystemSettings->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
            ], (err, result) => {
                if (err) {
                    orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->Error: ${err.message}`);
                    return;
                }
                orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->Result: ${result}`);
                this._inited = true;
                this._updateOrderData(orderEventSenderService.engineService);
            });

        });
    };

    _initOrderFromLoader(orderData) {
        //Order sent from loader
        //Set settings
        this.settings = orderData.settings;
        let circleDistanceSettings = [
            orderData.settings.CIRCLE_DISTANCE_5,
            orderData.settings.CIRCLE_DISTANCE_4,
            orderData.settings.CIRCLE_DISTANCE_3,
            orderData.settings.CIRCLE_DISTANCE_2,
            orderData.settings.CIRCLE_DISTANCE_1,
        ];
        let lastNotEmptyDistanceSetting = circleDistanceSettings.find(element => {
            return (!isNaN(parseFloat(element)) && element !== 0);
        });
        if (!lastNotEmptyDistanceSetting) {
            if (orderData.settings.CIRCLE_DISTANCE_1) {
                this.maxDistanceDistributionCircle = 5
            } else {
                this.maxDistanceDistributionCircle = 0;
            }
        } else {
            this.maxDistanceDistributionCircle = circleDistanceSettings.reverse().indexOf(lastNotEmptyDistanceSetting) + 1;
        }

        async.waterfall([
            callback => {
                if (!orderData.promo_code_id) {
                    return callback(null, 1);
                }
                orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->order has promocode with id=${orderData.promo_code_id}`);
                let promoCodeId = orderData.promo_code_id;
                sqlManager.getPromoCodeWorkerId(promoCodeId, (err, workerId) => {
                    if (err) {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getPromoCodeWorkerId->Error: ${err.message}`);
                        return callback(null, 1);
                    }
                    if (!workerId) {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getPromoCodeWorkerId->promo code have not worker`);
                        return callback(null, 1);
                    }
                    orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getPromoCodeWorkerId->promoWorkerId=${workerId}`);
                    this.promoWorkerId = parseInt(workerId);
                    return callback(null, 1);
                })
            },
            //Set order except car models
            (prevResult, callback) => {
                sqlManager.getOrderExceptCarModels(this.orderId)
                    .then(carModels => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getOrderExceptCarModels->Result: ${carModels}`);
                        if (carModels) {
                            this.exceptCarModels = carModels.map(model => {
                                return model.car_model_id
                            })
                        }
                        return callback(null, 1);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getOrderExceptCarModels->Error: ${err.message}`);
                        return callback(null, 1);
                    })
            },
            //Set client dislike workers
            (prevResult, callback) => {
                const clientId = parseInt(orderData.client_id);
                const positionId = parseInt(orderData.position_id);
                sqlManager.getClientDislikeWorkers(clientId, positionId)
                    .then(dislikeWorkers => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getClientDislikeWorkers->Result: ${dislikeWorkers}`);
                        if (dislikeWorkers) {
                            this.dislikeWorkerIds = dislikeWorkers.map(worker => {
                                return worker.worker_id
                            })
                        }
                        return callback(null, 1);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrderFromLoader->sqlManager.getClientDislikeWorkers->Error: ${err.message}`);
                        return callback(null, 1);
                    })
            },
            //Save system settings(server_id,process_id...)
            (prevResult, callback) => {
                orderSettingsManager.setSystemSettings(this.tenantId, this.orderId)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->orderSettingsManager.setSystemSettings->Result: ${result}`);
                        return callback(null, 1);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrderFromLoader->orderSettingsManager.setSystemSettings->Error: ${err.message}`);
                        return callback(null, 1);
                    })
            },
        ], (err, result) => {
            if (err) {
                orderLogger(this.orderId, 'error', `baseGootaxOrder->_initOrderFromLoader->Error: ${err.message}`);
                return;
            }
            orderLogger(this.orderId, 'info', `baseGootaxOrder->_initOrderFromLoader->Result: ${result}`);
            this._inited = true;
            this._updateOrderData(orderEventSenderService.engineService);
        });
    }


    /**
     * Init order from border
     * @param {object} orderData
     * @private
     */
    _initBorderOrder(orderData) {
        return new Promise((resolve, reject) => {
            orderLogger(this.orderId, 'info', 'baseGootaxOrder->_initBorderOrder CALLED!');
            if (orderData && orderData.worker && orderData.worker.callsign) {
                const orderId = this.orderId;
                const tenantId = this.tenantId;
                const workerCallsign = orderData.worker.callsign;
                redisWorkerReservedOrderListManager.addOrder(tenantId, workerCallsign, orderId, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `baseGootaxOrder->_initBorderOrder->redisWorkerReservedOrderListManager.addOrder->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `baseGootaxOrder->_initBorderOrder->redisWorkerReservedOrderListManager.addOrder->Result: ${result}`);
                    }
                });
                redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                    if (err) {
                        orderLogger(orderId, 'error', `baseGootaxOrder->_initBorderOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
                        return reject(err);
                    }
                    // список активных заказов
                    let active_orders = typeof workerData.worker.active_orders === "object" ?
                        Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])): [];
                    let index_del = active_orders.indexOf(orderId);
                    if(index_del == -1){
                        active_orders.push(orderId);
                    }
                    workerData.worker.active_orders = active_orders;

                    workerData.worker.last_order_id = orderId;
                    workerData.worker.status = "ON_ORDER";
                    if (typeof workerData.geo.parking_id !== "undefined" && workerData.geo.parking_id !== null) {
                        redisParkingManager.deleteWorkerFromParking(workerData.geo.parking_id, workerCallsign, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `baseGootaxOrder->_initBorderOrder->redisParkingManager.deleteWorkerFromParking->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `baseGootaxOrder->_initBorderOrder->redisParkingManager.deleteWorkerFromParking->Result: ${result}`);
                            }
                        })
                    }
                    workerData.geo.parking_id = null;
                    orderLogger(orderId, 'info', `baseGootaxOrder->_initBorderOrder->last_order_id:${workerData.worker.last_order_id}`);
                    redisWorkerManager.saveWorker(workerData, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `baseGootaxOrder->_initBorderOrder->redisWorkerManager.saveWorker->Error: ${err.message}`);
                            return reject(err);
                        } else {
                            orderLogger(orderId, 'info', `baseGootaxOrder->_initBorderOrder->redisWorkerManager.saveWorker->Result: ${result}`);
                            return resolve(1);
                        }
                    })
                })
            } else {
                orderLogger(this.orderId, 'error', 'baseGootaxOrder->_initBorderOrder->Error: order have not worker data!');
                return reject(new Error('order have not worker data'));
            }
        });
    }

    /**
     * Handle update_order_data event
     * @param {string} eventSenderService
     * @param {object} messageParams
     * @private
     */
    _updateOrderData(eventSenderService, messageParams = {}) {
        orderLogger(this.orderId, 'info', 'baseGootaxOrder->_updateOrderData called!');
        orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->messageParams: ${JSON.stringify(messageParams)}`);
        redisOrderManager.getOrder(this.tenantId, this.orderId, (err, orderData) => {
            if (err) {
                orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->getOrder->Error: ${err.message}`);
                return;
            }
            orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->getOrder->done!`);
            let orderStatusId = parseInt(orderData.status.status_id);
            let orderStatusGroup = orderData.status.status_group;
            let userModified = orderData.user_modifed;
            this.statusChanged = false;
            let subjectType;

            //Order status id changed
            if (this.statusArr[this.statusArr.length - 1] !== orderStatusId) {
                /*// TODO daimond
                console.log('новый статус');
                console.log(orderStatusId);
                console.log(this.statusArr[this.statusArr.length - 1]);
                console.log(this.isFromLoader);*/

                this.statusArr.push(orderStatusId);
                this.statusChanged = true;
                orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->order status id changed to ${orderStatusId}`);
                subjectType = orderEventSenderService.getEventSubjectType(eventSenderService);

                if (!this.isFromLoader) {
                    sqlManager.logOrderChangeData(this.tenantId, this.orderId, "status_id", this.orderId, "order", orderStatusId, subjectType, (err, result) => {
                        if (err) {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->sqlManager.logOrderChangeData->Error: ${err.message}`);
                        } else {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->sqlManager.logOrderChangeData->Result: ${JSON.stringify(result)}`);
                        }
                    });
                    const notifyOptions = {
                        smsNotifyPointRecipientAboutOrderExecuting: parseInt(this.settings.SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING),
                        pointRecipientsNotifiedAboutOrderExecution: this.pointRecipientsNotifiedAboutOrderExecution
                    };

                    // daimond (телефон)
                    orderNotificationManager.sendChangeOrderStatusNotificationToClient(orderData, notifyOptions)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->orderNotificationManager.sendChangeOrderStatusNotificationToClient->Info: ${result}`);
                            if (result.notify_point_recipient) {
                                this.pointRecipientsNotifiedAboutOrderExecution = true;
                            }
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->orderNotificationManager.sendChangeOrderStatusNotificationToClient->Error: ${err.message}`);
                        });
                } else {
                    this.isFromLoader = false;
                }
                //Order status group changed
                if (this.statusGroupArr[this.statusGroupArr.length - 1] !== orderStatusGroup) {
                    this.statusGroupArr.push(orderStatusGroup);
                    orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->order status group changed to ${orderStatusGroup}`);
                }
            }
            async.parallel([
                callback => {
                    if (this.statusChanged) {
                        this._persistLastOrderStatus(this.tenantId, this.orderId)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->_persistLastOrderStatus->Result: ${result}`);
                                callback(null, 1);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->_persistLastOrderStatus->
                                 tenant_id:${this.tenantId} order_id:${this.orderId}->Error: ${err.message}`);
                                callback(null, 1);
                            })
                    } else {
                        callback(null, 1);
                    }
                },
                callback => {
                    // if order has worker
                    // ad order modified by dispatcher
                    // or from client app
                    // or from worker app edited address or  predv_price
                    // then send order update event to worker
                    if (orderData.worker && orderData.worker.callsign && (userModified || eventSenderService === orderEventSenderService.clientService ||
                        (eventSenderService === orderEventSenderService.workerService && (messageParams.hasOwnProperty('predv_price') || messageParams.hasOwnProperty('address'))))) {
                        let messageString = orderEventWorkerMessage({
                            command: orderEventWorkerCommand.updateOrder,
                            tenant_id: orderData.tenant_id,
                            tenant_login: orderData.tenant_login,
                            worker_callsign: orderData.worker.callsign,
                            params: {
                                order_id: this.orderId
                            }
                        });
                        const messageTtl = 60 * 60 * 24 * 10;// 10 days
                        const workerCallsign = orderData.worker.callsign;
                        amqpWorkerMessageSender.sendMessage(orderData.tenant_id, workerCallsign, messageString, messageTtl, (err, result) => {
                            if (err) {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                            } else {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->amqpWorkerMessageSender.sendMessage->sent command: ${orderEventWorkerCommand.updateOrder} to worker ${workerCallsign}. Result: ${result}`);
                            }
                        });
                        pushApi.sendToWorkerUpdateOrder(this.tenantId, this.orderId, workerCallsign, (err, result) => {
                            if (err) {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->pushApi.sendToWorkerUpdateOrder->Error: ${err.message}`);
                            } else {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_updateOrderData->pushApi.sendToWorkerUpdateOrder->Result: ${result}`);
                            }
                            return callback(null, 1);
                        })
                    } else {
                        callback(null, 1);
                    }
                }
            ], (err, result) => {
                if (err) {
                    orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->Error: ${err.message}`);
                } else {
                    if (userModified) {
                        orderData.user_modifed = null;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_updateOrderData->saveOrder->Error: ${err.message}`);
                            }
                            this._statusHandler(orderData, eventSenderService, messageParams);
                        });
                    } else {
                        this._statusHandler(orderData, eventSenderService, messageParams);
                    }
                }

            });
        });
    };

    /**
     * Handle current order status
     * @param {object} orderData
     * @param {string} eventSenderService
     * @param {object} messageParams
     * @private
     */
    _statusHandler(orderData, eventSenderService, messageParams) {

        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler called!`);
        const orderStatusId = parseInt(orderData.status.status_id);
        const orderStatusGroup = orderData.status.status_group;

        console.log('Группа - '  + orderStatusGroup);
        console.log('Уточнение - '  + orderStatusId);
        console.log('параметры - '  + messageParams);
        console.log('\n' );

        switch (orderStatusGroup) {
            case "new": {
                switch (orderStatusId) {
                    case statusInfo.new_order.status_id:
                    case statusInfo.new_order_no_parking.status_id: {
                        orderStatusNewOrder(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusNewOrder->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusNewOrder->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.offer_order.status_id: {
                        orderStatusOfferOrder(this)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusOfferOrder->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusOfferOrder->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.worker_refused_order.status_id:
                    case statusInfo.worker_ignored_offer_order.status_id: {
                        orderStatusRefuseOfferOrder(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusRefuseOfferOrder->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusRefuseOfferOrder->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.free_order.status_id: {
                        orderStatusFreeOrder(this, orderData, messageParams)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusFreeOrder->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusFreeOrder->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.manual_mode.status_id: {
                        orderStatusManualMode(this, orderData, eventSenderService)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusManualMode->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusManualMode->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.outdated_order.status_id: {
                        orderStatusOutdated(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.worker_assigned_at_order_soft.status_id:
                    case statusInfo.worker_assigned_at_order_hard.status_id: {
                        orderStatusWorkerAssignedAtOrder(this, orderData, eventSenderService)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusWorkerAssignedAtOrder->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusWorkerAssignedAtOrder->Error: ${err.message}`);

                            });
                        break;
                    }
                    case statusInfo.worker_refused_order_assign.status_id: {
                        orderStatusWorkerRefusedOrderAssignment(this, orderData, eventSenderService)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusWorkerRefusedOrderAssignment->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusWorkerRefusedOrderAssignment->Error: ${err.message}`);
                            });
                        break;
                    }

                    // HOSPITAL STATUS - free for all
                    case statusInfo.hospital_free_order.status_id:{
                        orderStatusFreeHospitalForAll(this, orderData, messageParams)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Error: ${err.message}`);
                        });
                        break;
                    }
                    case statusInfo.hospital_free_order_company.status_id:{
                        orderStatusFreeHospitalForCompany(this, orderData, messageParams)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusOutdated->Error: ${err.message}`);
                        });
                        break;
                    }
                    default: {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->Have't handler for status ${orderStatusId}`);
                        this.orderTimer.clearAll();
                        orderActionManager.rejectOrderByTimeout(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderActionManager.rejectOrderByTimeout->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderActionManager.rejectOrderByTimeout->Error: ${err.message}`);
                            });
                        break;
                    }
                }
                break;

            }
            case "pre_order": {
                switch (orderStatusId) {
                    case statusInfo.new_pre_order.status_id:
                    case statusInfo.new_pre_order_no_parking.status_id: {
                        let orderTimeIsChanged = messageParams && messageParams.order_time;
                        preOrderStatusNew(this, orderData, orderTimeIsChanged)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusNew->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusNew->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.worker_refused_preorder.status_id: {
                        preOrderStatusWorkerRefusedOrderAssignment(this, orderData, eventSenderService)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusWorkerRefusedOrderAssignment->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusWorkerRefusedOrderAssignment->Error: ${err.message}`);

                            });
                        break;
                    }
                    case statusInfo.worker_accepted_preorder.status_id:
                    case statusInfo.worker_assigned_at_preorder_hard.status_id:
                    case statusInfo.worker_assigned_at_preorder_soft.status_id: {
                        let orderTimeIsChanged = messageParams && messageParams.order_time;
                        preOrderStatusWorkerAssigned(this, orderData, eventSenderService, orderTimeIsChanged)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusWorkerAssigned->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusWorkerAssigned->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.waiting_for_preorder_confirmation.status_id: {
                        preOrderStatusWaitingForConfirmation(this)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusWaitingForConfirmation->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusWaitingForConfirmation->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.worker_ignored_preorder_confirmation.status_id:
                    case statusInfo.worker_refused_preorder_confirmation.status_id: {
                        preOrderStatusRefuseConfirmation(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusRefuseConfirmation->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusRefuseConfirmation->Error: ${err.message}`);
                            });
                        break;
                    }
                    case statusInfo.worker_accepted_preorder_confirmation.status_id: {
                        preOrderStatusAcceptedConfirmation(this, orderData)
                            .then(result => {
                                orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->preOrderStatusAcceptedConfirmation->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->preOrderStatusAcceptedCo_order_change_datanfirmation->Error: ${err.message}`);
                            });
                        break;
                    }

                    // hospital pre company
                    case statusInfo.hospital_pre_order.status_id:
                    case statusInfo.hospital_pre_order_company.status_id:{
                        preOrderStatusHospitalForCompany(this, orderData)
                        .then(result => {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->pre_order_hospital_сompany->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->pre_order_hospital_сompany-->Error: ${err.message}`);
                        });
                        break;
                    }
                    default: {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->Have't handler for status ${orderStatusId}`);
                        break;
                    }
                }
                break;
            }
            case "car_assigned": {
                orderStatusWorkerAssigned(this, orderData)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusWorkerAssigned->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusWorkerAssigned->Error: ${err.message}`);

                    });
                break;
            }
            case "car_at_place": {
                orderStatusWorkerAtPlace(this, orderData)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusWorkerAtPlace->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusWorkerAtPlace->Error: ${err.message}`);

                    });
                break;
            }
            case "executing": {
                orderStatusExecuting(this, orderData)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusExecuting->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusExecuting->Error: ${err.message}`);

                    });
                break;
            }
            case "completed": {
                this.setFinished();
                orderStatusCompleted(this, orderData, eventSenderService)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusCompleted->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusCompleted->Error: ${err.message}`);

                    });
                break;
            }
            case "rejected": {
                this.setFinished();
                orderStatusRejected(this, orderData)
                    .then(result => {
                        orderLogger(this.orderId, 'info', `baseGootaxOrder->_statusHandler->orderStatusRejected->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(this.orderId, 'error', `baseGootaxOrder->_statusHandler->orderStatusRejected->Error: ${err.message}`);

                    });
                break;
            }
            default: {
                break;
            }
        }
    };

    /**
     * Persist last order status to redis
     * @param {number} tenantId
     * @param {number} orderId
     * @return {Promise}
     * @private
     */
    _persistLastOrderStatus(tenantId, orderId) {
        orderLogger(this.orderId, 'info', `baseGootaxOrder->_persistLastOrderStatus called!`);
        return new Promise((resolve, reject) => {
            redisOrderManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    return reject(err);
                } else {

                    orderItem.status.last_status_id = orderItem.status.status_id;
                    orderItem.status.last_status_group = orderItem.status.status_group;
                    redisOrderManager.saveOrder(orderItem, (err, result) => {
                        if (err) {
                            return reject(err);
                        }
                        resolve(result);
                    })
                }
            });
        })
    }

    /**
     * Persist order client phone to redis
     * @param {number} tenantId
     * @param {number} orderId
     * @param {string} clientPhone
     * @return {Promise}
     * @private
     */
    _persistOrderPhone(tenantId, orderId, clientPhone) {
        orderLogger(this.orderId, 'info', `baseGootaxOrder->_persistOrderPhone called!`);
        return new Promise((resolve, reject) => {
            try {
                redisActiveOrderPhoneManager.gerOrdersByPhone(tenantId, clientPhone, (err, orders) => {
                    if (err) {
                        return reject(err);
                    }
                    let orderIdsArr = [];
                    if (!orders) {
                        orderIdsArr.push(orderId);
                    } else {
                        if (typeof orders === "object" && orders !== null) {
                            orderIdsArr = Object.keys(orders).map(k => orders[k]);
                            if (!orderIdsArr.find((element, index, array) => parseInt(element) === parseInt(orderId))) {
                                orderIdsArr.push(orderId);
                            }
                        } else {
                            orderIdsArr = [];
                        }
                    }
                    redisActiveOrderPhoneManager.setOrdersByPhone(tenantId, clientPhone, orderIdsArr, (err, result) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(result);
                    });
                });
            } catch (err) {
                return reject(err);
            }
        })
    }

    /**
     * Set pre-order attribute
     * @param {number} tenantId
     * @param {number} orderId
     * @return {Promise}
     * @private
     */
    _setPreOrderAttribute(tenantId, orderId) {
        orderLogger(this.orderId, 'info', `baseGootaxOrder->_setPreOrderAttribute called!`);
        return new Promise((resolve, reject) => {
            redisOrderManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    return reject(err);
                } else {
                    orderItem.originally_preorder = 1;
                    redisOrderManager.saveOrder(orderItem, (err, result) => {
                        if (err) {
                            return reject(err);
                        }
                        resolve(result);
                    })
                }
            });
        })
    }
}

module.exports = BaseGootaxOrder;