'use strict';

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const serviceApi = require("../../../../services/externalWebServices/serviceApi");
const pushApi = require("../../../../services/externalWebServices/pushApi");
const async = require("async");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderActionManager = require("../orderActionManager");
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const redisHospitalWorkerOrderManager = require("../../../../database/redisDB/redisHospitalWorkerOrderManager");
const closeWorkerShift = require("../../../../workerShiftHandlers/closeWorkerShift");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const orderNotificationManager = require('../orderNotificationManager');
const config = require('../../../../services/lib').getConfig();
const nodeId = config.MY_ID;
const redisWorkerReservedOrderListManager = require('../../../../database/redisDB/redisWorkerReservedOrderListManager');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../../orderEvent/orderEventWorkerCommand');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const workerStatusManager = require('../../../../services/workerStatusManager');
const mergeArray = require("../../../../services/lib").mergeArray;
const OrderSettings = require('../../../../database/mongo/models/OrderSettings');
const OrderSettingsRepository = require('../../../../database/mongo/repository/OrderSettingsRepository');

const orderType = require("../../../orderEvent/orderDevice");
const getOrderStatusById = require('../../../../services/orderStatusManager').getOrderStatusById;

/**
 * Get pred Order status group - get orderType
 * @param {BaseGootaxOrder} orderInstance
 *  * @param {string} orderData
 * @return {Promise}
 */
function predStatus(orderInstance, orderType){
    let orderTypeDefault = orderType;
    if(orderInstance.statusArr.length >= 2){
        let predOrderStatusId = orderInstance.statusArr[orderInstance.statusArr.length - 2];
        const statusData = getOrderStatusById(predOrderStatusId);
        if(statusData){
            orderTypeDefault = (statusData.status_group === 'pre_order') ? 'preorder' : 'order';
        }
    }
    return orderTypeDefault;
}

/**
 * Order status rejected
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusRejected called!');
            const tenantId = parseInt(orderInstance.tenantId);
            const orderId = parseInt(orderInstance.orderId);
            const clientId = parseInt(orderData.client_id);
            const statusId = parseInt(orderData.status.status_id);
            const tenantLogin = orderData.tenant_login;

            let orderType = 'order';
            const workeraAssignCallsign = (orderData.worker && orderData.worker.callsign) ? parseInt(orderData.worker.callsign) : null;
            if(workeraAssignCallsign){
                orderType = predStatus(orderInstance,orderType );
            }
            orderLogger(orderId, 'info', `orderStatusRejected->orderTypeAssignWorker->${orderType}.`);

            const updateTime = Math.floor(Date.now() / 1000);
            const typeOrder = orderData.device.toString();
            if (orderInstance.statusArr[orderInstance.statusArr.length - 2] !== statusInfo.worker_coming_later.status_id) {
                orderInstance.workerLateTimestamp = null;
            }
            if (orderInstance.statusArr[orderInstance.statusArr.length - 2] !== statusInfo.waiting_for_payment.status_id) {
                orderInstance.startWaitingForPaymentTimestamp = null;
            }
            const lastStatusGroup = orderInstance.statusGroupArr[orderInstance.statusGroupArr.length - 2];
            orderData.update_time = updateTime;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel);
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.distributionCyclesTimeoutLabel);

            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.orderLateTimeoutLabel);

            let workersList = workeraAssignCallsign ? [workeraAssignCallsign] : [];
            let offeredWorkersList = typeof orderData.workers_offered === "object" ?
                Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];

            const workersRefuseAll = typeof orderData.refuse_workers_list === "object" ?
                Object.keys(orderData.refuse_workers_list).map(key => orderData.refuse_workers_list[key]) : [];

            //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
            if (typeof orderData.offered_workers_list === "object") {
                offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
            }
            workersList = mergeArray(offeredWorkersList, workersList);
            async.parallel({
                updateClientCounters: callback => {
                    serviceApi.updateClientCounters(tenantId, clientId, statusId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->serviceApi.updateClientCounters->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusRejected->serviceApi.updateClientCounters->Result: ${result}`);
                        return callback(null, 1);
                    });
                },
                insertRefuseWorker: callback => {

                    if(workersRefuseAll.length > 0){
                        try {
                            sqlManager.insertRefuseWorker(orderId, workersRefuseAll)
                            .then(result =>  {
                                let sucess = typeof result.sucess !== undefined ?  result.sucess : 0;
                                orderLogger(orderId, 'error', `orderStatusRejected->sqlManager.insertRefuseWorker->Result:: ${sucess}`);
                                return callback(null, 1);
                            })
                            .catch(err => {
                                orderLogger(orderId, 'error', `orderStatusRejected->sqlManager.insertRefuseWorker->Error ${err.message}`);
                                return callback(null, 1);
                            });
                        } catch (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->sqlManager.insertRefuseWorker->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                    }else {
                        return callback(null, 1);
                    }

                },
                deleteOrderPhone: callback => {
                    orderActionManager.deleteOrderPhone(orderData)
                        .then(result => {
                            orderLogger(orderId, 'info', `orderStatusRejected->orderActionManager.deleteOrderPhone->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderStatusRejected->orderActionManager.deleteOrderPhone->Error: ${err.message}`);
                        });
                },
                sendOrderToStatistic: callback => {
                    serviceApi.sendOrderToStatistic(tenantId, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->serviceApi.sendOrderToStatistic->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusRejected->serviceApi.sendOrderToStatistic->Result: ${result}`);
                        return callback(null, 1);
                    })
                },
                rejectWorker: callback => {
                    if (!workersList.length) {
                        return callback(null, 1);
                    }
                    orderLogger(orderId, 'info', `orderStatusRejected->order workers list: ${JSON.stringify(workersList)}`);
                    async.each(workersList,
                        (workerCallsign, cb) => {
                            redisWorkerReservedOrderListManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderStatusRejected->redisWorkerReservedOrderListManager.delOrder->worker:${workerCallsign}. Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `orderStatusRejected->redisWorkerReservedOrderListManager.delOrder->worker:${workerCallsign}. Result: ${result}`);
                                }
                            });

                            if(typeOrder == orderType.HOSPITAL ){
                                redisHospitalWorkerOrderManager.delOrder(tenantId, workerCallsign, orderId ,(err, result) => {
                                    if(err){
                                        orderLogger(orderId, 'error', `orderStatusCompleted->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                                    }else{
                                        orderLogger(orderId, 'info', `orderStatusRejected->redisHospitalWorkerOrderManager->delOrder->worker:${workerCallsign}. Result: ${result}`);
                                    }
                                });
                            }


                            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderStatusRejected->redisWorkerManager.getWorker->worker:${workerCallsign}. Error: ${err.message}`);
                                    return cb(null, 1);
                                }
                                const workerStatus = workerData.worker.status;
                                const workerShiftId = parseInt(workerData.worker.worker_shift_id);
                                const lastOrderId = parseInt(workerData.worker.last_order_id);
                                const lastOfferedOrderId = parseInt(workerData.worker.last_offered_order_id);
                                const needCloseShift = parseInt(workerData.worker.need_close_shift) === 1;
                                orderLogger(orderId, 'info', `
                                orderStatusRejected->worker:${workerCallsign}. 
                                workerStatus=${workerStatus}. 
                                workerShiftId=${workerShiftId}. 
                                lastOrderId=${lastOrderId}. 
                                lastOfferedOrderId=${lastOfferedOrderId}. 
                                needCloseShift=${needCloseShift}`);

                                // список активных заказов - удаление
                                let active_orders = typeof workerData.worker.active_orders === "object" ?
                                    Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])): [];
                                let index_del = active_orders.indexOf(orderId);
                                if(index_del > -1){
                                    active_orders.splice(index_del, 1);
                                }
                                workerData.worker.active_orders = active_orders;
                                orderLogger(orderId, 'info', `orderStatusRejected->deleteWorkerFormOrder->active_orders ${active_orders.join(',')} in workerCallsign: ${workerCallsign}`);

                               /* console.log('ВОДИТЕЛЬ ОТКЛОНИЛ заказа - список активных заказов');
                                console.log(workerData.worker.active_orders );*/

                                let needUpdateWorkerData = false;
                                let workerOnThisOrder = false;
                                if (workerStatus === workerStatusManager.onOrder && (active_orders.length === 0)) {
                                    orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. need set worker status=${workerStatusManager.free}: ${true}`);
                                    workerData.worker.status = workerStatusManager.free;
                                    needUpdateWorkerData = true;
                                    workerOnThisOrder = true;
                                }

                                if (workerStatus === workerStatusManager.offerOrder && lastOfferedOrderId === orderId) {
                                    orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. need set worker status=${workerStatusManager.free}: ${true}`);
                                    workerData.worker.status = workerStatusManager.free;
                                    needUpdateWorkerData = true;
                                }
                                if (!needUpdateWorkerData) {

                                    // send command to assign worker
                                    if(true ){ // workeraAssignCallsign != null && workerCallsign === workeraAssignCallsign ||
                                        const messageStringToAssign = orderEventWorkerMessage({
                                            command: orderEventWorkerCommand.orderListDelItem,
                                            tenant_id: tenantId,
                                            tenant_login: tenantLogin,
                                            worker_callsign: workerCallsign,
                                            params: {
                                                order_id: orderId,
                                                type: orderType
                                            }
                                        });

                                        const messageTtl = 60 * 60 * 24 * 3; // 3 days
                                        amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageStringToAssign, messageTtl, (err, result) => {
                                            if (err) {
                                                orderLogger(orderId, 'error', `orderStatusRejected->deleteOrderWorkerList->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                            } else {
                                                orderLogger(orderId, 'info', `orderStatusRejected->deleteOrderWorkerList->amqpWorkerMessageSender.sendMessage->Result: ${result}`);
                                            }
                                        });
                                    }
                                    return cb(null, 1);
                                }
                                if (workerOnThisOrder) {
                                    sqlManager.logWorkerRejectedOrderCounter(workerShiftId, orderId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. logWorkerCompletedOrderCounter->Error`);
                                            orderLogger(orderId, 'error', err.message);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. logWorkerCompletedOrderCounter->Result: ${result}`);
                                        }
                                    });
                                    if (orderInstance.workerLateTimestamp !== null) {
                                        const lateTimeSec = updateTime - orderInstance.workerLateTimestamp;
                                        sqlManager.logWorkerLateTime(workerShiftId, lateTimeSec, (err, result) => {
                                            if (err) {
                                                orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                            } else {
                                                orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logWorkerLateTime->Result: ${result}`);
                                            }
                                        });
                                    }
                                    if (orderInstance.startWaitingForPaymentTimestamp !== null) {
                                        const orderPaymentTimeSec = updateTime - orderInstance.startWaitingForPaymentTimestamp;
                                        sqlManager.logWorkerOrderPaymentTime(workerShiftId, orderPaymentTimeSec, (err, result) => {
                                            if (err) {
                                                orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logWorkerOrderPaymentTime->Error: ${err.message}`);
                                            } else {
                                                orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logWorkerOrderPaymentTime->Result: ${result}`);
                                            }
                                        });
                                    }
                                }
                                pushApi.sendToWorkerRejectOrder(tenantId, orderId, workerCallsign, statusId, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. pushApi.sendToWorkerRejectOrder->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. pushApi.sendToWorkerRejectOrder->Result: ${result}`);
                                    }
                                });
                                redisWorkerManager.saveWorker(workerData, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. saveWorker->Error: ${err.message}`);
                                        return cb(null, 1);
                                    }
                                    orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. saveWorker->Result: ${result}`);
                                    sqlManager.logOrderChangeData(tenantId, orderId, 'status', workerCallsign, 'worker', workerStatusManager.free, 'system', (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logOrderChangeData->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. sqlManager.logOrderChangeData->Result: ${result}`);
                                        }
                                    });
                                    let messageString = orderEventWorkerMessage({
                                        command: orderEventWorkerCommand.rejectOrder,
                                        tenant_id: tenantId,
                                        tenant_login: tenantLogin,
                                        worker_callsign: workerCallsign,
                                        params: {
                                            order_id: orderId
                                        }
                                    });
                                    const messageTtl = 60 * 60 * 24 * 10; // 10 days
                                    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. amqpWorkerMessageSender.sendMessage->
                                        published command: ${orderEventWorkerCommand.rejectOrder} to worker: ${workerCallsign}. Result: ${result}`);
                                        }
                                    });
                                    if (needCloseShift) {
                                        try {
                                            orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. need close shift.`);
                                            closeWorkerShift(tenantId, tenantLogin, workerCallsign, workerShiftId, 'timer', 'system', nodeId, (err, result) => {
                                                if (err) {
                                                    orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. closeWorkerShift->Error: ${err.message}`);
                                                    orderLogger(orderId, 'error', err.message);
                                                }
                                                orderLogger(orderId, 'info', `orderStatusRejected->worker:${workerCallsign}. closeWorkerShift->Result: ${result}`);
                                            })
                                        } catch (err) {
                                            orderLogger(orderId, 'error', `orderStatusRejected->worker:${workerCallsign}. needCloseShift->Error: ${err.message}`);
                                        }
                                    }
                                    return cb(null, 1);
                                })
                            })
                        }, (err) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusRejected->async.each offeredWorkersList->Error: ${err.message}`);
                            }
                            return callback(null, 1);
                        })

                },
                updateOrderAtDb: callback => {
                    sqlManager.updateOrderFromMysql(orderId, statusId, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->updateOrderAtDb->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderStatusRejected->updateOrderAtDb->Result: ${result}`);
                        }
                        return callback(null, 1);
                    });
                },
                clearPromoCodeAtDb: callback => {
                    sqlManager.updateOrderField(orderId, 'promo_code_id', null, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->clearPromoCodeAtDb->updateOrderField->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderStatusRejected->clearPromoCodeAtDb->updateOrderField->Result: ${result}`);
                        }
                        return callback(null, 1);
                    });
                },
                deleteOrder: callback => {
                    redisOrderManager.deleteOrder(tenantId, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusRejected->redisOrderManager.deleteOrder->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusRejected->redisOrderManager.deleteOrder->Result: ${result}`);
                        return callback(null, 1);
                    })
                },
                saveOrderSettings: callback => {
                    OrderSettingsRepository.create(new OrderSettings({
                        tenant_id: tenantId,
                        order_id: orderId,
                        settings: orderInstance.settings
                    }))
                        .then(result => {
                            orderLogger(orderId, 'info', `orderStatusRejected->saveOrderSettings->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderStatusRejected->saveOrderSettings->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                updatePush: callback => {
                    if (lastStatusGroup === "pre_order") {
                        orderNotificationManager.sendUpdatePreOrdersListToWorkers(
                            orderData,
                            {
                                sendPushFreeOrderForAll: orderInstance.settings.SEND_PUSH_PRE_ORDER_FOR_ALL,
                                dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                                exceptCarModels: orderInstance.exceptCarModels
                            })
                            .then(result => {
                                orderLogger(orderId, 'info', `orderStatusRejected->orderNotificationManager.sendUpdatePreOrdersListToWorkers->Result: ${result}`);
                                return callback(null, 1);
                            })
                            .catch(err => {
                                orderLogger(orderId, 'error', `orderStatusRejected->orderNotificationManager.sendUpdatePreOrdersListToWorkers->Error: ${err.message}`);
                                return callback(null, 1);
                            });
                    } else if (lastStatusGroup === "new") {
                        orderNotificationManager.sendUpdateFreeOrdersListToWorkers(
                            orderData,
                            {
                                sendPushFreeOrderForAll: orderInstance.settings.SEND_PUSH_FREE_ORDER_FOR_ALL,
                                distanceToFilterFreeOrders: orderInstance.settings.DISTANCE_TO_FILTER_FREE_ORDERS,
                                dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                                exceptCarModels: orderInstance.exceptCarModels
                            })
                            .then(result => {
                                orderLogger(orderId, 'info', `orderStatusRejected->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Result: ${result}`);
                                return callback(null, 1);
                            })
                            .catch(err => {
                                orderLogger(orderId, 'error', `orderStatusRejected->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Error: ${err.message}`);
                                return callback(null, 1);
                            });
                    } else {
                        return callback(null, 1);
                    }

                }
            }, (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderStatusRejected->Error: ${err.message}`);
                    return reject(err);
                }
                orderLogger(orderId, 'info', `orderStatusRejected->Results of rejecting order:`);
                orderLogger(orderId, 'info', result);
                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.destroyService,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderStatusRejected->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderStatusRejected->Published command: ${orderEventOrderCommand.destroyService} to order: ${orderId}. Result: ${result}`);
                    }
                });
                orderInstance.orderTimer.clearAll();
                orderInstance.orderTimer = null;
                return resolve(1);
            })
        } catch (err) {
            console.log(err);
            return reject(err);
        }

    })
};