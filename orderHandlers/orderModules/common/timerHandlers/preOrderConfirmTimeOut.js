'use strict';

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const orderWorkerAssignManager = require("../orderWorkerAssignManager");
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;


/**
 * PreOrder confirmation timeout
 * @param orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'PreOrderConfirmTimeOut called!');
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderInstance.orderId, 'error', `preOrderConfirmTimeOut->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                const currentStatusId = parseInt(orderData.status_id);
                if (currentStatusId === statusInfo.waiting_for_preorder_confirmation.status_id) {
                    orderLogger(orderInstance.orderId, 'info', `preOrderConfirmTimeOut->need send pre order to work`);
                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder: true,
                        isWorkerEventOwner: true,
                        newStatusId: statusInfo.worker_ignored_preorder_confirmation.status_id
                    });
                    return resolve(true);
                } else {
                    orderLogger(orderInstance.orderId, 'info', `preOrderConfirmTimeOut->order status: ${orderData.status_id}. do not need send pre order to work`);
                    return resolve(false);
                }
            });
        } catch (err) {
            return reject(err);
        }
    })
};