"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const nodeId = config.MY_ID;
const mysql = require('mysql');
const async = require("async");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const NodeCache = require("node-cache");
const myCache = new NodeCache({stdTTL: 3600, checkperiod: 120});

const workerStatusManager = require("../../services/workerStatusManager");
/**
 * SqlDB Manager
 * @return {SqlManager}
 */
function SqlManager() {
    this.pool = mysql.createPool({
        host: config.DB_MAIN_HOST,
        user: config.DB_MAIN_USERNAME,
        password: config.DB_MAIN_PASSWORD,
        database: config.DB_MAIN_NAME,
        connectionLimit: parseInt(config.DB_MAIN_CONNECTION_LIMIT),
        connectTimeout: parseInt(config.DB_MAIN_CONNECTION_TIMEOUT),
        acquireTimeout: parseInt(config.DB_MAIN_ACQUIRE_TIMEOUT),
    });

    this.pool.on('connection', connection => {
        mainLogger('info', "Sql pool: connected!");
    });

    this.pool.on('enqueue', () => {
        mainLogger('info', "Sql pool: waiting for available connection slot");
    });

    this.pool.on('error', (err) => {
        mainLogger('error', `Sql pool: error: ${err.message}`);
    });

}

/**
 * Save order event at tbl_order_change_data
 * @param  {Number} tenantId
 * @param  {Number} orderId
 * @param  {String} changeField
 * @param  {Number} changeObjectId
 * @param  {String} changeObjectType
 * @param  {String} changeVal
 * @param  {String} changeSubject
 * @param  {Function} callback
 */
SqlManager.prototype.logOrderChangeData = function (tenantId, orderId, changeField, changeObjectId, changeObjectType, changeVal, changeSubject, callback) {
    const self = this;
    const nowTime = parseInt((new Date()).getTime() / 1000);
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const changeVals = [tenantId, orderId, changeField, changeObjectId, changeObjectType, changeVal, changeSubject, nowTime];
        connection.query('INSERT INTO tbl_order_change_data SET tenant_id = ?, order_id = ?, change_field = ?, change_object_id = ?, change_object_type = ?, change_val = ?, change_subject = ?, change_time = ?', changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows);
            });
    });
};


/**
 * Create worker block at sqldb - tbl_worker_block
 * @param  {Number} workerId
 * @param  {Number} shiftId
 * @param  {String} typeBlock
 * @param  {Number} endBlock
 * @param  {Function} callback
 */
SqlManager.prototype.logWorkerBlock = function (workerId, shiftId, typeBlock, endBlock, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const startBlock = parseInt((new Date()).getTime() / 1000);
        const changeVals = [workerId, shiftId, typeBlock, startBlock, endBlock, 0, process.pid, nodeId];
        connection.query('INSERT INTO `tbl_worker_block` SET worker_id = ?, shift_id = ?, type_block= ?, start_block = ?, end_block = ?, is_unblocked = ?, pid = ?, server_id = ?', changeVals,
            (err, result) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, result.insertId);
            });
    });
};
/**
 * Set unblock  true at tbl_worker_block
 * @param  {Number} blockId
 * @param  {Function} callback
 */
SqlManager.prototype.logWorkerUnblock = function (blockId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const endBlock = parseInt((new Date()).getTime() / 1000);
        const changeVals = [1, endBlock, blockId];
        connection.query('UPDATE `tbl_worker_block` SET is_unblocked = ?, end_block = ? where block_id = ?', changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows);
            });
    });
};
/**
 * Get order notice type
 * @param  {Object} params
 * @param  {number} params.tenantId
 * @param  {number} params.positionId
 * @param  {number} params.cityId
 * @param  {number} params.statusId
 * @param  {string} params.typeGroup
 * @param  {Function} callback
 */
SqlManager.prototype.getOrderNoticeType = function (params, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const searchVals = [params.tenantId, params.positionId, params.cityId, params.statusId, params.typeGroup];
        connection.query("SELECT `notice`  from `tbl_client_status_event` where " +
            "`tenant_id` = ? " +
            "and `position_id` = ? " +
            "and `city_id` = ? " +
            "and `status_id` = ? " +
            "and  `group` = ? ", searchVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows[0].notice);
            }
        );
    });
};

/**
 * Get tenant setting value by tenantId,cityId,settingName
 * @param  {Number} tenantId
 * @param  {Number} cityId
 * @param  {String} settingName
 * @param  {Number} positionId
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantSettingValue = function (tenantId, cityId, settingName, positionId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [settingName, tenantId, cityId, positionId];
        connection.query('SELECT `value` FROM `tbl_tenant_setting` where `name`= ? and `tenant_id`= ? and `city_id`= ? and `position_id` = ?', selectVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows[0].value);
            });
    });
};


/**
 * Get tenant settings list
 * @param  {Number} tenantId
 * @param  {Number} cityId
 * @param  {Number} positionId
 * @param  {Array} settings
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantSettingList = function (tenantId, cityId, positionId, settings, callback) {
    let settingsList = '"' + settings.join('","') + '"';
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [tenantId, cityId, positionId];
        connection.query('SELECT `name`,`value` FROM `tbl_tenant_setting` where `name` in (' + settingsList + ') and `tenant_id`= ? and `city_id`= ? and `position_id` = ?', selectVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows);
            });
    });
};


/**
 * Get all tenant ids
 * @param  {Function} resultCallback
 */
SqlManager.prototype.getAllTenantIds = function (resultCallback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return resultCallback(err);
        }
        connection.query("SELECT `tenant_id` FROM `tbl_tenant`", null,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return resultCallback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return resultCallback(null, []);
                }
                const tenantIds = [];
                async.each(rows, (tenantRow, callback) => {
                    let tenantId = tenantRow.tenant_id;
                    tenantIds.push(tenantId);
                    callback(null, 1);
                }, err => {
                    if (err) {
                        return resultCallback(null, []);
                    }
                    return resultCallback(null, tenantIds);
                });

            });
    });
};
/**
 * Get all active block
 * @param  {Function} callback
 */
SqlManager.prototype.getAllActiveBlocks = function (callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        connection.query("SELECT * FROM `tbl_worker_block` where is_unblocked=0", null, (err, workersBlocks) => {
            if (err) {
                connection.release();
                return callback(err);
            }
            connection.release();
            return callback(null, workersBlocks);
        });
    });
};
/**
 * Get  worker shift by id
 * @param {Number} shiftId
 * @param  {Function} callback
 */
SqlManager.prototype.getWorkerShiftById = function (shiftId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        connection.query("SELECT * FROM `tbl_worker_shift` where id=" + shiftId, null,
            (err, shiftData) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (shiftData.length === 0) {
                    return callback(null, null);
                }
                return callback(null, shiftData[0]);
            });
    });
};
/**
 * Get worker by id
 * @param {Number} workerId
 * @param  {Function} callback
 */
SqlManager.prototype.getWorkerById = function (workerId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        connection.query("SELECT * FROM `tbl_worker` where worker_id=" + workerId, null,
            (err, workerData) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (workerData.length === 0) {
                    return callback(null, null);
                }
                return callback(null, workerData[0]);
            });
    });
};


/**
 * Get array of workers by tenant_id and city_id
 * @param {Number} tenantId
 * @param {Number} cityId
 * @param  {Function} callback
 */
SqlManager.prototype.getWorkersInCity = function (tenantId, cityId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [tenantId, cityId];
        connection.query("SELECT * FROM `tbl_worker` where `tenant_id` = ? and `city_id` = ? and block = 0", selectVals,
            (err, workersArray) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (workersArray.length === 0) {
                    return callback(null, null);
                }
                return callback(null, workersArray);
            });
    });
};

// todo insert refuse worker
SqlManager.prototype.insertRefuseWorker = function(order_id, workeresRefuse){
    const self = this;


    let rezult = {};
    rezult.error = 0;
    rezult.sucess = 0;

    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }

            async.each(workeresRefuse, (workerRefuse, callback) => {

                let shift_id = typeof workerRefuse.shift_id !== undefined ? workerRefuse.shift_id : null;
                let refuse_time = typeof workerRefuse.refuse_time !== undefined ? workerRefuse.refuse_time : null;
                let refuse_subject = typeof workerRefuse.refuse_subject !== undefined ? workerRefuse.refuse_subject : null;
                let worker_id = typeof workerRefuse.worker_id !== undefined ? workerRefuse.worker_id : null;
                let worker_Callsign = typeof workerRefuse.workerCallsign !== undefined ? workerRefuse.workerCallsign : null;

                if(worker_Callsign === null){
                    callback();
                }


                if(worker_id <= 0 && worker_Callsign){
                    self.getWorkerByCallSign(worker_Callsign).then(worker => {
                        worker_id =  typeof worker !== undefined ? worker.worker_id : null ;

                        if (order_id  && refuse_time && refuse_subject && worker_id){
                            const insertVals = [shift_id, order_id, refuse_time, refuse_subject, worker_id ];
                            connection.query("INSERT into `tbl_worker_refuse_order` set shift_id = ?, order_id = ?, refuse_time = ?, refuse_subject = ? , worker_id = ?", insertVals, (err, rows) => {
                                if (err) {
                                    rezult.error += 1;
                                }
                                rezult.sucess +=1;
                                callback();
                            });
                        }else{
                            callback();
                        }
                    })
                    .catch(err => {
                        callback();
                    });
                }else{
                    if (order_id  && refuse_time && refuse_subject && worker_id){
                        const insertVals = [shift_id, order_id, refuse_time, refuse_subject,worker_id ];
                        connection.query("INSERT into `tbl_worker_refuse_order` set shift_id = ?, order_id = ?, refuse_time = ?, refuse_subject = ? , worker_id = ?", insertVals, (err, rows) => {
                            if (err) {
                                rezult.error += 1;
                            }
                            rezult.sucess +=1;
                            callback();
                        });
                    }else{
                        callback();
                    }
                }
            }, err => {
                if (err) {
                    connection.release();
                    return reject(err);
                }
                connection.release();
                return resolve(rezult);
            });
        });
    });
};

SqlManager.prototype.getWorkerByCallSign = function (workerCallSign) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }

            connection.query("SELECT * FROM `tbl_worker` where callsign=" + workerCallSign, null,
            (err, workerData) => {
                if (err) {
                    connection.release();
                    return reject(err);
                }
                connection.release();
                if (workerData.length === 0) {
                    return resolve(null);
                }
                return resolve(workerData[0]);
            });
        });
    });
};

//todo  daimond запрос на получение данных водителя
SqlManager.prototype.getInfoAsignWorkerForOrder = function (tenantId, worker_callsign, positionId, orderCity) {
    const self = this;
    return new Promise((resolve, reject) => {
    self.pool.getConnection((err, connection) => {
        if (err) {
            return reject(err);
        }
        let sqlQuery, selectVals;

            // TODO добавить - AND whc.last_active = 1
            sqlQuery = `
            SELECT
                w.worker_id,
                w.callsign,
                w.tenant_id,
                wp.position_id,
                whc.city_id
            FROM
                tbl_worker w
                
            JOIN tbl_worker_has_position wp ON w.worker_id = wp.worker_id
            JOIN tbl_worker_has_city whc ON whc.worker_id = w.worker_id
  
            WHERE
                w.callsign = ?
            AND w.tenant_id = ?            
            AND w.block = 0
            AND wp.position_id = ?
            AND whc.city_id = ?
            GROUP BY
                worker_id`;
            selectVals = [worker_callsign, tenantId, positionId,  orderCity];

        connection.query(sqlQuery, selectVals,
            (err, workersArray) => {
                if (err) {
                    connection.release();
                    return reject(err);
                }
                connection.release();
                if ((workersArray.length === 0)) {
                    return reject (new Error('Can not find this worker_id at sql_DB positionId  -' + positionId + ' orderCity  - ' + orderCity));
                }

                let rezult = workersArray[0];

                if(typeof  rezult.worker_id == "undefined"){
                    return reject (new Error('Can not find this worker_id at sql_DB positionId  -' + positionId + ' orderCity  - ' + orderCity ));
                }


                let workerDate = {};
                workerDate.worker = {};
                workerDate.position = {};
                workerDate.car =  {};

                workerDate.worker.worker_id =  parseInt(rezult.worker_id);
                workerDate.worker.callsign = parseInt(rezult.callsign);
                workerDate.worker.tenant_id = parseInt(rezult.tenant_id);
                workerDate.worker.city_id = parseInt(rezult.city_id);

                workerDate.worker.cities = [ workerDate.worker.city_id];
                workerDate.worker.status = workerStatusManager.free;
                workerDate.position.position_id = parseInt(positionId);
                workerDate.position.positions = {positionId};
                workerDate.position.has_car = false;

                workerDate.geo = {};
                workerDate.geo.lat = null;
                workerDate.geo.lon = null;
                return resolve( workerDate);
            });
    });
    })
};
// todo
SqlManager.prototype.getWorkerCarLastShift = function ( worker) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            let sqlQuery, selectVals;

            sqlQuery = `
            SELECT
                car.car_id,
                car.model
            FROM
                tbl_worker worker                
            JOIN  tbl_worker_has_position pos  ON pos.worker_id = worker.worker_id  
            JOIN  tbl_worker_has_car worHasCar  ON worHasCar.has_position_id = pos.id  
            JOIN  tbl_car car  ON car.car_id = worHasCar.car_id  
            WHERE
                worker.worker_id = ?
            ORDER BY car.car_id DESC
            LIMIT 0 , 1  `;

            selectVals = [worker.worker.worker_id];

            connection.query(sqlQuery, selectVals,
                (err, workersArray) => {
                    if (err) {
                        connection.release();
                        reject(err) ;
                    }
                    connection.release();

                    if (workersArray.length === 0) {
                        return reject (new Error('Can not find this worker_car and car_model in Shift'));
                    }
                    worker.car.id = workersArray[0].id;
                    worker.car.model_id = workersArray[0].model_id;
                    return resolve(worker );
                });
        });
    })
};



/**
 * Get worker who good for this order
 * @param tenantId
 * @param orderCityId
 * @param orderPositionId
 * @param orderTariffClassId
 * @param hasCar
 * @param callback
 */
SqlManager.prototype.getGoodWorkerArrForOrder = function (tenantId, orderCityId, orderPositionId, orderTariffClassId, hasCar, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        let sqlQuery, selectVals;
        if (hasCar) {
            sqlQuery = `
        select w.worker_id,w.callsign,whc.city_id,w.tenant_id,w.device,w.device_info,w.device_token,wp.position_id,
        c.car_id,c.class_id,co.class_id from tbl_worker w
        join tbl_worker_has_position wp on w.worker_id = wp.worker_id
        join  tbl_worker_has_car wc on wp.id = wc.has_position_id
        join tbl_car c on wc.car_id =c.car_id
        left join tbl_car_has_worker_group_class co on co.car_id = c.car_id
        join tbl_worker_has_city whc on whc.worker_id = w.worker_id
        where w.tenant_id = ?  and whc.city_id = ? and whc.last_active=1
        and w.block=0 and wp.position_id = ?
        and ((c.class_id = ? AND co.class_id IS NULL) OR (co.class_id = ?) )
        group by worker_id`;
            selectVals = [tenantId, orderCityId, orderPositionId, orderTariffClassId, orderTariffClassId];
        } else {
            sqlQuery = `
            SELECT
                w.worker_id,
                w.callsign,
                whc.city_id,
                w.tenant_id,
                w.device,
                w.device_info,
                w.device_token,
                wp.position_id
            FROM
                tbl_worker w
            JOIN tbl_worker_has_position wp ON w.worker_id = wp.worker_id
            JOIN tbl_worker_has_city whc ON whc.worker_id = w.worker_id
            WHERE
                w.tenant_id = ?
            AND whc.city_id = ?
            AND whc.last_active = 1
            AND w.block = 0
            AND wp.position_id = ?
            GROUP BY
                worker_id
            UNION
                SELECT
                    w.worker_id,
                    w.callsign,
                    whc.city_id,
                    w.tenant_id,
                    w.device,
                    w.device_info,
                    w.device_token,
                    wp.position_id
                FROM
                    tbl_worker w
                JOIN tbl_worker_has_city whc ON whc.worker_id = w.worker_id
                JOIN tbl_worker_has_position wp ON w.worker_id = wp.worker_id
                JOIN tbl_worker_group whg ON wp.group_id = whg.group_id
                JOIN tbl_worker_group_can_view_client_tariff wgct ON wgct.group_id = whg.group_id
                WHERE
                    w.tenant_id = ?
                AND whc.city_id = ?
                AND whc.last_active = 1
                AND w.block = 0
                AND wgct.class_id = ?
                GROUP BY
                    worker_id`;
            selectVals = [tenantId, orderCityId, orderPositionId, tenantId, orderCityId, orderPositionId];
        }
        connection.query(sqlQuery, selectVals,
            (err, workersArray) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (workersArray.length === 0) {
                    return callback(null, null);
                }
                return callback(null, workersArray);
            });
    });
};


/**
 * Get tenant domain by id
 * @param {Number} tenantId
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantDomainById = function (tenantId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        connection.query("SELECT `domain` FROM `tbl_tenant` where `tenant_id`=" + tenantId, null,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows[0].domain);
            });
    });
};

/**
 * Get tenant id by domain
 * @param {String} tenantDomain
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantIdByDomain = function (tenantDomain, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [tenantDomain];
        connection.query("SELECT `tenant_id` FROM `tbl_tenant` where `domain`= ?", selectVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows[0].tenant_id);
            });
    });
};


/**
 * Ger worker money balanse
 * @param  {Number} tenantId
 * @param  {Number} workerId
 * @param  {Number} currencyId
 * @param  {Function} callback
 */
SqlManager.prototype.gerWorkerMoneyBalanse = function (tenantId, workerId, currencyId, callback) {
    const self = this;
    const workerAccountKind = 2;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [tenantId, workerId, workerAccountKind, currencyId];
        connection.query('SELECT `balance` FROM `tbl_account` where `tenant_id`= ? and `owner_id`= ? and `acc_kind_id`= ? and `currency_id`= ?', selectVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, 0);
                }
                return callback(null, rows[0].balance);
            });
    });
};
/**
 * Update order from mysql
 * @param  {Number} orderId
 * @param  {Number} statusId
 * @param  {Number} workerId
 * @param  {Number} carId
 * @param  {Number} updateTime
 * @param  {Function} callback
 */
SqlManager.prototype.updateOrderFromMysql = function (orderId, statusId, workerId, carId, updateTime, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const nowTime = parseInt((new Date()).getTime() / 1000);
        const changeVals = [parseInt(statusId), workerId, carId, nowTime, updateTime, parseInt(orderId)];
        connection.query("UPDATE tbl_order SET status_id = ?, worker_id = ?, car_id = ?, status_time = ?, update_time = ? where order_id = ?", changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows);
            }
        );
    });
};

/**
 * Update order fieldupdateOrderField
 * @param  {Number} orderId
 * @param  {String} fieldLabel
 * @param   fieldVal
 * @param  {Function} callback
 */
SqlManager.prototype.updateOrderField = function (orderId, fieldLabel, fieldVal, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const nowTime = parseInt((new Date()).getTime() / 1000);
        const changeVals = [fieldVal, nowTime, parseInt(orderId)];
        connection.query("UPDATE tbl_order SET " + fieldLabel + " = ?, update_time = ? where order_id = ?", changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows);
            }
        );
    });
};

/**
 *
 * @param tenantId
 * @param orderNumber
 * @param callback
 */
SqlManager.prototype.getOrderByNumber = function (tenantId, orderNumber, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const changeVals = [parseInt(tenantId), parseInt(orderNumber)];
        connection.query("SELECT * from  tbl_order  where tenant_id = ? and order_number = ?", changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows[0]);
            }
        );
    });
};

/**
 * log end of worker shift
 * @param {Number} workerShiftId
 * @param {String} shiftEndReason
 * @param {String} shiftEndEventSenderType
 * @param {Number} shiftEndEventSenderId
 * @param {Function} callback
 */
SqlManager.prototype.logEndOfWorkerShift = function (workerShiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const nowTime = parseInt((new Date()).getTime() / 1000);
        const changeVals = [nowTime, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, workerShiftId];
        connection.query("UPDATE tbl_worker_shift SET end_work = ?, shift_end_reason = ?, shift_end_event_sender_type = ?, shift_end_event_sender_id =?  where id = ?", changeVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                return callback(null, rows);
            }
        );
    });
};

/**
 * Log worker late(to order point) time at current shift
 * @param {Number} workerShiftId
 * @param {Number} lateTimeSec
 * @param {Function} callback
 */
SqlManager.prototype.logWorkerLateTime = function (workerShiftId, lateTimeSec, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newWorkerLateTime = shiftData.worker_late_time + lateTimeSec;
            const newWorkerLateCount = shiftData.worker_late_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newWorkerLateTime, newWorkerLateCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET worker_late_time = ?, worker_late_count = ?  where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        return callback(null, rows);
                    }
                );

            });
        } catch (err) {
            return callback(err);
        }
    });
};

/**
 * log worker completed order counter at current shift
 * @param workerShiftId
 * @param orderId
 * @param callback
 */
SqlManager.prototype.logWorkerCompletedOrderCounter = function (workerShiftId, orderId, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newCompletedOrderCount = shiftData.completed_order_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newCompletedOrderCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET completed_order_count = ? where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        const nowTimestamp = parseInt((new Date()).getTime() / 1000);
                        const insertVals = [workerShiftId, orderId, nowTimestamp, nowTimestamp];
                        connection.query("INSERT into `tbl_worker_shift_order` set shift_id = ?, order_id = ?, created_at = ?, updated_at = ?", insertVals, (err, rows) => {
                            if (err) {
                                connection.release();
                                return callback(err);
                            }
                            connection.release();
                            return callback(null, rows);

                        });

                    }
                );

            });
        } catch (err) {
            return callback(err);
        }
    });
};
/**
 * log worker rejected order counter at current shift
 * @param workerShiftId
 * @param orderId
 * @param callback
 */
SqlManager.prototype.logWorkerRejectedOrderCounter = function (workerShiftId, orderId, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newRejectedOrderCount = shiftData.rejected_order_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newRejectedOrderCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET rejected_order_count = ? where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        const nowTimestamp = parseInt((new Date()).getTime() / 1000);
                        const insertVals = [workerShiftId, orderId, nowTimestamp, nowTimestamp];
                        connection.query("INSERT into `tbl_worker_shift_order` set shift_id = ?, order_id = ?, created_at = ?, updated_at = ?", insertVals, (err, rows) => {
                            if (err) {
                                connection.release();
                                return callback(err);
                            }
                            connection.release();
                            return callback(null, rows);

                        });
                    }
                );
            });
        } catch (err) {
            return callback(err);
        }
    });
};

/**
 * log worker accepted order offer count at current shift
 * @param workerShiftId
 * @param callback
 */
SqlManager.prototype.logWorkerAcceptedOrderOfferCount = function (workerShiftId, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newAcceptedOrderOfferCount = shiftData.accepted_order_offer_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newAcceptedOrderOfferCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET accepted_order_offer_count = ? where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        return callback(null, rows);
                    }
                );
            });
        } catch (err) {
            return callback(err);
        }
    });
};

/**
 * log worker rejected order offer count at current shift
 * @param workerShiftId
 * @param callback
 */
SqlManager.prototype.logWorkerRejectedOrderOfferCount = function (workerShiftId, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newRejectedOrderOfferCount = shiftData.rejected_order_offer_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newRejectedOrderOfferCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET rejected_order_offer_count = ? where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        return callback(null, rows);
                    }
                );
            });
        } catch (err) {
            return callback(err);
        }
    });
};

/**
 * Log worker order payment time time at current shift
 * @param {Number} workerShiftId
 * @param {Number} orderPaymentTimeSec
 * @param {Function} callback
 */
SqlManager.prototype.logWorkerOrderPaymentTime = function (workerShiftId, orderPaymentTimeSec, callback) {
    const self = this;
    self.getWorkerShiftById(workerShiftId, (err, shiftData) => {
        if (err) {
            return callback(err);
        }
        try {
            const newWorkerOrderPaymentTime = shiftData.order_payment_time + orderPaymentTimeSec;
            const newWorkerOrderPaymentCount = shiftData.order_payment_count + 1;
            self.pool.getConnection((err, connection) => {
                if (err) {
                    return callback(err);
                }
                const changeVals = [newWorkerOrderPaymentTime, newWorkerOrderPaymentCount, workerShiftId];
                connection.query("UPDATE tbl_worker_shift SET order_payment_time = ?, order_payment_count = ?  where id = ?", changeVals,
                    (err, rows) => {
                        if (err) {
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        return callback(null, rows);
                    }
                );

            });
        } catch (err) {
            return callback(err);
        }
    });
};

/**
 * Get parking polygon by type
 * @param tenantId
 * @param cityId
 * @param parkingType
 * @param callback
 */
SqlManager.prototype.getParkingPolygon = function (tenantId, cityId, parkingType, callback) {
    const self = this;
    const cacheKey = `parking_${tenantId}_${cityId}_${parkingType}`;
    myCache.get(cacheKey, (err, formattedParkingCoords) => {
        if (err) {
            return callback(err);
        }
        if (formattedParkingCoords) {
            return callback(null, formattedParkingCoords);
        }
        self.pool.getConnection((err, connection) => {
            if (err) {
                return callback(err);
            }
            const selectVals = [tenantId, cityId, parkingType];
            connection.query("select `polygon` from `tbl_parking` where `tenant_id` = ? and `city_id` = ? and `type` = ?", selectVals,
                (err, rows) => {
                    if (err) {
                        connection.release();
                        return callback(err);
                    }
                    connection.release();
                    if (rows.length === 0) {
                        return callback(null, null);
                    }
                    try {
                        let parkingPolygon = rows[0].polygon;
                        parkingPolygon = JSON.parse(parkingPolygon);
                        const parkingCoords = parkingPolygon.geometry.coordinates[0];
                        if (Array.isArray(parkingCoords)) {
                            const formattedParkingCoords = parkingCoords.map(function (coords) {
                                return {
                                    latitude: coords[1],
                                    longitude: coords[0],
                                }
                            });
                            myCache.set(cacheKey, formattedParkingCoords, function (err, success) {
                                if (!err && success) {
                                    mainLogger('info', `${cacheKey} stored in cache succesfull`);
                                } else {
                                    mainLogger('error', `${cacheKey} NOT stored in cache`);
                                    mainLogger('error', err);
                                }
                            });
                            return callback(null, formattedParkingCoords);
                        }
                        return callback(new Error('Parking coords are not array'));
                    } catch (err) {
                        return callback(err);
                    }
                }
            );
        });

    });

};


/**
 * if order has promocode with type="worker",
 * method will return worker id
 * @param {Number} promoCodeId
 * @param callback
 */
SqlManager.prototype.getPromoCodeWorkerId = function (promoCodeId, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        connection.query("SELECT * FROM `tbl_promo_code` where code_id=" + promoCodeId, null,
            (err, promoCode) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (promoCode.length === 0) {
                    return callback(null, null);
                }
                return callback(null, promoCode[0].worker_id);
            });
    });
};

/**
 * Save external worker, which processed order
 * @param {number} orderId
 * @param {object} workerData
 * @param {string} workerData.name
 * @param {string} workerData.phone
 * @param {string} workerData.car_description
 * @param {string} workerData.color
 * @param {string} workerData.gos_number
 * @param {string} workerData.external_car_id
 * @param {string} workerData.external_worker_id
 * @returns {Promise<any>}
 */
SqlManager.prototype.saveOrderExternalWorker = function (orderId, workerData) {
    const self = this;
    const nowTime = parseInt((new Date()).getTime() / 1000);
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const insertVals = [
                orderId,
                workerData.name,
                workerData.phone,
                workerData.car_description,
                workerData.color,
                workerData.gos_number,
                workerData.external_car_id,
                workerData.external_worker_id,
                nowTime,
                nowTime
            ];
            connection.query("INSERT INTO `tbl_order_has_exchange_worker` set " +
                "`order_id`= ?, " +
                "`name` = ?, " +
                "`phone` = ?, " +
                "`car_description` = ?, " +
                "`color` = ?, " +
                "`gos_number` = ?, " +
                "`external_car_id` = ?, " +
                "`external_worker_id` = ?, " +
                "`created_at` = ?, " +
                "`updated_at` = ? "
                , insertVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        resolve(result);
                    }
                });
        })
    })
};

/**
 * Get order detail cost
 * @param orderId
 * @return {Promise<array>}
 */
SqlManager.prototype.getOrderDetailCost = function (orderId) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            connection.query("SELECT * FROM `tbl_order_detail_cost` where order_id= ?", [orderId],
                (err, orderDetailCost) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    if (orderDetailCost.length === 0) {
                        return resolve();
                    }
                    return resolve(orderDetailCost[0]);
                });
        });
    });
};

/**
 * Get currency by id
 * @param currencyId
 * @returns {Promise<array>}
 */
SqlManager.prototype.getCurrencyById = function (currencyId) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            connection.query("SELECT * FROM `tbl_currency` where currency_id= ?", [currencyId],
                (err, currency) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    if (currency.length === 0) {
                        return resolve();
                    }
                    return resolve(currency[0]);
                });
        });
    });
};

/**
 * Get order except car models
 * @param orderId
 * @returns {Promise<array>}
 */
SqlManager.prototype.getOrderExceptCarModels = function (orderId) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            connection.query("SELECT car_model_id FROM `tbl_order_except_car_models` where order_id= ?", [orderId],
                (err, carModels) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    if (carModels.length === 0) {
                        return resolve();
                    }
                    return resolve(carModels);
                });
        });
    });
};

/**
 * Get client dislike workers
 * @param clientId
 * @param positionId
 * @returns {Promise<array>}
 */
SqlManager.prototype.getClientDislikeWorkers = function (clientId, positionId) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            connection.query("SELECT worker_id FROM `tbl_client_worker_like` " +
                "where client_id = ? and position_id = ? and dislike_count > 0", [clientId, positionId],
                (err, dislikeWorkers) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    if (dislikeWorkers.length === 0) {
                        return resolve();
                    }
                    return resolve(dislikeWorkers);
                });
        });
    });
};

/**
 * Check insert to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkInsert = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const insertVals = [key, value];
            connection.query("INSERT INTO `tbl_check` set `key` = ?, `value` = ?", insertVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    return resolve();
                });
        });
    });
};


/**
 * Check select from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkSelect = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const selectVals = [key];
            connection.query("SELECT * from `tbl_check` where `key` = ?", selectVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    return resolve(result);
                });
        });
    });
};

/**
 * Check update to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkUpdate = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [value, key];
            connection.query('UPDATE  `tbl_check` SET `value` = ? where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    return resolve();
                });
        });
    });
};

/**
 * Check delete from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkDelete = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [key];
            connection.query('DELETE from `tbl_check` where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    return resolve();
                });
        });
    });
};

module.exports = exports = new SqlManager();