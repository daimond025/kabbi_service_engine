'use strict';

const orderLogger = require('../../../../services/logger').orderLogger;
const preOrderWatcher = require("../preOrderWatcher");

/**
 * Order status refuse preorder confirmation
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'preOrderStatusRefuseConfirmation called!');
            preOrderWatcher.sendPreOrderToWork(orderData)
                .then(result => {
                    orderLogger(orderInstance.orderId, 'info', `preOrderStatusRefuseConfirmation->_sendPreOrderToWork->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderInstance.orderId, 'err', `preOrderStatusRefuseConfirmation->_sendPreOrderToWork->Error: ${err.message}`);
                });
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};