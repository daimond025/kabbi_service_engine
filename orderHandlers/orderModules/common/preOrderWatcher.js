"use strict";

const redisOrderManager = require('../../../database/redisDB/redisOrderManager');
const statusInfo = require('../../../services/orderStatusManager').statusInfo;
const sqlManager = require('../../../database/sqlDB/sqlManager');
const amqpOrderMessageSender = require("../../../services/amqpOrderMessageSender");
const amqpWorkerMessageSender = require("../../../services/amqpWorkerMessageSender");
const logger = require('../../../services/logger');
const orderLogger = logger.orderLogger;
const orderEventOrderMessage = require('../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../orderEvent/orderEventWorkerCommand');
const redisWorkerManager = require('../../../database/redisDB/redisWorkerManager');
const PHPUnserialize = require("php-unserialize");
const pushApi = require("../../../services/externalWebServices/pushApi");
const orderWorkerAssignManager = require("./orderWorkerAssignManager");

const workerStatusManager = require("../../../services/workerStatusManager");
const orderType = require("../../orderEvent/orderDevice");
const redisWorkerReservedOrderListManager = require('../../../database/redisDB/redisWorkerReservedOrderListManager');

/**
 * @class PreOrderWatcher
 */
class PreOrderWatcher {
    /**
     * @constructor
     */
    constructor() {
    }

    /**
     * Run pre order watcher
     * @param {BaseGootaxOrder} orderInstance
     * @param {Object} orderData
     */
    runWatcher(orderInstance, orderData) {
        orderLogger(orderData.order_id, 'info', "preOrderWatcher->runWatcher called!");
        const tenantId = parseInt(orderData.tenant_id);
        const orderId = parseInt(orderData.order_id);
        const cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
        const orderTime = parseInt(orderData.order_time);
        let preOrderTimeToWork = orderInstance.settings.PRE_ORDER_WORKING;
        let preOrderTimeToStart = typeof(orderInstance.settings.START_TIME_BY_ORDER != undefined) ? parseFloat(orderInstance.settings.START_TIME_BY_ORDER) : preOrderTimeToWork;
        let preOrderTimeForRemind = orderInstance.settings.WORKER_PRE_ORDER_REMINDER;
        let orderDevice = orderData.device.toString();

        if (!preOrderTimeToWork) {
            orderLogger(orderData.order_id, 'error', `preOrderWatcher->runWatcher->bad preOrderTimeToWork value:${preOrderTimeToWork}`);
        } else {
            this._createPreOrderSenderToWork({
                    tenantId,
                    orderId,
                    orderTime,
                    preOrderTimeToWork,
                    preOrderTimeToStart,
                    cityTimeOffset,
                    orderDevice
                },
                orderInstance.orderTimer)
                .then(result => {
                    orderLogger(orderId, 'info', `preOrderWatcher->runWatcher->_createPreOrderSenderToWork->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderId, 'error', `preOrderWatcher->runWatcher->_createPreOrderSenderToWork->Error: ${err.message}`);
                });
        }

        if (!preOrderTimeForRemind) {
            orderLogger(orderData.order_id, 'error', `preOrderWatcher->runWatcher->bad preOrderTimeForRemind value:${preOrderTimeForRemind}`);
        } else {
            try {
                preOrderTimeForRemind = PHPUnserialize.unserialize(preOrderTimeForRemind);
                if (typeof preOrderTimeForRemind === "object" && preOrderTimeForRemind !== null) {
                    const preOrderTimeForRemindList = Object.keys(preOrderTimeForRemind).map(k => preOrderTimeForRemind[k]);
                    if (preOrderTimeForRemindList.length === 0) {
                        orderLogger(orderId, 'info', "preOrderWatcher->runWatcher->don't need send worker preorder reminder");
                    } else {
                        let reminderCounter = 1;

                        for (let timePreRemind of preOrderTimeForRemindList) {
                            timePreRemind = !isNaN(parseFloat(timePreRemind)) ? parseFloat(timePreRemind) : null;
                            if (timePreRemind) {
                                this._createPreOrderWorkerNotificationReminder({
                                    tenantId,
                                    orderId,
                                    orderTime,
                                    timePreRemind,
                                    cityTimeOffset,
                                    reminderCounter
                                }, orderInstance.orderTimer);
                                reminderCounter++;
                            }
                        }
                    }
                } else {
                    orderLogger(orderData.order_id, 'error', `preOrderWatcher->runWatcher->bad preOrderTimeForRemind type:${preOrderTimeForRemind}`);
                }
            } catch (err) {
                orderLogger(orderData.order_id, 'error', `preOrderWatcher->runWatcher->preOrderTimeForRemind Error: ${err.message}`);
            }
        }
    }

    /**
     * Create pre order sender to work
     * @param {object} options
     * param {number} options.tenantId
     * param {number} options.orderId
     * param {number} options.orderTime
     * param {number} options.preOrderTimeToWork
     * param {number} options.cityTimeOffset
     * @param {object} orderTimer
     * @return {Promise}
     * @private
     */
    _createPreOrderSenderToWork(options, orderTimer) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(options.orderId, 'info', "preOrderWatcher->_createPreOrderSenderToWork called!");
                const timeOfNotification = options.orderTime - (options.preOrderTimeToWork * 60);
                const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(options.cityTimeOffset);
                let startTime = timeOfNotification - tenantNowTime;
                startTime = startTime > 0 ? startTime : 0;
                orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->preOrder sender to work will be called after ${startTime / 60} minutes`);

                /*console.log('Ручное назначение заказа сработает через !');
                startTime = 20; console.log(startTime);*/

                // timer for permit start pre order
                const timeOfPermitStart = options.orderTime - (options.preOrderTimeToStart * 60);
                let permitStartTimer = timeOfPermitStart - tenantNowTime;
                permitStartTimer = permitStartTimer > 0 ? permitStartTimer : 0;
                orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->preOrder worker permit be start after ${permitStartTimer / 60} minutes`);

                /*console.log('Ручное назначение заказа сработает через 2!');
                permitStartTimer = 10;
                console.log(permitStartTimer);*/

                orderTimer.setServiceTimeout(orderTimer.preOrderPermitStart, () => {

                    this.permitPreOrderEdit(options.tenantId, options.orderId)
                    .then(result => {
                        orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Error: ${err.message}`);
                    });

                }, permitStartTimer * 1000);

                // todo предлагается разделить логику на две функции  (одна для больничных  вторая  старая  ) - удалить страрую версию d
                if(options.orderDevice == orderType.HOSPITAL ){

                    orderTimer.setServiceTimeout(orderTimer.preOrderSenderToWorkLabel, () => {
                        try{
                            orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->start sending hospital preOrder to work...`);
                            redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                                if(err){
                                    orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->getOrder->Error: ${err.message}`);
                                    return;
                                }
                                orderLogger(options.orderId, 'info', `preOrderSenderToWork->_createPreOrderSenderToWork->Hospital->getOrder->Result: ${orderData}`);

                                const currentStatusId = parseInt(orderData.status_id);
                                const tenantId = orderData.tenant_id;
                                const orderId = orderData.order_id;

                                let companyID = null;
                                if((typeof orderData.worker_company == "object") && ( orderData.worker_company != null) && (typeof orderData.worker_company.tenant_company_id != "undefined")){
                                    companyID = parseInt(orderData.worker_company.tenant_company_id );
                                }
                                if(companyID == null){
                                    // больничный заказ - передача в  статус свободный для всех
                                    this.preOrderToHospital(orderData)
                                    .then(result => {
                                        orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->_preOrderToHospital->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->_preOrderToHospital->Error: ${err.message}`);
                                    });
                                }
                                else if(companyID != null){

                                    orderLogger(options.orderId, 'info', "preOrderSenderToWork->_createPreOrderSenderToWork->getOrder->Hospital->Order has status: worker_accepted_preorder. Need check state of worker.");
                                    if(orderData && orderData.worker && orderData.worker.callsign){

                                        const workerCallsign = orderData.worker.callsign;
                                        const workerId = orderData.worker_id;
                                        const carId = orderData.car_id;
                                        const tenantLogin = orderData.tenant_login;

                                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                                            if(err || !workerData){
                                                orderLogger(orderId, 'info', "preOrderSenderToWork->_createPreOrderSenderToWork->getOrder->Hospital->Worker is not on shift, need send preorder to work");

                                                this.preOrderToHospital(orderData)
                                                .then(result => {
                                                    orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->preOrderToHospital->Result: ${result}`);
                                                })
                                                .catch(err => {
                                                    orderLogger(orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->preOrderToHospital->Error: ${err.message}`);
                                                });
                                            }else{
                                                // если водитель занят - водителю заказ предложиться после выполнения текущего
                                                orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->Worker assign to order ...`);

                                                const updateTime = Math.floor(Date.now() / 1000);
                                                orderData.status_id = statusInfo.worker_assigned_at_order_soft.status_id;
                                                orderData.status.status_id = statusInfo.worker_assigned_at_order_soft.status_id;
                                                orderData.status.name = statusInfo.worker_assigned_at_order_soft.name;
                                                orderData.status.status_group = statusInfo.worker_assigned_at_order_soft.status_group;
                                                orderData.status_time = updateTime;
                                                orderData.update_time = updateTime;

                                                redisOrderManager.saveOrder(orderData, (err, result) => {
                                                    if(err){
                                                        orderLogger(orderId, "error", `preOrderWatcher->_createPreOrderSenderToWork->Hospital->redisOrderManager.saveOrder->Hospital->Error: ${err.message}`);
                                                    }else{
                                                        orderLogger(orderId, "info", `preOrderWatcher->_createPreOrderSenderToWork->Hospital->redisOrderManager.saveOrder->Hospital->Result: ${result}`);

                                                        sqlManager.updateOrderFromMysql(orderId, orderData.status_id, workerId, carId, updateTime, (err, result) => {
                                                            if(err){
                                                                orderLogger(orderId, "error", `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(orderId, "info", `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                                                            }
                                                        });
                                                        sqlManager.updateOrderField(orderId, 'deny_refuse_order', 0, (err, result) => {
                                                            if(err){
                                                                orderLogger(orderId, 'error', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sqlManager.updateOrderField->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sqlManager.updateOrderField->Result: ${result}`);
                                                            }
                                                        });

                                                        const messageString = orderEventOrderMessage({
                                                            command: orderEventOrderCommand.updateOrderData,
                                                            tenant_id: tenantId,
                                                            order_id: orderId,
                                                            params: {}
                                                        });
                                                        amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                                                            if(err){
                                                                orderLogger(orderId, 'error', `preOrderWatcher->Hospital->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(orderId, 'info', `preOrderWatcher->Hospital->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.updateOrderData} to order: ${options.orderId}. Result: ${result}`);
                                                            }
                                                        });


                                                        const workerStatus = workerData.worker.status;
                                                        let active_orders = typeof workerData.worker.active_orders === "object" ?
                                                            Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])) : [];

                                                        // если заказ больничный -  предлагаем водителю если он свободен
                                                        if(workerStatus == workerStatusManager.free || active_orders.length == 0){

                                                            const isPreOrder = 'preorder';
                                                            const offer_sec = (typeof orderData.settings.PRE_ORDER_OFFER_SEC !== undefined) ? parseInt(orderData.settings.PRE_ORDER_OFFER_SEC) : 60;
                                                            let workerMessageString = orderEventWorkerMessage({
                                                                command: orderEventWorkerCommand.preOrderOffer,
                                                                tenant_id: tenantId,
                                                                tenant_login: tenantLogin,
                                                                worker_callsign: workerCallsign,
                                                                params: {
                                                                    offer_sec: offer_sec,
                                                                    order_id: orderId,
                                                                    type: isPreOrder
                                                                }
                                                            });
                                                            const messageTtl = 60 * 60 * 24 * 10;
                                                            amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, workerMessageString, messageTtl, (err, result) => {
                                                                if(err){
                                                                    orderLogger(orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sendMessage>to worker ${workerCallsign}->Error: ${err.message}`);
                                                                }else{
                                                                    orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->sendMessage->Result: ${result}`);
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        })
                                    }else{
                                        this.preOrderToHospital(orderData)
                                        .then(result => {
                                            orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->_preOrderToHospital->Result: ${result}`);
                                        })
                                        .catch(err => {
                                            orderLogger(orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->_preOrderToHospital->Error: ${err.message}`);
                                        });
                                    }
                                }else{
                                    orderLogger(orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Hospital->Don't need send preOrder to work. Order status_id: ${currentStatusId}`);
                                }
                                // permit edit order
                                if( (typeof orderData.edit !== undefined) && orderData.edit !== 0){
                                    this.permitPreOrderEdit(tenantId, orderId)
                                    .then(result => {
                                        orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Error: ${err.message}`);
                                    });
                                }
                            })
                        }catch (err) {
                            throw new Error(`preOrderWatcher->_createPreOrderSenderToWork->Hospital->Error: ${err.message}`);
                        }
                    }, startTime * 1000);
                }
                else{
                    orderTimer.setServiceTimeout(orderTimer.preOrderSenderToWorkLabel, () => {

                        try{
                            orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->start sending preOrder to work...`);

                            redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                                if(err){
                                    orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->getOrder->Error: ${err.message}`);
                                    return;
                                }
                                orderLogger(options.orderId, 'info', `preOrderSenderToWork->_createPreOrderSenderToWork->getOrder->Result: ${orderData}`);

                                const currentStatusId = parseInt(orderData.status_id);
                                if(currentStatusId === statusInfo.new_pre_order.status_id
                                    || currentStatusId === statusInfo.new_pre_order_no_parking.status_id
                                    || currentStatusId === statusInfo.worker_refused_preorder.status_id)
                                {
                                    this.sendPreOrderToWork(orderData)
                                    .then(result => {
                                        orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->_sendPreOrderToWork->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->_sendPreOrderToWork->Error: ${err.message}`);
                                    });
                                }else if(currentStatusId === statusInfo.worker_accepted_preorder.status_id
                                    || currentStatusId === statusInfo.worker_assigned_at_preorder_hard.status_id
                                    || currentStatusId === statusInfo.worker_assigned_at_preorder_soft.status_id){
                                    orderLogger(options.orderId, 'info', "preOrderSenderToWork->_createPreOrderSenderToWork->getOrder->Order has status: worker_accepted_preorder. Need check state of worker.");
                                    if(orderData && orderData.worker && orderData.worker.callsign){

                                        const workerCallsign = orderData.worker.callsign;
                                        const workerId = orderData.worker_id;
                                        const carId = orderData.car_id;
                                        const tenantLogin = orderData.tenant_login;

                                        redisWorkerManager.getWorker(options.tenantId, workerCallsign, (err, workerData) => {
                                            if(err || !workerData){
                                                orderLogger(options.orderId, 'info', "preOrderSenderToWork->_createPreOrderSenderToWork->getOrder->Worker is not on shift, need send preorder to work");
                                                this.sendPreOrderToWork(orderData)
                                                .then(result => {
                                                    orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->sendPreOrderToWork->Result: ${result}`);
                                                })
                                                .catch(err => {
                                                    orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->sendPreOrderToWork->Error: ${err.message}`);
                                                });
                                            }else{
                                                orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Sending preorder confirmation to worker...`);

                                                //set status: waiting_for_preorder_confirmation, add new timer to call after 2 min preorder_confirm_timeout event:
                                                const updateTime = Math.floor(Date.now() / 1000);
                                                orderData.status_id = statusInfo.waiting_for_preorder_confirmation.status_id;
                                                orderData.status.status_id = statusInfo.waiting_for_preorder_confirmation.status_id;
                                                orderData.status.name = statusInfo.waiting_for_preorder_confirmation.name;
                                                orderData.status.status_group = statusInfo.waiting_for_preorder_confirmation.status_group;
                                                orderData.status_time = updateTime;
                                                orderData.update_time = updateTime;
                                                redisOrderManager.saveOrder(orderData, (err, result) => {
                                                    if(err){
                                                        orderLogger(options.orderId, "error", `preOrderWatcher->_createPreOrderSenderToWork->redisOrderManager.saveOrder->Error: ${err.message}`);
                                                    }else{
                                                        orderLogger(options.orderId, "info", `preOrderWatcher->_createPreOrderSenderToWork->redisOrderManager.saveOrder->Result: ${result}`);
                                                        sqlManager.updateOrderFromMysql(options.orderId, orderData.status_id, workerId, carId, updateTime, (err, result) => {
                                                            if(err){
                                                                orderLogger(options.orderId, "error", `preOrderWatcher->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(options.orderId, "info", `preOrderWatcher-->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                                                            }
                                                        });
                                                        sqlManager.updateOrderField(options.orderId, 'deny_refuse_order', 0, (err, result) => {
                                                            if(err){
                                                                orderLogger(options.orderId, 'error', `preOrderWatcher->sqlManager.updateOrderField->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(options.orderId, 'info', `preOrderWatcher->sqlManager.updateOrderField->Result: ${result}`);
                                                            }
                                                        });

                                                        const messageString = orderEventOrderMessage({
                                                            command: orderEventOrderCommand.updateOrderData,
                                                            tenant_id: options.tenantId,
                                                            order_id: options.orderId,
                                                            params: {}
                                                        });
                                                        amqpOrderMessageSender.sendMessage(options.orderId, messageString, (err, result) => {
                                                            if(err){
                                                                orderLogger(options.orderId, 'error', `preOrderWatcher->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(options.orderId, 'info', `preOrderWatcher->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.updateOrderData} to order: ${options.orderId}. Result: ${result}`);
                                                            }
                                                        });
                                                        const confirmTimeout = 120;
                                                        const workerMessageString = orderEventWorkerMessage({
                                                            command: orderEventWorkerCommand.confirmPreOrder,
                                                            tenant_id: options.tenantId,
                                                            tenant_login: tenantLogin,
                                                            worker_callsign: workerCallsign,
                                                            params: {
                                                                confirm_sec: confirmTimeout,
                                                                order_id: options.orderId
                                                            }
                                                        });
                                                        amqpWorkerMessageSender.sendMessage(options.tenantId, workerCallsign, workerMessageString, confirmTimeout, (err, result) => {
                                                            if(err){
                                                                orderLogger(options.orderId, "error", `preOrderWatcher->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(options.orderId, 'info', `preOrderWatcher->published command: ${orderEventWorkerCommand.confirmPreOrder} to worker: ${workerCallsign}. Result: ${result} `);
                                                            }
                                                        });

                                                    }
                                                });

                                            }
                                        })
                                    }else{
                                        this.sendPreOrderToWork(orderData)
                                        .then(result => {
                                            orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->sendPreOrderToWork->Result: ${result}`);
                                        })
                                        .catch(err => {
                                            orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->sendPreOrderToWork->Error: ${err.message}`);
                                        });
                                    }
                                }else{
                                    orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->Don't need send preOrder to work. Order status_id: ${currentStatusId}`);
                                }
                                // permit edit order
                                if( (typeof orderData.edit !== undefined) && orderData.edit !== 0){
                                    this.permitPreOrderEdit(options.tenantId, options.orderId)
                                    .then(result => {
                                        orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(options.orderId, 'err', `preOrderWatcher->_createPreOrderSenderToWork->permitPreOrderEdit->Error: ${err.message}`);
                                    });
                                }
                            });
                        }catch(err){
                            throw new Error(`preOrderWatcher->_createPreOrderSenderToWork->Hospital->Error: ${err.message}`);
                        }
                    }, startTime * 1000);
                }
                return resolve(1);
            } catch (err) {
                orderLogger(options.orderId, 'error', `preOrderWatcher->_createPreOrderSenderToWork->Error: ${err.message}`);
                return reject(err);
            }
        });
    }

    /**
     * Create pre order worker notification reminder
     * @param {object} options
     * @param {number} options.tenantId
     * @param {number} options.orderId
     * @param {number} options.orderTime
     * @param {number} options.timePreRemind
     * @param {number} options.cityTimeOffset
     * @param {number} options.reminderCounter
     * @param {object} orderTimer
     * @private
     */
    _createPreOrderWorkerNotificationReminder(options, orderTimer) {
        try {
            orderLogger(options.orderId, 'info', "preOrderWatcher->_createPreOrderWorkerNotificationReminder called!");
            const timeOfNotification = parseInt(options.orderTime) - (parseInt(options.timePreRemind) * 60);
            const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(options.cityTimeOffset);
            let startTime = timeOfNotification - tenantNowTime;
            startTime = startTime > 0 ? startTime : 0;
            let serviceTimoutLabel;
            switch (options.reminderCounter) {
                case 1:
                    serviceTimoutLabel = orderTimer.preOrderFirstReminderLabel;
                    break;
                case 2:
                    serviceTimoutLabel = orderTimer.preOrderSecondReminderLabel;
                    break;
                case 3:
                    serviceTimoutLabel = orderTimer.preOrderThirdReminderLabel;
                    break;
                default:
                    break;
            }


            if (serviceTimoutLabel) {
                orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->preorder reminder will be called after ${startTime / 60} minutes`);
                orderTimer.setServiceTimeout(serviceTimoutLabel, () => {
                    orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->Start sending preorder reminder...`);
                    redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                        if (err) {
                            orderLogger(options.orderId, 'error', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->getOrder->Error: ${err.message}`);
                            return;
                        }
                        if (orderData.worker && orderData.worker.callsign
                            && (parseInt(orderData.status_id) === statusInfo.worker_accepted_preorder.status_id
                            || parseInt(orderData.status_id) === statusInfo.worker_assigned_at_preorder_soft.status_id
                            || parseInt(orderData.status_id) === statusInfo.worker_assigned_at_preorder_hard.status_id)) {
                            pushApi.sendToWorkerRemindPreOrder(options.tenantId, options.orderId, orderData.worker.callsign, orderData.order_number, options.timePreRemind, (err, result) => {
                                if (err) {
                                    orderLogger(options.orderId, 'error', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->sendToWorkerRemindPreOrder->Error: ${err.message}`);
                                }
                                orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->pushApi.sendToWorkerRemindPreOrder->Result: ${result}`);
                            })
                        } else {
                            orderLogger(options.orderId, 'info', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->Don't need send preorder reminder`);
                        }

                    })
                }, startTime * 1000)
            }
        } catch (err) {
            orderLogger(options.orderId, 'error', `preOrderWatcher->_createPreOrderWorkerNotificationReminder->Error: ${err.message}`);
        }
    }


    /**
     * Send preorder to worker (set new status)
     * @param orderData
     * @return {Promise}
     */
    sendPreOrderToWork(orderData, blockWorker = true) {
        return new Promise((resolve, reject) => {
            orderLogger(orderData.order_id, 'info', 'preOrderWatcher->sendPreOrderToWork called!');
            const newStatusId = statusInfo.new_order.status_id;

            if(blockWorker){
                orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                    isPreOrder: true,
                    isWorkerEventOwner: true,
                    newStatusId
                });
            }else{
                orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                    isPreOrder: true,
                    isWorkerEventOwner: false,
                    newStatusId
                });
            }

            return resolve(1);
        });
    }

    /**
     * Send preorder to hospital free status
     * @param orderData
     * @return {Promise}
     */
    preOrderToHospital(orderData) {
        return new Promise((resolve, reject) => {
            orderLogger(orderData.order_id, 'info', 'preOrderWatcher->preOrderToHospital called!');

            const tenantId = orderData.tenant_id;
            const orderId =  parseInt(orderData.order_id);
            const worker = (typeof orderData.worker != "undefined") ?  (orderData.worker) : null;

            let companyID = null;
            if((typeof orderData.worker_company == "object") && ( orderData.worker_company != null) && (typeof orderData.worker_company.tenant_company_id != "undefined")){
                companyID = parseInt(orderData.worker_company.tenant_company_id );
            }

            let newStatusId = statusInfo.hospital_free_order.status_id;
            if(companyID != null){
                newStatusId = statusInfo.hospital_free_order_company.status_id;
            }

            orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                isPreOrder: true,
                isWorkerEventOwner: true,
                newStatusId
            });

            return resolve(1);
        });
    }

   /**
    // permit start pre order
    * @param tenantId
    * @param orderId
    * @return {Promise}
    */
    permitPreOrderEdit(tenantId,  orderId){
        return new Promise((resolve, reject) => {
            try{
                redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                    if(err){
                        orderLogger(options.orderId, 'err', `preOrderWatcher->preOrderPermitStart->getOrder->Error: ${err.message}`);
                        return reject(err);
                    }

                    orderData.edit = 0;
                    redisOrderManager.saveOrder(orderData, (err, result) => {
                        if (err) {
                            orderLogger(this.orderId, 'error', `preOrderWatcher->preOrderPermitStart->saveOrder->Error: ${err.message}`);
                            return reject(err);
                        }

                        let orderId = orderData.order_id;
                        sqlManager.updateOrderField(orderId, 'edit', 0, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `preOrderWatcher->preOrderPermitStart->sqlManager.updateOrderField->Error: ${err.message}`);
                                return reject(err);
                            } else {
                                orderLogger(orderId, 'info', `preOrderWatcher->preOrderPermitStart->sqlManager.updateOrderField->Result: ${result}`);
                                return resolve(1);
                            }
                        });
                    });
                })
            }catch (err) {
                return reject(err);
            }
        });
    }
}

module.exports = exports = new PreOrderWatcher();