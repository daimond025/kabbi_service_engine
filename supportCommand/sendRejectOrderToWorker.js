const amqpWorkerMessageSender = require("../services/amqpWorkerMessageSender");
const sqlManager = require('../database/sqlDB/sqlManager');
const logger = require('../services/logger');
const orderLogger = logger.mainLogger;
const orderEventWorkerMessage = require('../orderHandlers/orderEvent/orderEventWorkerMessage');
const orderEventWorkerCommand = require('../orderHandlers/orderEvent/orderEventWorkerCommand');
const orderActionManager = require('../orderHandlers/orderModules/common/orderActionManager');
const redisOrderManager = require('../database/redisDB/redisOrderManager');

/**
 * Technical support command:
 *  send socket message to worker to reject him from order
 * @param tenantLogin
 * @param workerCallsign
 * @param orderNumber
 * @param callback
 * @returns {*}
 */
module.exports = function (tenantLogin, workerCallsign, orderNumber, callback) {
    sqlManager.getTenantIdByDomain(tenantLogin, (err, tenantId) => {
        if (err) {
            orderLogger('error', err);
            return callback(err);
        }
        sqlManager.getOrderByNumber(tenantId, orderNumber, (err, orderDataSql) => {
            if (err) {
                orderLogger('error', err);
                return callback(err);
            }
            if (orderDataSql && orderDataSql.order_id) {
                const orderId = parseInt( orderDataSql.order_id);

                redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                    if(err){
                        orderLogger(orderDataSql, 'error', `sendRejectOrderToWorker->redisOrderManager.getOrder->Error: ${err.message}`);
                        callback(new Error('Can not find this order at REDIS'))
                    }else{
                        orderActionManager.sendOrderToFreeStatus(orderData, false)
                        .then(result => {
                            orderLogger(orderId, 'info', `sendRejectOrderToWorker->orderActionManager->processAssignedOrder->orderActionManager.sendOrderToFreeStatus->result:${result}`);
                            callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `sendRejectOrderToWorker->orderActionManager->processAssignedOrder->orderActionManager.sendOrderToFreeStatus->Error:${err.message}`);
                            callback(new Error(`Cannot->set->order->to->free->status->is->error->${err.message}`))
                        });
                    }

                });

                /*let messageString = orderEventWorkerMessage({
                    command: orderEventWorkerCommand.rejectWorkerFromOrder,
                    tenant_id: tenantId,
                    tenant_login: tenantLogin,
                    worker_callsign: workerCallsign,
                    params: {
                        order_id: orderId
                    }
                });
                const messageTtl = 60 * 60 * 24 * 10; // 10 days
                amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                    if (err) {
                        mainLogger('error', err);
                    } else {
                        mainLogger('info', `sendRejectOrderToWorker->Sended reject_order  to worker ${workerCallsign}`);
                    }
                });
                callback(null, 1);

                */
            } else {
                callback(new Error('Can not find this order at sqlDB'))
            }

        });
    });
};