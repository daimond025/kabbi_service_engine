"use strict";

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const workerStatusManager = require('../../../../services/workerStatusManager');
const serviceApi = require("../../../../services/externalWebServices/serviceApi");
const async = require("async");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderActionManager = require("../orderActionManager");
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const closeWorkerShift = require("../../../../workerShiftHandlers/closeWorkerShift");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../../orderEvent/orderEventWorkerCommand');
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const config = require('../../../../services/lib').getConfig();
const nodeId = config.MY_ID;
const redisWorkerReservedOrderListManager = require('../../../../database/redisDB/redisWorkerReservedOrderListManager');
const redisHospitalWorkerOrderManager = require('../../../../database/redisDB/redisHospitalWorkerOrderManager');

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderReportService = require('../../common/orderReportService');
const OrderSettings = require('../../../../database/mongo/models/OrderSettings');
const OrderSettingsRepository = require('../../../../database/mongo/repository/OrderSettingsRepository');

const orderType = require("../../../orderEvent/orderDevice");

/**
 * Order completed
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {String} eventSenderService
 * @return {*}
 */
module.exports = (orderInstance, orderData, eventSenderService) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusCompleted called!');
            const tenantId = orderInstance.tenantId;
            const orderId = parseInt( orderInstance.orderId);
            const clientId = parseInt(orderData.client_id);
            const statusId = parseInt(orderData.status.status_id);
            const sendOrderReportToClientEmail = parseInt(orderData.client.send_to_email);
            const clientEmail = orderData.client.email;
            const tenantLogin = orderData.tenant_login;
            const workerCallsign = (orderData.worker && orderData.worker.callsign) ? parseInt(orderData.worker.callsign) : null;
            const isOrderProcessedInExternalExchange = orderData.exchange_info && orderData.exchange_info.order;

            const workersRefuseAll = typeof orderData.refuse_workers_list === "object" ?
                Object.keys(orderData.refuse_workers_list).map(key => orderData.refuse_workers_list[key]) : [];

            const updateTime = Math.floor(Date.now() / 1000);
            if (orderInstance.statusArr[orderInstance.statusArr.length - 2] !== statusInfo.worker_coming_later.status_id) {
                orderInstance.workerLateTimestamp = null;
            }
            if (orderInstance.statusArr[orderInstance.statusArr.length - 2] !== statusInfo.waiting_for_payment.status_id) {
                orderInstance.startWaitingForPaymentTimestamp = null;
            }
            orderLogger(orderId, 'info', 'OrderStatusCompleted CALLED!');
            async.parallel([
                callback => {
                    serviceApi.updateClientCounters(tenantId, clientId, statusId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusCompleted->serviceApi.updateClientCounters->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusCompleted->serviceApi.updateClientCounters->Result: ${result}`);
                        return callback(null, 1);
                    });
                },
                callback => {
                    if(workersRefuseAll.length > 0){
                        try {
                            sqlManager.insertRefuseWorker(orderId, workersRefuseAll)
                                .then(result =>  {
                                    let sucess = typeof result.sucess !== undefined ?  result.sucess : 0;
                                    orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.insertRefuseWorker->Result:: ${sucess}`);
                                    return callback(null, 1);
                                })
                                .catch(err => {
                                    orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.insertRefuseWorker->Error ${err.message}`);
                                    return callback(null, 1);
                                });
                        } catch (err) {
                            orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.insertRefuseWorker->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                    }else {
                        return callback(null, 1);
                    }

                },
                callback => {
                    orderActionManager.deleteOrderPhone(orderData)
                        .then(result => {
                            orderLogger(orderId, 'info', `orderStatusCompleted->orderActionManager.deleteOrderPhone->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderStatusCompleted->orderActionManager.deleteOrderPhone->Error: ${err.message}`);
                        });
                },
                callback => {
                    serviceApi.sendOrderToStatistic(tenantId, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusCompleted->serviceApi.sendOrderToStatistic->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusCompleted->serviceApi.sendOrderToStatistic->Result: ${result}`);
                        return callback(null, 1);
                    })
                },
                callback => {
                    if (!isOrderProcessedInExternalExchange) {
                        return callback(null, 1);
                    }
                    try {
                        const exchangeProgramId = orderData.exchange_info.exchange_program_id;
                        sqlManager.updateOrderField(orderId, 'processed_exchange_program_id', exchangeProgramId, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.updateOrderField->processed_exchange_program_id:${exchangeProgramId}->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.updateOrderField->processed_exchange_program_id:${exchangeProgramId}->Result: ${result}`);
                            }
                        });
                        const externalWorker = orderData.exchange_info.order.worker;
                        const externalCar = orderData.exchange_info.order.car;
                        const workerData = {
                            name: externalWorker.name,
                            phone: externalWorker.phone,
                            car_description: externalCar.name,
                            color: externalCar.color,
                            gos_number: externalCar.gos_number,
                            external_car_id: externalCar.external_car_id,
                            external_worker_id: externalCar.external_worker_id,
                        };
                        sqlManager.saveOrderExternalWorker(orderId, workerData)
                            .then(result => {
                                orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.saveOrderExternalWorker->Result: ${result}`);
                            })
                            .catch(err => {
                                orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.saveOrderExternalWorker->Error: ${err.message}`);
                            });
                        return callback(null, 1);
                    } catch (err) {
                        orderLogger(orderId, 'error', `orderStatusCompleted->Error: ${err.message}`);
                        return callback(null, 1);
                    }
                },
                callback => {
                    if (workerCallsign) {
                        orderLogger(orderId, 'info', `orderStatusCompleted->Order has worker: ${workerCallsign}`);
                        redisWorkerReservedOrderListManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusCompleted->redisWorkerReservedOrderListManager.delOrder->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `orderStatusCompleted->redisWorkerReservedOrderListManager.delOrder->Result: ${result}`);
                            }
                        });
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerDataTemp) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusCompleted->redisWorkerManager.getWorker->Error: ${err.message}`);
                                return callback(null, 1);
                            }
                            try {
                                const workerShiftId = workerDataTemp.worker.worker_shift_id;
                                sqlManager.logWorkerCompletedOrderCounter(workerShiftId, orderId, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.logWorkerCompletedOrderCounter->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.logWorkerCompletedOrderCounter->Result: ${result}`);
                                    }
                                });
                                if (orderInstance.workerLateTimestamp !== null) {
                                    const lateTimeSec = updateTime - orderInstance.workerLateTimestamp;
                                    sqlManager.logWorkerLateTime(workerShiftId, lateTimeSec, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.logWorkerLateTime->Result: ${result}`);
                                        }
                                    });
                                }
                                if (orderInstance.startWaitingForPaymentTimestamp !== null) {
                                    const orderPaymentTimeSec = updateTime - orderInstance.startWaitingForPaymentTimestamp;
                                    sqlManager.logWorkerOrderPaymentTime(workerShiftId, orderPaymentTimeSec, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.logWorkerOrderPaymentTime->Error : ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.logWorkerOrderPaymentTime->Result: ${result}`);
                                        }
                                    });
                                }
                            } catch (err) {
                                orderLogger(orderId, 'error', `orderStatusCompleted->Error: ${err.message}`);
                            }
                            const lastOrderId = parseInt(workerDataTemp.worker.last_order_id);
                            const needCloseShift = parseInt(workerDataTemp.worker.need_close_shift) === 1;
                            orderLogger(orderId, 'info', `orderStatusCompleted->lastOrderId: ${lastOrderId}`);
                            orderLogger(orderId, 'info', `orderStatusCompleted->worker status: ${workerDataTemp.worker.status}`);
                            orderLogger(orderId, 'info', `orderStatusCompleted->need close shift: ${needCloseShift}`);


                            orderLogger(orderId, 'info', `orderStatusCompleted->compare the value of status: ${workerDataTemp.worker.status === workerStatusManager.onOrder}`);
                            orderLogger(orderId, 'info', `orderStatusCompleted->compare the value of int order_id: ${lastOrderId === parseInt(orderId)}`);
                            orderLogger(orderId, 'info', `orderStatusCompleted->compare the value of order_id: ${ lastOrderId == orderId}`);

                            // список автиных поездок - удаление
                            let active_orders = typeof workerDataTemp.worker.active_orders === "object" ?
                                Object.keys(workerDataTemp.worker.active_orders).map(key => parseInt(workerDataTemp.worker.active_orders[key])): [];
                            let index_del = active_orders.indexOf(orderId);
                            if(index_del > -1){
                               active_orders.splice(index_del, 1);
                            }
                            workerDataTemp.worker.active_orders = active_orders;
                            orderLogger(orderId, 'info', `orderStatusCompleted->active_orders ${active_orders.join(',')} in workerCallsign: ${workerCallsign}`);


                            if ( //workerDataTemp.worker && workerDataTemp.worker.status
                                 // ( (lastOrderId === parseInt(orderId)) || ( lastOrderId == orderId))
                                 workerDataTemp.worker.status === workerStatusManager.onOrder
                                && (active_orders.length === 0)){
                                orderLogger(orderId, 'info', `orderStatusCompleted->need set worker status=${workerStatusManager.free}: ${true}`);
                                workerDataTemp.worker.status = workerStatusManager.free;
                            } else {
                                orderLogger(orderId, 'info', `orderStatusCompleted->need set worker status=${workerStatusManager.free}: ${false}`);
                            }
                            redisWorkerManager.saveWorker(workerDataTemp, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderStatusCompleted->redisWorkerManager.saveWorker->Error ${err.message}`);
                                }
                                orderLogger(orderId, 'info', `orderStatusCompleted->saveWorker->Result: ${result}`);
                                sqlManager.logOrderChangeData(tenantId, orderId, 'status', workerDataTemp.worker.callsign, 'worker', workerStatusManager.free, 'system', (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusCompleted->sqlManager.logOrderChangeData->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusCompleted->sqlManager.logOrderChangeData->Result: ${result}`);
                                    }
                                });
                                if (eventSenderService === orderEventSenderService.operatorService) {
                                    let messageString = orderEventWorkerMessage({
                                        command: orderEventWorkerCommand.completeOrder,
                                        tenant_id: tenantId,
                                        tenant_login: tenantLogin,
                                        worker_callsign: workerCallsign,
                                        params: {
                                            order_id: orderId
                                        }
                                    });
                                    const messageTtl = 60 * 60 * 24 * 10; // 10 days
                                    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusCompleted->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusCompleted->amqpWorkerMessageSender.sendMessage->Published command: ${orderEventWorkerCommand.completeOrder} to worker: ${workerCallsign}.Result: ${result}`);
                                        }
                                    });
                                }
                                if (needCloseShift) {
                                    orderLogger(orderId, 'info', `orderStatusCompleted->Need close shift of worker: ${workerCallsign}`);
                                    const workerShiftId = workerDataTemp.worker.worker_shift_id;
                                    closeWorkerShift(tenantId, tenantLogin, workerCallsign, workerShiftId, 'timer', 'system', nodeId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusCompleted->closeWorkerShift->Error: ${err.message}`);
                                        }
                                        orderLogger(orderId, 'info', `orderStatusCompleted->closeWorkerShift->Result: ${result}`);
                                    });
                                }
                                else{
                                    // показываем следующий заказ, назначенный на водителя
                                    const typeOrder = orderData.device.toString();

                                    if(typeOrder == orderType.HOSPITAL ){

                                        redisHospitalWorkerOrderManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                                            if(err){
                                                orderLogger(orderId, 'error', `orderStatusCompleted->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                                            }
                                            orderLogger(orderId, 'error', `orderStatusCompleted->redisHospitalWorkerOrderManager->delOrder->Result: ${result}`);


                                            redisHospitalWorkerOrderManager.getAll(tenantId, workerCallsign, (err_all, result_all) => {
                                                if(err_all){
                                                    orderLogger(orderId, 'error', `orderStatusCompleted->redisHospitalWorkerOrderManager->getAll->Error: ${err.message}`);
                                                }

                                                if(result_all.length > 0){
                                                    let orderOffer = Math.min.apply(null,result_all);

                                                    if((workerDataTemp.worker.status == workerStatusManager.free || active_orders.length == 0) && (orderOffer)) {

                                                        let workerMessageString = orderEventWorkerMessage({
                                                            command: orderEventWorkerCommand.preOrderOffer,
                                                            tenant_id: tenantId,
                                                            tenant_login: tenantLogin,
                                                            worker_callsign: workerCallsign,
                                                            params: {
                                                                order_id: orderOffer
                                                            }
                                                        });
                                                        const messageTtl = 60 * 60 * 24 * 10;
                                                        amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, workerMessageString, messageTtl, (err, result) => {
                                                            if(err){
                                                                orderLogger(orderId, 'err', `orderStatusCompleted->Hospital->sendMessageNextOrder>to worker ${workerCallsign}->Error: ${err.message}`);
                                                            }else{
                                                                orderLogger(orderId, 'info', `orderStatusCompleted->Hospital->sendMessageNextOrder->Result: ${result}`);
                                                            }
                                                        });
                                                    }
                                                }
                                            });

                                        });
                                    }
                                }
                                return callback(null, 1);
                            })

                        })
                    } else {
                        return callback(null, 1);
                    }
                },
                callback => {
                    if (!sendOrderReportToClientEmail) {
                        orderLogger(orderId, 'info', `orderStatusCompleted->sendOrderReportToClientEmail->Not need send client order report to email`);
                        return callback(null, 1);
                    }
                    if (!clientEmail) {
                        orderLogger(orderId, 'info', `orderStatusCompleted->sendOrderReportToClientEmail->Client have not email`);
                        return callback(null, 1);
                    }
                    orderReportService.sendOrderReportToClientEmail(orderData)
                        .then(result => {
                            orderLogger(orderId, 'info', `orderStatusCompleted->orderReportService.sendOrderReportToClientEmail->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderStatusCompleted->orderReportService.sendOrderReportToClientEmail->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                callback => {
                    OrderSettingsRepository.create(new OrderSettings({
                        tenant_id: tenantId,
                        order_id: orderId,
                        settings: orderInstance.settings
                    }))
                        .then(result => {
                            orderLogger(orderId, 'info', `orderStatusCompleted->saveOrderSettings->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderStatusCompleted->saveOrderSettings->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                },
                callback => {
                    redisOrderManager.deleteOrder(tenantId, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusCompleted->deleteOrder->Error: ${err.message}`);
                        }
                        orderLogger(orderId, 'info', `orderStatusCompleted->deleteOrder->Result: ${result}`);
                        return callback(null, 1);
                    })
                }
            ], (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderStatusCompleted->Error: ${err.message}`);
                    return reject(err);
                }
                orderLogger(orderId, 'info', `orderStatusCompleted->Results of completing order: ${result}`);
                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.destroyService,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', err.message);
                        return reject(err);
                    } else {
                        orderLogger(orderId, 'info', `orderStatusCompleted->Published command: ${orderEventOrderCommand.destroyService} to order: ${orderId}. Result: ${result}`);
                        orderInstance.orderTimer.clearAll();
                        orderInstance.orderTimer = null;
                        return resolve(1);
                    }
                });
            })
        } catch (err) {
            return reject(err);
        }
    });

};