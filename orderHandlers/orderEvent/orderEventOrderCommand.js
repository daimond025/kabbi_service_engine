'use strict';

const destroyService = "destroy_service";
const updateOrderData = "update_order_data";
const sendOrderToExchange = "send_to_exchange";
const refuseOfferedOrderMultiple = "refuse_offer_order_multiple";
const getOfferedOrderMultiple = "get_offered_order_multiple";
const orderOfferTimeout = "order_offer_timeout";
const preOrderConfirmTimeout = "preorder_confirm_timeout";
const workerLateTimeout = "worker_late_timeout";
const clientLateTimeout = "client_late_timeout";
const orderLateTimeout = "order_late_timeout";
const orderRejectTimeout = "order_reject_timeout";
const stopOffer = "stop_offer";
const removeWorker = "remove_worker";
const removeCompany = "remove_company";

const orderEventSenderService = require('./orderEventSenderService');

/**
 * is order event must be processed by order service
 * @param {string} event
 * @param  {string} eventSender
 * @returns {boolean}
 */
function isOrderEventMustBeProcessedByOrderService(event, eventSender) {

    if (orderEventSenderService.engineService === eventSender) {
        return [sendOrderToExchange, updateOrderData].includes(event)

    }

    if (orderEventSenderService.operatorService === eventSender) {
        return [sendOrderToExchange, updateOrderData].includes(event)
    }

    if (orderEventSenderService.clientService === eventSender) {
        return [updateOrderData].includes(event)
    }

    if (orderEventSenderService.workerService === eventSender) {
        return [updateOrderData].includes(event)
    }

    if (orderEventSenderService.exchangeService === eventSender) {
        return [updateOrderData].includes(event)
    }
}

/**
 * is command return real result or it is handle in background
 * @param command
 * @returns {boolean}
 */
function isReturnRealResult(command) {
    return [
        sendOrderToExchange,
        refuseOfferedOrderMultiple,
        getOfferedOrderMultiple,
        orderOfferTimeout,
        preOrderConfirmTimeout,
        workerLateTimeout,
        clientLateTimeout,
        orderLateTimeout,
        orderRejectTimeout,
        stopOffer,

        //freeOrderOffer  // todo
    ].includes(command)
}

module.exports = {
    destroyService,
    updateOrderData,
    sendOrderToExchange,
    refuseOfferedOrderMultiple,
    getOfferedOrderMultiple,
    orderOfferTimeout,
    preOrderConfirmTimeout,
    workerLateTimeout,
    clientLateTimeout,
    orderLateTimeout,
    orderRejectTimeout,
    stopOffer,
    removeWorker,
    removeCompany,
    isOrderEventMustBeProcessedByOrderService,
    isReturnRealResult
};
