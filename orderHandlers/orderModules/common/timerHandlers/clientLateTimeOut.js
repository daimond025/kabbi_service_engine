'use strict';

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const orderEventOrderMessage = require("../../../orderEvent/orderEventOrderMessage");

/**
 * Order late time out
 * @param {BaseGootaxOrder}  orderInstance
 * @returns {Promise<any>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'clientLateTimeout called!');
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                    if (err) {
                        orderLogger(orderInstance.orderId, 'error', `clientLateTimeout->Error: ${err.message}`);
                        return reject(err);
                    }
                    if (orderData && orderData.status && parseInt(orderData.status.status_id) === statusInfo.worker_arrived_order.status_id) {
                        orderData.status_id = statusInfo.client_is_not_coming.status_id;
                        orderData.status.status_id = statusInfo.client_is_not_coming.status_id;
                        orderData.status.name = statusInfo.client_is_not_coming.name;
                        const updateTime = Math.floor(Date.now() / 1000);
                        orderData.update_time = updateTime;
                        orderData.status_time = updateTime;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, 'error', `clientLateTimeout->redisOrderManager.saveOrder->Error: ${err.message}`);
                            }
                            sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.client_is_not_coming.status_id, orderData.worker_id, orderData.car_id, updateTime, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, "error", `clientLateTimeout->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderInstance.orderId, "info", `clientLateTimeout->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                                }
                            });
                            const messageString = orderEventOrderMessage({
                                command: orderEventOrderCommand.updateOrderData,
                                tenant_id: orderInstance.tenantId,
                                order_id: orderInstance.orderId,
                                params: {}
                            });
                            amqpOrderMessageSender.sendMessage(orderInstance.orderId, messageString, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, "error", `clientLateTimeout->amqpOrderMessageSender.sendMessage->${err.message}`);
                                    return reject(err);
                                } else {
                                    orderLogger(orderInstance.orderId, 'info', `clientLateTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderInstance.orderId}. Result: ${result}`);
                                    return resolve(true);
                                }
                            });
                        })
                    } else {
                        return resolve(true);
                    }
                }
            )
        } catch (err) {
            return reject(err);
        }
    })
};
