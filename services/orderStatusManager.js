const statusInfo = {
    new_order: {
        status_id: 1,
        name: "New order",
        status_group: "new"
    },
    offer_order: {
        status_id: 2,
        name: "Offer to a worker",
        status_group: "new"
    },
    worker_refused_order: {
        status_id: 3,
        name: "Worker has refused",
        status_group: "new"
    },
    worker_ignored_offer_order: {
        status_id: 109,
        name: "Worker ignored order offer",
        status_group: "new"
    },
    worker_assigned_at_order_soft: {
        status_id: 113,
        name: "Worker assigned at order(soft)",
        status_group: "new"
    },
    worker_assigned_at_order_hard: {
        status_id: 114,
        name: "Worker assigned at order(hard)",
        status_group: "new"
    },
    worker_refused_order_assign: {
        status_id: 115,
        name: "Worker refused assignment of order",
        status_group: "new"
    },
    free_order: {
        status_id: 4,
        name: "Free order",
        status_group: "new"
    },
    new_order_no_parking: {
        status_id: 5,
        name: "Is not bound to any parking zone",
        status_group: "new"
    },
    new_pre_order: {
        status_id: 6,
        name: "New order",
        status_group: "pre_order"
    },
    worker_accepted_preorder: {
        status_id: 7,
        name: "Worker has accepted an order",
        status_group: "pre_order"
    },
    worker_refused_preorder: {
        status_id: 10,
        name: "Worker order refusing",
        status_group: "pre_order"
    },
    new_pre_order_no_parking: {
        status_id: 16,
        name: "Is not bound to any parking zone",
        status_group: "pre_order"
    },
    worker_assigned_at_preorder_soft: {
        status_id: 111,
        name: "Worker assigned at preorder(soft)",
        status_group: "pre_order"
    },
    worker_assigned_at_preorder_hard: {
        status_id: 112,
        name: "Worker assigned at preorder(hard)",
        status_group: "pre_order"
    },
    worker_accepted_order: {
        status_id: 17,
        name: "Worker has accepted an order",
        status_group: "car_assigned"
    },
    worker_arrived_order: {
        status_id: 26,
        name: "Worker has arrived",
        status_group: "car_at_place"
    },
    client_is_not_coming: {
        status_id: 27,
        name: "Client is not coming out",
        status_group: "car_at_place"
    },
    order_execution: {
        status_id: 36,
        name: "Order execution",
        status_group: "executing"
    },
    waiting_for_payment: {
        status_id: 110,
        name: "Waiting for payment",
        status_group: "executing"
    },
    completed_paid: {
        status_id: 37,
        name: "Completed, paid",
        status_group: "completed"
    },
    completed_nopaid: {
        status_id: 38,
        name: "Completed, not paid",
        status_group: "completed"
    },
    outdated_order: {
        status_id: 52,
        name: "Outdated order",
        status_group: "new"
    },
    pre_order_executing: {
        status_id: 55,
        name: "Pre - order execution",
        status_group: "car_assigned"
    },
    worker_coming_later: {
        status_id: 54,
        name: "Worker is coming later",
        status_group: "car_assigned"
    },
    no_workers: {
        status_id: 107,
        name: "There are not free workers",
        status_group: "rejected"
    },
    manual_mode: {
        status_id: 108,
        name: "Manual mode",
        status_group: "new"
    },
    waiting_for_preorder_confirmation: {
        status_id: 116,
        name: "Waiting for preorder confirmation",
        status_group: "pre_order"
    },
    worker_ignored_preorder_confirmation: {
        status_id: 117,
        name: "Worker ignored preorder confirmation",
        status_group: "pre_order"
    },
    worker_refused_preorder_confirmation: {
        status_id: 118,
        name: "Worker refused preorder confirmation",
        status_group: "pre_order"
    },
    worker_accepted_preorder_confirmation: {
        status_id: 119,
        name: "Worker accepted preorder confirmation",
        status_group: "pre_order"
    },


    // Hospital all status - new, pre
    hospital_pre_order: {
        status_id: 203,
        name: "Hospital pre-order status for all",
        status_group: "pre_order"
    },
    hospital_free_order: {
        status_id: 204,
        name: "Hospital free status for all",
        status_group: "new"
    },

    // Hospital company status - new, pre
    hospital_pre_order_company: {
        status_id: 205,
        name: "Hospital pre-order status for company",
        status_group: "pre_order"
    },
    hospital_free_order_company: {
        status_id: 206,
        name: "Hospital free status for company",
        status_group: "new"
    },

};


/**
 * Get order status info by id
 * @param {Number} statusId
 * @return {Object} statusInfo
 */
function getOrderStatusById(statusId) {
    let resultStatus = null;
    Object.keys(statusInfo).forEach(key => {
        let status = statusInfo[key];
        if (statusId === status.status_id) {
            resultStatus = status;
        }
    });
    return resultStatus;
}

exports.statusInfo = statusInfo;
exports.getOrderStatusById = getOrderStatusById;
