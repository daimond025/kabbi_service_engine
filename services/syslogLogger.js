const syslog = require('modern-syslog');
const config = require('./lib').getConfig();
const serviceVersion = require('./lib').getServiceVersion();


/**
 * Main logger
 * @param  {String} category
 * @param  {String} message
 */
function mainLogger(category, message) {
    const consulServiceWorker = require("./consul/consulServiceWorker");
    const prefix = `[main] node_id=${config.MY_ID}${consulServiceWorker.status === "master" ? 'M' : ''} `;
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_MAIN_IDENT + '-' + serviceVersion, null, config.SYSLOG_MAIN_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}


/**
 * Order logger
 * @param  {Number} orderId
 * @param  {String} category
 * @param  {String} message
 */
function orderLogger(orderId, category, message) {
    const consulServiceWorker = require("./consul/consulServiceWorker");
    const prefix = `[order] node_id=${config.MY_ID}${consulServiceWorker.status === "master" ? 'M' : ''},order_id=${orderId} `;
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_ORDER_IDENT + '-' + serviceVersion, null, config.SYSLOG_ORDER_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

/**
 * Worker shift Logger
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {String} category
 * @param  {String} message
 */
function workerShiftLogger(tenantId, workerCallsign, category, message) {
    const consulServiceWorker = require("./consul/consulServiceWorker");
    const prefix = `[worker_shift] node_id=${config.MY_ID}${consulServiceWorker.status === "master" ? 'M' : ''},worker_shift_data=${tenantId + '_' + workerCallsign} `;
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_WORKERSHIFT_IDENT + '-' + serviceVersion, null, config.SYSLOG_WORKERSHIFT_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

/**
 * Worker block Logger
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {String} category
 * @param  {String} message
 */
function workerBlockLogger(tenantId, workerCallsign, category, message) {
    const consulServiceWorker = require("./consul/consulServiceWorker");
    const prefix = `[worker_block] node_id=${config.MY_ID}${consulServiceWorker.status === "master" ? 'M' : ''},worker_block_data=${tenantId + '_' + workerCallsign} `;
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_WORKERBLOCK_IDENT + '-' + serviceVersion, null, config.SYSLOG_WORKERBLOCK_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}


exports.mainLogger = mainLogger;
exports.orderLogger = orderLogger;
exports.workerShiftLogger = workerShiftLogger;
exports.workerBlockLogger = workerBlockLogger;

