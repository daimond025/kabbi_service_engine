"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const redisHospitalWorkerOrderManager = require("../../../../database/redisDB/redisHospitalWorkerOrderManager");
const orderActionManager = require('../orderActionManager');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const orderWorkerAssignManager = require('../../common/orderWorkerAssignManager');
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const redisWorkerShiftManager = require('../../../../database/redisDB/redisWorkerShiftManager');
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventWorkerCommand = require('../../../orderEvent/orderEventWorkerCommand');
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");

const orderType = require("../../../orderEvent/orderDevice");

const status_valid = [
    parseInt(statusInfo.hospital_pre_order.status_id),
    parseInt(statusInfo.hospital_free_order.status_id),
    parseInt(statusInfo.new_order.status_id),
    parseInt(statusInfo.new_pre_order.status_id)
];

function setStatusHospitalOrder(newStatusId, companyID){
    let newStatus = newStatusId;
    // no company
    if(newStatusId == statusInfo.new_pre_order.status_id){
        newStatus = statusInfo.hospital_pre_order.status_id;
    }
    else if(newStatusId == statusInfo.new_order.status_id){
        newStatus = statusInfo.hospital_free_order.status_id;
    }
    return newStatus;
}

/**
 * Stop offer order action
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance, event) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'removeCompany called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;

            const user_id = (typeof event.change_sender_id != undefined) ? event.change_sender_id : 0;

            redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderId, 'error', `removeCompany->getOrder->Error: ${err.message}`);
                    return reject(err);
                }

                orderLogger(orderId, 'info', "removeCompany  called!");
                const typeOrder = orderData.device.toString();
                const status_id =  parseInt(orderData.status_id);

                let companyID = null;
                if((typeof orderData.worker_company == "object") && ( orderData.worker_company != null) && (typeof orderData.worker_company.tenant_company_id != "undefined")){
                    companyID = parseInt(orderData.worker_company.tenant_company_id );
                }

                if( ((status_valid.includes(status_id)) && companyID == null)) {
                    orderLogger(orderId, 'info', `removeCompany->not called! status - ${orderData.status_id}`);
                    return resolve(true);
                }


                // вермя заказа
                const orderTime = parseInt(orderData.order_time);
                let preOrderTimeToWork = orderInstance.settings.PRE_ORDER_WORKING;
                const cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;

                const timeOfNotification = orderTime - (preOrderTimeToWork * 60);
                const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(cityTimeOffset);
                let startTime = timeOfNotification - tenantNowTime;
                startTime = startTime > 0 ? startTime : 0;

                let newStatusId = statusInfo.new_order.status_id;
                if(startTime > 0){
                    newStatusId = statusInfo.new_pre_order.status_id;
                }
                // для больничных заказов - свои статусы
                if((typeOrder == orderType.HOSPITAL)){
                    newStatusId = setStatusHospitalOrder(newStatusId, companyID);
                }
                orderLogger(orderId, 'info', `removeCompany is new status ${newStatusId}`);

                // лог  сообщения что компания  снята с заказа
                sqlManager.logOrderChangeData(tenantId, orderId, 'remove_company', companyID, 'order', companyID , "disputcher_"  + user_id  , (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `removeWorkerFromOrder->sqlManager.logOrderChangeData->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, "info", `removeWorkerFromOrder->sqlManager.logOrderChangeData->Result: ${result}`);
                    }
                });

                let workerCallsign = null;
                if (orderData && orderData.worker && orderData.worker.callsign) {
                    workerCallsign = parseInt(orderData.worker.callsign);
                }
                if(workerCallsign != null){
                    let isWorkerOnline = false;
                    redisWorkerShiftManager.getWorkerShiftId(orderData.tenant_login, workerCallsign, (err, activeShiftId) => {
                        if(err){
                            return reject(err);
                        }
                        // видитель на смене
                        isWorkerOnline = activeShiftId != null;
                        let workerId= (typeof orderData.worker_id != undefined) ? orderData.worker_id : 0;

                        // лог  сообщения что водитель снят с заказа
                        sqlManager.logOrderChangeData(tenantId, orderId, 'remove_worker', workerCallsign, 'order', workerId , "disputcher_"  + user_id  , (err, result) => {
                            if (err) {
                                orderLogger(orderId, "error", `removeCompany->sqlManager.logOrderChangeData->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, "info", `removeCompany->sqlManager.logOrderChangeData->Result: ${result}`);
                            }
                        });

                        // событие отказ от заказа - водителю
                        if(isWorkerOnline){
                            let messageString = orderEventWorkerMessage({
                                command: orderEventWorkerCommand.rejectOrder,
                                tenant_id: tenantId,
                                tenant_login: orderData.tenant_login,
                                worker_callsign: workerCallsign,
                                params: {
                                    order_id: orderId
                                }
                            });

                            const messageTtl = 60 * 60 * 24 * 10; // 10 days
                            amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `removeWorkerFromOrder->worker:${workerCallsign}. amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `removeWorkerFromOrder->worker:${workerCallsign}. amqpWorkerMessageSender.sendMessage->
                                        published command: ${orderEventWorkerCommand.rejectOrder} to worker: ${workerCallsign}. Result: ${result}`);
                                }
                            });
                        }


                        if(typeOrder == orderType.HOSPITAL ){

                            redisHospitalWorkerOrderManager.delOrder(tenantId,workerCallsign, orderId ,(err, result) => {
                                if(err){
                                    orderLogger(orderId, 'error', `removeCompany->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                                }

                                orderLogger(orderId, 'info', `removeCompany->redisHospitalWorkerOrderManager->delOrder->Result: ${result}`);

                            });
                        }
                        orderLogger(orderId, 'info', `removeWorkerFromOrder success remove worker!`);

                    });
                }

                // remove company
                let isPreOrder = (newStatusId == statusInfo.new_pre_order.status_id ||
                    newStatusId == statusInfo.hospital_pre_order_company.status_id ||
                    newStatusId == statusInfo.hospital_pre_order.status_id
                ) ? true : false;

                orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                    isPreOrder: isPreOrder,
                    isWorkerEventOwner: false,
                    newStatusId,
                    companyID
                });
                return resolve(true);
            });

        } catch (err) {
            return reject(err);
        }
    })
};