"use strict";

const sqlManager = require("../../database/sqlDB/sqlManager");
const redisOrderManager = require("../../database/redisDB/redisOrderManager");
const redisWorkerManager = require("../../database/redisDB/redisWorkerManager");
const redisWorkerShiftManager = require("../../database/redisDB/redisWorkerShiftManager");
const logger = require('../logger');
const mainLogger = logger.mainLogger;
const async= require("async");

/**
 * @class WorkResumeBase
 * Resume to processing orders,worker shifts, unblock timers
 */
module.exports = class WorkResumeBase {
    constructor() {
        this._ownMode = "own";
        this._clusterMode = 'cluster';
    }

    /**
     *
     * @returns {string}
     */
    get ownMode() {
        return this._ownMode;
    }

    /**
     *
     * @returns {string}
     */
    get clusterMode() {
        return this._clusterMode;
    }

    /**
     * Load orders, which was processed at node with id= nodeId
     * @param {Number} nodeId
     */
    loadOrders(nodeId) {

    }

    /**
     * Load worker shifts, which was processed at node with id= nodeId
     * @param {Number} nodeId
     */
    loadWorkerShifts(nodeId) {

    }

    /**
     * Load worker unblock timers, which was processed at node with id= nodeId
     * @param {Number} nodeId
     */
    loadWorkerUnblockTimers(nodeId) {

    }

    /**
     * Get processed orders at node with id = nodeId
     * @param nodeId
     * @returns {Promise}
     */
    static getProcessedOrders(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", `WorkResumeBase->getProcessedOrders called`);
            const processedOrders = [];
            sqlManager.getAllTenantIds((err, tenantArr) => {
                if (err) {
                    mainLogger("error", `WorkResumeBase->getProcessedOrders->getAllTenantIds->Error: ${err.message}`);
                    return reject(err);
                }
                async.each(tenantArr, function (tenantId, callback) {
                    redisOrderManager.getAllOrders(tenantId, (err, orders) => {
                        if (err) {
                            mainLogger("error", `WorkResumeBase->getProcessedOrders->getAllOrders->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                        if (Array.isArray(orders) && orders.length < 1) {
                            mainLogger("info", `WorkResumeBase->getProcessedOrders->getAllOrders->${tenantId} has no active orders`);
                            return callback(null, 1);
                        }
                        async.each(orders, (orderData, orderCb) => {
                            try {
                                let tenantId = parseInt(orderData.tenant_id);
                                let orderId = parseInt(orderData.order_id);
                                let orderNodeId = parseInt(orderData.server_id);
                                if (nodeId === orderNodeId) {
                                    processedOrders.push({tenantId, orderId})
                                }
                            } catch (err) {
                                mainLogger("error", `WorkResumeBase->getProcessedOrders->getAllOrders->Error of getting order data params.Error: ${err.message}`);
                                mainLogger("error", orderData);
                            }
                            return orderCb(null, 1);
                        }, (err) => {
                            if (err) {
                                mainLogger("error", `WorkResumeBase->getProcessedOrders->getAllOrders->async.each orders. Error: ${err.message}`);
                            }
                            return callback(null, 1);
                        })
                    })
                }, (err) => {
                    if (err) {
                        mainLogger("error", `WorkResumeBase->getProcessedOrders->getAllOrders->async.each tenantArr. Error: ${err.message}`);
                        return reject(err);
                    } else {
                        mainLogger("info", 'WorkResumeBase->getProcessedOrders->successful done');
                        return resolve(processedOrders);
                    }
                })
            });
        });
    }

    /**
     * Get processed worker shifts  at node with id = nodeId
     * @param nodeId
     */
    static getProcessedWorkerShifts(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", `WorkResumeBase->getProcessedWorkerShifts called!`);
            const processedWorkerShifts = [];
            sqlManager.getAllTenantIds((err, tenantArr) => {
                if (err) {
                    mainLogger("error", `WorkResumeBase->getProcessedWorkerShifts->sqlManager.getAllTenantIds->Error: ${err.message}`);
                    return reject(err);
                }
                async.each(tenantArr, function (tenantId, callback) {
                    redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
                        if (err) {
                            mainLogger("error", `WorkResumeBase->getProcessedWorkerShifts->getAllWorkers->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                        if (Array.isArray(workers) && workers.length < 1) {
                            mainLogger("info", `WorkResumeBase->getProcessedWorkerShifts->getAllWorkers->${tenantId} has no active workers`);
                            return callback(null, 1);
                        }
                        async.each(workers, (workerData, wrokerCb) => {
                            try {
                                let workerCallsign = parseInt(workerData.worker.callsign);
                                let tenantId = parseInt(workerData.worker.tenant_id);
                                let tenantLogin = workerData.worker.tenant_login;
                                let shiftId = parseInt(workerData.worker.worker_shift_id);
                                let workerNodeId = parseInt(workerData.server_id);
                                if (nodeId !== workerNodeId) {
                                    return wrokerCb(null, 1);
                                }
                                redisWorkerShiftManager.getWorkerShiftId(tenantLogin, workerCallsign, (err, activeShiftId) => {
                                    if (err) {
                                        mainLogger("error", `WorkResumeBase->sendWorkerShift->getWorkerShiftId->Error: ${err.message}`);
                                        return wrokerCb(null, 1);
                                    }
                                    if (!activeShiftId || (parseInt(activeShiftId) !== shiftId)) {
                                        const shiftLifeTime = 1;
                                        processedWorkerShifts.push({
                                            workerCallsign,
                                            tenantId,
                                            tenantLogin,
                                            shiftId,
                                            shiftLifeTime,
                                            workerNodeId
                                        });
                                        return wrokerCb(null, 1);
                                    }
                                    redisWorkerShiftManager.getTimeLeft(tenantLogin + "_" + workerCallsign, (err, ttlTime) => {
                                        if (err) {
                                            mainLogger("error", `WorkResumeBase->getTimeLeft->Error: ${err.message}`);
                                            return wrokerCb(null, 1);
                                        }
                                        let shiftLifeTime = 1;
                                        if (ttlTime) {
                                            shiftLifeTime = ttlTime;
                                        }
                                        processedWorkerShifts.push({
                                            workerCallsign,
                                            tenantId,
                                            tenantLogin,
                                            shiftId,
                                            shiftLifeTime,
                                            workerNodeId
                                        });
                                        return wrokerCb(null, 1);
                                    })
                                });
                            } catch (err) {
                                mainLogger("error", `WorkResumeBase->getProcessedWorkerShifts->->getAllOrders->Error of getting worker data params!Error: ${err.message}`);
                                mainLogger("error", workerData);
                                return wrokerCb(null, 1);
                            }
                        }, (err) => {
                            if (err) {
                                mainLogger("error", `WorkResumeBase->getProcessedWorkerShifts->->getAllWorkers->async.each workers. Error: ${err.message}`);
                            }
                            return callback(null, 1);
                        })
                    });
                }, (err) => {
                    if (err) {
                        mainLogger("error", `WorkResumeBase->getProcessedWorkerShifts->->getAllWorkers->async.each tenantArr. Error: ${err.message}`);
                        return reject(err);
                    }
                    mainLogger("info", 'WorkResumeBase->getProcessedWorkerShifts->successful done');
                    return resolve(processedWorkerShifts);
                });
            });
        });
    }

    /**
     * Get processed worker unblock timers at node with id = nodeId
     * @param nodeId
     */
    static getProcessedWorkerUnblockTimers(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", `WorkResumeBase->getProcessedWorkerUnblockTimers called!`);
            const processedWorkerUnblockTimers = [];
            sqlManager.getAllActiveBlocks((err, blockRows) => {
                if (err) {
                    mainLogger("error", `WorkResumeBase->getProcessedWorkerUnblockTimers->getAllActiveBlocks->Error: ${err.message}`);
                    return reject(err);
                }
                if (Array.isArray(blockRows) && blockRows.length < 1) {
                    mainLogger("info", `WorkResumeBase->getProcessedWorkerUnblockTimers->getAllActiveBlocks->No active blocks`);
                    return resolve(processedWorkerUnblockTimers);
                }
                async.each(blockRows, (blockData, blockCb) => {
                    try {
                        mainLogger("info", `WorkResumeBase->getProcessedWorkerUnblockTimers->block row: ${JSON.stringify(blockData)}`);
                        let blockId = parseInt(blockData.block_id);
                        let shiftId = parseInt(blockData.shift_id);
                        let typeBlock = blockData.type_block;
                        let workerId = parseInt(blockData.worker_id);
                        let endBlock = parseInt(blockData.end_block);
                        let nowTime = parseInt((new Date()).getTime() / 1000);
                        let blockNodeId = parseInt(blockData.server_id);
                        let timeToUnblock;
                        if (endBlock >= nowTime) {
                            timeToUnblock = (endBlock - nowTime) * 1000;
                        } else {
                            timeToUnblock = 1000;
                        }
                        if (nodeId === blockNodeId) {
                            sqlManager.getWorkerById(workerId, (err, workerData) => {
                                if (err) {
                                    mainLogger("error", `WorkResumeBase->getAllActiveBlocks->getWorkerById->WorkerId: ${workerId}->Error: ${err.message}`);
                                    return blockCb(null, 1);
                                }
                                if (!workerData) {
                                    return blockCb(null, 1);
                                }
                                const tenantId = workerData.tenant_id;
                                const workerCallsign = workerData.callsign;
                                processedWorkerUnblockTimers.push({
                                    tenantId,
                                    workerCallsign,
                                    shiftId,
                                    typeBlock,
                                    blockId,
                                    timeToUnblock
                                });
                                return blockCb(null, 1);
                            })
                        } else {
                            return blockCb(null, 1);
                        }
                    } catch (err) {
                        if (err) {
                            mainLogger("error", `WorkResumeBase->getProcessedWorkerUnblockTimers->getAllActiveBlocks->async.each blockRows->catch Error: ${err.message}`);
                        }
                        return blockCb(null, 1);
                    }
                }, (err) => {
                    if (err) {
                        mainLogger("error", `WorkResumeBase->getProcessedWorkerUnblockTimers->getAllActiveBlocks->async.each blockRows->Error: ${err.message}`);
                        return reject(err);
                    }
                    mainLogger("info", 'WorkResumeBase->getProcessedWorkerUnblockTimers->Worker block timers loaded to engine successful');
                    return resolve(processedWorkerUnblockTimers);
                })
            });
        });
    }
};


