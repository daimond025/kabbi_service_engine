"use strict";

const request = require('request');
const config = require("../lib").getConfig();


/**
 * Send order to engine
 * @param {Number}  tenantId
 * @param {Number}  orderId
 * @param {Number}  isFromLoader - (1/0) Was it order add after restart node process(isFromLoader = 1), or from rest api
 * @param {Function} callback
 */
function newOrderAuto(tenantId, orderId, isFromLoader, callback) {
    const getString =
        "order_id=" + orderId +
        "&tenant_id=" + tenantId +
        "&is_from_loader=" + isFromLoader;

    const requesOptions = {
        url: "http://" + config.MY_BIND_HOST + ":" + config.MY_BIND_PORT + "/neworder_auto?" + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requesOptions, resultData);
}
exports.newOrderAuto = newOrderAuto;

