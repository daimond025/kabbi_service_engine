const lib = require("./lib");
const config = lib.getConfig();
const redis = require("redis");
const retryStrategy = require("../database/redisDB/redisRetryStrategy");

/**
 * Event publisher
 * @param {Number} port
 * @param {String} ip
 */
function EventPublisher(port, ip) {
    this.redisClientDb = redis.createClient(port, ip, {
        retry_strategy: options => retryStrategy(options)

    });
    this.redisClientDb.on("EventPublisher connect", () => {
    });
    this.redisClientDb.on("EventPublisher reconnecting", () => {
    });

    this.redisClientDb.on("EventPublisher error", (err) => {
    });

}

/**
 * Publish event
 * @param {String} channel
 * @param {String|Object} message
 */
EventPublisher.prototype.publish = function (channel, message) {
    const self = this;
    self.redisClientDb.publish(channel, message);
};


module.exports = exports = new EventPublisher(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);