'use strict';

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");


/**
 * Order auto reject time out
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->Called`);
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderInstance.orderId, 'error', `orderRejectTimeOut->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                if (orderData && orderData.status && orderData.status.status_id && orderData.status.status_group) {
                    let currentStatusGroup = orderData.status.status_group;
                    let currentStatusId = orderData.status.status_id;
                    if ((currentStatusGroup === "new" && currentStatusId !== statusInfo.offer_order.status_id)
                        || (currentStatusGroup === "pre_order" && currentStatusId !== statusInfo.worker_accepted_preorder.status_id)) {
                        orderData.status_id = statusInfo.no_workers.status_id;
                        orderData.status.status_id = statusInfo.no_workers.status_id;
                        orderData.status.name = statusInfo.no_workers.name;
                        orderData.status.status_group = statusInfo.no_workers.status_group;
                        orderData.worker_id = null;
                        orderData.car_id = null;
                        orderData.car = null;
                        orderData.worker = null;
                        const updateTime = Math.floor(Date.now() / 1000);
                        orderData.update_time = updateTime;
                        orderData.status_time = updateTime;
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(orderInstance.orderId, 'error', `orderRejectTimeOut->redisOrderManager.saveOrder->Error: ${err.message}`);
                                return reject(err);
                            }
                            orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->redisOrderManager.saveOrder->Result: ${result}`);
                            sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.no_workers.status_id, null, null, updateTime, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, 'error', `orderRejectTimeOut->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                                }
                                orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                            });
                            const messageString = orderEventOrderMessage({
                                command: orderEventOrderCommand.updateOrderData,
                                tenant_id: orderInstance.tenantId,
                                order_id: orderInstance.orderId,
                                params: {
                                    status_id: statusInfo.no_workers.status_id
                                }
                            });
                            amqpOrderMessageSender.sendMessage(orderInstance.orderId, messageString, (err, result) => {
                                if (err) {
                                    orderLogger(orderInstance.orderId, 'error', `orderRejectTimeOut->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                                    return reject(err);
                                } else {
                                    orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderInstance.orderId}.Result: ${result}`);
                                    return resolve(true);
                                }
                            });
                        });
                    } else {
                        orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->Can't reject order by timer. Order status_id: ${currentStatusId}`);
                        return resolve(false);
                    }
                } else {
                    orderLogger(orderInstance.orderId, 'info', `orderRejectTimeOut->Bad order status data`);
                    return resolve(false);
                }
            });
        } catch (err) {
            return reject(err);

        }
    })
};
