'use strict';

const logger = require('../services/logger');
const workerBlockLogger = logger.workerBlockLogger;
const amqpWorkerPreOrderBlockMessageSender = require("../services/amqpWorkerPreOrderBlockMessageSender");
const uuid = require('uuid/v4');
const config = require("../services/lib").getConfig();
const nodeId = config.MY_ID;

/**
 * Unblock worker pre order ability
 * @param  {Number} blockedTenantId
 * @param  {Number} blockedCallsign
 * @param  {Number} blockId
 * @param  {Function} resultCallback
 */
module.exports = function (blockedTenantId, blockedCallsign, blockId, resultCallback) {
    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrder called!");
    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrder->block id= " + blockId);
    let publishData = JSON.stringify({
        uuid: uuid(),
        type: 'order_event',
        timestamp: Math.floor(Date.now() / 1000),
        sender_service: "engine_service",
        command: "worker_preorder_unblock",
        tenant_id: blockedTenantId,
        worker_callsign: blockedCallsign,
        change_sender_id: nodeId,
        params: {}
    });
    amqpWorkerPreOrderBlockMessageSender.sendMessage(blockId, publishData, (err, result) => {
        if (err) {
            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrder->amqpWorkerPreOrderBlockMessageSender.sendMessage->Error: ${err.message}`);
        } else {
            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', 'unblockWorkerPreOrder->Published event at channel:worker_preorder_block_' + blockId);
            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', publishData);
        }
    });
    return resultCallback(null, 1);
};