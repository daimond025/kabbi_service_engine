'use strict';

const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const logger = require('../services/logger');
const workerBlockLogger = logger.workerBlockLogger;
const ublockWorkerPreOrderTimer = require("./unblockWorkerPreOrderTimer");

/**
 * Block worker preorder
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {Function} resultCallback
 */
module.exports = function (tenantId, workerCallsign, resultCallback) {
    workerBlockLogger(tenantId, workerCallsign, 'info', "blockWorkerPreOrder called!");
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerPreOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
            return resultCallback(err);
        }
        if (!workerData) {
            return resultCallback(new Error('No worker at redis Db'));
        }
        const workerTimeOfBlock = workerData.worker.pre_order_block_houer * 60 * 60;
        const workerTimeOfEndBlock = parseInt((new Date()).getTime() / 1000 + workerTimeOfBlock);
        const workerShiftId = workerData.worker.worker_shift_id;
        const workerId = workerData.worker.worker_id;
        sqlManager.logWorkerBlock(workerId, workerShiftId, "pre_order", workerTimeOfEndBlock, (err, result) => {
            if (err) {
                workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerPreOrder->sqlManager.logWorkerBlock->Error: ${err.message}`);
                return resultCallback(err);
            }
            workerData.worker.preorder_end_block = workerTimeOfEndBlock;
            const blockId = result;
            redisWorkerManager.saveWorker(workerData, (err, result) => {
                if (err) {
                    workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerPreOrder->redisWorkerManager.saveWorker->Error: ${err.message}`);
                    return resultCallback(err);
                } else {
                    ublockWorkerPreOrderTimer(tenantId, workerCallsign, workerShiftId, blockId, workerTimeOfBlock * 1000, (err, result) => {
                        if (err) {
                            workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerPreOrder->ublockWorkerPreOrderTimer->Error: ${err.message}`);
                            return resultCallback(err);
                        }
                        return resultCallback(null, 1);
                    });
                }
            });

        });

    });
};