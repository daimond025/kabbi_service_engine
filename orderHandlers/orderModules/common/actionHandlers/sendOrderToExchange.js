"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;


/**
 * Send order to exchange
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<any>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'sendOrderToExchange called!');
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.sendToExchangeTimeoutLabel);
            return resolve(true);
        } catch (err) {
            return reject(err);
        }
    })
};
