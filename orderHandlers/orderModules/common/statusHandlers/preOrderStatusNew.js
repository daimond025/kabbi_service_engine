'use strict';

const orderNotificationManager = require('../orderNotificationManager');
const orderActionManager = require('../orderActionManager');
const preOrderWatcher = require('../preOrderWatcher');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderType = require("../../../orderEvent/orderDevice");

/**
 * PreOrder status new
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {Boolean} orderTimeIsChanged
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, orderTimeIsChanged) => {
    return new Promise((resolve, reject) => {
        try {
            const orderId = orderData.order_id;
            const typeOrder = orderData.device.toString();
            orderInstance.orderTimer.clearAll();
            if (orderInstance.statusChanged) {
                orderLogger(orderId, 'info', 'PreOrderStatusNew CALLED!');
                if (orderInstance.newStatusAlreadyWas === false) {
                    orderInstance.newStatusAlreadyWas = true;
                }
                if (orderData.worker && orderData.worker.callsign) {
                    orderActionManager.deleteWorkerFromPreOrder(orderData)
                        .then(result => {
                            orderLogger(orderId, 'info', `preOrderStatusNew->orderActionManager.deleteWorkerFromPreOrder->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `preOrderStatusNew->orderActionManager.deleteWorkerFromPreOrder->Error: ${err.message}`);
                        });
                }

            }

            if ((orderTimeIsChanged || orderInstance.statusChanged) && (typeOrder != orderType.HOSPITAL) ) {
                let startTime = 1;
                let restrictVisibilityOfPreOrder = orderInstance.settings.RESTRICT_VISIBILITY_OF_PRE_ORDER;
                let timeOfPreOrderVisibility = orderInstance.settings.TIME_OF_PRE_ORDER_VISIBILITY;


                if (restrictVisibilityOfPreOrder && timeOfPreOrderVisibility) {
                    let cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
                    const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(cityTimeOffset);
                    let orderTime = parseInt(orderData.order_time);
                    let timeDiffOfPreOrderVisibility = timeOfPreOrderVisibility * 60 * 60;
                    // If orderTime time at 15:00, and now 12:00, and setting=2 hours
                    //  12 + 2 < 15 => startTime = 15-12-2
                    if (tenantNowTime + timeDiffOfPreOrderVisibility < orderTime) {
                        startTime = orderTime - tenantNowTime - timeDiffOfPreOrderVisibility;
                    }
                }
                orderLogger(orderId, 'info', `preOrderStatusNew->orderNotificationManager.sendUpdatePreOrdersListToWorker will be called after ${startTime / 60} minutes `);
                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.preOrderUpdateListTimeoutLabel, () => {
                    orderNotificationManager.sendUpdatePreOrdersListToWorkers(
                        orderData,
                        {
                            sendPushFreeOrderForAll: orderInstance.settings.SEND_PUSH_PRE_ORDER_FOR_ALL,
                            dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                            exceptCarModels: orderInstance.exceptCarModels
                        })
                        .then(result => {
                            orderLogger(orderId, 'info', `preOrderStatusNew->orderNotificationManager.sendUpdatePreOrdersListToWorkers->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `preOrderStatusNew->orderNotificationManager.sendUpdatePreOrdersListToWorkers->Error: ${err.message}`);
                        })
                }, startTime * 1000);

            }
            orderActionManager.rejectOrderByTimeout(orderInstance, orderData)
                .then(result => {
                    orderLogger(orderId, 'info', `preOrderStatusNew->orderActionManager.rejectOrderByTimeout->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderId, 'info', `preOrderStatusNew->orderActionManager.rejectOrderByTimeout->Error: ${err.message}`);
                });
            preOrderWatcher.runWatcher(orderInstance, orderData);
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    });
};