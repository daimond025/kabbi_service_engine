'use strict';

const async = require('async');
const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const redisWorkerManager = require('../../../../database/redisDB/redisWorkerManager');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const orderDistributionManager = require('../../common/orderDistributionManager');
const orderApi = require('../../../../services/externalWebServices/orderApi');
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");
const redisAsyncAnswerManager = require('../../../../database/redisDB/redisAsyncAnswerManager');
const orderActionManager = require('../../../orderModules/common/orderActionManager');
const redisParkingManager = require('../../../../database/redisDB/redisParkingManager');
const workerStatusManager = require("../../../../services/workerStatusManager");
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventWorkerCommand = require('../../../orderEvent/orderEventWorkerCommand');

const mergeArray = require("../../../../services/lib").mergeArray;


// карта причин отклонения заказа
const refuseTimer = "timer";
const refuseDriver = "driver";
const refuseCauseTimer = [refuseTimer];
const refuseCauseDriver = [refuseDriver];

/**
 * Order offering timeout
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'OrderOfferTimeOut called!');
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderInstance.orderId, 'error', `orderOfferTimeOut->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                if (parseInt(orderData.status_id) !== statusInfo.offer_order.status_id) {
                    orderLogger(orderInstance.orderId, 'info', "orderOfferTimeOut->order is not offering now" + orderData.status_id);
                    return resolve(false);
                }

                const workersResponses = typeof orderData.workers_offered_responses === "object" ?
                    Object.keys(orderData.workers_offered_responses).map(key => orderData.workers_offered_responses[key]) : [];
                let allRefused = true;
                for (let response of workersResponses) {
                    //some worker try to get offered order
                    if (parseInt(response.params.status_id) === statusInfo.worker_accepted_order.status_id) {
                        allRefused = false; break;
                    }
                }

                // save refuse worker
                saveAndFormRefuseWorkers(orderData).then( rezult => {
                    orderLogger(orderInstance.orderId, 'info', `orderOfferTimeOut->workersResponses->${JSON.stringify(workersResponses)}`);
                    if (allRefused ) {
                        orderLogger(orderInstance.orderId, 'info', `orderOfferTimeOut->all workers refused from offer order`);
                        workersRefusedOfferOrder(orderData)
                            .then(result => {
                                orderLogger(orderInstance.orderId, 'info', `orderOfferTimeOut->workersRefusedOfferOrder->result:${result}`);
                                return resolve(true);
                            })
                            .catch(err => {
                                orderLogger(orderInstance.orderId, 'error', `orderOfferTimeOut->workersRefusedOfferOrder->Error: ${err.message}`);
                                return reject(err);
                            })
                    } else {
                        const offerOrderMode = orderInstance.settings.OFFER_ORDER_TYPE ? orderInstance.settings.OFFER_ORDER_TYPE : this.inRotationMode;
                        const orderDistributionType = orderInstance.settings.TYPE_OF_DISTRIBUTION_ORDER;
                        const offerOrderWorkersCount = offerOrderMode === this.inRotationMode ? 1 : orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT ? orderInstance.settings.OFFER_ORDER_WORKER_BATCH_COUNT : 2;
                        const checkWorkerRating = orderInstance.settings.CHECK_WORKER_RATING_TO_OFFER_ORDER;
                        const promoWorkerId = orderInstance.promoWorkerId;

                        const options = {
                            offerOrderMode: offerOrderMode,
                            orderDistributionType: orderDistributionType,
                            offerWorkersCount: offerOrderWorkersCount,
                            checkWorkerRating: checkWorkerRating,
                            promoWorkerId: promoWorkerId,
                        };
                        workersAcceptOfferOrder(orderData, options)
                            .then(result => {
                                orderLogger(orderInstance.orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->result:${result}`);
                                return resolve(true);
                            })
                            .catch(err => {
                                orderLogger(orderInstance.orderId, 'error', `orderOfferTimeOut->workersAcceptOfferOrder->Error: ${err.message}`);
                                orderActionManager.sendOrderToFreeStatus(orderData, false)
                                    .then(result => {
                                        orderLogger(orderInstance.orderId, 'info', `orderOfferTimeOut->orderActionManager.sendOrderToFreeStatus->result:${result}`);
                                        return resolve(true);
                                    })
                                    .catch(err => {
                                        orderLogger(orderInstance.orderId, 'error', `orderOfferTimeOut->orderActionManager.sendOrderToFreeStatus->Error:${err.message}`);
                                        return reject(err);
                                    });
                            })
                    }

                });
            })
        } catch (err) {
            return reject(err);
        }
    })
};

/**
 *
* form and save  workers who refuse with cause
* @param orderData
* @returns {Promise<[]>}
*/
function saveAndFormRefuseWorkers(orderData) {

    const workersResponses = typeof orderData.workers_offered_responses === "object" ?
        Object.keys(orderData.workers_offered_responses).map(key => orderData.workers_offered_responses[key]) : [];

    const workersRefuseResponses = workersResponses.filter(response => {
        return parseInt(response.params.status_id) !== statusInfo.worker_accepted_order.status_id
    });

    const workersRefuseAll = typeof orderData.refuse_workers_list === "object" ?
        Object.keys(orderData.refuse_workers_list).map(key => orderData.refuse_workers_list[key]) : [];

    return new Promise( (resolve, reject) => {
        async.each(workersRefuseResponses, (workerRefuseCircle, callback) => {

            let workerCallSign = ((typeof workerRefuseCircle.change_sender_id !== undefined) && (workerRefuseCircle.change_sender_id.length !== 0)) ? parseInt(workerRefuseCircle.change_sender_id) : null;
            let workerRefuseCause = ((typeof workerRefuseCircle.params.order_refuse !== undefined) && (workerRefuseCircle.params.order_refuse.length !== 0)) ? workerRefuseCircle.params.order_refuse : refuseTimer;
            let workerRefuseTime = ((typeof workerRefuseCircle.time !== undefined)) ? workerRefuseCircle.time : Math.floor(Date.now() / 1000);
            let workerRefuseDriver = refuseCauseDriver.includes(workerRefuseCause);

            if (workerCallSign !== null) {
                let find_in_all = 0;
                let need_replace = -1;

                workersRefuseAll.forEach(function (workerRefuseAllItem, i) {
                    let workerCallSignAll = parseInt(workerRefuseAllItem.workerCallsign);
                    let workerRefuseCauseAll = workerRefuseAllItem.refuse_subject;
                    let workerRefuseDriverAll = refuseCauseDriver.includes(workerRefuseCauseAll);

                    if (parseInt(workerCallSignAll) === parseInt(workerCallSign)) {
                        find_in_all = 1;
                        if ((!workerRefuseDriverAll && workerRefuseDriver)) {
                            find_in_all = 0;
                            need_replace = i;
                        }
                    }

                });


                if (find_in_all === 0) {
                    getWorkerInfo(orderData, workerCallSign, workerRefuseCause, workerRefuseTime).then(rezult => {
                        if(rezult.length == 0 ){
                            return callback(null, 1);
                        }

                        if ((need_replace === (-1))) {
                            workersRefuseAll.push(rezult);
                        }
                        if (need_replace >= 0) {
                            workersRefuseAll[need_replace] = rezult;
                        }
                        return callback(null, 1);
                    });
                }else {
                    return callback(null, 1);
                }
            }

        }, (err) => {
            if (err) {
                orderLogger(orderData.order_id, 'error', `orderOfferTimeOut->getWorkersInfoByDistance->Error: ${err.message}`);
            }
            if(workersRefuseAll.length > 0){
                orderData.refuse_workers_list = workersRefuseAll;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    return resolve(1);
                });
            } else {
                return resolve(1);
            }

        });

    });

}



function getWorkerInfo(orderData, workerCallSign, refusSubject, workerRefuseTime) {
    return  new Promise((resolve, reject) => {
        const workerInfo = {};
        const tenantId = parseInt(orderData.tenant_id);
        const orderId = parseInt(orderData.order_id);
        redisWorkerManager.getWorker(tenantId, workerCallSign, (err, workerData) => {
            if (err) {
                orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkerInfo->Error: ${err.message}`);

                workerInfo.shift_id = 0;
                workerInfo.order_id = orderId;
                workerInfo.refuse_time = workerRefuseTime;
                workerInfo.refuse_subject = refusSubject;
                workerInfo.worker_id = -1;
                workerInfo.workerCallsign = workerCallSign;
                return resolve(workerInfo);
            }

            let workerShiftId = workerData.worker.worker_shift_id;
            let worker_id = workerData.worker.worker_id;
            try {
                workerInfo.shift_id = workerShiftId;
                workerInfo.order_id = orderId;
                workerInfo.refuse_time = workerRefuseTime;
                workerInfo.refuse_subject = refusSubject;
                workerInfo.worker_id = worker_id;
                workerInfo.workerCallsign = workerCallSign;
            } catch (err) {
                return resolve(workerInfo);
            }
            return resolve(workerInfo);
        });


    });
}

/**
 * Get workers info by distance
* @param orderData
* @param workersResponses
* @returns {Promise<[]>}
*/
function getWorkersInfoByDistance(orderData, workersResponses) {
    return new Promise((resolve, reject) => {
        const workersInfo = [];
        const tenantId = parseInt(orderData.tenant_id);
        const orderId = parseInt(orderData.order_id);
        async.each(workersResponses, (response, callback) => {
            const workerCallsign = parseInt(response.change_sender_id);
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByDistance->Error: ${err.message}`);
                    return callback(null, 1);
                }
                let workerDistanceToOrder;
                try {
                    workerDistanceToOrder = orderDistributionManager.distanceFromWorkerToOrder(orderData, workerData);
                    workerData.distance_to_order = workerDistanceToOrder;
                } catch (err) {
                    orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByDistance->Error: ${err.message}`);
                }
                workersInfo.push(orderDistributionManager.getWorkerInfo(workerData));
                return callback(null, 1);
            })
        }, (err) => {
            if (err) {
                orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByDistance->Error: ${err.message}`);
            }
            return resolve(workersInfo);
        })
    });
}

/**
 * Get workers info by distance
 * @param orderData
 * @param workersResponses
 * @returns {Promise<[]>}
 */
function getWorkerIdsNegativeResponse( orderData, workersResponses) {
    return new Promise((resolve, reject) => {
        const orderId = parseInt(orderData.order_id);
        const workersInfo = [];

        async.each(workersResponses, (response, callback) => {
            const workerCallsign = parseInt(response.change_sender_id);
            if(workerCallsign > 0){
                workersInfo.push(workerCallsign);
            }
            return callback(null, 1);
        }, (err) => {
            if (err) {
                orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByDistance->Error: ${err.message}`);
            }
            return resolve(workersInfo);
        })
    });
}


/**
 * Get workers info by parking
 * @param orderData
 * @param workersResponses
 * @returns {Promise<[]>}
 */
function getWorkersInfoByParking(orderData, workersResponses) {
    return new Promise((resolve, reject) => {
        const workersInfo = [];
        const tenantId = parseInt(orderData.tenant_id);
        const orderId = parseInt(orderData.order_id);
        const parkingId = orderData.parking_id;
        redisParkingManager.getAllWorkersOnParking(parkingId, (err, workersAtParking) => {
            if (err) {
                orderLogger(orderId, "error", `orderOfferTimeOut->redisWorkerManager.getAllWorkersOnParking->Error: ${err.message}`);
            }
            workersAtParking = Array.isArray(workersAtParking) ? workersAtParking : [];
            async.each(workersResponses, (response, callback) => {
                const workerCallsign = parseInt(response.change_sender_id);
                redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByParking->Error: ${err.message}`);
                        return callback(null, 1);
                    }
                    const index = workersAtParking.indexOf(workerCallsign.toString());
                    if (index !== -1) {
                        workerData.parking_index = index;
                    }
                    workersInfo.push(orderDistributionManager.getWorkerInfo(workerData));
                    return callback(null, 1);
                })
            }, (err) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderOfferTimeOut->getWorkersInfoByParking->Error: ${err.message}`);
                }
                return resolve(workersInfo);
            })
        });

    });
}

/**
 * Some of workers try to get offered order
 * @param {object} orderData
 * @param {object} options
 * @param {string} options.offerOrderMode
 * @param {number} options.orderDistributionType
 * @param {number} options.offerWorkersCount
 * @param {boolean} options.checkWorkerRating
 * @param {number} options.promoWorkerId
 * @returns {Promise<boolean>}
 */
function workersAcceptOfferOrder(orderData, options) {
    return new Promise(async (resolve, reject) => {
        const tenantId = parseInt(orderData.tenant_id);
        const tenantLogin = orderData.tenant_login;
        const orderId = parseInt(orderData.order_id);
        const workersResponses = typeof orderData.workers_offered_responses === "object" ?
            Object.keys(orderData.workers_offered_responses).map(key => orderData.workers_offered_responses[key]) : [];
        const workersPositiveResponses = workersResponses.filter(response => {
            return parseInt(response.params.status_id) === statusInfo.worker_accepted_order.status_id
        });

        const workersNegativeResponses = workersResponses.filter(response => {
            return parseInt(response.params.status_id) !== statusInfo.worker_accepted_order.status_id        });
        const workersIdsNegativeResponses = await getWorkerIdsNegativeResponse(orderData, workersNegativeResponses);

        let offeredWorkersList = typeof orderData.workers_offered === "object" ?
            Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];

        orderLogger(orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->workersPositiveResponses: ${JSON.stringify(workersPositiveResponses)}`);

        let sortedWorkers = [];
        try {
            if (options.orderDistributionType === orderDistributionManager.distanceType) {
                let workersInfo = await getWorkersInfoByDistance(orderData, workersPositiveResponses);
                sortedWorkers = orderDistributionManager.sortRelevantWorkersByDistance(orderId, workersInfo, options);
            } else {
                let workersInfo = await getWorkersInfoByParking(orderData, workersPositiveResponses);
                sortedWorkers = orderDistributionManager.sortRelevantWorkersByParking(orderId, workersInfo, options);
            }
        } catch (err) {
            orderLogger(orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->Error: ${err.message}`);
        }


        if (sortedWorkers.length < 1) {
            return reject(new Error("Cant assign order, no sorted workers"));
        }

        /**
         * Try recursively assign worker from sorted workers
         * @param sortedWorkers
         * @param tenantLogin
         */
        function assignOrder(sortedWorkers, tenantLogin) {
            orderLogger(orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->result of sorting positive worker responses: ${JSON.stringify(sortedWorkers, (key, value) => {
                if (key === "worker_worker" || key === "worker_car") {
                    return '';
                }
                return value;
            }, 4)}`);
            if (sortedWorkers.length === 0) {
                return reject(new Error("Can not assign order to any worker"));
            }
            const topWorker = sortedWorkers.shift();
            const topWorkerCallsign = topWorker.worker_callsign;
            const taskObject = workersResponses.find(response => {
                return parseInt(response.change_sender_id) === topWorkerCallsign
            });

            let senderService = taskObject.sender_service;
            taskObject.command = orderEventOrderCommand.updateOrderData;
            orderLogger(orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->task will be send to orderApi: ${JSON.stringify(taskObject)}`);
            orderApi.sendRequest(taskObject, (err, requestResult) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderOfferTimeOut->workersAcceptOfferOrder->orderApi.sendRequest->Error: ${err.message}`);
                    return assignOrder(sortedWorkers);
                }
                orderLogger(orderId, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->orderApi.sendRequest->Result: ${JSON.stringify(requestResult)}`);
                const isGoodOrderAnswer = orderApi.isGoodResult(requestResult, senderService);
                if (!isGoodOrderAnswer) {
                    return assignOrder(sortedWorkers);
                }

                /// лог события с участием расстояния
                let worker_assigend_distance =  (typeof topWorker.worker_distance_to_order !== undefined) ?  topWorker.worker_distance_to_order : 0;
                sqlManager.logOrderChangeData(tenantId, orderId, 'status', topWorkerCallsign, 'worker', workerStatusManager.onOrder + "_" + "POSITION" + "_" + worker_assigend_distance , 'system', (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `orderOfferTimeOut->workersAcceptOfferOrder->workerDistance->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, "info", `orderOfferTimeOut->workersAcceptOfferOrder->workerDistance->Result: ${result}`);
                    }
                });

                sendToOrderUpdateEvent(tenantId, orderId)
                    .then(result => {
                        orderLogger(orderData.order_id, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->sendToOrderUpdateEvent->Result: ${result}`);
                        sendToWorkerOkResultEvent(tenantId, orderId, topWorkerCallsign, requestResult);

                        // всем высылаем - отказ от заказа
                        for (let offeredWorker of offeredWorkersList) {
                            if (parseInt(offeredWorker) !== parseInt( topWorkerCallsign)) {
                                if(!workersIdsNegativeResponses.includes(offeredWorker)){
                                    sendToWorkerOrderIsBusyEvent(orderId, tenantId, tenantLogin, offeredWorker);
                                }
                            }
                        }
                        return resolve(result);
                    })
                    .catch(err => {
                        orderLogger(orderData.order_id, 'info', `orderOfferTimeOut->workersAcceptOfferOrder->sendToOrderUpdateEvent->Error: ${err.message}`);
                        return reject(err);
                    });
            });
        }

        assignOrder(sortedWorkers, tenantLogin);
    })
}

/**
 * All worker refused or ignored offer order
 * @param orderData
 * @returns {Promise<boolean>}
 */
function workersRefusedOfferOrder(orderData) {
    return new Promise((resolve, reject) => {
        orderData.status_id = statusInfo.worker_ignored_offer_order.status_id;
        orderData.status.status_id = statusInfo.worker_ignored_offer_order.status_id;
        orderData.status.name = statusInfo.worker_ignored_offer_order.name;
        const updateTime = Math.floor(Date.now() / 1000);
        orderData.update_time = updateTime;
        orderData.status_time = updateTime;
        redisOrderManager.saveOrder(orderData, (err, result) => {
            if (err) {
                orderLogger(orderData.order_id, 'error', `orderOfferTimeOut->workersRefusedOfferOrder->redisOrderManager.saveOrder->Error: ${err.message}`);
                return reject(err);
            }
            sqlManager.updateOrderFromMysql(orderData.order_id, statusInfo.worker_ignored_offer_order.status_id, orderData.worker_id, orderData.car_id, updateTime, (err, result) => {
                if (err) {
                    orderLogger(orderData.order_id, "error", `orderOfferTimeOut->workersRefusedOfferOrder->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                } else {
                    orderLogger(orderData.order_id, "info", `orderOfferTimeOut->workersRefusedOfferOrder->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                }
            });
            sendToOrderUpdateEvent(orderData.tenant_id, orderData.order_id,)
                .then(result => {
                    orderLogger(orderData.order_id, 'info', `orderOfferTimeOut->workersRefusedOfferOrder->sendToOrderUpdateEvent->Result: ${result}`);
                    return resolve(result);
                })
                .catch(err => {
                    orderLogger(orderData.order_id, 'info', `orderOfferTimeOut->workersRefusedOfferOrder->sendToOrderUpdateEvent->Error: ${err.message}`);
                    return reject(err);
                });
        })
    })
}

/**
 *
 * @param tenantId
 * @param orderId
 * @param workerCallsign
 * @param requestResult
 */
function sendToWorkerOkResultEvent(tenantId, orderId, workerCallsign, requestResult) {
    let messageTTL = 900;
    // Запишем ответ в редис
    redisAsyncAnswerManager.setAsyncAnswer(requestResult.uuid, requestResult, messageTTL, (err, result) => {
        if (err) {
            orderLogger(orderId, 'error', `orderOfferTimeOut->sendToWorkerOkResultEvent->redisAsyncAnswerManager.setAsyncAnswer->Error: ${err.message}`);
        } else {
            orderLogger(orderId, 'info', `orderOfferTimeOut->sendToWorkerOkResultEvent->redisAsyncAnswerManager.setAsyncAnswer->Result: ${result}`);
        }
    });
    //также отправим результат по сокету
    const messageString = JSON.stringify(requestResult);
    messageTTL = 300;
    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTTL, (err, result) => {
        if (err) {
            orderLogger(orderId, "error", `orderOfferTimeOut->sendToWorkerOkResultEvent->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
        } else {
            orderLogger(orderId, 'info', `orderOfferTimeOut->sendToWorkerOkResultEvent->amqpWorkerMessageSender.sendMessage->published message: ${messageString} to worker: ${workerCallsign}. Result: ${result} `);
        }
    });
}


/**
 * Send order is busy answer of task handling to worker
 * @param orderId
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 */
function sendToWorkerOrderIsBusyEvent(orderId, tenantId, tenantLogin, workerCallsign) {

    let messageString = orderEventWorkerMessage({
        command: orderEventWorkerCommand.rejectWorkerFromOrder,
        tenant_id: tenantId,
        tenant_login: tenantLogin,
        worker_callsign: workerCallsign,
        params: {
            order_id: orderId
        }
    });

    const messageTtl = 60 * 60 * 24 * 10; // 10 days
    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
        if (err) {
            orderLogger(orderId, 'info', `orderOfferTimeOut->orderActionManager->deleteWorkerFormOrder->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
        } else {
            orderLogger(orderId, 'info', `orderOfferTimeOut->orderActionManager->deleteWorkerFormOrder->amqpWorkerMessageSender.sendMessage->published command: ${orderEventWorkerCommand.rejectWorkerFromOrder} to worker ${deleteWorkerCallsign}.Result: ${result}`);
        }
    });
}

/**
 * Send order is updated to order queue
 * @param tenantId
 * @param orderId
 * @returns {Promise<any>}
 */
function sendToOrderUpdateEvent(tenantId, orderId) {
    return new Promise((resolve, reject) => {
        const orderTask = orderEventOrderMessage({
            command: orderEventOrderCommand.updateOrderData,
            tenant_id: tenantId,
            order_id: orderId,
            params: {}
        });
        amqpOrderMessageSender.sendMessage(orderId, orderTask, (err, result) => {
            if (err) {
                return reject(err);
            }
            return resolve(result);
        });
    });
}



