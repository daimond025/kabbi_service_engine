'use strict';

const logger = require('../../../../services/logger');
const async = require('async');
const orderLogger = logger.orderLogger;
const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const redisWorkerManager = require('../../../../database/redisDB/redisWorkerManager');
const orderDistributionManager = require("../../common/orderDistributionManager");
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const blockWorkerOrder = require("../../../../workerBlockHandlers/blockWorkerOrder");
const workerStatusManager = require("../../../../services/workerStatusManager");
const mergeArray = require("../../../../services/lib").mergeArray;


/**
 * Order status refuse offer order
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            const orderId = parseInt(orderInstance.orderId);
            const tenantId = parseInt(orderInstance.tenantId);
            orderLogger(orderId, 'info', 'orderStatusRefuseOfferOrder called!');
            let offeredWorkersList = typeof orderData.workers_offered === "object" ?
                Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];
            //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
            if (typeof orderData.offered_workers_list === "object") {
                offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
            }
            const workerCallsign = orderData.worker ? orderData.worker.callsign : null;
            let workerList = workerCallsign ? [workerCallsign] : [];
            offeredWorkersList = mergeArray(offeredWorkersList, workerList);
            orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->order offered workers list: ${JSON.stringify(offeredWorkersList)}`);
            const offerOrderMode = orderInstance.settings.OFFER_ORDER_TYPE ? orderInstance.settings.OFFER_ORDER_TYPE : orderDistributionManager.inRotationMode;
            orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->order has offer order type: ${offerOrderMode}`);
            const needAddWorkerRejectCounters = offerOrderMode === orderDistributionManager.inRotationMode;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel);
            orderData.worker_id = null;
            orderData.car_id = null;
            orderData.worker = null;
            orderData.car = null;
            orderData.workers_offered = [];
            orderData.workers_offered_responses = [];
            const updateTime = Math.floor(Date.now() / 1000);
            orderData.update_time = updateTime;
            orderData.status_time = updateTime;
            sqlManager.updateOrderFromMysql(orderId, orderData.status_id, null, null, updateTime, (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderStatusRefuseOfferOrder->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                } else {
                    orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                }
            });
            if (offeredWorkersList.length) {
                async.each(offeredWorkersList,
                    (workerCallsign, callback) => {
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusRefuseOfferOrder->redisWorkerManager.getWorker->Error of getting worker ${workerCallsign}. Error: ${err.message}`);
                                return callback(null, 1)
                            }
                            if (workerData.worker.status === workerStatusManager.offerOrder && parseInt(workerData.worker.last_offered_order_id) === orderId) {
                                let needBlockWorker = false;
                                workerData.worker.status = workerStatusManager.free;
                                if (needAddWorkerRejectCounters) {
                                    const workerShiftId = workerData.worker.worker_shift_id;
                                    sqlManager.logWorkerRejectedOrderOfferCount(workerShiftId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusRefuseOfferOrder->sqlManager.logWorkerRejectedOrderOfferCount->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->sqlManager.logWorkerRejectedOrderOfferCount->Result: ${result}`);
                                        }
                                    });
                                    //add summary reject counter
                                    if (workerData.worker.summ_reject !== null && typeof workerData.worker.summ_reject !== "undefined") {
                                        workerData.worker.summ_reject = workerData.worker.summ_reject + 1;
                                    }
                                    else {
                                        workerData.worker.summ_reject = 1;
                                    }
                                    // add in row reject counter
                                    if (workerData.worker.in_row_reject !== null && typeof workerData.worker.in_row_reject !== "undefined") {
                                        workerData.worker.in_row_reject = workerData.worker.in_row_reject + 1;
                                    }
                                    else {
                                        workerData.worker.in_row_reject = 1;
                                    }


                                    const workerTypeBlock = workerData.worker.worker_type_block;
                                    if (workerTypeBlock === "IN_SHIFT") {
                                        const workerCountToBlock = workerData.worker.worker_count_to_block;
                                        if (parseInt(workerData.worker.summ_reject) === parseInt(workerCountToBlock) || workerData.worker.summ_reject > workerCountToBlock) {
                                            needBlockWorker = true;
                                        }
                                    }
                                    if (workerTypeBlock === "IN_ROW") {
                                        const workerCountToBlock = workerData.worker.worker_count_to_block;
                                        if (parseInt(workerData.worker.in_row_reject) === parseInt(workerCountToBlock) || workerData.worker.in_row_reject > workerCountToBlock) {
                                            needBlockWorker = true;
                                        }
                                    }
                                }
                                sqlManager.logOrderChangeData(tenantId, orderId, 'status', workerData.worker.callsign, 'worker', workerData.worker.status, 'system', (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, "error", `orderStatusRefuseOfferOrder->sqlManager.logOrderChangeData->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, "info", `orderStatusRefuseOfferOrder->sqlManager.logOrderChangeData->Result: ${result}`);
                                    }
                                });
                                if (needBlockWorker) {
                                    orderLogger(orderId, "info", `orderStatusRefuseOfferOrder->Need block worker ${workerData.worker.callsign}`);
                                    blockWorkerOrder(tenantId, workerData.worker.callsign, orderId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, "error", `orderStatusRefuseOfferOrder->blockWorkerOrder->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, "info", `orderStatusRefuseOfferOrder->blockWorkerOrder->Result: ${result}`);
                                        }
                                        return callback(null, 1);
                                    });
                                } else {
                                    redisWorkerManager.saveWorker(workerData, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, "error", `orderStatusRefuseOfferOrder->redisWorkerManager.saveWorker->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, "info", `orderStatusRefuseOfferOrder->redisWorkerManager.saveWorker->Result: ${result}`);
                                        }
                                        return callback(null, 1);
                                    });
                                }
                            } else {
                                return callback(null, 1);
                            }
                        });
                    }, (err) => {
                        if (err) {
                            orderLogger(orderId, "error", `orderStatusRefuseOfferOrder->async.each workers->Error: ${err.message}`);
                        }
                        redisOrderManager.saveOrder(orderData, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusRefuseOfferOrder->redisOrderManager.saveOrder->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->redisOrderManager.saveOrder->Result: ${result}`);
                            }
                            orderDistributionManager.runDistributionProcess(orderInstance, orderData);
                            return resolve(1);
                        });
                    })
            } else {
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderStatusRefuseOfferOrder->redisOrderManager.saveOrder->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderStatusRefuseOfferOrder->redisOrderManager.saveOrder->Result: ${result}`);
                    }
                    orderDistributionManager.runDistributionProcess(orderInstance, orderData);
                    return resolve(1);
                });
            }
        } catch (err) {
            return reject(err);
        }
    })
};
