'use strict';

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderNotificationManager = require('../orderNotificationManager');
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const orderActionManager = require('../orderActionManager');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");

/**
 * Order status free hospital order for all
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {*}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusFreeHospitalForAll called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;

            orderInstance.orderTimer.clearAll();

            const nowTime = parseInt((new Date()).getTime() / 1000);
            const orderExpiredTime = !isNaN(parseFloat(orderData.order_time)) ? parseFloat(orderData.order_time) : 0;
            const timeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
            const timeDiff = parseInt(orderExpiredTime) - (parseInt(nowTime) + parseInt(timeOffset));
            orderLogger(orderId, 'info', `orderStatusFreeOrder->OrderLateTimeout will bew called after ${timeDiff / 60} min`);


            if (orderData.worker && orderData.worker.callsign) {
                const workerCallsign = orderData.worker.callsign;
                const workerAssignedNewStatuses = [
                    statusInfo.worker_assigned_at_order_soft.status_id,
                    statusInfo.worker_assigned_at_order_hard.status_id
                ];
                const workerAssignedPreStatuses = [
                    statusInfo.worker_assigned_at_preorder_soft.status_id,
                    statusInfo.worker_assigned_at_preorder_hard.status_id,
                    statusInfo.worker_accepted_preorder.status_id
                ];
                let orderWasAssigned = false;
                let penultimateStatus;
                let isPreOrder = false;
                if (orderInstance.statusArr.length >= 2 && orderInstance.statusArr[orderInstance.statusArr.length - 2]) {
                    penultimateStatus = orderInstance.statusArr[orderInstance.statusArr.length - 2];
                    //Заказ был назначен
                    if (workerAssignedNewStatuses.indexOf(penultimateStatus) !== -1 || workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                    }
                    if (workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                        isPreOrder = true;
                    }
                }
                if (orderWasAssigned) {
                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder: isPreOrder,
                        isWorkerEventOwner: false,
                    });
                } else{
                    orderActionManager.deleteWorkersFromOrder(orderData)
                    .then(result => {
                        orderLogger(orderId, "info", `orderStatusFreeOrder->deleteWorkersFromOrder->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderStatusFreeOrder->deleteWorkersFromOrder->Error: ${err.message}`);
                    });
                }
                if (orderInstance.workerLateTimestamp !== null && (orderInstance.statusArr[orderInstance.statusArr.length - 2] === statusInfo.worker_coming_later.status_id)) {
                    try {
                        const lateTime = Math.floor(Date.now() / 1000) - orderInstance.workerLateTimestamp;
                        orderInstance.workerLateTimestamp = null;
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                orderLogger(orderId, "error", `orderStatusFreeOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
                            } else {
                                const workerShiftId = workerData.worker.worker_shift_id;
                                sqlManager.logWorkerLateTime(workerShiftId, lateTime, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Result: ${result}`);
                                    }
                                });
                            }
                        })
                    } catch (err) {
                        orderLogger(orderId, "error", `orderStatusFreeOrder->Error: ${err.message}`);
                    }
                }
            }

            // note fore all - free status
            /*async.parallel({
                orderData: callback => {
                    redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                        if (err) {
                            callback(err);
                        } else {
                            callback(null, orderData);
                        }
                    })

                    serviceApi.sendOrderToStatistic(this.tenantId, this.orderId, (err, result) => {
                        if (err) {
                            orderLogger(this.orderId, 'error', `baseGootaxOrder->initOrder->sendOrderToStatistic->Error: ${err.message}`);
                            return callback(null, 1);
                        } else {
                            orderLogger(this.orderId, 'info', `baseGootaxOrder->initOrder->sendOrderToStatistic->Result: ${result}`);
                            return callback(null, 1);
                        }
                    })
                },
            }, (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderDistributionManager->canOfferOrderForWorker>Error: ${err.message}`);
                    reject(err);
                } else {
                    let orderData = result.orderData;

                    this.isGoodWorkerForOrder(orderData, workerData, {
                        needCalcDistance: false,
                        distanceForSearch: null,
                        needCheckSocket: false,
                        needCheckStatus: false
                    })
                    .then(result => {
                        orderLogger(orderId, 'info', `orderDistributionManager->canOfferOrderForWorker>Result: ${JSON.stringify(result)}`);
                        resolve(result);
                    })
                    .catch(err => {
                        orderLogger(orderId, 'error', `orderDistributionManager->canOfferOrderForWorker>Error: ${err.message}`);
                        reject(err);
                    })
                }
            });*/

            console.log("СВОБОДНЫЙ больничный заказ");

            orderActionManager.rejectOrderByTimeout(orderInstance, orderData)
            .then(result => {
                orderLogger(orderId, 'info', `orderStatusFreeOrder->orderActionManager.rejectOrderByTimeout->Result: ${result}`);
            })
            .catch(err => {
                orderLogger(orderId, 'info', `orderStatusFreeOrder->orderActionManager.rejectOrderByTimeout->Error: ${err.message}`);
            });
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};

