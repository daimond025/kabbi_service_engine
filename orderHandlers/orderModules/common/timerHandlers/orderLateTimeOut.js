'use strict';

const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require("../../../orderEvent/orderEventOrderMessage");
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");


/**
 * Order late time out
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', `OrderLateTimeout TIMEOUT!`);
            redisOrderManager.getOrder(orderInstance.tenantId, orderInstance.orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderInstance.orderId, 'error', `orderLateTimeout->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                if (!orderData) {
                    orderLogger(orderInstance.orderId, 'info', 'orderLateTimeout->redisOrderManager.getOrder->No order at redisDB');
                    return reject(new Error('No order at redisDB'));
                }
                if (orderData.worker_id) {
                    orderLogger(orderInstance.orderId, 'info', `orderLateTimeout->redisOrderManager.getOrder->Bad order status: ${orderData.status_id}`);
                    return reject(new Error(`Bad order status: ${orderData.status_id}`));
                }
                orderData.status_id = statusInfo.outdated_order.status_id;
                orderData.status.status_id = statusInfo.outdated_order.status_id;
                orderData.status.name = statusInfo.outdated_order.name;
                orderData.status.status_group = statusInfo.outdated_order.status_group;
                orderData.worker_id = null;
                orderData.car_id = null;
                orderData.car = null;
                orderData.worker = null;
                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                orderData.status_time = updateTime;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderInstance.orderId, 'error', `orderLateTimeout->redisOrderManager.saveOrder->Error: ${err.message}`);
                        return reject(err);
                    }
                    orderLogger(orderInstance.orderId, 'info', `orderLateTimeout->redisOrderManager.saveOrder->Result: ${result}`);
                    sqlManager.updateOrderFromMysql(orderInstance.orderId, statusInfo.outdated_order.status_id, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderInstance.orderId, 'error', `orderLateTimeout->updateOrderFromMysql->Error: ${err.message}`);
                        }
                        orderLogger(orderInstance.orderId, 'info', `orderLateTimeout->updateOrderFromMysql->Result: ${result}`);
                    });

                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: orderInstance.tenantId,
                        order_id: orderInstance.orderId,
                        params: {}
                    });
                    amqpOrderMessageSender.sendMessage(orderInstance.orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderInstance.orderId, 'error', `orderLateTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            return reject(err);
                        }
                        orderLogger(orderInstance.orderId, 'info', `orderLateTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderInstance.orderId}. Result: ${result}`);
                        return resolve(true);
                    });
                });

            })
        } catch (err) {
            return reject(err);
        }
    })
};
