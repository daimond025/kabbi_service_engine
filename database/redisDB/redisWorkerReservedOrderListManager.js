"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const redis = require("redis");
const retryStrategy = require("./redisRetryStrategy");
/**
 * Worker reserved order list manager
 * @param {Number} port
 * @param {String} ip
 * @returns {redisWorkerReservedOrderListManager}
 */
function redisWorkerReservedOrderListManager(port, ip) {
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db13");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db13");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db13:" + err);
    });

    this.dbNumber = 13;

}


/**
 * Add order to worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisWorkerReservedOrderListManager.prototype.addOrder = function (tenantId, workerCallsign, orderId, callback) {
    const self = this;
    const key = `${tenantId}_${workerCallsign}`;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.sadd(key, orderId, (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });

    });
};

/**
 * Delete order form worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisWorkerReservedOrderListManager.prototype.delOrder = function (tenantId, workerCallsign, orderId, callback) {
    const self = this;
    const key = `${tenantId}_${workerCallsign}`;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.srem(key, orderId, (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });

    });
};


/**
 * Get all orders form worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisWorkerReservedOrderListManager.prototype.getAll = function (tenantId, workerCallsign,  callback) {
    const self = this;
    const key = `${tenantId}_${workerCallsign}`;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.smembers(key, function (err, result) {
            if (err) {
                return callback(err);
            }
            var orders_list = result.map(function(order) {
                return parseInt(order);
            });
            callback(null, orders_list);
        });
    });
};

/**
 * Delete all orders form worker active order list
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} orderId
 * @param {Function} callback
 */
redisWorkerReservedOrderListManager.prototype.delAll = function (tenantId, workerCallsign,  callback) {
    callback(null, 1);
    /*const self = this;
    const key = `${tenantId}_${workerCallsign}`;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.del(key, function (err, result) {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });*/
};


module.exports = exports = new redisWorkerReservedOrderListManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);