'use strict';

const async = require("async");
const mainLogger = require("../logger").mainLogger;




const redisOrderManager = require("../../database/redisDB/redisOrderManager");
const sqlManager = require("../../database/sqlDB/sqlManager");

const orderMainHandler = require('../../orderHandlers/orderMainHandler');
/**
 * @class getOrderAfterRestart
 */
class getOrderAfterRestart {
    /**
     *@constructor
     */
    constructor() {

    }


    /**
     * Run worker
     * @returns {Promise}
     */
    run() {
        return new Promise((resolve, reject) => {

            const processedOrders = [];
            sqlManager.getAllTenantIds((err, tenantArr) => {
                if (err) {
                    mainLogger("error", `getOrderAfterRestart->getProcessedOrders->getAllTenantIds->Error: ${err.message}`);
                    return reject(err);
                }

                async.each(tenantArr, function (tenantId, callback) {
                    redisOrderManager.getAllOrders(tenantId, (err, orders) => {
                        if (err) {
                            mainLogger("error", `getOrderAfterRestart->getProcessedOrders->getAllOrders->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                        async.each(orders, (orderData, orderCb) => {
                            try {

                                processedOrders.push(orderData)

                            } catch (err) {
                                mainLogger("error", `getOrderAfterRestart->getProcessedOrders->getAllOrders->Error of getting order data params.Error: ${err.message}`);
                                mainLogger("error", orderData);
                            }
                            return orderCb(null, 1);
                        }, (err) => {
                            if (err) {
                                mainLogger("error", `getOrderAfterRestart->getProcessedOrders->getAllOrders->async.each orders. Error: ${err.message}`);
                            }
                            return callback(null, 1);
                        })
                    })
                }, (err) => {
                    if (err) {
                        mainLogger("error", `getOrderAfterRestart->getProcessedOrders->getAllOrders->async.each tenantArr. Error: ${err.message}`);
                        return reject(err);
                    } else {

                        async.each(processedOrders, (orderData) => {
                            orderMainHandler( orderData.tenant_id, orderData.order_id, 0, (err, result) => {
                                mainLogger("info", 'getOrderAfterRestart->getProcessedOrders->successful restart' );
                            });

                        });
                        mainLogger("info", 'getOrderAfterRestart->getProcessedOrders->successful done');

                        return resolve(1);
                    }
                })
            });
        });
    }



}
module.exports = exports = new getOrderAfterRestart();