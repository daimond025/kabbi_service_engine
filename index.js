'use strict';

const nodemailer = require('nodemailer');
const express = require('express');
const bodyParser = require('body-parser');
const config = require("./services/lib").getConfig();
const mainLogger = require("./services/logger").mainLogger;
const http = require('http');

const mainRouter = require("./api/routes/main");
const app = express();
app.use(bodyParser.json());
app.use('/', mainRouter);
let processingSIGINT = false;
const server = http.createServer(app);

const bootstrap = require('./services/Bootstrap');




server.listen(config.MY_BIND_PORT, config.MY_BIND_HOST, () => {
    mainLogger('info', "http server started at " + config.MY_BIND_HOST + ":" + config.MY_BIND_PORT + '. Process ' + process.pid + ' started');
    bootstrap.start()
        .then(result => {
            mainLogger("info", ` bootstrap.start->result:${result}`);
        })
        .catch(err => {
            mainLogger("error", ` bootstrap.start->error: ${err.message}`);
            bootstrap.exit(254);
        })
});



/**
 * on SIGINT deregister service from consul and exit from process with code = 0
 */
process.on("SIGINT", () => {
    mainLogger("info", "daimond025");
    if (!processingSIGINT) {
        processingSIGINT = true;
        mainLogger("info", "New process event: SIGINT");
        bootstrap.exit(0);
    } else {
        mainLogger("info", "SIGINT already processing");
    }
});

/**
 * on uncaught exception send err trace at email and exit from process with code = 255
 */
process.on('uncaughtException', (err) => {
    mainLogger('error', 'Critical error at engine: ' + err.message);
    mainLogger('error', 'Error stack: ' + err.stack);
    const transporter = nodemailer.createTransport({
        service: config.MAIL_SUPPORT_PROVIDER,
        auth: {
            user: config.MAIL_SUPPORT_LOGIN,
            pass: config.MAIL_SUPPORT_PASSWORD
        }
    });
    console.log(err.message);
    const mailOptions = {
        from: config.MAIL_SUPPORT_LOGIN,
        to: config.MAIL_SUPPORT_LOGIN,
        subject: config.MAIL_SUPPORT_SUBJECT_ERROR,
        text: err.message,
        html: err.message + "  |  " + err.stack
    };
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            mainLogger('error', err.message);
        } else {
            mainLogger('error', 'Message sent: ' + info.response);
        }
        bootstrap.exit(255);
    });
});


/**
 * on warning send err trace at email
 */
process.on('warning', (err) => {
    mainLogger('error', 'Warning at engine service: ' + err.message);
    mainLogger('error', 'Warning stack: ' + err.stack);
    const transporter = nodemailer.createTransport({
        service: config.MAIL_SUPPORT_PROVIDER,
        auth: {
            user: config.MAIL_SUPPORT_LOGIN,
            pass: config.MAIL_SUPPORT_PASSWORD
        }
    });
    const mailOptions = {
        from: config.MAIL_SUPPORT_LOGIN,
        to: config.MAIL_SUPPORT_LOGIN,
        subject: config.MAIL_SUPPORT_SUBJECT_WARNING,
        text: err.message,
        html: err.message + "  |  " + err.stack
    };
    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            mainLogger('error', err.message);
        } else {
            mainLogger('error', 'Message sent: ' + info.response);
        }
    });
});

