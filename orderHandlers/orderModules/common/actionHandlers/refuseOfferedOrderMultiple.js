'use strict';
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const workerStatusManager = require("../../../../services/workerStatusManager");
const statusInfo = require("../../../../services/orderStatusManager").statusInfo;
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");


/**
 * Refuse offered order event
 * @param orderInstance
 * @param {object} event
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance, event) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'refuseOfferedOrderMultiple called!');
            const orderId = parseInt(orderInstance.orderId);
            const tenantId = parseInt(orderInstance.tenantId);

            // прична отказа
            let reasonRefuse = typeof event.params.order_refuse !== undefined ?  event.params.order_refuse : "timer";
            event.time = Math.floor(Date.now() / 1000);

            redisWorkerManager.getWorker(tenantId, event.change_sender_id, (err, workerData) => {
                if (err) {
                    orderLogger(orderId, "error", `refuseOfferedOrderMultiple->redisWorkerManager.getWorker->Error: ${err.message}`);
                }
                if (workerData.worker.status === workerStatusManager.offerOrder && parseInt(workerData.worker.last_offered_order_id) === orderId) {
                    workerData.worker.status = workerStatusManager.free;

                    sqlManager.logOrderChangeData(tenantId, orderId, 'status', event.change_sender_id, 'worker', workerStatusManager.refuseOrder + "_" + reasonRefuse, 'system', (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `refuseOfferedOrderMultiple->sqlManager.logOrderChangeData->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, "info", `refuseOfferedOrderMultiple->sqlManager.logOrderChangeData->Result: ${result}`);
                        }
                    });
                    redisWorkerManager.saveWorker(workerData, (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `refuseOfferedOrderMultiple->redisWorkerManager.saveWorker->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, "info", `refuseOfferedOrderMultiple->redisWorkerManager.saveWorker->Result: ${result}`);
                        }
                    });
                }
            });
            redisOrderManager.getOrder(tenantId, orderId, (err, order) => {
                if (err) {
                    orderLogger(orderId, "error", `refuseOfferedOrderMultiple->redisWorkerManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                const currentStatusId = parseInt(order.status_id);
                if (currentStatusId !== statusInfo.offer_order.status_id) {
                    return reject(new Error('order is not at offering status'));
                }
                order.workers_offered_responses = typeof order.workers_offered_responses === "object" ?
                    Object.keys(order.workers_offered_responses).map(key => order.workers_offered_responses[key]) : [];
                order.workers_offered_responses.push(event);

                redisOrderManager.saveOrder(order, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `refuseOfferedOrderMultiple->redisWorkerManager.saveOrder->Error: ${err.message}`);
                        return reject(err);
                    }
                    orderLogger(orderId, "info", `refuseOfferedOrderMultiple->all workers give response of offering order`);

                    return resolve(true);
                })
            })
        } catch (err) {
            return reject(err);
        }
    })
};