const request = require('request');
const config = require("../lib").getConfig();
const apiServiceUrl = config.API_SERVICE_URL;
const PHPUnserialize = require('php-unserialize');
const lib = require('../../services/lib');


/**
 * Send request to add orders at statistic
 * @param {Number} orderId
 * @param {Number} tenantId
 * @param {Function} callback
 */
function sendOrderToStatistic(tenantId, orderId, callback) {
    const getString = "order_id=" + orderId + "&tenant_id=" + tenantId;
    const requestOptions = {
        url: apiServiceUrl + 'v1/order/order_statistic?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, null);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(null, null);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send request service_api to define time ro order
 * @param {Object} options
 */
function defineTimeFromDriverToOrder(options) {
    return new  Promise((resolve, reject) => {
        let requestOptions = {
            url: apiServiceUrl + 'v1/route-analyzer/distance_matrix',
            form: {
                city_id : options.city_id,
                tenant_id : options.tenant_id,
                tenant_login : options.tenant_login,
                origins : lib.serialize(options.origins),
                destinations : lib.serialize( options.destinations),
            }
        };
        request.post(requestOptions, resultData);

        function resultData(error, response, body) {

            if (!error && parseInt(response.statusCode) === 200) {
                try {
                    const info = JSON.parse(body);
                    if (parseInt(info.code) === 0) {
                        const distance_rezult = (typeof( info.result) !== "undefined" ) ? PHPUnserialize.unserialize( info.result) : [];
                        return resolve(distance_rezult);
                    } else {
                        return resolve( null);
                    }
                } catch (err) {
                    return reject(err);
                }
            } else {
                return reject(null, null);
            }
        }
    });
}


/**
 * Send sms notification to client
 * @param  {Object} options
 * @param  {Number} options.orderId
 * @param  {Number} options.tenantId
 * @param  {Number} options.statusId
 * @returns {Promise<boolean>}
 */
function sendSmsNotificationToClient(options) {
    return new Promise((resolve, reject) => {
        const getString = "order_id=" + options.orderId + "&status_id=" + options.statusId + "&tenant_id=" + options.tenantId;
        const requestOptions = {
            url: apiServiceUrl + 'v1/notification/send_sms_notification_for_client?' + getString
        };

        function resultData(error, response, body) {
            if (error) {
                return reject(error)
            }
            if (parseInt(response.statusCode) === 200) {
                return resolve(true);
            }
            const statusCode = response.statusCode;
            let errorBody;
            try {
                errorBody = JSON.parse(body);
            } catch (err) {
                errorBody = statusCode
            }

            return reject(new Error(`status code:${statusCode}. error: ${JSON.stringify(errorBody)}`));
        }

        request(requestOptions, resultData);
    });
}

function sendVoiceotificationToClient(options) {
    return new Promise((resolve, reject) => {
        const getString = "order_id=" + options.orderId + "&status_id=" + options.statusId + "&tenant_id=" + options.tenantId;
        const requestOptions = {
            url: apiServiceUrl + 'v1/notification/send_sms_notification_for_client?' + getString
        };

        function resultData(error, response, body) {
            if (error) {
                return reject(error)
            }
            if (parseInt(response.statusCode) === 200) {
                return resolve(true);
            }
            const statusCode = response.statusCode;
            let errorBody;
            try {
                errorBody = JSON.parse(body);
            } catch (err) {
                errorBody = statusCode
            }

            return reject(new Error(`status code:${statusCode}. error: ${JSON.stringify(errorBody)}`));
        }

        request(requestOptions, resultData);
    });
}

/**
 * Send sms notification to client passenger
 * @param  {Object} options
 * @param  {Number} options.orderId
 * @param  {Number} options.tenantId
 * @param  {Number} options.statusId
 * @returns {Promise<boolean>}
 */
function sendSmsNotificationToClientPassenger(options) {
    return new Promise((resolve, reject) => {
        const getString = "order_id=" + options.orderId + "&status_id=" + options.statusId + "&tenant_id=" + options.tenantId;
        const requestOptions = {
            url: apiServiceUrl + 'v1/notification/send_sms_notification_for_client_passenger?' + getString
        };

        function resultData(error, response, body) {
            if (error) {
                return reject(error)
            }
            if (parseInt(response.statusCode) === 200) {
                return resolve(true);
            }
            const statusCode = response.statusCode;
            let errorBody;
            try {
                errorBody = JSON.parse(body);
            } catch (err) {
                errorBody = statusCode
            }

            return reject(new Error(`status code:${statusCode}. error: ${JSON.stringify(errorBody)}`));
        }

        request(requestOptions, resultData);
    })
}

/**
 * Send sms notification for point recipient
 * @param tenantId
 * @param orderId
 * @param statusId
 * @returns {Promise<boolean>}
 */
function sendSmsNotificationForPointsRecipient(tenantId, orderId, statusId) {
    return new Promise((resolve, reject) => {
        const getString = "tenant_id=" + tenantId + "&order_id=" + orderId + "&status_id=" + statusId;
        const requestOptions = {
            url: apiServiceUrl + 'v1/notification/send_notification_for_points_recipient?' + getString
        };

        function resultData(error, response, body) {
            if (error) {
                return reject(error)
            }
            if (parseInt(response.statusCode) === 200) {
                return resolve(true);
            } else {
                const statusCode = response.statusCode;
                let errorBody;
                try {
                    errorBody = JSON.parse(body);
                } catch (err) {
                    errorBody = statusCode
                }

                return reject(new Error(`status code:${statusCode}. error: ${JSON.stringify(errorBody)}`));
            }
        }

        request(requestOptions, resultData);
    })
}

/**
 * Update client order counters (count of completed and rejected orders)
 * @param {Number} tenantId
 * @param {Number} clientId
 * @param {Number} statusId
 * @param {Function} callback
 */
function updateClientCounters(tenantId, clientId, statusId, callback) {
    const getString = "client_id=" + clientId + "&status_id=" + statusId + "&tenant_id=" + tenantId;
    const requestOptions = {
        url: apiServiceUrl + 'v1/notification/update_client_order_counters?' + getString
    };

    function sendRequest(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, sendRequest);
}

/**
 * Send email
 * @param {number} tenantId
 * @param {string} type
 * @param {string} to
 * @param {string} cityId
 * @param {string} lang
 * @param {object} emailParams
 * @return {Promise<any>}
 */
function sendEmail(tenantId, type, to, cityId, lang, emailParams) {
    return new Promise((resolve, reject) => {
        const getString = `tenant_id=${tenantId}&type=${type}&to=${to}&city_id=${cityId}`;

        Object.keys(emailParams).map((key, index) => {
            if (emailParams[key] === null) {
                emailParams[key] = '';
            }
        });

        const requestOptions = {
            url: `${apiServiceUrl}v1/email/send?${getString}`,
            formData: emailParams
        };

        function sendRequest(error, response, body) {
            if (error) {
                return reject(error);
            }
            if (parseInt(response.statusCode) !== 200) {
                return reject(new Error(`bad response status code:${response.statusCode}`));
            }
            return resolve(1);
        }

        request.post(requestOptions, sendRequest);
    })
}

exports.sendOrderToStatistic = sendOrderToStatistic;
exports.sendSmsNotificationToClient = sendSmsNotificationToClient;
exports.sendSmsNotificationToClientPassenger = sendSmsNotificationToClientPassenger;
exports.sendSmsNotificationForPointsRecipient = sendSmsNotificationForPointsRecipient;
exports.updateClientCounters = updateClientCounters;
exports.sendEmail = sendEmail;

exports.defineTimeFromDriverToOrder =  defineTimeFromDriverToOrder;
