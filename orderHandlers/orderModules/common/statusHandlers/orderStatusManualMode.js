'use strict';

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const orderActionManager = require('../orderActionManager');
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;


/**
 * Order status manual mode (frozen order)
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {String} eventSenderService
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, eventSenderService) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusManualMode called!');
            const orderId = orderInstance.orderId;
            orderInstance.orderTimer.clearAll();
            if (orderData.worker && orderData.worker.callsign ) {
                const workerAssignedNewStatuses = [
                    statusInfo.worker_assigned_at_order_soft.status_id,
                    statusInfo.worker_assigned_at_order_hard.status_id
                ];
                const workerAssignedPreStatuses = [
                    statusInfo.worker_assigned_at_preorder_soft.status_id,
                    statusInfo.worker_assigned_at_preorder_hard.status_id,
                    statusInfo.worker_accepted_preorder.status_id
                ];
                let orderWasAssigned = false;
                let penultimateStatus;
                let isPreOrder = false;
                if (orderInstance.statusArr.length >= 2 && orderInstance.statusArr[orderInstance.statusArr.length - 2]) {
                    penultimateStatus = orderInstance.statusArr[orderInstance.statusArr.length - 2];
                    //Заказ был назначен
                    if (workerAssignedNewStatuses.indexOf(penultimateStatus) !== -1 || workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                    }
                    if (workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                        isPreOrder = true;
                    }
                }
                if (orderWasAssigned) {
                    const isWorkerEventOwner = eventSenderService === orderEventSenderService.workerService;
                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder,
                        isWorkerEventOwner,
                    });
                } else {
                    orderActionManager.deleteWorkersFromOrder(orderData)
                        .then(result => {
                            orderLogger(orderId, "info", `orderStatusManualMode->deleteWorkersFromOrder->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderId, "error", `orderStatusManualMode->deleteWorkersFromOrder->Error: ${err.message}`);
                        });
                }
            }
            orderActionManager.forceRejectOutOfDateOrder(orderInstance, orderData)
                .then(result => {
                    orderLogger(orderId, "info", `orderStatusManualMode->orderActionManager.forceRejectOutOfDateOrder->Result: ${result}`);
                    return resolve(1);
                })
                .catch(err => {
                    orderLogger(orderId, "info", `orderStatusManualMode->orderActionManager.forceRejectOutOfDateOrder->Error: ${err.message}`);
                    return resolve(1);
                });
        } catch (err) {
            return reject(err);
        }
    })
};
