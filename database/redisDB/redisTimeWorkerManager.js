"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const PHPUnserialize = require('php-unserialize');
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const redis = require("redis");
const async = require("async");
const retryStrategy = require("./redisRetryStrategy");

/**
 * Worker manager
 * @param {Number} port
 * @param {String} ip
 * @returns {RedisWorkerManager}
 */
function redisTimeWorkerManager(port, ip) {
    //Workers
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db0");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db0");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db0:" + err);
    });

    this.dbNumber = 14;
    this.saveTime = 600;

}




/**
 * Get worker data by tenantId and his callsign
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param {Function} callback
 */
redisTimeWorkerManager.prototype.getWorkersTime = function (tenantId, orderId, callback) {
    const self = this;
    let key = tenantId.toString() + "_" + orderId.toString();
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(key, (err, workerTime) => {
            if (err) {
                return callback(err);
            }
            try {
                workerTime = PHPUnserialize.unserialize(workerTime);
                if (workerTime === null || typeof(workerTime) === "undefined") {
                    callback(null, []);
                }
                callback(null, workerTime);
            } catch (err) {
                return callback(err);
            }
        });

    });

};


/**
 * Save worker data
 * @param  {Object} workersDataTime
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param  {Function} callback
 */
redisTimeWorkerManager.prototype.saveWorkerTime = function (tenantId, orderId,workersDataTime) {

    return new  Promise((resolve, reject) => {
        const self = this;
        let worker;

        // formate date
        let save_date = [];
        workersDataTime.forEach(function (item, i) {
            save_date[i] = item;
        });


        try {
            worker = lib.serialize(save_date);
        } catch (err) {
            return callback(err);
        }

        let key = tenantId.toString() + "_" + orderId.toString();
        self.redisClientDb.select(self.dbNumber, () => {
            self.redisClientDb.set(key, worker, 'EX', this.saveTime , (err, setResult) => {
                if (err) {
                    return reject(err);
                }

                if (setResult.toString() === "OK" || !err ){
                    return resolve(workersDataTime);
                }
                return reject(new Error('Error - hset bad result. Hset result:' + setResult));
            });
        });
    });
};

/**
 * Delete Worker from redis
 * @param {Number} tenantId
 * @param {Number} callsign
 * @param {Function} callback
 */
redisTimeWorkerManager.prototype.deleteWorkersTime = function (tenantId, orderId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hdel([tenantId, orderId], (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });

    });
};


module.exports = exports = new redisTimeWorkerManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);