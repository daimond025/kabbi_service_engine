'use strict';

const updateOrder = "update_order";
const rejectWorkerFromOrder = "reject_worker_from_order";
const rejectOrder = "reject_order";
const completeOrder = "complete_order";
const offerOrder = "offer_order";
const confirmPreOrder = "confirm_preorder";
const orderListAddItem = "order_list_add_item";
const orderListDelItem = "order_list_del_item";
const preOrderOffer= "pre_order_offer";

module.exports = {
    updateOrder,
    rejectWorkerFromOrder,
    rejectOrder,
    completeOrder,
    offerOrder,
    confirmPreOrder,
    orderListAddItem,
    orderListDelItem,
    preOrderOffer
};