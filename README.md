## Common description

Сервис на node.js выполняющий функции:
- Работа с заказами
- Работа со сменами водителей
- Работа с блокировками водителей

## Requirements version of Node.js
Node version : ^v.6.9.1

## Install Dependencies

```bash
npm install
```
    
## How to run service
* Заполнить .env файл по примеру .env.example файла


* Проверить корректность .env файла: 
```bash
npm run check-env
``` 
При успешном выполнении в консоль выведет: OK. В случае ошибки в консоль выведет описание ошибки.

* Запустить сервис: 
```bash
npm run start
```
Или
```bash
node index.js
```

* Возобновить работу сервиса (сервис находит данные, которые обрабатывались ранее на данной ноде, и берет их в работу): 
```bash
npm run resume
```
Или
```bash
node index.js --resume
```

* Запустить сервис и распределить данные, которые обрабатывались на указанных нодах, по всему кластеру: 
```bash
node index.js --resume --from_ids 1 2 3
```


## API description
@TODO