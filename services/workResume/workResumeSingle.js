"use strict";
const WorkResumeBase = require("./workResumeBase");
const logger = require('../logger');
const mainLogger = logger.mainLogger;
const orderMainHandler = require("../../orderHandlers/orderMainHandler");
const watchWorkerShift = require("../../workerShiftHandlers/watchWorkerShift");
const unblockWorkerOrderTimer = require("../../workerBlockHandlers/unblockWorkerOrderTimer");
const unblockWorkerPreOrderTimer = require("../../workerBlockHandlers/unblockWorkerPreOrderTimer");
const async= require("async");

/**
 * @class WorkResumeSingle
 * Load to processing orders,worker shifts, unblock timers
 * to this node
 */
class WorkResumeSingle extends WorkResumeBase {

    /**
     * Load orders which was processed at node with id= nodeId to this node
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadOrders(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeSingle->loadOrders called");
            mainLogger("info", `WorkResumeSingle->loadOrders->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedOrders(nodeId)
                .then(orders => {
                    async.each(orders, (orderData, orderCb) => {
                        orderMainHandler(orderData.tenantId, orderData.orderId, 1, (err, result) => {
                            if (err) {
                                mainLogger("error", `loadOrders->orderMainHandler->Error of order ${orderData.orderId} : ${err.message}`);
                            } else {
                                mainLogger("info", `loadOrders->orderMainHandler->Load order: ${orderData.orderId} result: ${result}`);
                            }
                            return orderCb(null, 1);
                        });
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    })
                })
                .catch(err => {
                    return reject(err);
                })
        })
    }

    /**
     * Load worker shifts  which was processed at node with id= nodeId to this node
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadWorkerShifts(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeSingle->loadWorkerShifts called");
            mainLogger("info", `WorkResumeSingle->loadWorkerShifts->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedWorkerShifts(nodeId)
                .then(processedShifts => {
                    async.each(processedShifts, (shiftData, shiftCb) => {
                        watchWorkerShift(shiftData.tenantId, shiftData.tenantLogin, shiftData.workerCallsign, shiftData.shiftLifeTime, shiftData.shiftId, (err, result) => {
                            if (err) {
                                mainLogger("error", `WorkResumeSingle->loadWorkerShifts->watchWorkerShift. Error: ${err.message}`);
                            } else {
                                mainLogger("info", `WorkResumeSingle->loadWorkerShifts->watchWorkerShift: Workers ${shiftData.tenantLogin + "_" + shiftData.workerCallsign} sent to engine`);
                            }
                            return shiftCb(null, 1);
                        });
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    })
                })
                .catch(err => {
                    return reject(err);
                })
        })
    }

    /**
     * Load worker unblock timers  which was processed at node with id= nodeId to this node
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadWorkerUnblockTimers(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeSingle->loadWorkerUnblockTimers called");
            mainLogger("info", `WorkResumeSingle->loadWorkerUnblockTimers->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedWorkerUnblockTimers(nodeId)
                .then(processedWorkerUnblockTimers => {
                    async.each(processedWorkerUnblockTimers, (timerData, timerCb) => {
                        if (timerData.typeBlock === "order") {
                            unblockWorkerOrderTimer(timerData.tenantId, timerData.workerCallsign, timerData.shiftId, timerData.blockId, timerData.timeToUnblock, (err, result) => {
                                if (err) {
                                    mainLogger("error", `loadWorkerAllUnblockTimers->unblockWorkerOrderTimer->Error: ${err.message}`);
                                } else {
                                    mainLogger("info", `loadWorkerAllUnblockTimers->unblockWorkerOrderTimer->Block_id=${timerData.blockId} ->Result: ${result}`);
                                }
                                return timerCb(null, 1);
                            })
                        } else {
                            unblockWorkerPreOrderTimer(timerData.tenantId, timerData.workerCallsign, timerData.shiftId, timerData.blockId, timerData.timeToUnblock, (err, result) => {
                                if (err) {
                                    mainLogger("error", `loadWorkerAllUnblockTimers->unblockWorkerPreOrderTimer-> Error: ${err.message}`);
                                } else {
                                    mainLogger("info", `loadWorkerAllUnblockTimers->unblockWorkerPreOrderTimer->Block_id=${timerData.blockId} ->Result: ${result}`);
                                }
                                return timerCb(null, 1);
                            })
                        }
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    });

                })
                .catch(err => {
                    return reject(err);
                })
        })
    }
}

module.exports = exports = new WorkResumeSingle();