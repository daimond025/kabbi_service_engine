'use strict';

const serviceTimer = require('../services/serviceTimer');
const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const lib = require("../services/lib");
const DataProcessingService = require('../services/dataProcessingService');
const config = lib.getConfig();
const logger = require('../services/logger');
const workerBlockLogger = logger.workerBlockLogger;
const amqp = require('amqplib/callback_api');

/**
 * Create worker preorder unblock timer
 * @param  {Number} blockedTenantId
 * @param  {Number} blockedCallsign
 * @param  {Number} shiftId
 * @param  {Number} blockId
 * @param  {Number} timeToUnblock
 * @param  {Function} callback
 */
module.exports = function (blockedTenantId, blockedCallsign, shiftId, blockId, timeToUnblock, callback) {
    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrderTimer called!");
    let connSelfClosed = false;
    let chanelSelfClosed = false;
    const workerUnblockTimer = new serviceTimer();
    startListenQueue();
    return callback(null, 1);

    function startListenQueue() {
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
            if (err) {
                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->connect->Error: ${err.message}. Need to try reconnect...`);
                return setTimeout(startListenQueue, 2000);
            }
            conn.on("error", err => {
                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', err.message);
                if (err.message !== "Connection closing") {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->connection->Error ${err.message}`);
                }
            });
            conn.on("close", () => {
                if (!connSelfClosed) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->connection closed, need to try reconnect...`);
                    workerUnblockTimer.clearAll();
                    return setTimeout(startListenQueue, 2000);
                }
            });
            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->rabbitmq->connected`);
            conn.createChannel((err, ch) => {
                if (err) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->create channel->Error: ${err.message}`);
                }
                ch.on("error", (err) => {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->channel->Error: ${err.message}`);
                });
                ch.on("close", () => {
                    if (!chanelSelfClosed) {
                        workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->rabbitmq->channel closed`);
                    }
                });
                const q = `worker_preorder_block_${blockId}`;
                const messageWorkTime = 2;
                ch.assertQueue(q, {
                    durable: true
                });
                ch.prefetch(1);
                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->Waiting task for worker_preorder_block_${blockId}`);
                DataProcessingService.addWorkerBlock(blockId);
                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrderTimer->Start unblock worker preorder after " + timeToUnblock + " ms");
                try {
                    workerUnblockTimer.setServiceTimeout('unblockWorkerOrderTimer', () => {
                        workerPreOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId);
                    }, timeToUnblock);
                } catch (err) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->setServiceTimeout->Error: ${err.message}`);
                }
                ch.consume(q, message => {
                    if (message) {
                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->New task for for worker_preorder_block_${blockId}!`);
                        let taskObject;
                        try {
                            taskObject = JSON.parse(message.content.toString());
                        } catch (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->Task content parsing->Error: ${err.message}`);
                        } finally {
                            if (taskObject) {
                                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->new task object: ${taskObject}`);
                                if (taskObject.command === "worker_preorder_unblock") {
                                    workerPreOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId);
                                } else {
                                    setTimeout(() => {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->Task done!`);
                                        try {
                                            ch.ack(message);
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->ch.ack->Error: ${err.message}`);
                                        }
                                    }, messageWorkTime * 1000);
                                }
                            }
                        }
                    }
                }, {noAck: false});

                function workerPreOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrderTimer->Starting to unblock worker preorder block..");
                    DataProcessingService.delWorkerBlock(blockId);
                    redisWorkerManager.getWorker(blockedTenantId, blockedCallsign, (err, workerDataBlocked) => {
                        if (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerPreOrderTimer->redisWorkerManager.getWorker->Error: ${err.message}`);
                        }
                        if (workerDataBlocked) {
                            if (parseInt(workerDataBlocked.worker.worker_shift_id) === parseInt(shiftId)) {
                                workerDataBlocked.worker.preorder_end_block = null;
                                redisWorkerManager.saveWorker(workerDataBlocked, (err, result) => {
                                    if (err) {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerPreOrderTimer->redisWorkerManager.saveWorker->Error: ${err.message}`);
                                    } else {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerPreOrderTimer->result of set worker status to Unblock preorder in redis: ${result}`);
                                    }
                                });
                            }
                        }
                    });
                    sqlManager.logWorkerUnblock(blockId, (err, result) => {
                        if (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerPreOrderTimer->sqlManager.logWork->Error: ${err.message}`);
                        } else {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->Result of log unblock worker preorder block to sqldb:${result}`);
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->Unsubscribe from worker_preorder_block_${blockId} channel`);
                            try {
                                ch.deleteQueue(q, {ifUnused: false, ifEmpty: false}, (err, ok) => {
                                    if (err) {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->deleteQueue->Error: ${err.message}`);
                                    } else {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerPreOrderTimer->worker_preorder_block_${blockId} queue deleted.`);
                                        try {
                                            ch.close();
                                            chanelSelfClosed = true;
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrderTimer->AMQP channel closed.");
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->AMQP channel close->Error: ${err.message}`);
                                        }
                                        try {
                                            conn.close();
                                            connSelfClosed = true;
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerPreOrderTimer->AMQP connection closed.");
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->AMQP connection close->Error: ${err.message}`);
                                        }
                                        workerUnblockTimer.clearAll();
                                    }
                                });
                            } catch (err) {
                                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerPreOrderTimer->deleteQueue->Error: ${err.message}`);
                            }

                        }
                    });
                }
            });
        });
    }

};

