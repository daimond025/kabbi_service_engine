'use strict';

const mongoose = require('mongoose');
const lib = require("../../services/lib");
const config = lib.getConfig();
const mainLogger = require("../../services/logger").mainLogger;

class MongoDB {

    /**
     * @param {string} url
     * @param {object} options
     */
    constructor(url, options) {

        this.url = url;
        this.options = {
            useNewUrlParser: true,
            autoIndex: false, // Don't build indexes
            reconnectTries: options.reconnectTries, // Never stop trying to reconnect
            reconnectInterval: options.reconnectInterval, // Reconnect every 500ms
            poolSize: options.poolSize, // Maintain up to 10 socket connections
            // If not connected, return errors immediately rather than waiting for reconnect
            bufferMaxEntries: 0,
          //  replicaSet: options.replicaSet
        };
        this.connection = null;
    }

    /**
     * Connect to db
     * @returns {Promise<boolean>}
     */
    connect() {
        const self = this;

        return new Promise((resolve, reject) => {
            if (self.connection) {
                return resolve(true);
            }
            mongoose.connect(self.url, this.options)
                .then(() => {
                    this.connection = mongoose.connection;
                    this._connectionEventHandler(this.connection);
                    return resolve(true);
                })
                .catch(err => {
                    return reject(err);
                })
        });
    }

    /**
     *
     * @param connection
     * @private
     */
    _connectionEventHandler(connection) {
        this.connection.on('connecting', () => {
            mainLogger("info", `MongoDB->connection event->connecting`);
        });

        this.connection.on('connected', () => {
            mainLogger("info", `MongoDB->connection event->connected`);
        });

        this.connection.on('open', () => {
            mainLogger("info", `MongoDB->connection event->open`);
        });

        this.connection.on('disconnecting', () => {
            mainLogger("info", `MongoDB->connection event->open`);
        });

        this.connection.on('disconnected', () => {
            mainLogger("info", `MongoDB->connection event->disconnected`);
        });

        this.connection.on('close', () => {
            mainLogger("info", `MongoDB->connection event->close`);
        });
        this.connection.on('reconnected', () => {
            mainLogger("info", `MongoDB->connection event->reconnected`);
        });

        this.connection.on('error', err => {
            mainLogger("error", `MongoDB->connection error->${err.message}`);
        });

        this.connection.on('fullsetup', data => {
            mainLogger("info", `MongoDB->connection event->fullsetup ${data}`);
        });
    }

    /**
     * get db connection
     * @returns {null|*}
     */
    get db() {
        return this.connection;
    }

    /**
     * Close db connection
     * @param {boolean} force
     * @returns {Promise<boolean>}
     */
    close(force = true) {
        return new Promise((resolve, reject) => {
            if (!this.connection) {
                return resolve(true);
            }
            this.connection.close(force)
                .then(() => {
                    return resolve(true);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }
}

const options = {
    replicaSet: config.MONGODB_MAIN_REPLICA_SET,
    reconnectTries: config.MONGODB_MAIN_RECONNECT_TRIES,
    reconnectInterval: config.MONGODB_MAIN_RECONNECT_INTERVAL_MS,
    poolSize: config.MONGODB_MAIN_POOL_SIZE,
};

module.exports = exports = new MongoDB(config.MONGODB_MAIN_DSN, options);