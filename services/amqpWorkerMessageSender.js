"use strict";

const lib = require("./lib");
const config = lib.getConfig();
const mainLogger = require('./logger').mainLogger;
const amqp = require('amqplib/callback_api');
// Time to live for worker message queue in second. After this time queue will be deleted if there no active consumers.
// For default it's 2 weeks
const workerQueueExpireTime = 1209600;

/**
 * Amqp worker message publisher
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {String} messageString
 * @param {Number} messageTTL - time of live message in queue in seconds
 * @param {Function} callback
 */

function sendMessage(tenantId, workerCallsign, messageString, messageTTL, callback) {
    const queueLabel = `${tenantId}_worker_${workerCallsign}`;
    amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
        if (err) {
            return callback(err);
        } else {
            conn.createChannel(function (err, ch) {
                if (err) {
                    return callback(err);
                } else {
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true,
                            expires: workerQueueExpireTime * 1000
                        });
                        ch.sendToQueue(queueLabel, new Buffer(messageString), {
                            persistent: true,
                            expiration: messageTTL * 1000,
                            contentType: 'application/json',
                            contentEncoding: 'utf-8'
                        });
                        return callback(null, 'message sent');
                    } catch (err) {
                        return callback(new Error(`Err of sending message:${messageString} to queue:${queueLabel}. Error: ${err.message}`));
                    }

                }
            });
            setTimeout(function () {
                try {
                    conn.close();
                } catch (err) {
                    mainLogger('error', err);
                }
            }, 500);
        }
    });
}

exports.sendMessage = sendMessage;
