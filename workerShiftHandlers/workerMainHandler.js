'use strict';

const BaseKabbiWorker = require('../workerShiftHandlers/workerModules/baseKabbiWorker');
const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const redisOrderManager = require('../database/redisDB/redisOrderManager');
const redisParkingManager = require('../database/redisDB/redisParkingManager');
const redisWorkerShiftManager = require('../database/redisDB/redisWorkerShiftManager');
const redisWorkerReservedOrderListManager = require('../database/redisDB/redisWorkerReservedOrderListManager');
const workerEventCommand = require('../workerShiftHandlers/workerModules/common/workerEventCommand');

const logger = require('../services/logger');
const workerShiftLogger = logger.workerShiftLogger;

const amqp = require('amqplib/callback_api');

const config = require("../services/lib").getConfig();
const DataProcessingService = require("../services/dataProcessingService");


/* Creating service that waiting event of ending worker shift
* @param  {Number} tenantId
* @param  {String} tenantLogin
* @param  {Number} workerCallsign
* @param  {Number} shiftLifeTime
* @param  {Number} shiftId
* @param  {Function} callback
*/
module.exports = function (tenantId, tenantLogin, workerCallsign, shiftLifeTime, shiftId, callback) {
    try
    {
        workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift called!");
        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->redisWorkerManager.getWorker->Error: ${err.message}`);
                return callback(new Error('Error of getting worker from redis'));
            } else if (!workerData) {
                return callback(new Error('Error of getting worker from redis'));
            } else {
                const workerContainer = {};
                workerContainer.instance = new BaseKabbiWorker(tenantId, workerCallsign, shiftLifeTime);

                workerEventQueueListener(tenantId,workerCallsign, shiftId, workerContainer);
                return callback(null, 1);
            }
        })
    }catch(err){
        console.log(err);
        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->redisWorkerManager.getWorker->Error: ${err.message}`);
    }
};

/**
 * Worker event queue listener
 * @param tenantId
 * @param workerCallsign
 * @param shiftId
 * @param workerContainer
 */
function workerEventQueueListener(tenantId,workerCallsign, shiftId, workerContainer){
    // Delay to delete queue
    const messageDeteleQueue= 1;
    let connSelfClosed = false;
    let chanelSelfClosed = false;
    startListenQueue();

    /**
     * Order queue listener
     */
    function startListenQueue(){
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
            if(err){
                workerShiftLogger(tenantId, workerCallsign, 'error', `workerMainHandler->queue->connect->Error: ${err.message}. Need to try reconnect...`);
                return setTimeout(startListenQueue, 2000);
            }
            conn.on("error", err => {
                workerShiftLogger(tenantId, workerCallsign, 'error', err.message);
                if(err.message !== "Connection closing"){
                    workerShiftLogger(tenantId, workerCallsign, 'error', `workerMainHandler->queue->connection->Error ${err.message}`);
                }
            });
            conn.on("close", () => {
                if(!connSelfClosed){
                    workerShiftLogger(tenantId, workerCallsign, 'error', `workerMainHandler->queue->connection closed, need to try reconnect...`);
                    return setTimeout(startListenQueue, 2000);
                }
            });
            workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler->queue->connected`);
            conn.createChannel((err, ch) => {
                if (err) {
                    workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->create channel->Error: ${err.message}`);
                }
                ch.on("error", (err) => {
                    workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->chanel->Error: ${err.message}`);
                });
                ch.on("close", () => {
                    if (!chanelSelfClosed) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->channel closed`);
                    }
                });
                const q = `${tenantId}_worker_${workerCallsign}_shift`;

                ch.assertQueue(q, {
                    durable: true
                });
                ch.prefetch(1);
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->Waiting  task for ${tenantId}_worker_${workerCallsign}_shift`);
                ch.consume(q, message => {
                    workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler->queue->new task!`);
                    handleTask(message, q, conn, ch);
                },{noAck: false});
            });
        });
    }

    /**
     * Order task handler
     * @param {object} message
     * @param {string} queueLabel
     * @param {object} connection - queue connection
     * @param {object} channel - queue channel
     */
    function handleTask(message, queueLabel, connection, channel) {

        let taskObject;
        try {
            taskObject = JSON.parse(message.content.toString());
        } catch (err) {
            workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler->JSON.parse(message.content.toString())->Error: ${err.message}`);
            ack(channel,message);
            return;
        }

        let command = taskObject.command;

        workerContainer.instance.doEvent(taskObject)
        .then(result => {
            workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler-> workerContainer.instance.doEvent->Result: ${result}`);

            console.log("Обработка сообщения");
            console.log(taskObject);
            console.log(result);

            if(result === true && (workerEventCommand.workerEndWork === command) ){
                return destroyQueue(connection, channel, queueLabel);
            }else{
                ack(channel, message);
            }
        })
        .catch(err => {
            workerShiftLogger(tenantId, workerCallsign, 'error', `workerMainHandler-> workerContainer.instance.doEvent->Error: ${err.message}`);
            ack(channel, message);
        });
    }



    /**
     * Acknowledge  message in queue
     * @param channel
     * @param message
     */
    function ack(channel,message){

        workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler->queue->task done`);
        try{
            channel.ack(message);
            workerShiftLogger(tenantId, workerCallsign, 'info', `workerMainHandler->queue->channel.ack(message)->DONE`);
        }catch(err){
            workerShiftLogger(tenantId, workerCallsign, 'error', `workerMainHandler->queue->channel.ack(message)->Error: ${err.message}`);
        }
    }

    /**
     * Destroy order queue
     * @param connection
     * @param channel
     * @param queueLabel
     */
    function destroyQueue(connection, channel, queueLabel){
        try{
            setTimeout(() => {
                channel.deleteQueue(queueLabel, {ifUnused: false, ifEmpty: false}, (err, ok) => {
                    if(err){
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->deleteQueue->Error: ${err.message}`);
                    }else{
                        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->${tenantId}_worker_${workerCallsign}_shift queue deleted.`);
                        try{
                            channel.close();
                            chanelSelfClosed = true;
                            workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift->AMQP channel closed.");
                        }catch(err){
                            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->AMQP channel close->Error: ${err.message}`);
                        }
                        try{
                            connection.close();
                            connSelfClosed = true;
                            workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift->AMQP connection closed.");
                        }catch(err){
                            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->AMQP connection close->Error: ${err.message}`);
                        }
                        delete workerContainer.instance;
                    }
                });
            }, messageDeteleQueue);
        }catch(err){
            console.log(err);
            workerShiftLogger(tenantId, workerCallsign, 'error', `orderMainHandler->queue->deleteQueue->Error: ${err.message}`);
        }
    }
}