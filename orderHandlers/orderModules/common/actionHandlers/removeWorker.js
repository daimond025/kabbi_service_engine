"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const redisHospitalWorkerOrderManager = require("../../../../database/redisDB/redisHospitalWorkerOrderManager");
const redisAsyncAnswerManager = require("../../../../database/redisDB/redisAsyncAnswerManager");
const orderActionManager = require('../orderActionManager');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const orderWorkerAssignManager = require('../../common/orderWorkerAssignManager');
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const redisWorkerShiftManager = require('../../../../database/redisDB/redisWorkerShiftManager');
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventWorkerCommand = require('../../../orderEvent/orderEventWorkerCommand');
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");
const orderApi = require("../../../../services/externalWebServices/orderApi");

const orderType = require("../../../orderEvent/orderDevice");

const status_valid = [
    parseInt(statusInfo.new_order.status_id),
    parseInt(statusInfo.new_pre_order.status_id)
];

function setStatusHospitalOrder(newStatusId, companyID){

    let newStatus = newStatusId;

    // no company
    if(!Number.isInteger(companyID)){

        if(newStatusId == statusInfo.new_pre_order.status_id){
            newStatus = statusInfo.hospital_pre_order.status_id;
        }
        else if(newStatusId == statusInfo.new_order.status_id){
            newStatus = statusInfo.hospital_free_order.status_id;
        }
    }
    else{
        // company
        if(newStatusId == statusInfo.new_pre_order.status_id){
            newStatus = statusInfo.hospital_pre_order_company.status_id;
        }
        else if(newStatusId == statusInfo.new_order.status_id){
            newStatus = statusInfo.hospital_free_order_company.status_id;
        }

    }
    return newStatus;
}

/**
 * Stop offer order action
 * @param {BaseGootaxOrder} orderInstance
 * @returns {Promise<boolean>}
 */
module.exports = (orderInstance, event) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'removeWorker called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;
            const user_id = (typeof event.change_sender_id != undefined) ? event.change_sender_id : 0;

            redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                if (err) {
                    orderLogger(orderId, 'error', `removeWorkerFromOrder->getOrder->Error: ${err.message}`);
                    return reject(err);                }

                orderLogger(orderId, 'info', "removeWorkerFromOrder->redisOrderManager->called!");
                const typeOrder = orderData.device.toString();
                const status_id =  parseInt(orderData.status_id);
                const worker = (typeof orderData.worker != "undefined") ?  (orderData.worker) : null;

                // условие для небольничных заказов
                if((status_valid.includes(status_id)) && (worker == null)) {
                    orderLogger(orderId, 'info', `removeWorkerFromOrder  not called! status - ${orderData.status_id}`);
                    return resolve(true);
                }

                // вермя заказа
                const orderTime = parseInt(orderData.order_time);
                let preOrderTimeToWork = orderInstance.settings.PRE_ORDER_WORKING;
                const cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;

                let companyID = null;
                if((typeof orderData.worker_company == "object") && ( orderData.worker_company != null) && (typeof orderData.worker_company.tenant_company_id != "undefined")){
                    companyID = parseInt(orderData.worker_company.tenant_company_id );
                }


                const timeOfNotification = orderTime - (preOrderTimeToWork * 60);
                const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(cityTimeOffset);
                let startTime = timeOfNotification - tenantNowTime;
                startTime = startTime > 0 ? startTime : 0;

                let newStatusId = statusInfo.new_order.status_id;
                if(startTime > 0){
                    newStatusId = statusInfo.new_pre_order.status_id;
                }
                // для больничных заказов - свои статусы
                if((typeOrder == orderType.HOSPITAL)){
                    newStatusId = setStatusHospitalOrder(newStatusId, companyID);
                }

                orderLogger(orderId, 'info', `removeWorkerFromOrder is new status ${newStatusId}`);
                // set new status
                let isPreOrder = (newStatusId == statusInfo.new_pre_order.status_id ||
                    newStatusId == statusInfo.hospital_pre_order_company.status_id ||
                    newStatusId == statusInfo.hospital_pre_order.status_id
                ) ? true : false;


                let isWorkerOnline = false;
                redisWorkerShiftManager.getWorkerShiftId(orderData.tenant_login, orderData.worker.callsign, (err, activeShiftId) => {
                    if(err){
                        return reject(err);
                    }
                    // видитель на смене
                    isWorkerOnline = activeShiftId != null;
                    let workerCallsign = (typeof orderData.worker.callsign != undefined) ? orderData.worker.callsign : null;
                    let workerId = (typeof orderData.worker_id != undefined) ? orderData.worker_id : 0;

                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder: isPreOrder,
                        isWorkerEventOwner: false,
                        newStatusId
                    });

                    // лог  сообщения что водитель снят с заказа
                    sqlManager.logOrderChangeData(tenantId, orderId, 'remove_worker', workerCallsign, 'order', workerId , "disputcher_"  + user_id  , (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `removeWorkerFromOrder->sqlManager.logOrderChangeData->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, "info", `removeWorkerFromOrder->sqlManager.logOrderChangeData->Result: ${result}`);
                        }
                    });

                    // команда водителю отказ от заказа
                    if(isWorkerOnline){
                        let messageString = orderEventWorkerMessage({
                            command: orderEventWorkerCommand.rejectOrder,
                            tenant_id: tenantId,
                            tenant_login: orderData.tenant_login,
                            worker_callsign: workerCallsign,
                            params: {
                                order_id: orderId
                            }
                        });

                        const messageTtl = 60 * 60 * 24 * 10; // 10 days
                        amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `removeWorkerFromOrder->worker:${workeredisHospitalWorkerOrderManager.delOrderrCallsign}. amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `removeWorkerFromOrder->worker:${workerCallsign}. amqpWorkerMessageSender.sendMessage->
                                        published command: ${orderEventWorkerCommand.rejectOrder} to worker: ${workerCallsign}. Result: ${result}`);
                            }
                        });
                    }

                    if(typeOrder == orderType.HOSPITAL ){

                        redisHospitalWorkerOrderManager.delOrder(tenantId,workerCallsign, orderId ,(err, result) => {
                            if(err){
                                orderLogger(orderId, 'error', `removeWorkerFromOrder->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                            }

                            orderLogger(orderId, 'info', `removeWorkerFromOrder->redisHospitalWorkerOrderManager->delOrder->Result: ${result}`);

                        });
                    }
                    orderLogger(orderId, 'info', `removeWorkerFromOrder success remove worker!`);

                    // Запишем ответ в редис
                    let result = orderApi.getFormattedGoodResult(event.uuid, orderId, event.sender_service);
                    let messageTTL = 300;
                    // Запишем ответ в редис
                    redisAsyncAnswerManager.setAsyncAnswer(result.uuid, result, messageTTL, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderMainHandler->redisAsyncAnswerManager.setAsyncAnswer->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderMainHandler->redisAsyncAnswerManager.setAsyncAnswer->Result: ${result}`);
                        }
                    });
                    return resolve(true);
                });
            });

        } catch (err) {
            return reject(err);
        }
    })
};