'use strict';
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const redisWorkerManager = require('../../../../database/redisDB/redisWorkerManager');
const redisHospitalWorkerOrderManager = require("../../../../database/redisDB/redisHospitalWorkerOrderManager");
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderType = require("../../../orderEvent/orderDevice");
const workerStatusManager = require("../../../../services/workerStatusManager");


function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
/**
 * Get previos order status - show worker offer message
 * @param  {Number} orderId
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @return {Promise}
 */
function checkPrevPreAssignStatus(orderId, tenantId, workerCallsign){
    return new Promise((resolve, reject) => {
        try{
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                if(err){
                    return resolve(false);
                }
                let workerStatus = (typeof workerData.worker.status != undefined) ? workerData.worker.status : workerStatusManager.onOrder;

                const workerStatusArr = [
                    workerStatusManager.free
                ];

                if(workerStatusArr.includes(workerStatus)){
                    return resolve(true);
                }

                return resolve(false);
            });
        }catch(err){
            return reject(err);
        }
    });
}
/**
 * action assign to worker
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {string} eventSenderService
 * @param {boolean} showWorker
 */
function assignWorkerAction(orderInstance, orderData, eventSenderService, showWorker){
    const orderId = orderInstance.orderId;
    const tenantId = orderData.tenant_id;

    try{
        if(orderInstance.statusChanged){

            let isPreOrder = false;
            orderWorkerAssignManager.processAssignedOrder(orderData, eventSenderService, {
                isPreOrder,
                showWorker
            });
            orderWorkerAssignManager.createOrderWorkeReminder(orderInstance, orderData, false);


            const typeOrder = orderData.device.toString();
            const workerCallsign = orderData.worker && orderData.worker.callsign ? orderData.worker.callsign : null;

            if(typeOrder == orderType.HOSPITAL && workerCallsign != null){
                redisHospitalWorkerOrderManager.addOrder(tenantId, workerCallsign, orderId, (err, result) => {
                    if(err){
                        orderLogger(orderId, 'error', `orderStatusWorkerAssignedAtOrder->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                    }else{
                        orderLogger(orderId, 'info', `orderStatusWorkerAssignedAtOrder->redisHospitalWorkerOrderManager->delOrder->Sucess: ${result}`);
                    }
                });
            }
        }
    }catch(err){
        orderLogger(orderId, 'error', `orderStatusWorkerAssignedAtOrder->redisHospitalWorkerOrderManager->addOrder->Error: ${err.message}`);
    }

}

/**
 * Order status worker assigned at order
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {string} eventSenderService
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, eventSenderService) => {
    return new Promise((resolve, reject) => {

        try {
            orderInstance.orderTimer.clearAll();
            orderLogger(orderInstance.orderId, 'info', 'orderStatorderIdusWorkerAssignedAtOrder called!');


            const tenantId = orderData.tenant_id;
            const orderId = orderInstance.orderId;
            const workerCallsign = orderData.worker.callsign;
            let userModified = orderData.user_modifed;

            let showWorker = false;
            checkPrevPreAssignStatus(orderId, tenantId, workerCallsign).then(result => {
                orderLogger(orderId, 'info', `baseGootaxOrder->_initOrder->_persistLastOrderStatus->Result: ${result}`);
                showWorker = (typeof result === "boolean") ? result : false;
                assignWorkerAction(orderInstance, orderData, eventSenderService,showWorker);
            })
            .catch(err => {
                orderLogger(orderId, 'error', `baseGootaxOrder->_initOrder->_persistLastOrderStatus->Error: ${err.message}`);
                assignWorkerAction(orderInstance, orderData, eventSenderService, showWorker);
            });

            // timer to complate order - to 00:10 next day - status complate (37);
            try {

                let tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                let hour = randomInteger(0,2);
                let minute = randomInteger(1,50);
                tomorrow.setHours(hour); tomorrow.setMinutes(minute);

                let tenantNowTime = parseInt((new Date()).getTime() / 1000);
                let tomorrowTimer = parseInt(tomorrow.getTime() / 1000);

                let startTime = tomorrowTimer - tenantNowTime;
                startTime = startTime > 0 ? startTime : 1;

                let userModife = typeof orderData.worker_company.user_contact != undefined ?
                    parseInt(orderData.worker_company.user_contact) : 0;

                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.orderComplateTimeoutLabel, () => {
                    try{

                        orderLogger(orderId, 'info', `orderStatusWorkerAssignedAtOrder->${orderInstance.orderTimer.orderComplateTimeoutLabel} called!`);

                        const messageString = orderEventOrderMessage({
                            sender_service: orderEventSenderService.operatorService,
                            change_sender_id: userModife,
                            command: orderEventOrderCommand.updateOrderData,
                            tenant_id: tenantId,
                            order_id: orderId,
                            params: {
                                status_id: statusInfo.completed_paid.status_id
                            }
                        });
                        amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                            if(err){
                                orderLogger(orderId, "error", `orderStatusWorkerAssignedAtOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            }else{
                                orderLogger(orderId, 'info', `orderStatusWorkerAssignedAtOrder->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderId}. Result: ${result}`);

                            }
                        });
                    }catch(err){
                        orderLogger(orderId, 'error', `orderStatusCarAtPlace->setServiceTimeout->Error: ${err.message}`);
                    }
                }, startTime * 1000, userModife, tenantId, orderId )
            } catch (err) {
                orderLogger(orderId, 'error', `orderStatusCarAtPlace->setServiceTimeout->Error: ${err.message}`);
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    });
};

