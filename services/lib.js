/**
 * Get config
 * @return {*}
 */
exports.getConfig = () => {
    return require('dotenv-safe').load().parsed;
};

/**
 * Get service version
 */
exports.getServiceVersion = () => {
    return require('../package.json').version;
};

/**
 * Get random item from array
 * @param {Array} items
 * @return {*}
 */
exports.randomItem = (items) => {
    return items[Math.floor(Math.random() * items.length)];

};

exports.roundNumberMore = (x, n) => {
    if(isNaN(x) || isNaN(n)) return false;

    let m = Math.pow(10,n);

    return Math.round(x*m)/m;
};

/**
 * PHP serializer
 * @param mixed_value
 * @return {void|string|XML|*}
 */
exports.serialize = (mixed_value) => {
    var val, key, okey,
        ktype = '',
        vals = '',
        count = 0,
        _utf8Size = function (str) {
            var size = 0,
                i = 0,
                l = str.length,
                code = '';
            for (i = 0; i < l; i++) {
                code = str.charCodeAt(i);
                if (code < 0x0080) {
                    size += 1;
                } else if (code < 0x0800) {
                    size += 2;
                } else {
                    size += 3;
                }
            }
            return size;
        };
    var getType = function (inp) {
        var match, key, cons, types, type = typeof inp;

        if (type === 'object' && !inp) {
            return 'null';
        }
        if (type === 'object') {
            if (!inp.constructor) {
                return 'object';
            }
            cons = inp.constructor.toString();
            match = cons.match(/(\w+)\(/);
            if (match) {
                cons = match[1].toLowerCase();
            }
            types = ['boolean', 'number', 'string', 'array'];
            for (key in types) {
                if (cons == types[key]) {
                    type = types[key];
                    break;
                }
            }
        }
        return type;
    };
    type = getType(mixed_value);

    switch (type) {
        case 'function':
            val = '';
            break;
        case 'boolean':
            val = 'b:' + (mixed_value ? '1' : '0');
            break;
        case 'number':
            val = (Math.round(mixed_value) == mixed_value ? 'i' : 'd') + ':' + mixed_value;
            break;
        case 'string':
            val = 's:' + _utf8Size(mixed_value) + ':"' + mixed_value + '"';
            break;
        case 'array':
        case 'object':
            val = 'a';
            for (key in mixed_value) {
                if (mixed_value.hasOwnProperty(key)) {
                    ktype = getType(mixed_value[key]);
                    if (ktype === 'function') {
                        continue;
                    }

                    okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
                    vals += this.serialize(okey) + this.serialize(mixed_value[key]);
                    count++;
                }
            }
            val += ':' + count + ':{' + vals + '}';
            break;
        case 'undefined':
        default:
            val = 'N';
            break;
    }
    if (type !== 'object' && type !== 'array') {
        val += ';';
    }
    val = val.replace(/};/gi, '}');
    return val;
};

/**
 * Merge two arrays and removes all duplicates elements
 * @param array1
 * @param array2
 * @returns {Array}
 */
exports.mergeArray = (array1, array2) => {
    const result_array = [];
    const arr = array1.concat(array2);
    let len = arr.length;
    const assoc = {};

    while (len--) {
        const item = arr[len];

        if (!assoc[item]) {
            result_array.unshift(item);
            assoc[item] = true;
        }
    }
    return result_array;
};

/**
 * Create default service response
 * @param {Object} res
 * @param {Object} err
 */
exports.defaultApiAnswer = (res, err) => {
    let answer;
    if (err) {
        answer = {
            "result": 0,
            "error": err.message
        };
        res.write(JSON.stringify(answer));
        res.end();
    } else {
        answer = {
            "info": "ok",
            "result": 1
        };
        res.write(JSON.stringify(answer));
        res.end();
    }
};

/**
 * Create bad service response
 * @param {Object} res
 */
exports.badRequestApiAnswer = (res) => {
    const answer = {
        "result": 0,
        "error": 'Bad income params'
    };
    res.write(JSON.stringify(answer));
    res.end();
};
