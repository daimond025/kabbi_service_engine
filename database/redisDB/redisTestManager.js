"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const redis = require("redis");
const retryStrategy = require("./redisRetryStrategy");
const mainLogger = require('../../services/logger').mainLogger;

/**
 * Manager for redis test db
 * @param  port
 * @param  ip
 */
function RedisTestManager(port, ip) {
    //Test db
    this.redisClientDb = redis.createClient(port, ip, {
        retry_strategy: function (options) {
            return retryStrategy(options);
        }
    });
    this.redisClientDb.on("connect", () => {
        mainLogger('info', `connected Redis db${this.dbNumber}`);
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', `reconnecting Redis db${this.dbNumber}`);
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', `Redis error in db${this.dbNumber}: ${err.message}`);
    });

    this.dbNumber = 15;

}

/**
 * Set value
 * @param key
 * @param value
 * @return {Promise}
 */
RedisTestManager.prototype.setValue = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.redisClientDb.select(self.dbNumber, () => {
            const setResult = self.redisClientDb.set(key, value, redis.print);
            if (setResult) {
                resolve(setResult);
            } else {
                reject(new Error(`Bad redis set result: ${setResult}`));
            }
        });
    });
};


/**
 * Get value
 * @param key
 * @return {Promise}
 */
RedisTestManager.prototype.getValue = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.redisClientDb.select(self.dbNumber, () => {
            self.redisClientDb.get(key, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    });
};

/**
 * Update value
 * @param key
 * @param value
 */
RedisTestManager.prototype.updateValue = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.redisClientDb.select(self.dbNumber, () => {
            const setResult = self.redisClientDb.set(key, value, redis.print);
            if (setResult) {
                resolve(setResult);
            } else {
                reject(new Error(`Bad redis update result: ${setResult}`));
            }
        });
    });
};

/**
 * Delete key
 * @param key
 * @return {Promise}
 */
RedisTestManager.prototype.deleteKey = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.redisClientDb.select(self.dbNumber, () => {
            self.redisClientDb.del(key, (err, delResult) => {
                if (err) {
                    reject(err);
                } else {
                    if (delResult) {
                        resolve(delResult);
                    } else {
                        reject(new Error(`Bad redis delete result: ${delResult}`));
                    }

                }
            })
        });
    });
};


module.exports = exports = new RedisTestManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);