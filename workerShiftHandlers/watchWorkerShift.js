'use strict';

const serviceTimer = require('../services/serviceTimer');
const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const closeWorkerShift = require("./closeWorkerShift");
const logger = require('../services/logger');
const DataProcessingService = require('../services/dataProcessingService');
const workerShiftLogger = logger.workerShiftLogger;
const config = require("../services/lib").getConfig();
const nodeId = config.MY_ID;
const amqp = require('amqplib/callback_api');
const async = require("async");
const geoLib = require("geolib");
/**
 * Saving worker services data
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
function saveWorkerShiftServerData(tenantId, workerCallsign, callback) {
    workerShiftLogger(tenantId, workerCallsign, 'info', 'Start saving worker shift server data to redis');
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->saveWorkerShiftServerData->redisWorkerManager.getWorker->Error:${err.message}`);
            return callback(err);
        }
        if (!workerData) {
            return callback(new Error('No worker at redisDB'));
        }
        workerData.server_id = nodeId;
        workerData.process_id = process.pid;
        redisWorkerManager.saveWorker(workerData, (err, result) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->saveWorkerShiftServerData->redisWorkerManager.saveWorker->Error: ${err.message}`);
                return callback(err);
            }
            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->saveWorkerShiftServerData->redisWorkerManager.saveWorker->Result: ${result}`);
            callback(null, 1);
        });
    });
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


/**
 * Is good worker parking position
 * @param tenantId
 * @param workerCallsign
 * @param workerData
 * @param parkingType
 * @param {function}  callback
 */
function isGoodWorkerParkingPosition(tenantId, workerCallsign, workerData, parkingType, callback) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->isGoodWorkerParkingPosition called!`);
    try {
        const workerStatus = workerData.worker.status;
        const cityId = workerData.worker.city_id;
        const geoData = workerData.geo;
        const workerLat = !isNaN(parseFloat(geoData.lat)) ? parseFloat(geoData.lat) : null;
        const workerLon = !isNaN(parseFloat(geoData.lon)) ? parseFloat(geoData.lon) : null;
        if (!workerLat || !workerLon) {
            return callback(null, false);
        }
        if (['ON_ORDER', 'OFFER_ORDER'].indexOf(workerStatus) !== -1) {
            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->isGoodWorkerParkingPosition->parking is good. worker status: ${workerStatus}`);
            return callback(null, true);
        }
        sqlManager.getParkingPolygon(tenantId, cityId, parkingType, (err, parkingPolygon) => {
            if (err) {
                return callback(err);
            }
            if (!parkingPolygon) {
                return callback(null, true);
            }
            try {
                const isWorkerInside = geoLib.isPointInside(
                    {latitude: workerLat, longitude: workerLon},
                    parkingPolygon
                );
                if (!isWorkerInside) {
                    workerShiftLogger(tenantId, workerCallsign, 'info ', `watchWorkerShift->isGoodWorkerParkingPosition->Worker is not in parking`);
                    workerShiftLogger(tenantId, workerCallsign, 'info ', `watchWorkerShift->isGoodWorkerParkingPosition->Worker geodata:`);
                    workerShiftLogger(tenantId, workerCallsign, 'info ', geoData);
                }
                return callback(null, isWorkerInside);
            } catch (err) {
                return callback(err);
            }
        })
    } catch (err) {
        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->isGoodWorkerParkingPosition->Error: ${err.message}`);
        return callback(err);
    }
}

/**
 * Worker parking position check timer (Close shift, if  special setting exists)
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param shiftId
 * @param parkingCheckType
 * @param badParkingPositionMaxMin
 * @param workerShiftTimer
 */
function workerParkingPositionCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, parkingCheckType, badParkingPositionMaxMin, workerShiftTimer) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerParkingPositionCheckerTimer called!`);
    const positionCheckerIntervalSec = getRandomInt(40, 80);
    workerShiftTimer.setServiceInterval('workerParkingPositionChecker', () => {
        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->redisWorkerManager.getWorker->Error: ${err.message}`);
            } else if (!workerData) {
                workerShiftLogger(tenantId, workerCallsign, 'info ', 'watchWorkerShift->redisWorkerManager.getWorker->No worker in redisDB')
            } else {
                workerParkingPositionChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, parkingCheckType, badParkingPositionMaxMin, workerShiftTimer);
            }
        })
    }, positionCheckerIntervalSec * 1000);
}

/**
 * Worker parking position checker
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param workerData
 * @param shiftId
 * @param parkingCheckType
 * @param badParkingPositionMaxMin
 * @param workerShiftTimer
 */
function workerParkingPositionChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, parkingCheckType, badParkingPositionMaxMin, workerShiftTimer) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerParkingPositionChecker called!`);
    try {
        isGoodWorkerParkingPosition(tenantId, workerCallsign, workerData, parkingCheckType, (err, isGoodParking) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerParkingPositionChecker->isGoodWorkerParkingPosition->Error: ${err.message}`);
            } else if (isGoodParking) {
                workerShiftLogger(tenantId, workerCallsign, 'info ', `watchWorkerShift->workerParkingPositionChecker->Worker in parking. Do not need start worker parking checker.`);
                workerShiftTimer.clearServiceTimeout('workerParkingPositionCheckerTimeout');
            } else {
                workerShiftLogger(tenantId, workerCallsign, 'info ', `watchWorkerShift->workerParkingPositionChecker->Worker is not in parking`);
                let workerParkingPositionCheckerTimeoutExist = false;
                for (let timer of workerShiftTimer.activeTimers) {
                    if (timer.timerLabel === 'workerParkingPositionCheckerTimeout') {
                        workerParkingPositionCheckerTimeoutExist = true;
                    }
                }
                if (workerParkingPositionCheckerTimeoutExist) {
                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerParkingPositionChecker->Don't need start worker parking checker. There are active timer exist`);
                } else {
                    workerShiftTimer.setServiceTimeout('workerParkingPositionCheckerTimeout', () => {
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerParkingPositionChecker->workerParkingPositionCheckerTimeout->redisWorkerManager.getWorker: Error: ${err.message}`);
                            } else if (!workerData) {
                                workerShiftLogger(tenantId, workerCallsign, 'info ', 'watchWorkerShift->workerParkingPositionChecker->workerParkingPositionCheckerTimeout->redisWorkerManager.getWorker->No worker in redisDB')
                            }
                            isGoodWorkerParkingPosition(tenantId, workerCallsign, workerData, parkingCheckType, (err, isGoodParking) => {
                                if (err) {
                                    workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerParkingPositionChecker->isGoodWorkerParkingPosition->Error: ${err.message}`);
                                } else if (isGoodParking) {
                                    workerShiftLogger(tenantId, workerCallsign, 'info ', `watchWorkerShift->workerParkingPositionChecker->isGoodWorkerParkingPosition!!`);
                                } else {
                                    workerShiftTimer.clearAll();
                                    closeWorkerShift(tenantId, tenantLogin, workerCallsign, shiftId, 'bad_parking', 'system', nodeId, (err, result) => {
                                        if (err) {
                                            workerShiftLogger(tenantId, workerCallsign, 'error', `workerParkingPositionChecker->closeWorkerShift->Error: ${err.message}`);
                                        } else {
                                            workerShiftLogger(tenantId, workerCallsign, 'info', `workerParkingPositionChecker->closeWorkerShift->Result: ${result}`);
                                        }
                                    });
                                }
                            });
                        })
                    }, badParkingPositionMaxMin * 60 * 1000);
                }
            }
        });
    } catch (err) {
        workerShiftLogger(tenantId, workerCallsign, 'error', err);
    }
}

/**
 * Worker coords update checker (Close shift, if  special setting exists)
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param shiftId
 * @param badUpdateCoordsMaxMin
 * @param workerShiftTimer
 */
function workerCoordsUpdateCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, badUpdateCoordsMaxMin, workerShiftTimer) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateCheckerTimer called!`);
    const positionCheckerIntervalSec = getRandomInt(40, 80);
    workerShiftTimer.setServiceInterval('workerCoordsUpdateChecker', () => {
        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateCheckerTimer->Start checking worker position update time...`);
        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerCoordsUpdateCheckerTimer->getWorker->Error: ${err.message}`);
            } else if (!workerData) {
                workerShiftLogger(tenantId, workerCallsign, 'info ', 'watchWorkerShift->workerCoordsUpdateCheckerTimer->No worker in redisDB')
            } else {
                workerCoordsUpdateChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, badUpdateCoordsMaxMin, workerShiftTimer);
            }
        })
    }, positionCheckerIntervalSec * 1000);
}


/**
 *  Worker coords update checker
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param workerData
 * @param shiftId
 * @param badUpdateCoordsMaxMin
 * @param workerShiftTimer
 */
function workerCoordsUpdateChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, badUpdateCoordsMaxMin, workerShiftTimer) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker called!`);
    try {
        let workerStatus = workerData.worker.status;
        if (['ON_ORDER', 'OFFER_ORDER'].indexOf(workerStatus) !== -1) {
            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->Don't need close shift. Worker status: ${workerStatus}`);
        } else {
            let geoData = workerData.geo;
            if (geoData.hasOwnProperty("update_time")) {
                let lastUpdateTime = parseInt(geoData.update_time);
                let needCloseWorkerShift = false;
                let nowTimestamp = parseInt((new Date()).getTime() / 1000);
                let badUpdateCoordsMaxSec = badUpdateCoordsMaxMin * 60;
                if (nowTimestamp - lastUpdateTime > badUpdateCoordsMaxSec) {
                    needCloseWorkerShift = true;
                }
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->Now timestamp: ${nowTimestamp}`);
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->Last position update timestamp: ${lastUpdateTime}`);
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->nowTimestamp - lastUpdateTime=${nowTimestamp - lastUpdateTime}`);
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->Need close worker shift? result : ${needCloseWorkerShift}`);
                if (needCloseWorkerShift) {
                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->Worker geodata: ${geoData}`);
                    workerShiftTimer.clearAll();
                    closeWorkerShift(tenantId, tenantLogin, workerCallsign, shiftId, 'bad_coords', 'system', nodeId, (err, result) => {
                        if (err) {
                            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerCoordsUpdateChecker->closeWorkerShift->Error: ${err.message}`);
                        } else {
                            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->closeWorkerShift->Result: ${result}`);
                        }
                    });
                }
            } else {
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerCoordsUpdateChecker->worker has't filed update_time in geo object`);
            }
        }
    } catch (err) {
        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerCoordsUpdateChecker->Error: ${err.message}`);
    }
}

/**
 * Coords and parking position checker
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param shiftId
 * @param checkSettings
 * @param workerShiftTimer
 */
function coordsAndParkingPositionCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, checkSettings, workerShiftTimer) {
    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->coordsAndParkingPositionCheckerTimer called!`);
    const positionCheckerIntervalSec = getRandomInt(40, 80);
    const badUpdateCoordsMaxMin = checkSettings.workerCoordsPositionSettings.bad_update_coords_max_min;
    const badParkingPositionMaxMin = checkSettings.workerParkingPositionSettings.bad_parking_position_max_min;
    const parkingCheckType = checkSettings.workerParkingPositionSettings.parking_check_type;
    workerShiftTimer.setServiceInterval('coordsAndParkingPositionChecker', () => {
        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->coordsAndParkingPositionChecker->Start checking coords and parking position ...`);
        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerCoordsUpdateChecker->redisWorkerManager.getWorker->Error: ${err.message}`);
            } else if (!workerData) {
                workerShiftLogger(tenantId, workerCallsign, 'info ', 'watchWorkerShift->workerCoordsUpdateChecker->redisWorkerManager.getWorker->No worker in redisDB')
            } else {
                workerCoordsUpdateChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, badUpdateCoordsMaxMin, workerShiftTimer);
                workerParkingPositionChecker(tenantId, tenantLogin, workerCallsign, workerData, shiftId, parkingCheckType, badParkingPositionMaxMin, workerShiftTimer);
            }
        })
    }, positionCheckerIntervalSec * 1000);
}

/**
 * Worker Position Checker
 * @param tenantId
 * @param tenantLogin
 * @param workerCallsign
 * @param shiftId
 * @param workerShiftTimer
 * @param callback
 */
function workerPositionChecker(tenantId, tenantLogin, workerCallsign, shiftId, workerShiftTimer, callback) {
    workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->workerPositionChecker called!');
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->redisWorkerManager.getWorker->Error: ${err.message}`);
            return callback(err);
        }
        if (!workerData) {
            return callback(new Error('No worker at redisDB'));
        }
        const workerCityId = workerData.worker.city_id;
        const workerPositionId = workerData.position.position_id;
        async.parallel({
            workerCoordsPositionSettings: callback => {
                const workerCoordsPositionSetting = {
                    need_check: 0,
                    bad_update_coords_max_min: null
                };
                sqlManager.getTenantSettingValue(tenantId, workerCityId, 'NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME', workerPositionId, (err, setting) => {
                    if (err) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->sqlManager.getTenantSettingValue->Error: ${err.message}`);
                        return callback(null, workerCoordsPositionSetting);
                    }
                    setting = parseInt(setting);
                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME value: ${setting}`);
                    if (setting === 1) {
                        sqlManager.getTenantSettingValue(tenantId, workerCityId, 'WORKER_POSITION_CHECK_DELAY_TIME', workerPositionId, (err, checkDelayMin) => {
                            if (err) {
                                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->sqlManager.getTenantSettingValue->Error: ${err.message}`);
                                return callback(null, workerCoordsPositionSetting);
                            }
                            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->WORKER_POSITION_CHECK_DELAY_TIME value: ${checkDelayMin}`);
                            if (checkDelayMin) {
                                workerCoordsPositionSetting.need_check = 1;
                                workerCoordsPositionSetting.bad_update_coords_max_min = checkDelayMin;
                                return callback(null, workerCoordsPositionSetting);
                            } else {
                                workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->workerPositionChecker->Don not need check worker position update time');
                                return callback(null, workerCoordsPositionSetting);
                            }
                        })
                    } else {
                        workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->workerPositionChecker->Don not need check worker position update time');
                        return callback(null, workerCoordsPositionSetting);
                    }
                })
            },
            workerParkingPositionSetting: callback => {
                const workerParkingPositionSetting = {
                    need_check: 0,
                    bad_parking_position_max_min: null,
                    parking_check_type: null
                };
                sqlManager.getTenantSettingValue(tenantId, workerCityId, 'NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION', workerPositionId, (err, setting) => {
                    if (err) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->sqlManager.getTenantSettingValue->Error: ${err.message}`);
                        return callback(null, workerParkingPositionSetting);
                    }
                    setting = parseInt(setting);
                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION value: ${setting}`);
                    if (setting === 1) {
                        sqlManager.getTenantSettingValue(tenantId, workerCityId, 'WORKER_PARKING_POSITION_CHECK_DELAY_TIME', workerPositionId, (err, checkDelayMin) => {
                            if (err) {
                                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->sqlManager.getTenantSettingValue->Error: ${err.message}`);
                                return callback(null, workerParkingPositionSetting);
                            }
                            if (checkDelayMin) {
                                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->WORKER_PARKING_POSITION_CHECK_DELAY_TIME value: ${checkDelayMin}`);
                                sqlManager.getTenantSettingValue(tenantId, workerCityId, 'WORKER_PARKING_POSITION_CHECK_TYPE', workerPositionId, (err, parkingCheckType) => {
                                    if (err) {
                                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->sqlManager.getTenantSettingValue->Error: ${err.message}`);
                                        return callback(null, workerParkingPositionSetting);
                                    }
                                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->WORKER_PARKING_POSITION_CHECK_TYPE value: ${parkingCheckType}`);
                                    if (['basePolygon', 'reseptionArea'].indexOf(parkingCheckType) === -1) {
                                        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->Bad parking checker type. ParkingCheckType = ${parkingCheckType}`);
                                        return callback(null, workerParkingPositionSetting);
                                    }
                                    workerParkingPositionSetting.need_check = 1;
                                    workerParkingPositionSetting.bad_parking_position_max_min = checkDelayMin;
                                    workerParkingPositionSetting.parking_check_type = parkingCheckType;
                                    return callback(null, workerParkingPositionSetting);

                                })
                            } else {
                                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerParkingPositionChecker->Dont need check worker parking position. CheckDelayMin:${checkDelayMin}`);
                                return callback(null, workerParkingPositionSetting);
                            }
                        })
                    } else {
                        workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->workerParkingPositionChecker->Dont need check worker parking posistion');
                        return callback(null, workerParkingPositionSetting);
                    }
                })
            }

        }, (err, checkSettings) => {
            if (err) {
                return callback(err);
            }
            let needCheckCoords = !!(checkSettings && checkSettings.workerCoordsPositionSettings && parseInt(checkSettings.workerCoordsPositionSettings.need_check) === 1);
            let needCheckParking = !!( checkSettings && checkSettings.workerParkingPositionSettings && parseInt(checkSettings.workerParkingPositionSettings.need_check) === 1);
            if (needCheckCoords && needCheckParking) {
                coordsAndParkingPositionCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, checkSettings, workerShiftTimer)
            } else if (needCheckCoords) {
                workerCoordsUpdateCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, checkSettings.workerCoordsPositionSettings.bad_update_coords_max_min, workerShiftTimer);
            } else if (needCheckParking) {
                workerParkingPositionCheckerTimer(tenantId, tenantLogin, workerCallsign, shiftId, checkSettings.workerParkingPositionSettings.parking_check_type, checkSettings.workerParkingPositionSettings.bad_parking_position_max_min, workerShiftTimer);
            } else {
                workerShiftLogger(tenantId, workerCallsign, 'info ', 'watchWorkerShift->workerParkingPositionChecker->Dont need check worker position and coors update time');
            }
            return callback(null, 1);
        })
    })
}


/**
 * Creating service that waiting event of ending worker shift
 * @param  {Number} tenantId
 * @param  {String} tenantLogin
 * @param  {Number} workerCallsign
 * @param  {Number} shiftLifeTime
 * @param  {Number} shiftId
 * @param  {Function} callback
 */
module.exports = function (tenantId, tenantLogin, workerCallsign, shiftLifeTime, shiftId, callback) {
    workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift called!");
    saveWorkerShiftServerData(tenantId, workerCallsign, (err, result) => {
        if (err) {
            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->saveWorkerShiftServerData->Error: ${err.message}`);
            return callback(err);
        }
        const workerShiftTimer = new serviceTimer();
        DataProcessingService.addWorkerShift(shiftId);
        try {
            workerShiftTimer.setServiceTimeout('workerShiftTimer', () => {
                workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->workerShiftTimer timeout!');
                workerShiftTimer.clearAll();
                closeWorkerShift(tenantId, tenantLogin, workerCallsign, shiftId, 'timer', 'system', nodeId, (err, result) => {
                    if (err) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->closeWorkerShift->Error: ${err.message}`);
                        workerShiftLogger(tenantId, workerCallsign, 'error', err);
                    } else {
                        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->closeWorkerShift->Result: ${result}`);
                    }
                });
            }, shiftLifeTime * 1000);
        } catch (err) {
            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->setServiceTimeout->Error: ${err.message}`);
        }
        workerPositionChecker(tenantId, tenantLogin, workerCallsign, shiftId, workerShiftTimer, (err, result) => {
            if (err) {
                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->workerPositionChecker->Error: ${err.message}`);
            } else {
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->workerPositionChecker->Result: ${result}`);
            }
        });
        workerShiftLogger(tenantId, workerCallsign, 'info', 'watchWorkerShift->WorkerShiftTimer was created! Will be called after shiftLifeTime ' + shiftLifeTime + ' seconds.');
        let connSelfClosed = false;
        let chanelSelfClosed = false;
        startListenQueue();
        return callback(null, 1);
        function startListenQueue() {
            amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
                if (err) {
                    workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->connect->Error: ${err.message}. Need to try reconnect...`);
                    return setTimeout(startListenQueue, 2000);
                }
                conn.on("error", err => {
                    workerShiftLogger(tenantId, workerCallsign, 'error', err.message);
                    if (err.message !== "Connection closing") {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->connection->Error ${err.message}`);
                    }
                });
                conn.on("close", () => {
                    if (!connSelfClosed) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->connection closed, need to try reconnect...`);
                        return setTimeout(startListenQueue, 2000);
                    }
                });
                workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->rabbitmq->connected`);
                conn.createChannel((err, ch) => {
                    if (err) {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->create channel->Error: ${err.message}`);
                    }
                    ch.on("error", (err) => {
                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->chanel->Error: ${err.message}`);
                    });
                    ch.on("close", () => {
                        if (!chanelSelfClosed) {
                            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->rabbitmq->channel closed`);
                        }
                    });
                    const q = `${tenantId}_worker_${workerCallsign}_shift`;
                    const messageWorkTime = 2;
                    ch.assertQueue(q, {
                        durable: true
                    });
                    ch.prefetch(1);
                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->Waiting  task for ${tenantId}_worker_${workerCallsign}_shift`);
                    ch.consume(q, message => {
                        if (message) {
                            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->New task for ${tenantId}_worker_${workerCallsign}_shift!`);
                            let taskObject;
                            try {
                                taskObject = JSON.parse(message.content.toString());
                            } catch (err) {
                                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->Task content parsing->Error: ${err.message}`);
                            } finally {
                                if (taskObject) {
                                    workerShiftLogger(tenantId, workerCallsign, 'info', taskObject);
                                    if (taskObject.command === "worker_end_work") {
                                        workerShiftTimer.clearAll();
                                        DataProcessingService.delWorkerShift(shiftId);
                                        try {
                                            ch.deleteQueue(q, {ifUnused: false, ifEmpty: false}, (err, ok) => {
                                                if (err) {
                                                    workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->deleteQueue->Error: ${err.message}`);
                                                } else {
                                                    workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->${tenantId}_worker_${workerCallsign}_shift queue deleted.`);
                                                    try {
                                                        ch.close();
                                                        chanelSelfClosed = true;
                                                        workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift->AMQP channel closed.");
                                                    } catch (err) {
                                                        workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->AMQP channel close->Error: ${err.message}`);
                                                    }
                                                    try {
                                                        conn.close();
                                                        connSelfClosed = true;
                                                        workerShiftLogger(tenantId, workerCallsign, 'info', "watchWorkerShift->AMQP connection closed.");
                                                    } catch (err) {
                                                        workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->AMQP connection close->Error: ${err.message}`);
                                                    }
                                                }
                                            });
                                        } catch (err) {
                                            workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->deleteQueue->Error: ${err.message}`);
                                        }
                                    } else {
                                        setTimeout(() => {
                                            workerShiftLogger(tenantId, workerCallsign, 'info', `watchWorkerShift->Task done!`);
                                            try {
                                                ch.ack(message);
                                            } catch (err) {
                                                workerShiftLogger(tenantId, workerCallsign, 'error', `watchWorkerShift->ch.ack->Error: ${err.message}`);
                                            }
                                        }, messageWorkTime * 500);
                                    }
                                }
                            }
                        }
                    }, {noAck: false});
                });
            });
        }
    });
};






