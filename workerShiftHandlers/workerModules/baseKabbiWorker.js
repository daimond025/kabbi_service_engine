"use strict";

const DataProcessingService = require('../../services/dataProcessingService');
const redisWorkerManager = require('../../database/redisDB/redisWorkerManager');
const redisWorkerShiftManager = require('../../database/redisDB/redisWorkerShiftManager');
const redisWorkerReservedOrderListManager = require('../../database/redisDB/redisWorkerReservedOrderListManager');
const redisOrderManager = require('../../database/redisDB/redisOrderManager');
const redisParkingManager = require('../../database/redisDB/redisParkingManager');
const workerSettingsManager = require('../../orderHandlers/orderModules/common/orderSettingsManager');
const sqlManager = require('../../database/sqlDB/sqlManager');
const workerEventCommand = require('./common/workerEventCommand');

const amqpWorkerShiftMessageSender = require("../../services/amqpWorkerShiftMessageSender");
const async = require("async");
const geoLib = require("geolib");
const uuid = require('uuid/v4');
const config = require("../../services/lib").getConfig();
const nodeId = config.MY_ID;
const OrderTimer = require("../../services/serviceTimer");
const workerStatus = require("../../services/workerStatusManager");
const logger = require('../../services/logger');
const workerShiftLogger = logger.workerShiftLogger;

/**
 * Base Kabbi worker
 * @class BaseKabbiWorker
 */
class BaseKabbiWorker{

    /**
     * @constructor
     * @param {Number} tenantId
     * @param {Number} workerCallsign
     * @param {Number} shiftLifeTime
     */
    constructor(tenantId, workerCallsign, shiftLifeTime){
        workerShiftLogger(tenantId, workerCallsign, 'info', `BaseKabbiWorker called!`);

        this.tenantId = tenantId;
        this.tenantLogin = null;
        this.workerCallsign = workerCallsign;
        this.shiftLifeTime = shiftLifeTime;

        this.shiftTimeStart = 0;

        // Service to manage order timers events
        this.orderTimer = new OrderTimer();

        // settings
        this.settings = {};

        // default setting
        this.parkingDelay = 10;
        this.parkingType = 'basePolygon';
        this.positionDelay = 10;

        this._inited = false;

        // close shift
        this._shift = false;

        // some worker params
        this.workerParkingId = 0;
        this.workerShiftId = 0;

        // status cancel end shift
        this.worker_work = [
            workerStatus.onOrder,
            workerStatus.offerOrder,
            workerStatus.removeFromOrder,
        ];

        this._initOrder();
    }

    /**
     * Init order
     * @private
     */
    _initOrder(){
        try{
            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->_initOrder called!`);
            redisWorkerManager.getWorker(this.tenantId, this.workerCallsign, (err, workerData) => {
                if(err){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->_initOrder->Error of getting order from redis: ${err.message}. ${this.tenantId} ${this.workerCallsign}`);
                    return;
                }

                this.workerParkingId = workerData.geo.parking_id;
                this.workerShiftId = parseInt(workerData.worker.worker_shift_id);
                this.tenantLogin = workerData.worker.tenant_login;
                this.shiftTimeStart = (typeof workerData.worker.worker_shift_start !== "undefined") ? parseInt(workerData.worker.worker_shift_start) : Math.floor(Date.now() / 1000);

                //Order sent from external services
                async.waterfall([
                    callback => {
                        // setting worker
                        workerSettingsManager.persistWorkerSettings(this.tenantId, this.workerCallsign)
                        .then(workerSettings => {
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->_initOrder->workerSettingsManager.persistSettings->Result: ${workerSettings}`);
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', workerSettings);

                            //Set settings
                            this.settings = workerSettings;

                            // not working same time
                            if((this.settings.WORKER_PARKING_POSITION_CHECK_DELAY_TIME === this.settings.NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME)){
                                this.settings.WORKER_PARKING_POSITION_CHECK_DELAY_TIME += this.getRandomInt(1, 3)

                            }

                            return callback(null, 1);
                        })
                        .catch(err => {
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->initOrder->orderSettingsManager.persistSettings->Error: ${err.message}`);
                            return callback(err);
                        })
                    },
                    //Save system settings(server_id,process_id...)
                    (prevResult, callback) => {
                        workerSettingsManager.setWorkerSystemSettings(this.tenantId, this.workerCallsign)
                        .then(result => {
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->initOrder->orderSettingsManager.setSystemSettings->Result: ${result}`);
                            return callback(null, 1);
                        })
                        .catch(err => {
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->initOrder->orderSettingsManager.setSystemSettings->Error: ${err.message}`);
                            return callback(null, 1);
                        })
                    },

                    // set timer to finish shift
                    (prevResult, callback) => {
                        this.workerShiftTimer((rezult) => {
                            return callback(null, 1);
                        });
                    },

                    // set interval workerParkingPositionChecker
                    (prevResult, callback) => {
                        this.workerParkingPositionChecker((rezult) => {
                            return callback(null, 1);
                        });
                    },

                    // set interval workerCoordsUpdateCheker
                    (prevResult, callback) => {
                        this.workerCoordsUpdateChecker((rezult) => {
                            return callback(null, 1);
                        });
                    },
                ], (err, result) => {
                    if(err){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->initOrder->Error: ${err.message}`);
                        return;
                    }
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->initOrder->Result: ${result}`);
                    DataProcessingService.addWorkerShift(this.workerShiftId);
                    this._inited = true;

                    console.log("Init worker shift handler:  " + this.workerCallsign);
                });

            });
        }catch(err){
            console.log(err);
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->_initOrder->Error: ${err.message}. ${this.tenantId} ${this.workerCallsign}`);
        }
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * SET WORK SHIFT TIMER
     */
    workerShiftTimer(callback){
        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerShiftTimer called!`);
        try{
            let time_end = this.shiftLifeTime + this.shiftTimeStart;
            let time_now = Math.floor(Date.now() / 1000);
            let time = (time_end - time_now);
            time = time > 0 ? time * 1000 : 1;
            //time = 10 * 1000; // todo test

            this.orderTimer.setServiceTimeout(this.orderTimer.workerShiftTimer, () => {
                let self = this;
                this.workerShiftEnd(function(err, rezult){
                    if(err){
                        workerShiftLogger(self.tenantId, self.workerCallsign, 'error', `BaseKabbiWorker->workerShiftEnd->Error: ${err.message}`);
                    }else{
                        const need_close = (typeof rezult === "boolean") ? rezult : false;
                        console.log("Смена водителя по таймеру");
                        console.log(need_close);

                        if(need_close){
                            self.orderTimer.clearAll();
                            self.close_shift('timer', 'system', nodeId);
                        }
                    }
                });

            }, time);

            callback(null, 1);
        }catch(err){
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerShiftTimer->Error: ${err.message}`);
            return callback(err);
        }
    }

    /**
     * funtion end work shift
     */
    workerShiftEnd(callback){
        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerShiftEnd called!`);
        let need_close = false;
        try{
            redisWorkerManager.getWorker(this.tenantId, this.workerCallsign, (err, workerData) => {
                if(err){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerShiftEnd->redisWorkerManager.getWorker->Error: ${err.message}`);
                    need_close = true;
                    return callback(new Error('No worker at redis Db'), need_close);
                }
                if(!workerData){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerShiftEnd->redisWorkerManager.getWorker->Error: ${err.message}`);
                    need_close = true;
                    return callback(new Error('No worker at redis Db'), need_close);
                }

                workerShiftLogger(this.tenantId, this.workerCallsign, "info", `BaseKabbiWorker->workerShiftEnd->worker status="${workerData.worker.status}"`);

                let active_orders = typeof workerData.worker.active_orders === "object" ?
                    Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])) : [];


                if(active_orders.length > 0){
                    let lastOrderId = parseInt(active_orders.shift());
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerShiftEnd->active_orders ${active_orders.join(',')} in workerCallsign: ${this.workerCallsign}`);

                    redisOrderManager.getOrder(this.tenantId, lastOrderId, (err, orderData) => {
                        if(err){
                            need_close = true;
                            return callback(new Error(`No order ${lastOrderId} at redis Db`), need_close);
                        }else{
                            workerData.worker.need_close_shift = 1;
                            redisWorkerManager.saveWorker(workerData, (err, result) => {
                                if(err){
                                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerShiftEnd->redisOrderManager.saveWorker->Error: ${err.message}`);
                                    return callback(new Error(`BaseKabbiWorker->redisOrderManager.saveWorker->Error worker ${err.message} at redis Db`), need_close);
                                }
                                return callback(null, need_close);
                            });
                        }
                    });
                }else{
                    need_close = true;
                    return callback(null, need_close);
                }

            });
        }catch(err){
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerShiftEnd->Error: ${err.message}`);
            return callback(err);
        }

    }


    /**
     * SET WORKER PAKING TIMER
     */
    workerParkingPositionChecker(callback){
        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerParkingPositionChecker called!`);
        try{
            const setting = parseInt(this.settings.NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION);
            if(setting === 1){

                let parking_delay = typeof (this.settings.WORKER_PARKING_POSITION_CHECK_DELAY_TIME !== undefined) ?
                    parseInt(this.settings.WORKER_PARKING_POSITION_CHECK_DELAY_TIME) : this.parkingDelay;

                parking_delay = parking_delay * 60 * 1000;
                //parking_delay = 5 * 1000; // todo Test

                let parking_type = typeof (this.settings.WORKER_PARKING_POSITION_CHECK_TYPE !== undefined) ?
                    (this.settings.WORKER_PARKING_POSITION_CHECK_TYPE) : this.parkingType;

                this.orderTimer.setServiceInterval(this.orderTimer.workerParkingTimeoutLabel, () => {
                    let self = this;
                    this.positionWorker(parking_type, function(err, rezult){
                        if(err){
                            workerShiftLogger(self.tenantId, self.workerCallsign, 'error', `BaseKabbiWorker->positionWorker->Error: ${err.message}`);
                        }else{
                            const need_close = (typeof rezult === "boolean") ? rezult : false;

                            console.log("Смена водителя по паркингу");
                            console.log(need_close);
                            if(need_close){
                                self.orderTimer.clearAll();
                                self.close_shift('bad_parking', 'system', nodeId);
                            }
                        }
                    });

                }, parking_delay);

            }
            callback(null, 1);
        }catch(err){
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->positionWorker->Error: ${err.message}`);
            return callback(err);
        }
    }

    /**
     * get position aria and close worker shift
     * @param {String} parkingType
     * @param {Function} callback
     */
    positionWorker(parkingType, callback){
        let need_close = false;
        try{
            redisWorkerManager.getWorker(this.tenantId, this.workerCallsign, (err, workerData) => {
                if(err){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->positionWorker->redisWorkerManager.getWorker->Error: ${err.message}`);
                    return callback(new Error('No worker at redis Db'), need_close);
                }else if(!workerData){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', 'BaseKabbiWorker->positionWorker->redisWorkerManager.getWorker->No worker in redisDB');
                    need_close = true;
                    return callback(new Error('No worker at redis Db'), need_close);
                }else{
                    const workerStatus = workerData.worker.status;
                    const cityId = workerData.worker.city_id;
                    const geoData = workerData.geo;
                    const workerLat = !isNaN(parseFloat(geoData.lat)) ? parseFloat(geoData.lat) : null;
                    const workerLon = !isNaN(parseFloat(geoData.lon)) ? parseFloat(geoData.lon) : null;

                    if(this.worker_work.indexOf(workerStatus) !== -1){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->positionWorker->parking is good. worker status: ${workerStatus}`);
                        return callback(null, need_close);
                    }

                    sqlManager.getParkingPolygon(this.tenantId, cityId, parkingType, (err, parkingPolygon) => {
                        if(err){
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error ', `BaseKabbiWorker->positionWorker->sqlManager->-getParkingPolygon->Error: ${err.message}`);
                            return callback(err, need_close);
                        }
                        if(!parkingPolygon){
                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', `BaseKabbiWorker->positionWorker->sqlManager->-getParkingPolygon->noPoligonType: ${parkingPolygon}`);
                            return callback(new Error('No parkingPolygon at redis DB'), need_close);
                        }
                        try{
                            const isWorkerInside = geoLib.isPointInside(
                                {latitude: workerLat, longitude: workerLon},
                                parkingPolygon
                            );

                            if(!isWorkerInside){
                                workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', `BaseKabbiWorker->positionWorker->isGoodWorkerParkingPosition->Worker is not in parking`);
                                workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', `BaseKabbiWorker->positionWorker->isGoodWorkerParkingPosition->Worker geodata:`);
                                workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', geoData);
                            }
                            need_close = !isWorkerInside;
                            return callback(null, need_close);
                        }catch(err){
                            return callback(err, need_close);
                        }
                    });
                }
            });
        }catch(err){
            console.log(err);
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->positionWorker->Error: ${err.message}`);
            return callback(err);
        }
    }


    /**
     * SET WORKER UPDATE COORDS TIMER
     */
    workerCoordsUpdateChecker(callback){
        try{
            const setting = parseInt(this.settings.NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME);
            if(setting === 1){

                let position_delay = typeof (this.settings.WORKER_POSITION_CHECK_DELAY_TIME !== undefined) ?
                    parseInt(this.settings.WORKER_POSITION_CHECK_DELAY_TIME) : this.positionDelay;

                let limit_update_time = position_delay;

                position_delay = position_delay * 60 * 1000;
                //position_delay = 5 * 1000; // todo test


                this.orderTimer.setServiceInterval(this.orderTimer.workerCoordsTimeoutLabel, () => {
                    let self = this;
                    this.workerCoords(limit_update_time, function(err, rezult){
                        if(err){
                            workerShiftLogger(self.tenantId, self.workerCallsign, 'error', `BaseKabbiWorker->workerCoords->Error: ${err.message}`);
                        }else{
                            const need_close = (typeof rezult === "boolean") ? rezult : false;
                            console.log("Смена водителя по обновлениям координат");
                            console.log(rezult);
                            
                            if(need_close){
                                self.orderTimer.clearAll();
                                self.close_shift('bad_coords', 'system', nodeId);
                            }
                        }

                    });

                }, position_delay);

            }
            callback(null, 1);
        }catch(err){
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->positionWorker->Error: ${err.message}`);
            return callback(err);
        }
    }

    /**
     * get time  update coord and close worker shift
     * @param {number} limit_time
     * @param {Function} callback
     */
    workerCoords(limit_time, callback){
        let need_close = false;
        try{
            redisWorkerManager.getWorker(this.tenantId, this.workerCallsign, (err, workerData) => {
                if(err){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->coordesWorker->redisWorkerManager.getWorker->Error: ${err.message}`);
                    return callback(new Error('No worker at redis Db'), need_close);
                }else if(!workerData){
                    workerShiftLogger(this.tenantId, this.workerCallsign, 'info ', 'BaseKabbiWorker->coordesWorker->redisWorkerManager.getWorker->No worker in redisDB');
                    need_close = true;
                    return callback(new Error('No worker at redis Db'), need_close);
                }else{
                    const workerStatus = workerData.worker.status;
                    if(this.worker_work.indexOf(workerStatus) !== -1){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->coordesWorker->status is good. worker status: ${workerStatus}`);
                        return callback(null, need_close);
                    }

                    let geoData = workerData.geo;
                    if(geoData.hasOwnProperty("update_time")){
                        let lastUpdateTime = parseInt(geoData.update_time);
                        let nowTimestamp = parseInt((new Date()).getTime() / 1000);
                        let updateCoordsMaxSec = limit_time * 60;
                        if(nowTimestamp - lastUpdateTime > updateCoordsMaxSec){
                            need_close = true;
                        }
                    }else{
                        need_close = true;
                    }
                    return callback(null, need_close);
                }
            });
        }catch(err){
            console.log(err);
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->coordesWorker->Error: ${err.message}`);
            return callback(err);
        }
    }


    /**
     * send message to queue to close shift worker
     * @param {String} EndReason
     * @param {String} SenderType
     * @param {String} SenderId
     */
    close_shift(EndReason, SenderType, SenderId){
        try{
            const messageData = JSON.stringify({
                uuid: uuid(),
                type: 'order_event',
                timestamp: Math.floor(Date.now() / 1000),
                sender_service: "engine_service",
                command: workerEventCommand.workerEndWork,
                tenant_id: this.tenantId,
                worker_callsign: this.workerCallsign,
                change_sender_id: nodeId,
                params: {
                    shiftEndReason: EndReason,
                    shiftEndSenderType: SenderType,
                    shiftEndId: SenderId,

                }
            });

            if(!this._shift){
                amqpWorkerShiftMessageSender.sendMessage(this.tenantId, this.workerCallsign, messageData, (err, result) => {
                    if(err){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `close_shift->amqpWorkerShiftMessageSender.sendMessage->Error: ${err.message}`);
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `close_shift->amqpWorkerShiftMessageSender.sendMessage->Error: ${err}`);
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `close_shift->amqpWorkerShiftMessageSender.sendMessage->Error: ${err.toString()}`);
                    }else{
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', 'close_shift->Published event at chanel:' +  this.tenantLogin + "_worker_" +  this.workerCallsign + '_shift');
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', messageData);
                    }
                });
            }

        }catch(err){
            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->close_shift->Error: ${err.message}. ${this.tenantId} ${this.workerCallsign}`);
        }
    }


    /**
     * Do worker event
     * @param event
     * @returns {Promise<boolean>}doEvent
     */
    async doEvent(event){

        let result = false;

        switch(event.command){
            case workerEventCommand.workerEndWork:
                result = await this.workerEndShift(event);
                break;
            default:
                result = false;
        }
        return result;
    }

    /**
     * Do worker event
     * @param event
     * @returns {Promise<boolean>}doEvent
     */
    workerEndShift(event){
        return new Promise((resolve, reject) => {
            try{
                const shiftEndReason = typeof event.params.shiftEndReason !== "undefined" ? event.params.shiftEndReason : 'timer';
                const shiftEndEventSenderType = typeof event.params.shiftEndSenderType !== "undefined" ? event.params.shiftEndSenderType : 'system';
                const shiftEndEventSenderId = typeof event.params.shiftEndId !== "undefined" ? event.params.shiftEndId : nodeId;

                redisWorkerManager.getWorker(this.tenantId, this.workerCallsign, (err, workerData) => {
                    if(err){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->coordesWorker->redisWorkerManager.getWorker->Error: ${err.message}`);
                        return resolve(true);
                    }
                    const workerStatus = workerData.worker.status;
                    if(this.worker_work.indexOf(workerStatus) !== -1){
                        workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->status is good. worker status: ${workerStatus}`);
                        return resolve(false);
                    }else{
                        async.parallel({
                                delWorkerFromParking: callback => {
                                    if(this.workerParkingId){
                                        redisParkingManager.deleteWorkerFromParking(this.workerParkingId, this.workerCallsign, (err, result) => {
                                            if(err){
                                                workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->deleteWorkerFromParking->Error: ${err.message}`);
                                                return callback(err);
                                            }else{
                                                workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->deleteWorkerFromParking->Result: ${result}`);
                                            }
                                            return callback(null, 1);
                                        });
                                    }else{
                                        return callback(null, 1);
                                    }
                                },
                                delWorkerFromOnline: callback => {
                                    //return callback(null, 1);
                                    redisWorkerManager.deleteWorker(this.tenantId, this.workerCallsign, (err, result) => {
                                        if(err){
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->redisDeleteWorker->Error: ${err.message}`);
                                            return callback(err);
                                        }else{
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->redisDeleteWorker->Result: ${result}`);
                                        }
                                        return callback(null, 1);
                                    });
                                },
                                delWorkerShift: callback => {
                                    redisWorkerShiftManager.deleteWorkerShift(this.tenantLogin, this.workerCallsign, (err, result) => {
                                        if(err){
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->deleteWorkerShift->Error: ${err.message}`);
                                            return callback(err);
                                        }else{
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->deleteWorkerShift->Result: ${result}`);
                                        }
                                        return callback(null, 1);
                                    });
                                },
                                closeShiftInMysql: callback => {
                                    sqlManager.logEndOfWorkerShift(this.workerShiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, (err, result) => {
                                        if(err){
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->logEndOfWorkerShift->Error: ${err.message}`);
                                            return callback(err);
                                        }else{
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->logEndOfWorkerShift->Result: ${JSON.stringify(result)}`);
                                        }
                                        return callback(null, 1);
                                    });
                                },
                                delWorkerReservedOrderList: callback => {
                                    redisWorkerReservedOrderListManager.delAll(this.tenantId, this.workerCallsign, (err, result) => {
                                        if(err){
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->delWorkerReservedOrderList->Error: ${err.message}`);
                                            return callback(err);
                                        }else{
                                            workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->delWorkerReservedOrderList->Result: ${result}`);
                                        }
                                        return callback(null, 1);
                                    });
                                }
                            },
                            (err, results) => {
                                if(err){
                                    workerShiftLogger(this.tenantId, this.workerCallsign, 'error', `BaseKabbiWorker->workerEndShift->Error: ${err.message}`);
                                    return resultCallback(err);
                                }
                                workerShiftLogger(this.tenantId, this.workerCallsign, 'info', `BaseKabbiWorker->workerEndShift->Result: ${JSON.stringify(results)}`);

                                this._shift = true;
                                DataProcessingService.delWorkerShift(this.workerShiftId);
                                this.orderTimer.clearAll();
                                return resolve(true);
                            });

                    }
                });
            }catch(err){
                return reject(err);
            }
        });
    }
}


module.exports = BaseKabbiWorker;