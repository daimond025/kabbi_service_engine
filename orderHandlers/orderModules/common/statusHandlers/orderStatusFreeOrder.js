'use strict';

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderNotificationManager = require('../orderNotificationManager');
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const orderActionManager = require('../orderActionManager');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");

/**
 * Order status free order
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {Object} messageParams
 * @return {*}
 */
module.exports = (orderInstance, orderData, messageParams) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusFreeOrder called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;
            if (typeof messageParams === "object" && messageParams.hasOwnProperty('is_from_order_distribution_cycle') && messageParams.is_from_order_distribution_cycle) {
                orderInstance.orderTimer.clearAllExeptThis([orderInstance.orderTimer.distributionCyclesTimeoutLabel]);
            } else {
                orderInstance.orderTimer.clearAll();
            }
            if (orderInstance.statusChanged) {
                orderInstance.startWaitingForPaymentTimestamp = null;
                orderNotificationManager.sendUpdateFreeOrdersListToWorkers(
                    orderData,
                    {
                        sendPushFreeOrderForAll: orderInstance.settings.SEND_PUSH_FREE_ORDER_FOR_ALL,
                        distanceToFilterFreeOrders: orderInstance.settings.DISTANCE_TO_FILTER_FREE_ORDERS,
                        dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                        exceptCarModels: orderInstance.exceptCarModels
                    })
                    .then(result => {
                        orderLogger(orderId, 'info', `orderStatusFreeOrder->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(orderId, 'error', `orderStatusFreeOrder->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Error: ${err.message}`);
                    });
            }
            const nowTime = parseInt((new Date()).getTime() / 1000);
            const orderExpiredTime = !isNaN(parseFloat(orderData.order_time)) ? parseFloat(orderData.order_time) : 0;
            const timeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
            const timeDiff = parseInt(orderExpiredTime) - (parseInt(nowTime) + parseInt(timeOffset));
            orderLogger(orderId, 'info', `orderStatusFreeOrder->OrderLateTimeout will bew called after ${timeDiff / 60} min`);
            if (orderData.worker && orderData.worker.callsign) {
                const workerCallsign = orderData.worker.callsign;
                const workerAssignedNewStatuses = [
                    statusInfo.worker_assigned_at_order_soft.status_id,
                    statusInfo.worker_assigned_at_order_hard.status_id
                ];
                const workerAssignedPreStatuses = [
                    statusInfo.worker_assigned_at_preorder_soft.status_id,
                    statusInfo.worker_assigned_at_preorder_hard.status_id,
                    statusInfo.worker_accepted_preorder.status_id
                ];
                let orderWasAssigned = false;
                let penultimateStatus;
                let isPreOrder = false;
                if (orderInstance.statusArr.length >= 2 && orderInstance.statusArr[orderInstance.statusArr.length - 2]) {
                    penultimateStatus = orderInstance.statusArr[orderInstance.statusArr.length - 2];
                    //Заказ был назначен
                    if (workerAssignedNewStatuses.indexOf(penultimateStatus) !== -1 || workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                    }
                    if (workerAssignedPreStatuses.indexOf(penultimateStatus) !== -1) {
                        orderWasAssigned = true;
                        isPreOrder = true;
                    }
                }
                if (orderWasAssigned) {
                    orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                        isPreOrder: isPreOrder,
                        isWorkerEventOwner: false,
                    });
                    setOrderLateTimeout(orderId, tenantId, orderInstance.orderTimer, timeDiff);
                } else {
                    orderActionManager.deleteWorkersFromOrder(orderData)
                        .then(result => {
                            orderLogger(orderId, "info", `orderStatusFreeOrder->deleteWorkersFromOrder->Result: ${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderId, "error", `orderStatusFreeOrder->deleteWorkersFromOrder->Error: ${err.message}`);
                        })
                        .then(() => {
                            setOrderLateTimeout(orderId, tenantId, orderInstance.orderTimer, timeDiff);
                        });
                }
                if (orderInstance.workerLateTimestamp !== null && (orderInstance.statusArr[orderInstance.statusArr.length - 2] === statusInfo.worker_coming_later.status_id)) {
                    try {
                        const lateTime = Math.floor(Date.now() / 1000) - orderInstance.workerLateTimestamp;
                        orderInstance.workerLateTimestamp = null;
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                orderLogger(orderId, "error", `orderStatusFreeOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
                            } else {
                                const workerShiftId = workerData.worker.worker_shift_id;
                                sqlManager.logWorkerLateTime(workerShiftId, lateTime, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusFreeOrder->sqlManager.logWorkerLateTime->Result: ${result}`);
                                    }
                                });
                            }
                        })
                    } catch (err) {
                        orderLogger(orderId, "error", `orderStatusFreeOrder->Error: ${err.message}`);
                    }
                }
            } else {
                setOrderLateTimeout(orderId, tenantId, orderInstance.orderTimer, timeDiff);
            }

            //* TODO релаизация преложения заказа заныытым водителям
            /*if(parseInt(orderInstance.settings.OFFER_FREE_ORDER) === 1){
                orderLogger(orderId, 'info', 'orderActionManager->rejectOrderByTimeout->rejectOrderByTimeout called!');
                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.orderRejectTimeout,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.orderRejectTimeout} to order: ${orderId}. Result: ${result}`);
                    }
                });
            }*/

            orderActionManager.rejectOrderByTimeout(orderInstance, orderData)
                .then(result => {
                    orderLogger(orderId, 'info', `orderStatusFreeOrder->orderActionManager.rejectOrderByTimeout->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderId, 'info', `orderStatusFreeOrder->orderActionManager.rejectOrderByTimeout->Error: ${err.message}`);
                });
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};

/**
 * Set order late time out
 * @param orderId
 * @param tenantId
 * @param orderTimer
 * @param timeDiff
 */
function setOrderLateTimeout(orderId, tenantId, orderTimer, timeDiff) {
    orderLogger(orderId, 'info', 'orderStatusFreeOrder->setOrderLateTimeout CALLED!');
    try {
        orderTimer.setServiceTimeout(orderTimer.orderLateTimeoutLabel, () => {
            orderLogger(orderId, 'info', 'orderStatusFreeOrder->setOrderLateTimeout->orderLateTimeout CALLED!');
            const messageString = orderEventOrderMessage({
                command: orderEventOrderCommand.orderLateTimeout,
                tenant_id: tenantId,
                order_id: orderId,
                params: {}
            });
            amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                if (err) {
                    orderLogger(orderId, "error", `orderStatusFreeOrder->setOrderLateTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                } else {
                    orderLogger(orderId, 'info', `orderStatusFreeOrder->setOrderLateTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.orderLateTimeout} to order: ${orderId}. Result: ${result}`);
                }
            });
        }, timeDiff * 1000, orderId, tenantId);
    } catch (err) {
        orderLogger(orderId, 'error', `orderStatusFreeOrder->setOrderLateTimeout->setServiceTimeout->Error: ${err.message}`);
    }
}
