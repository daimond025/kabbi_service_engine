'use strict';
const logger = require('../../../services/logger');
const async = require('async');
const orderLogger = logger.orderLogger;
const redisWorkerManager = require("../../../database/redisDB/redisWorkerManager");
const redisOrderManager = require('../../../database/redisDB/redisOrderManager');
const sqlManager = require("../../../database/sqlDB/sqlManager");
const pushApi = require("../../../services/externalWebServices/pushApi");
const amqpWorkerMessageSender = require("../../../services/amqpWorkerMessageSender");
const amqpOrderMessageSender = require("../../../services/amqpOrderMessageSender");
const closeWorkerShift = require("../../../workerShiftHandlers/closeWorkerShift");
const config = require('../../../services/lib').getConfig();
const nodeId = config.MY_ID;
const redisWorkerReservedOrderListManager = require('../../../database/redisDB/redisWorkerReservedOrderListManager');
const orderEventOrderMessage = require('../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../orderEvent/orderEventWorkerCommand');
const redisActiveOrderPhoneManager = require('../../../database/redisDB/redisActiveOrderPhoneManager');
const statusInfo = require('../../../services/orderStatusManager').statusInfo;
const workerStatusManager = require("../../../services/workerStatusManager");
const mergeArray = require("../../../services/lib").mergeArray;

/**
 * @class OrderActionManager
 */
class OrderActionManager {
    /**
     * @constructor
     */
    constructor() {
    }

    /**
     * Send order to free status
     * @param {Object} orderData
     * @param {Boolean} isFromDistributionCycle
     * @return {Promise}
     */
    sendOrderToFreeStatus(orderData, isFromDistributionCycle) {
        function setOrderFreeStatus() {
            return new Promise((resolve, reject) => {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus called!');
                const orderId = parseInt( orderData.order_id);
                const tenantId = orderData.tenant_id;
                orderData.status_id = statusInfo.free_order.status_id;
                orderData.status.status_id = statusInfo.free_order.status_id;
                orderData.status.name = statusInfo.free_order.name;
                orderData.status.status_group = statusInfo.free_order.status_group;
                orderData.worker_id = null;
                orderData.car_id = null;
                orderData.worker = null;
                orderData.car = null;
                orderData.workers_offered = [];
                orderData.workers_offered_responses = [];
                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                orderData.status_time = updateTime;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->redisOrderManager.saveOrder->Error: ${err.message}`);
                        return reject(err);
                    }
                    orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->redisOrderManager.saveOrder->Result: ${result}`);
                    sqlManager.updateOrderFromMysql(orderId, statusInfo.free_order.status_id, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                        }
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`)
                    });

                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {
                            is_from_order_distribution_cycle: isFromDistributionCycle
                        }
                    });


                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            return reject(err);
                        } else {
                            orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.updateOrderData} to order ${orderId}.Result: ${result}`);
                            return resolve(1);
                        }
                    });

                })
            });
        }

        return new Promise((resolve, reject) => {
            const orderId = orderData.order_id;
            orderLogger(orderData.order_id, 'info', 'orderActionManager->sendOrderToFreeStatus called!');
            let offeredWorkersList = typeof orderData.workers_offered === "object" ? Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];
            //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
            if (typeof orderData.offered_workers_list === "object") {
                offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
            }
            if ((orderData.worker && orderData.worker.callsign) || (offeredWorkersList.length > 0)) {
                orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->Order has workers: ${offeredWorkersList}!`);
                this.deleteWorkersFromOrder(orderData)
                    .then(result => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->deleteWorkersFromOrder->Result: ${result}`);
                        return setOrderFreeStatus();
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToFreeStatus->deleteWorkersFromOrder->Error: ${err.message}`);
                        return setOrderFreeStatus();
                    })
                    .then(setFreeStatusResult => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->Result: ${setFreeStatusResult}`);
                        return resolve(1);
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->Error: ${err.message}`);
                        return reject(err);
                    })
            } else {
                setOrderFreeStatus()
                    .then(setFreeStatusResult => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToFreeStatus->Result: ${setFreeStatusResult}`);
                        return resolve(1);

                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToFreeStatus->setOrderFreeStatus->Error: ${err.message}`);
                        return reject(err);
                    })
            }
        });
    }

    /**
     * Send order to manual status(freeze order)
     * @param {Object} orderData
     * @param {Boolean} isFromDistributionCycle
     * @return {Promise}
     */
    sendOrderToManualStatus(orderData, isFromDistributionCycle) {

        function setOrderManualStatus() {
            return new Promise((res, rej) => {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->sendOrderToManualStatus->setOrderManualStatus called!');
                const orderId = orderData.order_id;
                const tenantId = orderData.tenant_id;
                orderData.status_id = statusInfo.manual_mode.status_id;
                orderData.status.status_id = statusInfo.manual_mode.status_id;
                orderData.status.name = statusInfo.manual_mode.name;
                orderData.status.status_group = statusInfo.manual_mode.status_group;
                orderData.worker_id = null;
                orderData.car_id = null;
                orderData.worker = null;
                orderData.car = null;
                orderData.workers_offered = [];
                orderData.workers_offered_responses = [];
                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                orderData.status_time = updateTime;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->redisOrderManager.saveOrder->Error: ${err.message}`);
                        return rej(err);
                    }
                    orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->redisOrderManager.saveOrder->Result: ${result}`);
                    sqlManager.updateOrderFromMysql(orderId, statusInfo.manual_mode.status_id, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                        }
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`)
                    });

                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {
                            is_from_order_distribution_cycle: isFromDistributionCycle
                        }
                    });
                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            return rej(err);
                        } else {
                            orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.updateOrderData} to order ${orderId}.Result: ${result}`);
                            return res(1);
                        }
                    });
                })
            });
        }

        return new Promise((resolve, reject) => {
            const orderId = orderData.order_id;
            orderLogger(orderData.order_id, 'info', 'orderActionManager->sendOrderToManualStatus called!');
            let offeredWorkersList = typeof orderData.workers_offered === "object" ? Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];
            //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
            if (typeof orderData.offered_workers_list === "object") {
                offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
            }
            if ((orderData.worker && orderData.worker.callsign) || (offeredWorkersList.length > 0)) {
                orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->Order has workers: ${offeredWorkersList}!`);
                this.deleteWorkersFromOrder(orderData)
                    .then(result => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->deleteWorkersFromOrder->Result: ${result}`);
                        return setOrderManualStatus();
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToManualStatus->deleteWorkersFromOrder->Error: ${err.message}`);
                        return setOrderManualStatus();
                    })
                    .then(setManualStatusResult => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->Result: ${setManualStatusResult}`);
                        return resolve(1);
                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->Error: ${err.message}`);
                        return reject(err);
                    })
            } else {
                setOrderManualStatus()
                    .then(setManualStatusResult => {
                        orderLogger(orderId, "info", `orderActionManager->sendOrderToManualStatus->Result: ${setManualStatusResult}`);
                        return resolve(1);

                    })
                    .catch(err => {
                        orderLogger(orderId, "error", `orderActionManager->sendOrderToManualStatus->setOrderManualStatus->Error: ${err.message}`);
                        return reject(err);
                    })
            }
        });
    }

    /**
     * Reject order by timeout if order older then ORDER_REJECT_TIME setting
     * @param {BaseGootaxOrder} orderInstance
     * @param {object} orderData
     * @returns {Promise}
     */
    rejectOrderByTimeout(orderInstance, orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->rejectOrderByTimeout called!');
                const tenantId = orderData.tenant_id;
                const orderId = orderData.order_id;
                const orderRejectTime = !isNaN(parseFloat(orderInstance.settings.ORDER_REJECT_TIME)) ? parseFloat(orderInstance.settings.ORDER_REJECT_TIME) : 0;

                if (!orderRejectTime) {
                    orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->empty value of setting ORDER_REJECT_TIME. Value: ${orderRejectTime}`);
                    return resolve(1);
                }
                let cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
                let orderTime = !isNaN(parseFloat(orderData.order_time)) ? parseFloat(orderData.order_time) : 0;
                let timeOfReject = parseInt(orderTime) + parseInt(orderRejectTime * 60);
                let tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(cityTimeOffset);
                let startTime = parseInt(timeOfReject) - parseInt(tenantNowTime);
                if (startTime <= 0) {
                    startTime = 1;
                }

                orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->reject order by time will bew called after ${startTime / 60} min`);
                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.rejectOrderByTimeoutLabel, () => {
                    orderLogger(orderId, 'info', 'orderActionManager->rejectOrderByTimeout->rejectOrderByTimeout called!');
                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.orderRejectTimeout,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {}
                    });
                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->rejectOrderByTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.orderRejectTimeout} to order: ${orderId}. Result: ${result}`);
                        }
                    });
                }, startTime * 1000, tenantId, orderId);
                return resolve(1);
            } catch (err) {
                orderLogger(orderData.order_id, 'error', `orderActionManager->rejectOrderByTimeout->Error: ${err.message}`);
                return reject(err);
            }
        });
    }

    /**
     * Send event `order_reject_timeout` if order older then 3 moths
     * @param orderInstance
     * @param orderData
     * @returns {Promise<any>}
     */
    forceRejectOutOfDateOrder(orderInstance, orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->forceRejectOutOfDateOrder called!');
                const tenantId = orderData.tenant_id;
                const orderId = orderData.order_id;
                const orderRejectTime = 60 * 60 * 24 * 90;// ~3 months
                let cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
                let orderTime = !isNaN(parseFloat(orderData.order_time)) ? parseFloat(orderData.order_time) : 0;
                let timeOfReject = parseInt(orderTime) + orderRejectTime;
                let tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(cityTimeOffset);
                let startTime = parseInt(timeOfReject) - parseInt(tenantNowTime);
                if (startTime <= 0) {
                    startTime = 1;
                }
                orderLogger(orderId, 'info', `orderActionManager->forceRejectOutOfDateOrder->reject order by time will bew called after ${startTime / 60} min`);
                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.rejectOrderByTimeoutLabel, () => {
                    orderLogger(orderId, 'info', 'orderActionManager->forceRejectOutOfDateOrder timer called!');
                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.orderRejectTimeout,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {}
                    });
                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'info', `orderActionManager->forceRejectOutOfDateOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->forceRejectOutOfDateOrder->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.orderRejectTimeout} to order: ${orderId}. Result: ${result}`);
                        }
                    });
                }, startTime * 1000, tenantId, orderId);
                return resolve(1);
            } catch (err) {
                orderLogger(orderData.order_id, 'error', `orderActionManager->forceRejectOutOfDateOrder->Error: ${err.message}`);
                return reject(err);
            }
        });
    }

    /**
     * Send order to exchange by timeout
     * @param orderInstance
     * @param orderData
     * @returns {Promise<any>}
     */
    sendOrderToExchangeByTimeout(orderInstance, orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->sendOrderToExchangeByTimeout called!');
                const tenantId = orderData.tenant_id;
                const orderId = orderData.order_id;
                const needSendToExchange = !isNaN(parseInt(orderInstance.settings.EXCHANGE_ENABLE_SALE_ORDERS)) ? parseInt(orderInstance.settings.EXCHANGE_ENABLE_SALE_ORDERS) : 0;
                if (!needSendToExchange) {
                    orderLogger(orderId, 'info', `orderActionManager->sendOrderToExchangeByTimeout->Not need send order to exchange. EXCHANGE_ENABLE_SALE_ORDERS: ${needSendToExchange}`);
                    return resolve(1);
                }
                const sendingTime = !isNaN(parseInt(orderInstance.settings.EXCHANGE_HOW_WAIT_WORKER)) ? parseInt(orderInstance.settings.EXCHANGE_HOW_WAIT_WORKER) : false;
                orderLogger(orderId, 'info', `orderActionManager->sendOrderToExchangeByTimeout->order will be send to exchange after ${sendingTime} seconds`);
                orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.sendToExchangeTimeoutLabel, () => {
                    orderLogger(orderId, 'info', 'orderActionManager->sendOrderToExchangeByTimeout->sendOrderToExchangeByTimeout called!');
                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.sendOrderToExchange,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {}
                    });
                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'info', `orderActionManager->sendOrderToExchangeByTimeout->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->sendOrderToExchangeByTimeout->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.sendOrderToExchange} to order: ${orderId}. Result: ${result}`);
                        }
                    });
                }, sendingTime * 1000, tenantId, orderId);
                return resolve(1);
            } catch (err) {
                orderLogger(orderData.order_id, 'error', `orderActionManager->sendOrderToExchangeByTimeout->Error: ${err.message}`);
                return reject(err);
            }
        })
    }


    /**
     * Delete order phone from redis
     * @param {object} orderData
     * @returns {Promise}
     */
    deleteOrderPhone(orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->deleteOrderPhone called!');
                const phoneValue = orderData.phone;
                const tenantId = orderData.tenant_id;
                const orderId = orderData.order_id;
                redisActiveOrderPhoneManager.gerOrdersByPhone(tenantId, phoneValue, (err, orders) => {
                    if (err) {
                        orderLogger(orderId, 'info', `orderActionManager->redisActiveOrderPhoneManager.gerOrdersByPhone->Error: ${err.message}`);
                        return reject(err);
                    }
                    if (!orders) {
                        return resolve(1);
                    }
                    if (typeof orders === "object" && orders !== null) {
                        let orderIdsArr = Object.keys(orders).map(k => orders[k]);
                        orderIdsArr = orderIdsArr.filter(i => parseInt(i) !== parseInt(orderId));
                        if (orderIdsArr.length === 0) {
                            redisActiveOrderPhoneManager.deletePhoneOrders(tenantId, phoneValue, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'info', `orderActionManager->redisActiveOrderPhoneManager.deletePhoneOrders->
                                    Error: ${err.message}`);
                                    return reject(err);
                                }
                                return resolve(1);
                            })
                        } else {
                            redisActiveOrderPhoneManager.setOrdersByPhone(tenantId, phoneValue, orderIdsArr, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'info', `orderActionManager->redisActiveOrderPhoneManager.setOrdersByPhone->
                                    Error: ${err.message}`);
                                    return reject(err);
                                }
                                return resolve(1);
                            });
                        }
                    } else {
                        return resolve(1);
                    }
                })
            } catch (err) {
                return reject(err);
            }
        });
    }

    /**
     * Delete workers from order
     * @param {object} orderData
     * @returns {Promise}
     */
    deleteWorkersFromOrder(orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->deleteWorkerFormOrder called!');
                const orderId =  parseInt(orderData.order_id);
                const tenantId = orderData.tenant_id;
                const statusId = orderData.status_id;
                const tenantLogin = orderData.tenant_login;
                const workerCallsign = orderData.worker ? orderData.worker.callsign : null;
                let offeredWorkersList = typeof orderData.workers_offered === "object" ?
                    Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];
                //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
                if (typeof orderData.offered_workers_list === "object") {
                    offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
                }
                orderData.worker_id = null;
                orderData.car_id = null;
                orderData.worker = null;
                orderData.car = null;
                orderData.workers_offered = [];
                orderData.workers_offered_responses = [];
                orderData.deny_refuse_order = 0;
                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->saveOrder->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->saveOrder->Result: ${result}`);
                    }
                    sqlManager.updateOrderFromMysql(orderId, statusId, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->updateOrderFromMysql->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->updateOrderFromMysql->Result: ${result}`);
                        }
                    });
                    sqlManager.updateOrderField(orderId, 'deny_refuse_order', 0, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->updateOrderField->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->updateOrderField->Result: ${result}`);
                        }
                    });
                    let deleteWorkersList = workerCallsign ? [workerCallsign] : [];
                    if (Array.isArray(offeredWorkersList) && offeredWorkersList.length > 0) {
                        deleteWorkersList = mergeArray(offeredWorkersList, deleteWorkersList);
                    }
                    if (!deleteWorkersList.length) {
                        orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->order ${orderId} have't workers to delete`);
                        resolve(1);
                    }

                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->deleteWorkersList:${deleteWorkersList}`);
                    async.each(deleteWorkersList,
                        (deleteWorkerCallsign, callback) => {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->Order has worker: ${deleteWorkerCallsign}`);
                            redisWorkerReservedOrderListManager.delOrder(tenantId, deleteWorkerCallsign, orderId, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->redisWorkerReservedOrderListManager.delOrder->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->redisWorkerReservedOrderListManager.delOrder->result:${result}`);
                                }
                            });
                            redisWorkerManager.getWorker(tenantId, deleteWorkerCallsign, (err, workerDataTemp) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->getWorker->Error: ${err.message}`);
                                    return callback(null, 1);
                                }
                                let needSetWorkerToFree = false;
                                if (workerDataTemp.worker && workerDataTemp.worker.status && workerDataTemp.worker.status === workerStatusManager.offerOrder) {
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->getWorker->Worker status == ${workerStatusManager.offerOrder}`);
                                    if (parseInt(workerDataTemp.worker.last_offered_order_id) === parseInt(orderId)) {
                                        workerDataTemp.worker.status = workerStatusManager.free;
                                        needSetWorkerToFree = true;
                                    }
                                }
                                if (workerDataTemp.worker && workerDataTemp.worker.status && (workerDataTemp.worker.status === workerStatusManager.onOrder || workerDataTemp.worker.status === workerStatusManager.free)) {
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->getWorker->Worker status == ON_ORDER`);

                                    // список активных заказов - удаление
                                    let active_orders = typeof workerDataTemp.worker.active_orders === "object" ?
                                        Object.keys(workerDataTemp.worker.active_orders).map(key => parseInt(workerDataTemp.worker.active_orders[key])): [];
                                    let index_del = active_orders.indexOf(orderId);
                                    if(index_del > -1){
                                        active_orders.splice(index_del, 1);
                                        needSetWorkerToFree = true;
                                    }
                                    /*console.log('ВОДИТЕЛЬ УДАЛИТЬ2 С ЗАКАЗА  - список активных заказов');
                                    console.log(active_orders);
                                    console.log(index_del);*/

                                    workerDataTemp.worker.active_orders = active_orders;
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->active_orders ${active_orders.join(',')} in workerCallsign: ${workerCallsign}`);
                                    //if (parseInt(workerDataTemp.worker.last_order_id) === parseInt(orderId)) {
                                    if (active_orders.length === 0) {
                                        workerDataTemp.worker.status = workerStatusManager.free;
                                        workerDataTemp.worker.last_order_id = null;
                                    }
                                    if (index_del > (-1)){

                                    }
                                }

                                const needCloseShift = parseInt(workerDataTemp.worker.need_close_shift) === 1;
                                if (needCloseShift) {
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->Need close shift of worker:  ${deleteWorkerCallsign} `);
                                    const workerShiftId = workerDataTemp.worker.worker_shift_id;
                                    closeWorkerShift(tenantId, tenantLogin, deleteWorkerCallsign, workerShiftId, 'timer', 'system', nodeId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->closeWorkerShift->Error: ${err.message}`);
                                        }
                                        orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->closeWorkerShift->Result: ${result}`);
                                    })
                                }
                                if (!needSetWorkerToFree ) {
                                    return callback(null, 1);
                                }
                                pushApi.rejectWorkerFromOrder(tenantId, orderId, deleteWorkerCallsign, statusId, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->rejectWorkerFromOrder->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->rejectWorkerFromOrder->Result: ${result}`);
                                    }
                                });
                                redisWorkerManager.saveWorker(workerDataTemp, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->saveWorker->Error: ${err.message}`);
                                        return callback(null, 1);
                                    }
                                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->saveWorker->Result: ${result}`);
                                    sqlManager.logOrderChangeData(tenantId, orderId, 'status', deleteWorkerCallsign, 'worker', workerStatusManager.free, 'system', (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->logOrderChangeData->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->logOrderChangeData->Result: ${result}`);
                                        }
                                    });
                                    let messageString = orderEventWorkerMessage({
                                        command: orderEventWorkerCommand.rejectWorkerFromOrder,
                                        tenant_id: tenantId,
                                        tenant_login: tenantLogin,
                                        worker_callsign: deleteWorkerCallsign,
                                        params: {
                                            order_id: orderId
                                        }
                                    });
                                    const messageTtl = 60 * 60 * 24 * 10; // 10 days
                                    amqpWorkerMessageSender.sendMessage(tenantId, deleteWorkerCallsign, messageString, messageTtl, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->amqpWorkerMessageSender.sendMessage->published command: ${orderEventWorkerCommand.rejectWorkerFromOrder} to worker ${deleteWorkerCallsign}.Result: ${result}`);
                                        }
                                    });
                                    return callback(null, 1);
                                });
                            })
                        }, (err) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->async.each(deleteWorkersList)->Error: ${err.message}`);
                            }
                            return resolve(1);
                        });
                });
            } catch (err) {
                return reject(err);
            }
        });
    }

    /**
     * Delete worker from pre order
     * @param {object} orderData
     * @returns {Promise}
     */
    deleteWorkerFromPreOrder(orderData) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', 'orderActionManager->deleteWorkerFromPreOrder called!');
                const orderId = orderData.order_id;
                const tenantId = orderData.tenant_id;
                const statusId = orderData.status_id;
                const tenantLogin = orderData.tenant_login;
                const workerCallsign = orderData.worker ? orderData.worker.callsign : null;
                orderData.worker_id = null;
                orderData.car_id = null;
                orderData.worker = null;
                orderData.car = null;
                orderData.deny_refuse_order = 0;
                const updateTime = Math.floor(Date.now() / 1000);
                orderData.update_time = updateTime;
                if (workerCallsign) {
                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->order has worker: ${workerCallsign}`);
                    redisWorkerReservedOrderListManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->redisWorkerReservedOrderListManager.delOrder->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->redisWorkerReservedOrderListManager.delOrder->Result:${result}`);
                        }
                    });
                    sqlManager.updateOrderFromMysql(orderId, statusId, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormPreOrder->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                        }
                    });
                    sqlManager.updateOrderField(orderId, 'deny_refuse_order', 0, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormPreOrder->sqlManager.updateOrderField->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->sqlManager.updateOrderField->Result: ${result}`);
                        }
                    });
                    pushApi.rejectWorkerFromOrder(tenantId, orderId, workerCallsign, statusId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormPreOrder->rejectWorkerFromOrder->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->rejectWorkerFromOrder->Result: ${result}`);
                        }
                    });
                    const messageString = orderEventWorkerMessage({
                        command: orderEventWorkerCommand.rejectWorkerFromOrder,
                        tenant_id: tenantId,
                        tenant_login: tenantLogin,
                        worker_callsign: workerCallsign,
                        params: {
                            order_id: orderId
                        }
                    });
                    const messageTtl = 60 * 60 * 24 * 10; // 10 days
                    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', err);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->amqpWorkerMessageSender.sendMessage->published command: ${orderEventWorkerCommand.rejectWorkerFromOrder} to worker ${workerCallsign}.Result: ${result}`);
                        }
                    });
                    redisOrderManager.saveOrder(orderData, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderActionManager->deleteWorkerFormOrder->redisOrderManager.saveOrder->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormOrder->redisOrderManager.saveOrder->Result: ${result}`);
                        }
                        return resolve(1);
                    })
                } else {
                    orderLogger(orderId, 'info', `orderActionManager->deleteWorkerFormPreOrder->Order ${orderId} have't worker`);
                    return resolve(1);
                }
            } catch (err) {
                return reject(err);
            }
        });
    }
}

module.exports = exports = new OrderActionManager();
