'use strict';
const DataProcessingService = require('./dataProcessingService');
const lib = require("../services/lib");
const config = lib.getConfig();
const consulServiceWorker = require('../services/consul/consulServiceWorker');

class Metric {

    constructor() {

    }

    /**
     * Get Metric
     * @param format
     * @returns {string}
     */
    getMetric(format) {
        const activeOrdersCount = DataProcessingService.activeOrdersListLength;
        const activeWorkerShiftsCount = DataProcessingService.activeWorkerShiftsListLength;
        const activeWorkerBlocksCount = DataProcessingService.activeWorkerBlocksListLength;
        const metrics = {
            activeOrdersCount,
            activeWorkerShiftsCount,
            activeWorkerBlocksCount
        };
        let result = '';
        switch (format) {
            case 'influx':
                result = this._getMetricForInfluxDB(metrics);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Get metric for InfluxDB
     * @param {object} metrics
     * @param {string} metrics.activeOrdersCount
     * @param {string} metrics.activeWorkerShiftsCount
     * @param {string} metrics.activeWorkerBlocksCount
     * @param {object} metrics
     * @returns {string}
     * @private
     */
    _getMetricForInfluxDB(metrics) {
        const iMaster = consulServiceWorker.status === "master";
        return `${config.CONSUL_SERVICE_NAME},host=service-engine${config.MY_ID} master=${iMaster},` +
            `orders_count=${metrics.activeOrdersCount},` +
            `worker_shifts_count=${metrics.activeWorkerShiftsCount},` +
            `worker_blocks_count=${metrics.activeWorkerBlocksCount}`;
    }
}

module.exports = exports = new Metric();