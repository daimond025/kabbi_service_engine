'use strict';

const serviceTimer = require('../services/serviceTimer');
const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const logger = require('../services/logger');
const DataProcessingService = require('../services/dataProcessingService');
const lib = require("../services/lib");
const config = lib.getConfig();
const workerBlockLogger = logger.workerBlockLogger;
const amqp = require('amqplib/callback_api');
/**
 * Create worker order unblock timer
 * @param  {Number} blockedTenantId
 * @param  {Number} blockedCallsign
 * @param  {Number} shiftId
 * @param  {Number} blockId
 * @param  {Number} timeToUnblock
 * @param  {Function} callback
 */
module.exports = function (blockedTenantId, blockedCallsign, shiftId, blockId, timeToUnblock, callback) {
    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer called!");
    let connSelfClosed = false;
    let chanelSelfClosed = false;
    const workerUnblockTimer = new serviceTimer();
    startListenQueue();
    return callback(null, 1);

    function startListenQueue() {
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
            if (err) {
                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->connect error->Error: ${err.message}. Need to try reconnect...`);
                return setTimeout(startListenQueue, 2000);
            }
            conn.on("error", err => {
                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', err.message);
                if (err.message !== "Connection closing") {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->connection->Error ${err.message}`);
                }
            });
            conn.on("close", () => {
                if (!connSelfClosed) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->connection closed, need to try reconnect...`);
                    workerUnblockTimer.clearAll();
                    return setTimeout(startListenQueue, 2000);
                }
            });
            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->rabbitmq->connected`);
            conn.createChannel((err, ch) => {
                if (err) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->create channel->Error: ${err.message}`);
                }
                ch.on("error", (err) => {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->channel->Error: ${err.message}`);
                });
                ch.on("close", () => {
                    if (!chanelSelfClosed) {
                        workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->rabbitmq->channel closed`);
                    }
                });
                const q = `worker_order_block_${blockId}`;
                const messageWorkTime = 2;
                ch.assertQueue(q, {
                    durable: true
                });
                ch.prefetch(1);
                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->Waiting task for worker_order_block_${blockId}`);
                DataProcessingService.addWorkerBlock(blockId);
                try {
                    workerUnblockTimer.setServiceTimeout('unblockWorkerOrderTimer', () => {
                        workerOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId);
                    }, timeToUnblock);
                } catch (err) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->setServiceTimeout->Error: ${err.message}`);
                }
                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer->start unblock worker order after " + timeToUnblock + " ms");
                ch.consume(q, (message) => {
                    if (message) {
                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->new task for for worker_order_block_${blockId}!`);
                        let taskObject;
                        try {
                            taskObject = JSON.parse(message.content.toString());
                        } catch (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->task content parsing->Error: ${err.message}`);
                        } finally {
                            if (taskObject) {
                                workerBlockLogger(blockedTenantId, blockedCallsign, 'info', taskObject);
                                if (taskObject.command === "worker_order_unblock") {
                                    workerOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId);
                                } else {
                                    setTimeout(() => {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->task done!`);
                                        try {
                                            ch.ack(message);
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->ch.ack->Error: ${err.message}`);
                                        }
                                    }, messageWorkTime * 1000);
                                }
                            }
                        }
                    }
                }, {noAck: false});

                function workerOrdersUnblocker(blockedTenantId, blockedCallsign, blockId, shiftId) {
                    workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer->starting to unblock worker order block..");
                    DataProcessingService.delWorkerBlock(blockId);
                    redisWorkerManager.getWorker(blockedTenantId, blockedCallsign, (err, workerDataBlocked) => {
                        if (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerOrderTimer->redisWorkerManager.getWorker->Error: ${err.message}`);
                        }
                        if (workerDataBlocked) {
                            if (workerDataBlocked.worker.status === "BLOCKED" && parseInt(workerDataBlocked.worker.worker_shift_id) === parseInt(shiftId)) {
                                workerDataBlocked.worker.status = "FREE";
                                workerDataBlocked.worker.order_end_block = null;
                                workerDataBlocked.worker.blocked_order_id = null;
                                redisWorkerManager.saveWorker(workerDataBlocked, (err, result) => {
                                    if (err) {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerOrderTimer->saveWorker->Error: ${err.message}`);
                                    } else {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->saveWorker->Result: ${result}`);
                                    }
                                });
                            }
                        }
                    });
                    sqlManager.logWorkerUnblock(blockId, (err, result) => {
                        if (err) {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'err', `unblockWorkerOrderTimer->sqlManager.logWorkerUnblock->Error: ${err.message}`);
                        } else {
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->sqlManager.logWorkerUnblock->${result}`);
                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer->unsubscribe from worker_order_block channel");
                            try {
                                ch.deleteQueue(q, {ifUnused: false, ifEmpty: false}, (err, ok) => {
                                    if (err) {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->deleteQueue->Error: ${err.message}`);
                                    } else {
                                        workerBlockLogger(blockedTenantId, blockedCallsign, 'info', `unblockWorkerOrderTimer->worker_order_block_${blockId} queue deleted.`);
                                        try {
                                            ch.close();
                                            chanelSelfClosed = true;
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer->AMQP channel closed.");
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->AMQP channel closing error: ${err.message}`);
                                        }
                                        try {
                                            conn.close();
                                            connSelfClosed = true;
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'info', "unblockWorkerOrderTimer->AMQP connection closed.");
                                        } catch (err) {
                                            workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->AMQP connection closing error: ${err.message}`);
                                        }
                                        workerUnblockTimer.clearAll();
                                    }
                                });
                            } catch (err) {
                                workerBlockLogger(blockedTenantId, blockedCallsign, 'error', `unblockWorkerOrderTimer->deleteQueue->Error: ${err.message}`);
                            }
                        }

                    });
                }
            });
        });
    }
};

