'use strict';

const rabbitmqHttpService = require('../services/rabbitmqHttpService');
const logger = require('../services/logger');
const workerShiftLogger = logger.workerShiftLogger;
const config = require("../services/lib").getConfig();
const nodeId = config.MY_ID;
const uuid = require('uuid/v4');
const amqpWorkerShiftMessageSender = require("../services/amqpWorkerShiftMessageSender");
const workerEventCommand = require('../workerShiftHandlers/workerModules/common/workerEventCommand');
const sqlManager = require('../database/sqlDB/sqlManager');

/**
 * Close worker shift
 * @param  {Number} tenantId
 * @param  {String} tenantLogin
 * @param  {Number} workerCallsign
 * @param  {Number} shiftId
 * @param  {String} shiftEndReason
 * @param  {String} shiftEndEventSenderType
 * @param  {Number} shiftEndEventSenderId
 * @param  {Function} resultCallback
 */
module.exports = function(tenantId, tenantLogin, workerCallsign, shiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, resultCallback){
    workerShiftLogger(tenantId, workerCallsign, 'info', "closeWorkerShift called!");
    try{
        /* const shiftEndReason_ = typeof shiftEndReason !== "undefined" ? shiftEndReason : 'timer';
         const shiftEndEventSenderType_ = typeof shiftEndEventSenderType !== "undefined" ? shiftEndEventSenderType : 'system';
         const shiftEndEventSenderId_ = typeof shiftEndEventSenderId !== "undefined" ? shiftEndEventSenderId : nodeId;*/
        amqpWorkerShiftMessageSender.existQueue(tenantId, workerCallsign, (err, result) => {
            if(err){
                return resultCallback(null, 1);
            }else{
                rabbitmqHttpService.getQueueConsumersCount(`${tenantId}_worker_${workerCallsign}_shift`, null, (err, result) => {
                    if(err){
                        return resultCallback(new Error(`getQueueConsumersCount error: ${err.message}`));
                    }else{
                        if(result >= 1){
                            const messageData = JSON.stringify({
                                uuid: uuid(),
                                type: 'order_event',
                                timestamp: Math.floor(Date.now() / 1000),
                                sender_service: "engine_service",
                                command: workerEventCommand.workerEndWork,
                                tenant_id: tenantId,
                                worker_callsign: workerCallsign,
                                change_sender_id: nodeId,
                                params: {
                                    shiftEndReason: shiftEndReason,
                                    shiftEndSenderType: shiftEndEventSenderType,
                                    shiftEndId: shiftEndEventSenderId,
                                }
                            });
                            amqpWorkerShiftMessageSender.sendMessage(tenantId, workerCallsign, messageData, (err, result) => {
                                if(err){
                                    workerShiftLogger(tenantId, this.workerCallsign, 'error', `close_shift->amqpWorkerShiftMessageSender.sendMessage->Error: ${err.message}`);
                                }else{
                                    workerShiftLogger(tenantId, workerCallsign, 'info', 'close_shift->Published event at chanel:' + tenantLogin + "_worker_" + workerCallsign + '_shift');
                                    workerShiftLogger(tenantId, workerCallsign, 'info', messageData);
                                }
                            });
                            return resultCallback(null, 1);
                        }else{
                            closeShiftInDB(tenantId, workerCallsign, shiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, function(err, result){
                                return resultCallback(null, 1);
                            });
                        }
                    }
                });
            }
        });
    }catch(err){
        console.log(err);
        workerShiftLogger(tenantId, workerCallsign, 'error', `BaseKabbiWorker->close_shift->Error: ${err.message}. ${this.tenantId} ${this.workerCallsign}`);
    }

};


function closeShiftInDB(tenantId, workerCallsign, workerShiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, callback){
    sqlManager.logEndOfWorkerShift(workerShiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, (err, result) => {
        if(err){
            workerShiftLogger(tenantId, workerCallsign, 'error', `close_shift->logEndOfWorkerShift->Error: ${err.message}`);
        }
        workerShiftLogger(tenantId, workerCallsign, 'info', `close_shift->logEndOfWorkerShift->Result: ${JSON.stringify(result)}`);
        return callback(null, 1);
    });

}