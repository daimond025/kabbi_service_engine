'use strict';

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");

/**
 * Get order to work by worker on multiple offering
 * @param orderInstance
 * @param {object} event
 * @returns {Promise<any>}
 */
module.exports = (orderInstance, event) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'getOfferOrderMultiple called!');
            const orderId = parseInt(orderInstance.orderId);
            const tenantId = parseInt(orderInstance.tenantId);
            event.time = Math.floor(Date.now() / 1000);

            redisOrderManager.getOrder(tenantId, orderId, (err, order) => {
                if (err) {
                    orderLogger(orderId, "error", `getOfferOrderMultiple->redisOrderManager.getOrder->Error: ${err.message}`);
                    return reject(err);
                }
                const currentStatusId = parseInt(order.status_id);
                if (currentStatusId !== statusInfo.offer_order.status_id) {
                    return reject(new Error('order is not at offering status'));
                }
                order.workers_offered_responses = typeof order.workers_offered_responses === "object" ?
                    Object.keys(order.workers_offered_responses).map(key => order.workers_offered_responses[key]) : [];
                order.workers_offered_responses.push(event);

                redisOrderManager.saveOrder(order, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `getOfferOrderMultiple->redisOrderManager.saveOrder->Error: ${err.message}`);
                        return reject(err);
                    }
                    // todo new
                    orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel);

                    let orderOfferTimeOut = (typeof order.settings.NEW_ORDER_TIME_ASSIGN !== undefined) ? parseInt(order.settings.NEW_ORDER_TIME_ASSIGN) : 5;

                    if(orderInstance.settingNewOrderAssign >= 0){
                        orderInstance.settingNewOrderAssign += orderOfferTimeOut;
                    }
                    if(orderInstance.settingNewOrderAssign >= (orderInstance.settings.ORDER_OFFER_SEC + orderInstance.offerSecSystemIncrease)){
                        orderOfferTimeOut = 1;
                    }

                    orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel, () => {
                        orderLogger(orderId, 'info', `orderStatusOfferOrder->${orderInstance.orderTimer.offerOrderTimeoutLabel} called`);
                        const messageString = orderEventOrderMessage({
                            command: orderEventOrderCommand.orderOfferTimeout,
                            tenant_id: tenantId,
                            order_id: orderId,
                            params: {}
                        });
                        amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                            if (err) {
                                orderLogger(orderId, "error", `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.orderOfferTimeout} to order: ${orderId}. Result: ${result}`);
                            }
                        })
                    }, (orderOfferTimeOut) * 1000);


                    return resolve(true);
                });
            });
        } catch (err) {
            return reject(err);
        }
    })
};