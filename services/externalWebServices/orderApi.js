"use strict";

const request = require('request');
const config = require("../lib").getConfig();
const logger = require('../logger');
const mainLogger = logger.mainLogger;
const orderEventSenderService = require('../../orderHandlers/orderEvent/orderEventSenderService');

function sendRequest(taskObject, callback) {
    const requestOptions = {
        url: `${config.API_ORDER_URL}v1/orders/${taskObject.command}`,
        method: 'POST',
        headers: {
            "content-type": "application/json",
        },
        json: taskObject
    };


    /**
     *
     * @param error
     * @param response
     * @param body
     * @returns {*}
     */
    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            if (typeof body === "object") {
                return callback(null, body);
            } else if (typeof body === "string") {
                try {
                    let result = JSON.parse(body);
                    return callback(null, result)
                } catch (err) {
                    mainLogger('error', 'orderApi PARSING-error:');
                    mainLogger('error', body);
                    mainLogger('error', response);
                    return callback(err);
                }
            } else {
                mainLogger('error', 'orderApi BAD-body:');
                mainLogger('error', body);
                mainLogger('error', response);
                return callback(new Error('bad body'));
            }
        } else {
            mainLogger('error', 'orderApi BAD-response:');
            mainLogger('error', body);
            mainLogger('error', response);
            let errInfo = error || body;
            return callback(errInfo);
        }
    }

    request(requestOptions, resultData);
}


/**
 * get formatted error result
 * @param {string} uuid
 * @param {number} orderId
 * @param {string|null} senderService
 * @param {string} errorType
 * @returns {{uuid: *, order_id: *, type: string, code: *, info: *, result: {set_result: number}}}
 */
function getFormattedErrorResult(uuid, orderId, senderService, errorType = 'INTERNAL_ERROR') {
    let errorCode, info;
    if (!errorTypes().includes(errorType)) {
        throw new Error('unsupported errorType');
    }
    let services = [
        orderEventSenderService.clientService,
        orderEventSenderService.workerService
    ];
    if (services.includes(senderService)) {
        switch (errorType) {
            case 'ORDER_IS_BUSY':
                errorCode = 70;
                info = 'ORDER_IS_BUSY';
                break;
            default:
                errorCode = 1;
                info = 'INTERNAL_ERROR';
                break;
        }
    }

    services = [
        orderEventSenderService.engineService,
        orderEventSenderService.operatorService,
        orderEventSenderService.exchangeService
    ];

    if (services.includes(senderService)) {
        switch (errorType) {
            default:
                errorCode = 107;
                info = 'INTERNAL_ERROR';
                break;
        }
    }

    return {
        uuid: uuid,
        order_id: orderId,
        type: 'response',
        code: errorCode,
        info: info,
        result:  {
            "set_result": 0
        }
    };
}

/**
 * get formatted good result
 * @param uuid
 * @param orderId
 * @param senderService
 * @returns {{uuid: *, order_id: *, type: string, code: *, info: string, result: {set_result: number}}}
 */
function getFormattedGoodResult(uuid, orderId, senderService) {
    let code;

    let services = [
        orderEventSenderService.clientService,
        orderEventSenderService.workerService
    ];
    if (services.includes(senderService)) {
        code = 0
    }

    services = [
        orderEventSenderService.engineService,
        orderEventSenderService.operatorService,
        orderEventSenderService.exchangeService
    ];
    if (services.includes(senderService)) {
        code = 100;
    }

    return {
        uuid: uuid,
        order_id: orderId,
        type: 'response',
        code: code,
        info: "OK",
        result: {
            "set_result": 1
        }
    };
}

/**
 * Is good result
 * @param result
 * @param senderService
 * @returns {boolean}
 */
function isGoodResult(result, senderService) {
    let isGood = true;

    let services = [
        orderEventSenderService.clientService,
        orderEventSenderService.workerService
    ];

    if (services.includes(senderService)) {
        isGood = result.code === 0;
    }

    services = [
        orderEventSenderService.engineService,
        orderEventSenderService.operatorService,
        orderEventSenderService.exchangeService
    ];

    if (services.includes(senderService)) {
        isGood = result.code === 100;
    }
    return isGood;
}

function errorTypes() {
    return ['INTERNAL_ERROR', 'ORDER_IS_BUSY'];
}

exports.sendRequest = sendRequest;
exports.getFormattedErrorResult = getFormattedErrorResult;
exports.getFormattedGoodResult = getFormattedGoodResult;
exports.isGoodResult = isGoodResult;
exports.errorTypes = errorTypes;