'use strict';

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");


/**
 * Order status offer order
 * @param {BaseGootaxOrder} orderInstance
 * @return {Promise}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusOfferOrder called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel);
            orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.offerOrderTimeoutLabel, () => {
                orderLogger(orderId, 'info', `orderStatusOfferOrder->${orderInstance.orderTimer.offerOrderTimeoutLabel} called`);
                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.orderOfferTimeout,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.orderOfferTimeout} to order: ${orderId}. Result: ${result}`);
                    }
                })
            }, (orderInstance.settings.ORDER_OFFER_SEC + orderInstance.offerSecSystemIncrease) * 1000);

            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};
