'use strict';

const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const logger = require('../services/logger');
const workerBlockLogger = logger.workerBlockLogger;
const ublockWorkerOrderTimer = require("./unblockWorkerOrderTimer");

/**
 * Block worker order
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {Number} orderId
 * @param  {Function} resultCallback

 */
module.exports = function (tenantId, workerCallsign, orderId, resultCallback) {
    workerBlockLogger(tenantId, workerCallsign, 'info', "blockWorkerOrder called!");
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerOrder->redisWorkerManager.getWorker->Error: ${err.message}`);
            return resultCallback(err);
        }
        if (!workerData) {
            return resultCallback(new Error('No worker at redis Db'));
        }
        if (workerData.worker.status === "ON_ORDER" || workerData.worker.status === "BLOCKED") {
            return resultCallback(new Error('Bad worker status. Status=' + workerData.worker.status));
        }
        const workerTimeOfBlock = workerData.worker.worker_time_of_block * 60;
        const workerTimeOfEndBlock = parseInt((new Date()).getTime() / 1000 + workerTimeOfBlock);
        const workerShiftId = workerData.worker.worker_shift_id;
        const workerId = workerData.worker.worker_id;
        sqlManager.logWorkerBlock(workerId, workerShiftId, "order", workerTimeOfEndBlock, (err, result) => {
            if (err) {
                workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerOrder->sqlManager.logWorkerBlock->Error: ${err.message}`);
                return resultCallback(err);
            }
            workerData.worker.summ_reject = 0;
            workerData.worker.in_row_reject = 0;
            workerData.worker.status = "BLOCKED";
            workerData.worker.order_end_block = workerTimeOfEndBlock;
            workerData.worker.blocked_order_id = orderId;
            const blockId = result;
            redisWorkerManager.saveWorker(workerData, (err, result) => {
                if (err) {
                    workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerOrder->redisWorkerManager.saveWorker->Error: ${err.message}`);
                    return resultCallback(err);
                } else {
                    ublockWorkerOrderTimer(tenantId, workerCallsign, workerShiftId, blockId, workerTimeOfBlock * 1000, (err, result) => {
                        if (err) {
                            workerBlockLogger(tenantId, workerCallsign, 'error', `blockWorkerOrder->ublockWorkerOrderTimer->Error: ${err.message}`);
                            return resultCallback(err);
                        }
                        return resultCallback(null, 1);
                    });
                }
            })

        });

    });
};