'use strict';

const config = require("../../services/lib").getConfig();
const nodeId = config.MY_ID;
const uuid = require('uuid/v4');
const orderEventSenderService = require('./orderEventSenderService');

/**
 * Create order AMQP message on order event
 * @param {Object} options
 * @param {string} options.command
 * @param {number} options.tenant_id
 * @param {number} options.order_id
 * @param {object} options.params
 * @return {string} message
 */

module.exports = (options) => {
    return JSON.stringify({
        uuid: uuid(),
        type: 'order_event',
        timestamp: Math.floor(Date.now() / 1000),

        sender_service: (typeof options.sender_service != "undefined") ? options.sender_service : orderEventSenderService.engineService,

        command: options.command,
        tenant_id: options.tenant_id,
        order_id: options.order_id,

        change_sender_id: (typeof options.change_sender_id != "undefined") ? options.change_sender_id :  nodeId,

        last_update_time: Math.floor(Date.now() / 1000),
        lang: 'en',
        params: options.params
    });
};