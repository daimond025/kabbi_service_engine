"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const redisOrderManager = require("../../../../database/redisDB/redisOrderManager");
const redisParkingManager = require("../../../../database/redisDB/redisParkingManager");
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const orderNotificationManager = require('../orderNotificationManager');
const async = require('async');
const redisWorkerReservedOrderListManager = require('../../../../database/redisDB/redisWorkerReservedOrderListManager');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const workerStatusManager = require('../../../../services/workerStatusManager');


/**
 * Order status worker assigned
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusWorkerAssigned called!');
            if (orderInstance.statusChanged) {
                //Предпоследний статус - предложение заказа
                let acceptedFromOfferProcess = false;
                if (orderInstance.statusArr[orderInstance.statusArr.length - 2] === statusInfo.offer_order.status_id) {
                    acceptedFromOfferProcess = true;
                }
                const tenantId = parseInt(orderData.tenant_id);
                const orderId = parseInt(orderData.order_id);
                const timeToClient = orderData.time_to_client;
                const workerLateDelay = timeToClient * 60 * 1000;
                orderInstance.orderTimer.clearAll();
                if (orderData && orderData.worker && orderData.worker.callsign) {
                    const workerCallsign = parseInt(orderData.worker.callsign);
                    let offeredWorkersList = typeof orderData.workers_offered === "object" ?
                        Object.keys(orderData.workers_offered).map(key => orderData.workers_offered[key]) : [];
                    //@todo удалить это кусок после релиза ветки `5848_timer_to_collect_responses`
                    if (typeof orderData.offered_workers_list === "object") {
                        offeredWorkersList = Object.keys(orderData.offered_workers_list).map(key => orderData.offered_workers_list[key]);
                    }
                    if (offeredWorkersList.length) {
                        orderLogger(orderId, "info", `orderStatusWorkerAssigned->offeredWorkersList: ${offeredWorkersList}`);
                        async.each(offeredWorkersList,
                            (offeredWorkerCallsign, callback) => {
                                if (offeredWorkerCallsign !== workerCallsign) {
                                    redisWorkerManager.getWorker(tenantId, offeredWorkerCallsign, (err, workerData) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusWorkerAssigned->redisWorkerManager.getWorker->Error of getting worker ${workerCallsign}. Error: ${err.message}`);
                                            return callback(null, 1)
                                        }
                                        orderLogger(orderId, "info", JSON.stringify(workerData));
                                        if (workerData.worker.status === workerStatusManager.offerOrder && parseInt(workerData.worker.last_offered_order_id) === orderId) {
                                            workerData.worker.status = workerStatusManager.free;
                                            redisWorkerManager.saveWorker(workerData, (err, result) => {
                                                if (err) {
                                                    orderLogger(orderId, "error", `orderStatusWorkerAssigned->redisWorkerManager.saveWorker->Error: ${err.message}`);
                                                } else {
                                                    orderLogger(orderId, "info", `orderStatusWorkerAssigned->redisWorkerManager.saveWorker->Result: ${result}`);
                                                }
                                                orderLogger(orderId, "info", `orderStatusWorkerAssigned->Set status ${workerStatusManager.free} for worker: ${offeredWorkerCallsign}`);
                                                return callback(null, 1);
                                            });
                                        } else {
                                            return callback(null, 1)
                                        }
                                    })
                                }
                            }, (err) => {
                                if (err) {
                                    orderLogger(orderId, "error", `orderStatusWorkerAssigned->async.each workers->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, "info", `orderStatusWorkerAssigned->async.each offeredWorkersList done`);
                                }
                            })
                    }
                    redisWorkerReservedOrderListManager.addOrder(tenantId, workerCallsign, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusWorkerAssigned->redisWorkerReservedOrderListManager.addOrder->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderStatusWorkerAssigned->redisWorkerReservedOrderListManager.addOrder->Result: ${result}`);
                        }
                    });
                    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderStatusWorkerAssigned->getWorker->Error: ${err.message}`);
                        } else {

                            // список активных заказов
                            let active_orders = typeof workerData.worker.active_orders === "object" ?
                                Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])): [];
                            let index_del = active_orders.indexOf(orderId);
                            if(index_del == -1){
                                active_orders.push(orderId);
                            }
                            workerData.worker.active_orders = active_orders;
                            orderLogger(orderId, 'info', `orderStatusWorkerAssigned->active_orders ${active_orders.join(',')} in workerCallsign: ${workerCallsign}`);

                           /* console.log('ВОДИТЕЛЬ ПРИНЯЛ ЗАКАЗ - список активных заказов');
                            console.log(workerData.worker.active_orders );*/

                            workerData.worker.last_order_id = orderId;
                            if (typeof workerData.geo.parking_id !== "undefined" && workerData.geo.parking_id !== null) {
                                redisParkingManager.deleteWorkerFromParking(workerData.geo.parking_id, workerCallsign, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderStatusWorkerAssigned->redisParkingManager.deleteWorkerFromParking->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderStatusWorkerAssigned->redisParkingManager.deleteWorkerFromParking->Result: ${result}`);
                                    }
                                })
                            }
                            workerData.geo.parking_id = null;
                            redisWorkerManager.saveWorker(workerData, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `orderStatusWorkerAssigned->redisWorkerManager.saveWorker->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `orderStatusWorkerAssigned->redisWorkerManager.saveWorker->Result: ${result}`);
                                }
                            });
                            if (acceptedFromOfferProcess) {
                                try {
                                    const workerShiftId = workerData.worker.worker_shift_id;
                                    sqlManager.logWorkerAcceptedOrderOfferCount(workerShiftId, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusWorkerAssigned->sqlManager.logWorkerAcceptedOrderOfferCount->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusWorkerAssigned->sqlManager.logWorkerAcceptedOrderOfferCount->Result: ${result}`);
                                        }
                                    });
                                } catch (err) {
                                    orderLogger(orderId, 'error', `orderStatusWorkerAssigned->acceptedFromOfferProcess->Error: ${err.message}`);
                                }
                            }
                        }
                    })
                }
                try {
                    orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.workerLateTimeoutLabel, () => {
                        orderLogger(orderId, 'info', `orderStatusWorkerAssigned->${orderInstance.orderTimer.workerLateTimeoutLabel} called!`);
                        const messageString = orderEventOrderMessage({
                            command: orderEventOrderCommand.workerLateTimeout,
                            tenant_id: tenantId,
                            order_id: orderId,
                            params: {}
                        });
                        amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusWorkerAssigned->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `orderStatusWorkerAssigned->amqpOrderMessageSender.sendMessage->published command: ${orderEventOrderCommand.workerLateTimeout} to order: ${orderId}. Result: ${result}`);
                            }
                        });
                    }, workerLateDelay, tenantId, orderId)
                } catch (err) {
                    orderLogger(orderId, 'error', `orderStatusWorkerAssigned->setServiceTimeout->Error: ${err.message}`);
                }
                orderData.workers_offered = [];
                orderData.workers_offered_responses = [];
                redisOrderManager.saveOrder(orderData, (err, result) => {
                    if (err) {
                        orderLogger(orderInstance.orderId, 'error', `orderStatusWorkerAssigned->redisOrderManager.saveOrder->Error: ${err.message}`);
                    } else {
                        orderLogger(orderInstance.orderId, 'info', `orderStatusWorkerAssigned->redisOrderManager.saveOrder->Result: ${result}`);
                    }
                });
                orderNotificationManager.sendUpdateFreeOrdersListToWorkers(
                    orderData,
                    {
                        sendPushFreeOrderForAll: orderInstance.settings.SEND_PUSH_FREE_ORDER_FOR_ALL,
                        distanceToFilterFreeOrders: orderInstance.settings.DISTANCE_TO_FILTER_FREE_ORDERS,
                        dislikeWorkerIds: orderInstance.dislikeWorkerIds,
                        exceptCarModels: orderInstance.exceptCarModels
                    })
                    .then(result => {
                        orderLogger(orderId, 'info', `orderStatusWorkerAssigned->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Result: ${result}`);
                    })
                    .catch(err => {
                        orderLogger(orderId, 'error', `orderStatusWorkerAssigned->orderNotificationManager.sendUpdateFreeOrdersListToWorkers->Error: ${err.message}`);
                    });
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    });
};
