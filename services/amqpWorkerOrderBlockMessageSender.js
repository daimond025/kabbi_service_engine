"use strict";

const lib = require("./lib");
const config = lib.getConfig();
const amqp = require('amqplib/callback_api');
const mainLogger = require('./logger').mainLogger;

/**
 *  Amqp worker order block  message publisher
 * @param {Number} blockId
 * @param {String} messageString
 * @param {Function} callback
 */

function sendMessage(blockId, messageString, callback) {
    const queueLabel = `worker_order_block_${blockId}`;
    amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
        if (err) {
            return callback(err);
        } else {
            conn.createChannel(function (err, ch) {
                if (err) {
                    return callback(err);
                } else {
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true
                        });
                        ch.sendToQueue(queueLabel, new Buffer(messageString), {
                            persistent: true
                        });
                        return callback(null, 'message sent');
                    } catch (err) {
                        return callback(`Err of sending message:${messageString} to queue:${queueLabel}`);
                    }

                }
            });
            setTimeout(function () {
                try {
                    conn.close();
                } catch (err) {
                    mainLogger('error', err);
                }
            }, 500);
        }
    });
}

exports.sendMessage = sendMessage;
