'use strict';

const async = require("async");
const mainLogger = require("../logger").mainLogger;




const redisOrderManager = require("../../database/redisDB/redisOrderManager");
const redisWorkerManager = require("../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../database/sqlDB/sqlManager");

const orderMainHandler = require('../../orderHandlers/orderMainHandler');
const workerMainHandler = require('../../workerShiftHandlers/workerMainHandler');
/**
 * @class getOrderAfterRestart
 */
class getWorkersAfterRestart {
    /**
     *@constructor
     */
    constructor() {

    }


    /**
     * Run worker
     * @returns {Promise}
     */
    run() {
        return new Promise((resolve, reject) => {

            const processedWorkers = [];
            sqlManager.getAllTenantIds((err, tenantArr) => {
                if (err) {
                    mainLogger("error", `getWorkersAfterRestart->getProcessedWorkers->getAllTenantIds->Error: ${err.message}`);
                    return reject(err);
                }

                async.each(tenantArr, function (tenantId, callback) {
                    redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
                        if (err) {
                            mainLogger("error", `getWorkersAfterRestart->getProcessedWorkers->getAllWorkers->Error: ${err.message}`);
                            return callback(null, 1);
                        }
                        async.each(workers, (worker, orderCb) => {
                            try {
                                processedWorkers.push(worker);
                            } catch (err) {
                                mainLogger("error", `getWorkersAfterRestart->getProcessedWorkers->getAllWorkers->Error of getting worker data params.Error: ${err.message}`);
                            }
                            return orderCb(null, 1);
                        }, (err) => {
                            if (err) {
                                mainLogger("error", `getWorkersAfterRestart->getProcessedWorkers->getAllWorkers->async.each worker. Error: ${err.message}`);
                            }
                            return callback(null, 1);
                        })
                    })
                }, (err) => {
                    if (err) {
                        mainLogger("error", `getWorkersAfterRestart->getProcessedWorkers->getAllWorkers->async.each tenantArr. Error: ${err.message}`);
                        return reject(err);
                    } else {

                        async.each(processedWorkers, (workerData) => {

                            const  tenantId = workerData.worker.tenant_id;
                            const  tenantLogin = workerData.worker.tenantLogin;
                            const  workerCallsign = workerData.worker.callsign;
                            const  shiftLifeTime = (typeof workerData.worker.worker_shift_time !== "undefined") ? parseInt( workerData.worker.worker_shift_time) : 24 * 3600;
                            const  shiftId = workerData.worker.worker_shift_id;

                            workerMainHandler(tenantId,tenantLogin ,workerCallsign, shiftLifeTime, shiftId,  (err, result) => {
                                mainLogger("info", 'getOrderAfterRestart->getProcessedOrders->successful restart' );
                            });

                        });
                        mainLogger("info", 'getOrderAfterRestart->getProcessedOrders->successful done');

                        return resolve(1);
                    }
                })
            });
        });
    }



}
module.exports = exports = new getWorkersAfterRestart();