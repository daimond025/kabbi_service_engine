'use strict';

const request = require('request');
const config = require("../lib").getConfig();
const servicePushUrl = config.SERVICE_PUSH_URL;
/**
 * Send push to worker to offer him order
 * @param  {Number} tenantId
 * @param  {Number} orderId
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
function sendNewOrderToWorker(tenantId, orderId, workerCallsign, callback) {
    const getString = "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId;
    const requesOptions = {
        url: `${servicePushUrl}notification/send_to_worker_new_order?${getString}`
    };
    //Кинули пуш
    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requesOptions, resultData);
}


/**
 * Send push to worker about rejecting him from current order
 * @param  {Number} orderId
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {Number} statusId
 * @param  {Function} callback
 */
function rejectWorkerFromOrder(tenantId, orderId, workerCallsign, statusId, callback) {
    const url = servicePushUrl + 'notification/reject_worker_from_order?' + "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&status_id=" + statusId;
    const requestOptions = {
        url: url
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send push to worker about rejecting his order
 * @param {Number} orderId
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {Number} statusId
 * @param {Function} callback
 */
function sendToWorkerRejectOrder(tenantId, orderId, workerCallsign, statusId, callback) {
    const url = servicePushUrl + 'notification/send_to_worker_reject_order?' + "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&status_id=" + statusId;
    const requestOptions = {
        url: url
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send push to worker about updating his order
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param {Number} workerCallsign
 * @param {Function} callback
 */
function sendToWorkerUpdateOrder(tenantId, orderId, workerCallsign, callback) {
    const getString = "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_to_worker_update_order?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send to worker message, that he is assigned at order
 * @param tenantId
 * @param orderId
 * @param workerCallsign
 * @param isPreOrder
 * @param callback
 */
function sendAssignAtOrder(tenantId, orderId, workerCallsign, isPreOrder, callback) {
    const isPreOrderInt = isPreOrder ? 1 : 0;
    const getString = "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&is_pre_order=" + isPreOrderInt;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_assign_at_order?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send to worker message, that he refused order assignment
 * @param tenantId
 * @param orderId
 * @param workerCallsign
 * @param isPreOrder
 * @param callback
 */
function sendRefuseOrderAssignment(tenantId, orderId, workerCallsign, isPreOrder, callback) {
    const isPreOrderInt = isPreOrder ? 1 : 0;
    const getString = "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&is_pre_order=" + isPreOrderInt;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_refuse_order_assignment?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}


/**
 * Send push to workers about updating list of preorders
 * @param  {Number} tenantId
 * @param  {Number} orderId
 * @param  {String} workers   - 1,2,6,8,9 - string of workers callsign to send notification
 * @param  {Number} soundOn
 * @param  {Number} pushForAll
 * @param  {Function} callback
 */
function sendPreOrderForAll(tenantId, orderId, workers, soundOn, pushForAll, callback) {
    soundOn = parseInt(soundOn) === 1 ? 1 : 0;
    pushForAll = parseInt(pushForAll) === 1 ? 1 : 0;
    const getString = "workers=" + workers + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&sound_on=" + soundOn + "&for_all=" + pushForAll;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_pre_order_for_all?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send push to workers about updating list of free orders
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param {String} workers -  1,2,6,8,9 - string of workers callsign to send notification
 * @param {Number} soundOn
 * @param {Number} pushForAll
 * @param {Function} callback
 */
function sendFreeOrderForAll(tenantId, orderId, workers, soundOn, pushForAll, callback) {
    soundOn = parseInt(soundOn) === 1 ? 1 : 0;
    pushForAll = parseInt(pushForAll) === 1 ? 1 : 0;
    const getString = "workers=" + workers + "&order_id=" + orderId + "&tenant_id=" + tenantId + "&sound_on=" + soundOn + "&for_all=" + pushForAll;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_free_order_for_all?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

/**
 * Send push to worker to remnd about his preorder
 * @param  {Number} tenantId
 * @param  {Number} orderId
 * @param  {Number} workerCallsign
 * @param  {Number} orderNumber
 * @param  {Number} timePreRemind
 * @param  {Function}  callback
 */
function sendToWorkerRemindPreOrder(tenantId, orderId, workerCallsign, orderNumber, timePreRemind, callback) {
    const getString = "worker_callsign=" + workerCallsign + "&order_id=" + orderId + "&order_number=" + orderNumber + "&remind_time=" + timePreRemind + "&tenant_id=" + tenantId;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_to_worker_remind_pre_order?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}


/**
 * Send push notification to  client
 * @param  {Object} options
 * @param  {Number} options.tenantId
 * @param  {Number} options.orderId
 * @param  {Number} options.positionId
 * @param  {Number} options.cityId
 * @param  {String} options.deviceToken
 * @param  {String} options.typeDevice
 * @param  {String} options.appId
 * @param  {String} options.statusGroup
 * @param  {Number} options.statusId
 * @param  {String} options.lang
 * @param  {Boolean} options.visible
 * @param  {Function} callback
 */
function sendPushNotificationToClient(options, callback) {
    if (options.lang === null || typeof options.lang === "undefined") {
        options.lang = "ru";
    }
    let visible = options.visible === true ? 1 : 0;
    const getString = `city_id=${options.cityId}&device_token=${options.deviceToken}&order_id=${options.orderId}&position_id=${options.positionId}&status_group=${options.statusGroup}&status_id=${options.statusId}&tenant_id=${options.tenantId}&type_device=${options.typeDevice}&app_id=${options.appId}&lang=${options.lang}&visible=${visible}`;
    const requestOptions = {
        url: servicePushUrl + 'notification/send_push_notification_for_client?' + getString
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(error);
        }
    }

    request(requestOptions, resultData);
}

module.exports = {
    sendNewOrderToWorker,
    rejectWorkerFromOrder,
    sendToWorkerUpdateOrder,
    sendPreOrderForAll,
    sendFreeOrderForAll,
    sendToWorkerRemindPreOrder,
    sendToWorkerRejectOrder,
    sendPushNotificationToClient,
    sendAssignAtOrder,
    sendRefuseOrderAssignment
};
