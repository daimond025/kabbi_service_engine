"use strict";

const redisWorkerReservedOrderListManager = require('../../../database/redisDB/redisWorkerReservedOrderListManager');
const redisOrderManager = require('../../../database/redisDB/redisOrderManager');
const redisWorkerManager = require('../../../database/redisDB/redisWorkerManager');
const sqlManager = require('../../../database/sqlDB/sqlManager');
const logger = require('../../../services/logger');
const orderLogger = logger.orderLogger;
const amqpWorkerMessageSender = require("../../../services/amqpWorkerMessageSender");
const amqpOrderMessageSender = require("../../../services/amqpOrderMessageSender");
const blockWorkerPreOrder = require('../../../workerBlockHandlers/blockWorkerPreOrder');
const pushApi = require('../../../services/externalWebServices/pushApi');
const orderEventOrderMessage = require('../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../orderEvent/orderEventWorkerMessage');
const orderEventSenderService = require('../../orderEvent/orderEventSenderService');
const orderEventOrderCommand = require('../../orderEvent/orderEventOrderCommand');
const orderEventWorkerCommand = require('../../orderEvent/orderEventWorkerCommand');
const getOrderStatusById = require('../../../services/orderStatusManager').getOrderStatusById;
const orderActionManager = require('./orderActionManager');
const workerStatusManager = require("../../../services/workerStatusManager");

const order_type = require("../../../orderHandlers/orderEvent/orderDevice");
const statusInfo = require('../../../services/orderStatusManager').statusInfo;

const PHPUnserialize = require("php-unserialize");
/**
 * Process order assign/refuse order events
 * @class orderWorkerAssignManager
 */
class OrderWorkerAssignManager {

    /**
     *@constructor
     */
    constructor() {
    }

    /**
     * If order assigned to worker, this method:
     * add order_id to worker reserved list at redisDB,
     * send message about this to worker queue,
     * send push notification
     * @param {Object} orderData - order data from redis
     * @param {string} eventSenderService
     * @param {object} options
     * param {number} options.isPreOrder
     * param {boolean} options.showWorker
     */

    processAssignedOrder(orderData, eventSenderService, options) {
        try {
            orderLogger(orderData.order_id, 'info', `workerOrderAssignManager->processAssignedOrder called!`);
            const tenantId = orderData.tenant_id;
            const orderId =  parseInt(orderData.order_id);
            const workerCallsign = orderData.worker.callsign;
            const tenantLogin = orderData.tenant_login;

            const isPreOrder =  ((typeof options.isPreOrder  == "boolean") && options.isPreOrder) ? 'preorder' : 'order';
            const showWorker =  ((typeof options.showWorker  == "boolean") && options.showWorker) ? options.showWorker: false;

            // timer show
            let offer_sec = 60;
            let timerOrder = (typeof orderData.settings.FREE_ORDER_OFFER_SEC !== undefined) ? parseInt(orderData.settings.FREE_ORDER_OFFER_SEC) : 60;
            let timerPreOrder = (typeof orderData.settings.PRE_ORDER_OFFER_SEC !== undefined) ? parseInt(orderData.settings.PRE_ORDER_OFFER_SEC) : 60;

            if(isPreOrder == 'preorder'){
                offer_sec = timerPreOrder;
            }else{
                offer_sec = timerOrder;
            }

            const typeOrder = orderData.device.toString();


            redisWorkerReservedOrderListManager.addOrder(tenantId, workerCallsign, orderId, (err, result) => {
                if (err) {
                    orderLogger(orderId, 'error', `workerOrderAssignManager->processAssignedOrder->redisWorkerReservedOrderListManager.addOrder->Error: ${err.message}`);
                } else {
                    orderLogger(orderId, 'info', `workerOrderAssignManager->processAssignedOrder->redisWorkerReservedOrderListManager.addOrder->Result: ${result}`);

                    if (orderEventSenderService.workerService !== eventSenderService && showWorker ) {

                        const messageString = orderEventWorkerMessage({
                            command: orderEventWorkerCommand.preOrderOffer,
                            tenant_id: tenantId,
                            tenant_login: tenantLogin,
                            worker_callsign: workerCallsign,
                            params: {
                                offer_sec: offer_sec,
                                order_id: orderId,
                                type: isPreOrder
                            }
                        });
                        const messageTtl = 60 * 60 * 24 * 3; // 3 days
                        amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `workerOrderAssignManager->processAssignedOrder->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `workerOrderAssignManager->processAssignedOrder->amqpWorkerMessageSender.sendMessage->Result ${result}`);
                            }
                        });
                    }
                    pushApi.sendAssignAtOrder(tenantId, orderId, workerCallsign, isPreOrder, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `workerOrderAssignManager->processAssignedOrder->pushApi.sendAssignAtOrder->Error: ${err.message}`);
                            orderLogger(orderId, 'error', err);
                        } else {
                            orderLogger(orderId, 'info', `workerOrderAssignManager->processAssignedOrder->pushApi.sendAssignAtOrder->Result: ${result}`);
                        }
                    })
                }
            });
        } catch (err) {
            orderLogger(orderData.order_id, "error", `workerOrderAssignManager->processAssignedOrder->Error: ${err.message}`);
        }
    }

    /*
    * Reminder for worker
    * sent offer message to worker by timer
    * @param {BaseGootaxOrder} orderInstance
    * @param {Object} orderData - order data from redis
    * @param {Boolean} isPreOrder
    *
    * */
    createOrderWorkeReminder(orderInstance, orderData, isPreOrder){
        try {
            orderLogger(orderData.order_id, 'info', `workerOrderAssignManager->createOrderWorkeReminder called!`);
            const tenantId = parseInt(orderData.tenant_id);
            const orderId = parseInt(orderData.order_id);
            const cityTimeOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
            const orderTime = parseInt(orderData.order_time);
            let preOrderTimeForRemind = orderInstance.settings.WORKER_PRE_ORDER_REMINDER;
            const orderType = isPreOrder ? 'preorder' : 'free';
            const timerOrder = (typeof orderData.settings.FREE_ORDER_OFFER_SEC !== undefined) ? parseInt(orderData.settings.FREE_ORDER_OFFER_SEC) : 60;


            if (!preOrderTimeForRemind) {
                orderLogger(orderId, 'error', `workerOrderAssignManager->createOrderWorkeReminder->bad preOrderTimeForRemind value:${preOrderTimeForRemind}`);
            } else {
                try {
                    preOrderTimeForRemind = PHPUnserialize.unserialize(preOrderTimeForRemind);
                    if (typeof preOrderTimeForRemind === "object" && preOrderTimeForRemind !== null) {
                        const preOrderTimeForRemindList = Object.keys(preOrderTimeForRemind).map(k => preOrderTimeForRemind[k]);
                        if (preOrderTimeForRemindList.length === 0) {
                            orderLogger(orderId, 'info', "workerOrderAssignManager->createOrderWorkeReminder->don't need send worker preorder reminder");
                        } else {
                            let reminderCounter = 1;

                            for (let timePreRemind of preOrderTimeForRemindList) {
                                timePreRemind = !isNaN(parseFloat(timePreRemind)) ? parseFloat(timePreRemind) : null;
                                if (timePreRemind) {
                                    this._createOrderWorkerReminderTimer({
                                        tenantId,
                                        orderId,
                                        orderTime,
                                        timePreRemind,
                                        cityTimeOffset,
                                        reminderCounter,
                                        orderType,
                                        timerOrder
                                    }, orderInstance.orderTimer);
                                    reminderCounter++;
                                }
                            }
                        }
                    } else {
                        orderLogger(orderData.order_id, 'error', `pworkerOrderAssignManager->createOrderWorkeReminder->bad preOrderTimeForRemind type:${preOrderTimeForRemind}`);
                    }
                } catch (err) {
                    orderLogger(orderData.order_id, 'error', `workerOrderAssignManager->createOrderWorkeReminder Error: ${err.message}`);
                }
            }

        } catch (err) {
            orderLogger(orderData.order_id, "error", `workerOrderAssignManager->createOrderWorkeReminder->Error: ${err.message}`);
        }
    }
    /**
     * Create  order worker reminder timer - send message
     * @param {object} options
     * @param {number} options.tenantId
     * @param {number} options.orderId
     * @param {number} options.orderTime
     * @param {number} options.timePreRemind
     * @param {number} options.cityTimeOffset
     * @param {number} options.reminderCounter
     * @param {object} orderTimer
     * @private
     */
    _createOrderWorkerReminderTimer(options, orderTimer) {
        try {
            orderLogger(options.orderId, 'info', "workerOrderAssignManager->_createOrderWorkerReminderTimer called!");
            const timeOfNotification = parseInt(options.orderTime) - (parseInt(options.timePreRemind) * 60);
            const tenantNowTime = parseInt((new Date()).getTime() / 1000) + parseInt(options.cityTimeOffset);
            let startTime = timeOfNotification - tenantNowTime;
            startTime = startTime > 0 ? startTime : 0;
            let serviceTimoutLabel;
            switch (options.reminderCounter) {
                case 1:
                    serviceTimoutLabel = orderTimer.preOrderFirstReminderLabel;
                    break;
                case 2:
                    serviceTimoutLabel = orderTimer.preOrderSecondReminderLabel;
                    break;
                case 3:
                    serviceTimoutLabel = orderTimer.preOrderThirdReminderLabel;
                    break;
                default:
                    break;
            }

            if (serviceTimoutLabel) {
                orderLogger(options.orderId, 'info', `workerOrderAssignManager->_createOrderWorkerReminderTimer->order reminder will be called after ${startTime / 60} minutes`);
                orderTimer.setServiceTimeout(serviceTimoutLabel, () => {
                    orderLogger(options.orderId, 'info', `workerOrderAssignManager->_createOrderWorkerReminderTimer->Start sending preorder reminder...`);
                    redisOrderManager.getOrder(options.tenantId, options.orderId, (err, orderData) => {
                        if (err) {
                            orderLogger(options.orderId, 'error', `workerOrderAssignManager->_createOrderWorkerReminderTimer->getOrder->Error: ${err.message}`);
                            return;
                        }
                        const tenantLogin = orderData.tenant_login;

                        if (orderData.worker && orderData.worker.callsign
                            && ( parseInt(orderData.status_id) === statusInfo.worker_assigned_at_order_soft.status_id
                                || parseInt(orderData.status_id) === statusInfo.worker_assigned_at_order_hard.status_id)) {

                            const messageString = orderEventWorkerMessage({
                                command: orderEventWorkerCommand.orderListAddItem,
                                tenant_id: options.tenantId,
                                tenant_login: tenantLogin,
                                worker_callsign: orderData.worker.callsign,
                                params: {
                                    offer_sec: options.timerOrder,
                                    order_id: options.orderId,
                                    type: options.orderType
                                }
                            });

                            const messageTtl = 60 * 60 * 24 * 3; // 3 days
                            amqpWorkerMessageSender.sendMessage(options.tenantId, orderData.worker.callsign, messageString, messageTtl, (err, result) => {
                                if (err) {
                                    orderLogger(options.orderId, 'error', `workerOrderAssignManager->_createOrderWorkerReminderTimer->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                } else {
                                    orderLogger(options.orderId, 'info', `workerOrderAssignManager->_createOrderWorkerReminderTimer->amqpWorkerMessageSender.sendMessage->Result ${result}`);
                                }
                            });
                        } else {
                            orderLogger(options.orderId, 'info', `workerOrderAssignManager->_createOrderWorkerReminderTimer->Don't need send preorder reminder`);
                            orderTimer.clearServiceTimeout(orderTimer.preOrderFirstReminderLabel);
                            orderTimer.clearServiceTimeout(orderTimer.preOrderSecondReminderLabel);
                            orderTimer.clearServiceTimeout(orderTimer.preOrderThirdReminderLabel);
                        }

                    })
                }, startTime * 1000)
            }
        } catch (err) {
            orderLogger(options.orderId, 'error', `workerOrderAssignManager->_createOrderWorkerReminderTimer->Error: ${err.message}`);
        }
    }

    /**
     * Worker refused order assignment. This method:
     * delete worker from order,
     * delete order from worker reserved list,
     * send message about this to worker queue,
     * send push notify to worker,
     * set order to new status if need
     * call block worker preorder if need
     * @param {Object} orderData - order data from redis
     * @param {Object} options
     * @param {boolean} options.isPreOrder - is it preorder
     * @param {boolean} options.isWorkerEventOwner -was this event initiates by worker app
     * @param {Number}  [options.newStatusId] - set new status for this order if need
     */
    processRefusedOrderAssignment(orderData, options) {
        try {
            orderLogger(orderData.order_id, 'info', `workerOrderAssignManager->processRefusedOrderAssignment called!`);
            const tenantId = orderData.tenant_id;
            const tenantLogin = orderData.tenant_login;
            const orderId =  parseInt(orderData.order_id);
            const workerCallsign = orderData.worker ? orderData.worker.callsign : null;
            const orderType = options.isPreOrder ? 'preorder' : 'order';
            const cityOffset = !isNaN(parseFloat(orderData.time_offset)) ? parseFloat(orderData.time_offset) : 0;
            const orderTime = orderData.order_time;
            const updateTime = Math.floor(Date.now() / 1000);
            if (options.newStatusId) {
                const statusData = getOrderStatusById(options.newStatusId);
                orderData.status_id = statusData.status_id;
                orderData.status.status_id = statusData.status_id;
                orderData.status.name = statusData.name;
                orderData.status.status_group = statusData.status_group;
                orderData.status_time = updateTime;
            }
            orderData.worker_id = null;
            orderData.car_id = null;
            orderData.worker = null;
            orderData.car = null;
            orderData.update_time = updateTime;
            orderData.deny_refuse_order = 0;


            if(typeof options.companyID !== undefined &&  options.companyID !== undefined ){
                orderData.worker_company = null;
            }
            //Save order and send message to order queue
            redisOrderManager.saveOrder(orderData, (err, result) => {
                if (err) {
                    orderLogger(orderId, "error", `workerOrderAssignManager->processRefusedOrderAssignment->redisOrderManager.saveOrder->Error: ${err.message}`);
                } else {
                    orderLogger(orderId, "info", `workerOrderAssignManager->refuseOrderAssignment->redisOrderManager.saveOrder->Result: ${result}`);
                    sqlManager.updateOrderFromMysql(orderId, orderData.status_id, null, null, updateTime, (err, result) => {
                        if (err) {
                            orderLogger(orderId, "error", `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderFromMysql->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, "info", `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderFromMysql->Result: ${JSON.stringify(result)}`);
                        }
                    });
                    sqlManager.updateOrderField(orderId, 'deny_refuse_order', 0, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderField->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderField->Result: ${result}`);
                        }
                    });

                    // delete company
                    if(typeof options.companyID !== undefined &&  options.companyID !== undefined){
                        sqlManager.updateOrderField(orderId, 'company_id', null, (err, result) => {
                            if (err) {
                                orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderField->Error: ${err.message}`);
                            } else {
                                orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->sqlManager.updateOrderField->Result: ${result}`);
                            }
                        });
                    }

                    const messageString = orderEventOrderMessage({
                        command: orderEventOrderCommand.updateOrderData,
                        tenant_id: tenantId,
                        order_id: orderId,
                        params: {}
                    });

                    amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', ``);
                        } else {
                            orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.updateOrderData} to order: ${orderId}. Result: ${result}`);
                        }
                    });
                }
            });
            if (workerCallsign) {
                if (options.isPreOrder && options.isWorkerEventOwner) {
                    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                        if (err) {
                            orderLogger(orderId, "error", `workerOrderAssignManager->processRefusedOrderAssignment->redisWorkerManager.getWorker->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, "info", `workerOrderAssignManager->processRefusedOrderAssignment->redisWorkerManager.getWorker->Result: ${workerData}`);

                            // СЃРїРёСЃРѕРє Р°РІС‚РёРЅС‹С… РїРѕРµР·РґРѕРє - СѓРґР°Р»РµРЅРёРµ
                            let active_orders = typeof workerData.worker.active_orders === "object" ?
                                Object.keys(workerData.worker.active_orders).map(key => parseInt(workerData.worker.active_orders[key])): [];
                            let index_del = active_orders.indexOf(orderId);
                            if(index_del > -1){
                                active_orders.splice(index_del, 1);
                            }

                            workerData.worker.active_orders = active_orders;
                            if(active_orders.length === 0){
                                workerData.worker.status = workerStatusManager.free;
                            }

                            redisWorkerManager.saveWorker(workerData, (err, result) => {
                                if(err){
                                    orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->redisWorkerManager.saveWorker->Error ${err.message}`);
                                }
                                orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->saveWorker->Result: ${result}`);

                                if (parseFloat(workerData.worker.pre_order_reject_min) > 0 && parseFloat(workerData.worker.pre_order_block_houer) > 0) {
                                    const tenantNowTime = parseInt((new Date()).getTime() / 1000) + cityOffset;
                                    if ((parseInt(orderTime) - parseInt(tenantNowTime)) < parseFloat(workerData.worker.pre_order_reject_min * 60)) {
                                        blockWorkerPreOrder(tenantId, workerCallsign, (err, result) => {
                                            if (err) {
                                                orderLogger(orderId, "error", `workerOrderAssignManager->processRefusedOrderAssignment->blockWorkerPreOrder->Error: ${err.message}`);
                                            } else {
                                                orderLogger(orderId, "info", `workerOrderAssignManager->processRefusedOrderAssignment->blockWorkerPreOrder->Result: ${result}`);
                                            }

                                        });
                                    } else {
                                        orderLogger(orderId, "info", "workerOrderAssignManager->processRefusedOrderAssignment->Don't need block worker pre order. Pre order abort time is good");
                                    }
                                } else {
                                    orderLogger(orderId, "info", "workerOrderAssignManager->processRefusedOrderAssignment->Don't need block worker pre order. Empty pre order block params");
                                }
                            });

                        }
                    });

                    redisWorkerReservedOrderListManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->redisWorkerReservedOrderListManager.delOrder->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->redisWorkerReservedOrderListManager.delOrder->Result: ${result}`);
                            const messageString = orderEventWorkerMessage({
                                command: orderEventWorkerCommand.orderListDelItem,
                                tenant_id: tenantId,
                                tenant_login: tenantLogin,
                                worker_callsign: workerCallsign,
                                params: {
                                    order_id: orderId,
                                    type: orderType
                                }
                            });
                            const messageTtl = 60 * 60 * 24 * 3; // 3 days
                            amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->amqpWorkerMessageSender.sendMessage->Result: ${result}`);
                                }
                            });
                            pushApi.sendRefuseOrderAssignment(tenantId, orderId, workerCallsign, options.isPreOrder, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, 'error', `workerOrderAssignManager->processRefusedOrderAssignment->pushApi.sendRefuseOrderAssignment->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `workerOrderAssignManager->processRefusedOrderAssignment->pushApi.sendRefuseOrderAssignment->Result: ${result}`);
                                }
                            })
                        }
                    })
                }
            }
        } catch (err) {
            orderLogger(orderData.order_id, "error", `workerOrderAssignManager->processRefusedOrderAssignment->Error: ${err.message}`);
        }
    }
}

module.exports = exports = new OrderWorkerAssignManager();