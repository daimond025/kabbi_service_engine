'use strict';

const orderLogger = require('../../../../services/logger').orderLogger;
const redisOrderManager = require('../../../../database/redisDB/redisOrderManager');
const orderEventWorkerMessage = require('../../../../orderHandlers/orderEvent/orderEventWorkerMessage');
const amqpWorkerMessageSender = require("../../../../services/amqpWorkerMessageSender");
const orderEventWorkerCommand = require('../../../../orderHandlers/orderEvent/orderEventWorkerCommand');

const logger = require('../../../../services/logger');
const mainLogger = logger.mainLogger;
/**
 * Order status accepted preorder confirmation
 * Вывоз метода для отображения поездки - принудительно
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'preOrderStatusAcceptedConfirmation called!');
            const tenantId = orderInstance.tenantId;
            const orderId = orderInstance.orderId;

            redisOrderManager.getOrder(tenantId, orderId, (err, orderDates) => {
                // если водитель одобрил - показ активности на выполнение
                if (orderDates && orderDates.worker && orderDates.worker.callsign) {
                    const workerCallsign = orderDates.worker.callsign;
                    const tenantLogin = orderDates.tenant_login;

                    let offer_sec = (typeof orderData.settings.PRE_ORDER_OFFER_SEC !== undefined) ? parseInt(orderData.settings.PRE_ORDER_OFFER_SEC) : 60;
                    const isPreOrder = 'preorder';

                    let messageString = orderEventWorkerMessage({
                        command: orderEventWorkerCommand.preOrderOffer,
                        tenant_id: tenantId,
                        tenant_login: tenantLogin,
                        worker_callsign: workerCallsign,
                        params: {
                            offer_sec: offer_sec,
                            order_id: orderId,
                            type: isPreOrder
                        }
                    });
                    const messageTtl = 60 * 60 * 24 * 10;
                    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                        if (err) {
                            mainLogger('error', err);
                        } else {
                            mainLogger('info', `preOrderStatusAcceptedConfirmation->Sended execute_start to worker ${workerCallsign}`);
                        }
                    });
                }
            });


            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};