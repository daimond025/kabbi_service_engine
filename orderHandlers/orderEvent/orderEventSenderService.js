'use strict';

const engineService = "engine_service";
const clientService = "client_service";
const workerService = "worker_service";
const operatorService = "operator_service";
const exchangeService = "exchange_service";

function needAddAsyncAnswer(service) {
    if ([clientService, workerService, operatorService].indexOf(service) !== -1) {
        return true;
    }
}

function getEventSubjectType(eventSenderService) {
    let subjectType = '';
    switch (eventSenderService) {
        case engineService:
            subjectType = 'system';
            break;
        case clientService:
            subjectType = 'client';
            break;
        case workerService:
            subjectType = 'worker';
            break;
        case operatorService:
            subjectType = 'disputcher';
            break;
        case exchangeService:
            subjectType = 'exchange';
            break;
        default:
            subjectType = 'system';
            break;
    }
    return subjectType
}


module.exports = {
    engineService,
    clientService,
    workerService,
    operatorService,
    exchangeService,
    needAddAsyncAnswer,
    getEventSubjectType
};
