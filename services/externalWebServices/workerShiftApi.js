const request = require('request');
const config = require("../lib").getConfig();



/**
 * Send order shift to engine
 * @param {Number} tenantId
 * @param {String} tenantLogin
 * @param {Number} workerCallsign
 * @param {Number} shiftLifeTime
 * @param {Number} shiftId
 * @param {Function} callback
 */
function watchWorkerShift(tenantId, tenantLogin, workerCallsign, shiftLifeTime, shiftId, callback) {
    const getString =
        "tenant_id=" + tenantId +
        "&tenant_login=" + tenantLogin +
        "&worker_callsign=" + workerCallsign +
        "&shift_life_time=" + shiftLifeTime +
        "&shift_id=" + shiftId;
    const url = "http://" + config.MY_BIND_HOST + ":" + config.MY_BIND_PORT + "/watch_worker_shift?" + getString;
    const requesOptions = {
        url: url
    };
    //Кинули пуш
    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, null);
                }
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(null, null);
        }
    }

    request(requesOptions, resultData);
}
exports.watchWorkerShift = watchWorkerShift;

