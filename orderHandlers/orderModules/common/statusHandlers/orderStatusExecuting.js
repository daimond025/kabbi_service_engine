'use strict';

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;

/**
 * Order executing
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {*}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusExecuting called!');
            const tenantId = orderInstance.tenantId;
            const orderId = orderInstance.orderId;
            const statusId = orderData.status.status_id;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.clientLateTimeoutLabel);
            if (orderInstance.statusChanged) {
                //Если нажали стоп, запишем таймстап
                if (statusInfo.waiting_for_payment.status_id === statusId) {
                    orderInstance.startWaitingForPaymentTimestamp = Math.floor(Date.now() / 1000);
                }
                // Если нажали продолжить поездку, залогируем время между СТОП и ПРОДОЛЖИТЬ ПОЕЗДКУ
                if (statusInfo.order_execution.status_id === statusId && orderInstance.startWaitingForPaymentTimestamp) {
                    try {
                        const updateTime = Math.floor(Date.now() / 1000);
                        const orderPaymentTimeSec = updateTime - orderInstance.startWaitingForPaymentTimestamp;
                        orderInstance.startWaitingForPaymentTimestamp = null;
                        const workerCallsign = orderData.worker.callsign;
                        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                            if (err) {
                                orderLogger(orderId, 'error', `orderStatusExecuting->redisWorkerManager.getWorker->Error ${err.message}`);
                            } else {
                                try {
                                    const workerShiftId = workerData.worker.worker_shift_id;
                                    sqlManager.logWorkerOrderPaymentTime(workerShiftId, orderPaymentTimeSec, (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderStatusExecuting->sqlManager.logWorkerOrderPaymentTime->Error ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderStatusExecuting->sqlManager.logWorkerOrderPaymentTime->Result: ${result}`);
                                        }
                                    });
                                } catch (err) {
                                    orderLogger(orderId, 'error', `orderStatusExecuting->Error: ${err.message}`);
                                }
                            }
                            return resolve(1);
                        });
                    } catch (err) {
                        orderLogger(orderId, 'error', `orderStatusExecuting->Error ${err.message}`);
                        return reject(err);
                    }
                } else {
                    return resolve(1);
                }
            } else {
                return resolve(1);
            }
        }
        catch (err) {
            return reject(err);
        }
    });
};
