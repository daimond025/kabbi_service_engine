'use strict';

const packageInfo = require('../package.json');
const request = require('request');
const lib = require("./lib");
const config = lib.getConfig();
const amqp = require('amqplib/callback_api');
const sqlManager = require('../database/sqlDB/sqlManager');
const redisTestManager = require('../database/redisDB/redisTestManager');
const consulAgent = require("../services/consul/consulAgent");
const uuid = require('uuid/v4');

function version(req, res, next) {
    const name = packageInfo.name;
    const version = packageInfo.version;
    res.setHeader('Content-Type', 'text/plain');
    let info = `${name} v${version}
work on nodejs ${process.version}`;
    res.send(info);
}

function status(req, res, next) {
    const STATUS_OK = 'OK   ';
    const STATUS_ERROR = 'ERROR';
    const name = packageInfo.name;
    const version = packageInfo.version;
    let isNot = ' ';
    let hasError = false;
    let redisStatus, redisError,
        dbStatus, dbError,
        serviceAPIStatus, serviceAPIError,
        pushAPIStatus, pushAPIError,
        phoneAPIStatus, phoneAPIError,
        orderAPIStatus, orderAPIError,
        rabbitMQManagementAPIStatus, rabbitMQManagementAPIError,
        rabbitmqStatus, rabbitmqError,
        consulStatus, consulError;
    redisChecker()
        .then(() => {
            redisStatus = STATUS_OK;
            redisError = '';
        })
        .catch(redisErr => {
            redisStatus = STATUS_ERROR;
            redisError = `(${redisErr.message})`;
            hasError = true;
        })
        .then(() => {
            return dbChecker();
        })
        .then(() => {
            dbStatus = STATUS_OK;
            dbError = '';
        })
        .catch(sqlErr => {
            dbStatus = STATUS_ERROR;
            dbError = `(${sqlErr.message})`;
            hasError = true;
        })
        .then(() => {
            return serviceAPIChecker()
        })
        .then(() => {
            serviceAPIStatus = STATUS_OK;
            serviceAPIError = '';
        })
        .catch(serErr => {
            serviceAPIStatus = STATUS_ERROR;
            serviceAPIError = `(${serErr.message})`;
            hasError = true;
        })
        .then(() => {
            return pushAPIChecker()
        })
        .then(() => {
            pushAPIStatus = STATUS_OK;
            pushAPIError = '';
        })
        .catch(pushErr => {
            pushAPIStatus = STATUS_ERROR;
            pushAPIError = `(${pushErr.message})`;
            hasError = true;
        })
        .then(() => {
            return phoneAPIChecker()
        })
        .then(() => {
            phoneAPIStatus = STATUS_OK;
            phoneAPIError = '';
        })
        .catch(phoneErr => {
            phoneAPIStatus = STATUS_ERROR;
            phoneAPIError = `(${phoneErr.message})`;
            hasError = true;
        })
        .then(() => {
            return orderAPIChecker()
        })
        .then(() => {
            orderAPIStatus = STATUS_OK;
            orderAPIError = '';
        })
        .catch(orderErr => {
            orderAPIStatus = STATUS_ERROR;
            orderAPIError = `(${orderErr.message})`;
            hasError = true;
        })
        .then(() => {
            return rabbitmqManagementAPIChecker()
        })
        .then(() => {
            rabbitMQManagementAPIStatus = STATUS_OK;
            rabbitMQManagementAPIError = '';
        })
        .catch(mqHTTPErr => {
            rabbitMQManagementAPIStatus = STATUS_ERROR;
            rabbitMQManagementAPIError = `(${mqHTTPErr.message})`;
            hasError = true;
        })
        .then(() => {
            return rabbitmqChecker()
        })
        .then(() => {
            rabbitmqStatus = STATUS_OK;
            rabbitmqError = '';
        })
        .catch(rabbitErr => {
            rabbitmqStatus = STATUS_ERROR;
            rabbitmqError = `(${rabbitErr.message})`;
            hasError = true;
        })
        .then(() => {
            return consulChecker()
        })
        .then(() => {
            consulStatus = STATUS_OK;
            consulError = '';
        })
        .catch(consulErr => {
            consulStatus = STATUS_ERROR;
            consulError = `(${consulErr.message})`;
            hasError = true;
        })
        .then(() => {
            if (hasError) {
                isNot = ' NOT ';
            }
            let status = `${name} v${version}
work on nodejs ${process.version}            
            
[ ${dbStatus} ] database "db" ${dbError}
[ ${redisStatus} ] database "redis" ${redisError}
[ ${serviceAPIStatus} ] api "service" ${serviceAPIError}
[ ${phoneAPIStatus} ] api "phone" ${phoneAPIError}
[ ${orderAPIStatus} ] api "order" ${orderAPIError}
[ ${rabbitMQManagementAPIStatus} ] api "rabbitMQ-management-HTTP-API" ${rabbitMQManagementAPIError}
[ ${pushAPIStatus} ] service "push" ${pushAPIError}
[ ${rabbitmqStatus} ] service "RabbitMQ" ${rabbitmqError}
[ ${consulStatus} ] service "consulAgent" ${consulError}

SERVICE${isNot}OPERATIONAL`;
            res.setHeader('Content-Type', 'text/plain');
            res.send(`${status}`);
        });
}


/**
 * Check redis
 * @return {Promise}
 */
function redisChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        redisTestManager.setValue(key, 'push_test_set')
            .then(() => {
                return redisTestManager.getValue(key)
            })
            .then(() => {
                return redisTestManager.updateValue(key, 'push_test_update')
            })
            .then(() => {
                return redisTestManager.deleteKey(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

/**
 * Check db
 * @return {Promise}
 */
function dbChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        sqlManager.checkInsert(key, 'push_test_insert')
            .then(() => {
                return sqlManager.checkSelect(key)
            })
            .then(() => {
                return sqlManager.checkUpdate(key, 'push_test_update')
            })
            .then(() => {
                return sqlManager.checkDelete(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

/**
 * Check service api
 * @return {Promise}
 */
function serviceAPIChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${config.API_SERVICE_URL}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}

/**
 * Check push service
 * @return {Promise}
 */
function pushAPIChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${config.SERVICE_PUSH_URL}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}

/**
 * Check phone api
 * @return {Promise}
 */
function phoneAPIChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${config.API_PHONE_URL}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}

/**
 * Check order api
 * @return {Promise}
 */
function orderAPIChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${config.API_ORDER_URL}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}

/**
 * Check rabbitmq management api
 * @return {Promise}
 */
function rabbitmqManagementAPIChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${config.API_RABBITMQ_MANAGEMENT_URL}`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}

/**
 * Consul checker
 * @returns {Promise}
 */
function consulChecker() {
    return new Promise((resolve, reject) => {
        consulAgent.getAgentInfo()
            .then(info => {
                return resolve(info);
            })
            .catch(err => {
                return reject(err);
            })
    });
}

/**
 * Rabbitmq checker
 * @return {Promise}
 */
function rabbitmqChecker() {
    return new Promise((resolve, reject) => {
        const queueLabel = `rabbitmq_checker`;
        const messageString = 'test';
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn2) => {
            if (err) {
                return reject(err);
            }
            conn2.createChannel((err, ch2) => {
                if (err) {
                    conn2.close();
                    return reject(err);
                }
                ch2.assertQueue(queueLabel, {
                    durable: true
                });
                ch2.prefetch(1);
                ch2.consume(queueLabel, (message) => {
                    try {
                        ch2.ack(message);
                        setTimeout(() => {
                            ch2.close();
                            conn2.close();
                            resolve();
                        }, 100);
                    } catch (err) {
                        reject(err);
                    }

                })
            })
        });
        setTimeout(() => {
            amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
                if (err) {
                    return reject(err);
                }
                conn.createChannel((err, ch) => {
                    if (err) {
                        conn.close();
                        return reject(err);
                    }
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true
                        });
                        ch.sendToQueue(queueLabel, new Buffer(messageString), {
                            persistent: true
                        });
                        setTimeout(() => {
                            conn.close();
                        }, 100);
                    } catch (err) {
                        return reject(`Err of sending message:${messageString} to queue:${queueLabel}`);
                    }
                });
            });
        }, 250);
    })
}

module.exports = {
    version, status
};
