"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const PHPUnserialize = require('php-unserialize');
const redis = require("redis");
const async = require("async");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const retryStrategy = require("./redisRetryStrategy");


/**
 * Order manager
 * @param  {Number} port
 * @param  {String} ip
 * @returns {RedisOrderManager}
 */
function RedisOrderManager(port, ip) {
    //Orders
    this.dbNumber = 1;

    this.redisClientDb = redis.createClient(port, ip, {
        retry_strategy: function (options) {
            return retryStrategy(options);
        }
    });

    this.redisClientDb.on("connect", () => {
        mainLogger('info', `connected Redis db${this.dbNumber}`);
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', `reconnecting Redis db${this.dbNumber}`);
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', `Redis error in db${this.dbNumber}: ${err.message}`);
    });


}


/**
 * Get all active orders by tenantId
 * @param {Number} tenantId
 * @param  {Function} callback
 */
RedisOrderManager.prototype.getAllOrders = function (tenantId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hvals(tenantId, (err, orders) => {
            if (err) {

                return callback(err);
            }
            const ordersResultArr = [];
            async.each(orders, (orderItem, callback) => {
                try {
                    let order = PHPUnserialize.unserialize(orderItem);
                    ordersResultArr.push(order);
                    return callback();
                } catch (err) {
                    return callback();
                }
            }, function (err) {
                if (err) {
                    return callback(err);
                }
                return callback(null, ordersResultArr);
            });
        });
    });
};

/**
 * Get order data by tenantId and orderId
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param  {Function} callback
 */
RedisOrderManager.prototype.getOrder = function (tenantId, orderId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, function () {
        self.redisClientDb.hget([tenantId, orderId], (err, order) => {
            if (err) {
                return callback(err);
            }
            try {
                order = PHPUnserialize.unserialize(order);
                if (order === null || typeof(order) === "undefined") {
                    return callback(new Error("No order at redisDB"));
                }
                return callback(null, order);
            } catch (err) {
                return callback(err);
            }
        });

    });

};


/**
 * Save order data
 * @param {Object} orderData
 * @param  {Function} callback
 */
RedisOrderManager.prototype.saveOrder = function (orderData, callback) {
    const self = this;
    let tenantId, orderId, order;
    if (orderData.tenant_id) {
        tenantId = orderData.tenant_id;
    } else {
        return callback(new Error('Bad order tenant id'));
    }
    if (orderData.order_id) {
        orderId = orderData.order_id;
    } else {
        return callback(new Error('Bad order id'));
    }
    try {
        order = lib.serialize(orderData);
    } catch (err) {
        return callback(err);
    }
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hset(tenantId, orderId, order, (err, hsetResult) => {
            if (err) {
                return callback(err);
            }

            if (hsetResult === 1 || hsetResult === 0) {
                return callback(null, 1);
            }

            return callback(new Error('Error - hset bad result. Hset result:' + hsetResult));
        });
    });
};

/**
 * Delete order
 * @param {Number} tenantId
 * @param {Number} orderId
 * @param  {Function} callback
 */
RedisOrderManager.prototype.deleteOrder = function (tenantId, orderId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hdel([tenantId, orderId], (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });

    });
};


module.exports = exports = new RedisOrderManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);