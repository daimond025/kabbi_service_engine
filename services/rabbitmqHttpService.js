'use strict';
const request = require('request');
const lib = require("./lib");
const config = lib.getConfig();
const rabbitHttpApiUrl = config.API_RABBITMQ_MANAGEMENT_URL;
/**
 * Get queue consumers count
 * @param {String} queueLabel - Label of queue
 * @param {String} vhost  - The name of the "virtual host" (or vhost) that specifies the namespace for entities (such as exchanges and queues) referred to by the protocol.
 * Note that this is not virtual hosting in the HTTP sense.
 * Default value = '/'
 * @param {Function} callback
 */
exports.getQueueConsumersCount = function (queueLabel, vhost, callback) {
    if (typeof vhost !== "string" && typeof vhost !== "number") {
        vhost = '/';
    }
    vhost = encodeURIComponent(vhost);
    const requestOptions = {
        url: rabbitHttpApiUrl + `queues/${vhost}/${queueLabel}`
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);

                let count = !isNaN(parseInt(info.consumers)) ? parseInt(info.consumers) : 0;
                return callback(null, count);
            } catch (err) {
                return callback(err);
            }
        } else {
            return callback(new Error('rabbitHttpApi bad answer'));
        }
    }

    request(requestOptions, resultData);
};
