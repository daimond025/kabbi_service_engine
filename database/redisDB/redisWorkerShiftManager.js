"use strict";
const lib = require("../../services/lib");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const config = lib.getConfig();
const redis = require("redis");
const retryStrategy = require("./redisRetryStrategy");


/**
 * Worker shift manager
 * @param {Number} port
 * @param  {String} ip
 * @returns {RedisWorkerShiftManager}
 */
function RedisWorkerShiftManager(port, ip) {
    //WorkerShifts
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db7");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db7");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db7:" + err);
    });

    this.dbNumber = 7;

}


/**
 * Get time left of worker shift
 * @param  {String} workerShiftKey
 * @param  {Function} callback
 */
RedisWorkerShiftManager.prototype.getTimeLeft = function (workerShiftKey, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, function () {
        self.redisClientDb.ttl(workerShiftKey, (err, ttl) => {
            if (err) {
                return callback(err);
            }
            callback(null, ttl);
        });
    });
};

/**
 * Get worker shift id
 * @param  {String} tenantLogin
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
RedisWorkerShiftManager.prototype.getWorkerShiftId = function (tenantLogin, workerCallsign, callback) {
    const self = this;
    const workerShiftKey = tenantLogin + "_" + workerCallsign;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.get(workerShiftKey, (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

/**
 * Delete worker shift
 * @param  {String} tenantLogin
 * @param  {Number} workerCallsign
 * @param  {Function} callback
 */
RedisWorkerShiftManager.prototype.deleteWorkerShift = function (tenantLogin, workerCallsign, callback) {
    const self = this;
    const workerShiftKey = tenantLogin + "_" + workerCallsign;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.del(workerShiftKey, function (err, result) {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};


module.exports = exports = new RedisWorkerShiftManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);