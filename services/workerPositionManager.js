'use strict';

const positionInfo = {
    simple_taxi_driver: {
        position_id: 1,
        name: 'Simple taxi driver',
        module_id: 1,
        has_car: 1
    },
    simple_truck_driver: {
        position_id: 2,
        name: 'Simple truck driver',
        module_id: 2,
        has_car: 1
    },
    simple_courier: {
        position_id: 3,
        name: 'Simple courier',
        module_id: 3,
        has_car: 0
    },
    simple_master: {
        position_id: 4,
        name: 'Simple master',
        module_id: 4,
        has_car: 0
    },
    courier_on_moto: {
        position_id: 5,
        name: 'Courier on moto',
        module_id: 3,
        has_car: 0
    },
    courier_on_auto: {
        position_id: 6,
        name: 'Courier on auto',
        module_id: 3,
        has_car: 0
    },
    plumber: {
        position_id: 7,
        name: 'Plumber',
        module_id: 4,
        has_car: 0
    },
    electrician: {
        position_id: 8,
        name: 'Electrician',
        module_id: 4,
        has_car: 0
    },
    computer_repair: {
        position_id: 9,
        name: 'Computer Repair',
        module_id: 3,
        has_car: 0
    },
    configuring_computers: {
        position_id: 10,
        name: 'Configuring Computers',
        module_id: 4,
        has_car: 0
    },
    opening_of_locks_apartments_and_cars: {
        position_id: 11,
        name: 'Opening of locks, apartments and cars',
        module_id: 4,
        has_car: 0
    },
    test_charging_and_rental_a_accumulator: {
        position_id: 12,
        name: 'Test, charging and rental a accumulator',
        module_id: 4,
        has_car: 0
    },
    general_worker: {
        position_id: 13,
        name: 'General worker',
        module_id: 4,
        has_car: 0
    },
    handyman: {
        position_id: 14,
        name: 'Handyman',
        module_id: 4,
        has_car: 0
    },
    housekeeper: {
        position_id: 15,
        name: 'Housekeeper',
        module_id: 4,
        has_car: 0
    },
    locksmith: {
        position_id: 16,
        name: 'Locksmith',
        module_id: 4,
        has_car: 0
    },
    doctor: {
        position_id: 17,
        name: 'Doctor',
        module_id: 4,
        has_car: 0
    },
    nurse: {
        position_id: 18,
        name: 'Nurse',
        module_id: 4,
        has_car: 0
    },
    lawyer: {
        position_id: 19,
        name: 'Lawyer',
        module_id: 4,
        has_car: 0
    },
    emergency_commissioner: {
        position_id: 20,
        name: 'Emergency Commissioner',
        module_id: 4,
        has_car: 0
    },
    nanny: {
        position_id: 21,
        name: 'Nanny',
        module_id: 4,
        has_car: 0
    },
    cleaner: {
        position_id: 22,
        name: 'Cleaner',
        module_id: 4,
        has_car: 0
    },
    promotions_manager: {
        position_id: 23,
        name: 'Рromotions manager',
        module_id: 4,
        has_car: 0
    },
    repair_of_household_appliances: {
        position_id: 24,
        name: 'Repair of household appliances',
        module_id: 4,
        has_car: 0
    },
    hairdresser: {
        position_id: 25,
        name: 'Нairdresser',
        module_id: 4,
        has_car: 0
    },
    manicure_and_pedicure: {
        position_id: 26,
        name: 'Manicure and pedicure',
        module_id: 4,
        has_car: 0
    },
    masseur: {
        position_id: 27,
        name: 'Masseur',
        module_id: 4,
        has_car: 0
    },
    makeup_artist: {
        position_id: 28,
        name: 'Makeup artist',
        module_id: 4,
        has_car: 0
    },
    mounting_of_tire: {
        position_id: 29,
        name: 'Mounting of tire',
        module_id: 4,
        has_car: 0
    },
    sober_driver: {
        position_id: 30,
        name: 'Sober driver',
        module_id: 4,
        has_car: 0
    },
};


/**
 * Get position by id
 * @param {Number} positionId
 * @returns {object}
 */
function getPositionById(positionId) {
    let resultPosition = null;
    Object.keys(positionInfo).forEach(key => {
        let position = positionInfo[key];
        if (positionId === position.position_id) {
            resultPosition = position;
        }
    });
    return resultPosition;
}


/**
 * Does position has car
 * @param positionId
 * @returns {boolean}
 */
function positionHasCar(positionId) {
    const position = this.getPositionById(positionId);
    return position && position.has_car === 1;

}

exports.positionInfo = positionInfo;
exports.getPositionById = getPositionById;
exports.positionHasCar = positionHasCar;