'use strict';

const config = require("../lib").getConfig();
const consul = require("./consulAgent").consul;
const async = require("async");
const uuid = require("uuid/v4");
const mainLogger = require("../logger").mainLogger;
const serviceTimer = require("../serviceTimer");
const workResumeSingle = require("../workResume/workResumeSingle");
const workResumeCluster = require("../workResume/workResumeCluster");
const commandLineArgs = require('command-line-args');
const consoleOptionDefinitions = [
    {name: 'resume', type: Boolean},
    {name: 'from_ids', type: Number, multiple: true},
];
const consoleOptions = commandLineArgs(consoleOptionDefinitions);

/**
 * @class ConsulServiceWorker
 */
class ConsulServiceWorker {
    /**
     *@constructor
     */
    constructor() {
        this.id = config.MY_ID + "-" + uuid();
        this.sessionId = null;
        this.sessionTTL = config.CONSUL_SERVICE_SESSION_TTL;
        this.healthCheckTTL = config.CONSUL_SERVICE_HEALTH_CHECK_TTL;
        this.serviceInfo = {
            name: config.CONSUL_SERVICE_NAME,
            address: config.MY_BIND_HOST,
            port: parseInt(config.MY_BIND_PORT),
            id: this.id,
            check: {
                ttl: this.healthCheckTTL,
                deregistercriticalserviceafter: config.CONSUL_SERVICE_CRITICAL_DEREGISTER_AFTER,
                status: "passing"
            }
        };
        this.iMasterNode = false;
        this.needResume = consoleOptions.resume ? consoleOptions.resume : false;
        this.nodeIdsToProcess = consoleOptions.from_ids ? consoleOptions.from_ids : [];
        this.consulTimer = new serviceTimer();
    }


    /**
     * Run worker
     * @returns {Promise}
     */
    run() {
        return new Promise((resolve, reject) => {
            consul.agent.service.register(this.serviceInfo)
                .then(() => {
                    mainLogger("info", "consulServiceWorker->run->service registered in consul");
                    //Create session
                    return consul.session.create({
                        lockdelay: "1s",
                        name: config.CONSUL_SERVICE_NAME,
                        ttl: this.sessionTTL,
                        consistent: true
                    })
                })
                .catch(registerErr => {
                    mainLogger("error", "consulServiceWorker->run->consul register error: " + registerErr.message);
                    return reject(registerErr);
                })
                .then(session => {
                    mainLogger("info", "consulServiceWorker->run->session created");
                    this.sessionId = session.ID;
                    //Try to set this sevice as master
                    mainLogger("info", "consulServiceWorker->run->try to set service as MASTER node in consul...");
                    return consul.kv.set({
                        key: config.CONSUL_SERVICE_MASTER_KEY_NAME,
                        value: JSON.stringify(this.serviceInfo),
                        acquire: this.sessionId,
                        consistent: true
                    })
                })
                .catch(sessionErr => {
                    mainLogger("error", "consulServiceWorker->run->session create error: " + sessionErr.message);
                    return reject(sessionErr);
                })
                .then(setMasterResult => {
                    mainLogger("info", "consulServiceWorker->run->result of setting service as MASTER node: " + setMasterResult);
                    this.iMasterNode = setMasterResult;
                })
                .catch(setMasterNodeErr => {
                    mainLogger("error", "consulServiceWorker->run->setting service as MASTER node error: " + setMasterNodeErr.message);
                })
                .then(() => {
                    if (this.needResume) {
                        mainLogger("info", `consulServiceWorker->run->service running at RESUME mode`);
                        if (this.nodeIdsToProcess.length) {
                            mainLogger("info", `consulServiceWorker->run->starting to process RESUME orders from node ids:  ${this.nodeIdsToProcess}...`);
                            async.each(this.nodeIdsToProcess, (nodeId, cb) => {
                                Promise.all(
                                    [
                                        workResumeSingle.loadOrders(nodeId),
                                        workResumeSingle.loadWorkerUnblockTimers(nodeId),
                                        workResumeSingle.loadWorkerShifts(nodeId)
                                    ])
                                    .then(results => {
                                        mainLogger("info", `consulServiceWorker->run->workResumeSingle results: ${results}`);
                                        return cb(null, 1);
                                    })
                                    .catch(err => {
                                        mainLogger("error", `consulServiceWorker->run->workResumeSingle error: ${err.message}`);
                                        return cb(null, 1);
                                    });
                            }, (err) => {
                                if (err) {
                                    mainLogger("error", `consulServiceWorker->run->workResumeSingle->error: ${err.message}`);
                                    return reject(err);
                                } else {
                                    mainLogger("info", `consulServiceWorker->run->workResumeSingle->result: true.`);
                                    this._runConsulWatcher();
                                    return resolve(1);
                                }
                            })
                        } else {
                            let nodeId = parseInt(config.MY_ID);
                            mainLogger("info", `consulServiceWorker->run->starting to RESUME self orders from node id: ${nodeId}...`);
                            Promise.all(
                                [
                                    workResumeSingle.loadOrders(nodeId),
                                    workResumeSingle.loadWorkerUnblockTimers(nodeId),
                                    workResumeSingle.loadWorkerShifts(nodeId)
                                ])
                                .then(results => {
                                    mainLogger("info", `consulServiceWorker->run->workResumeSingle result: ${results}.`);
                                    this._runConsulWatcher();
                                    return resolve(1);
                                })
                                .catch(err => {
                                    mainLogger("error", `consulServiceWorker->run->workResumeSingle error: ${err.message}`);
                                    return reject(err);
                                });
                        }
                    } else {
                        this._runConsulWatcher();
                        return resolve(1);
                    }
                })
        })
    }

    /**
     * Run watcher
     * @private
     */
    _runConsulWatcher() {
        mainLogger("info", `consulServiceWorker->_runConsulWatcher`);
        //Self Health check interval
        this.consulTimer.setServiceInterval("consulHealthCheckInterval", () => {
            consul.agent.check.pass({id: `service:${this.id}`}, (err) => {
                if (err) {
                    mainLogger("error", `consulServiceWorker->_runConsulWatcher->consul.agent.check.pass error: ${err.message}`);
                } else {
                    //  mainLogger("info", "consulServiceWorker->_runConsulWatcher->consul.agent.check.pass: true");
                }
            });
        }, 3 * 1000);

        //Renew session intrerval
        this.consulTimer.setServiceInterval("consulSessionRenewInterval", () => {
            consul.session.renew(this.sessionId, function (err, renew) {
                if (err) {
                    mainLogger("error", `consulServiceWorker->_runConsulWatcher->consul.session.renew error: ${err.message}`);
                } else {
                    // mainLogger("info", "consulServiceWorker->_runConsulWatcher->consul.session.renew result: " + JSON.stringify(renew));
                }
            });
        }, 3 * 1000);


        //Watch for change of master node  and whenever possible set this node as master
        const masterKeyWatcher = consul.watch({
            method: consul.kv.get,
            options: {
                key: config.CONSUL_SERVICE_MASTER_KEY_NAME,
                consistent: true
            }
        });

        masterKeyWatcher.on('change', (data) => {
            mainLogger("info", `consulServiceWorker->_runConsulWatcher->masterKeyWatcher->change event:${JSON.stringify(data)}`);
            if (data && data.Session !== this.sessionId) {
                mainLogger("info", 'consulServiceWorker->_runConsulWatcher->masterKeyWatcher->try to set service as MASTER node in consul...');
                this.consulTimer.setServiceTimeout("setMasterNodeTimeout", () => {
                    consul.kv.set({
                        key: config.CONSUL_SERVICE_MASTER_KEY_NAME,
                        value: JSON.stringify(this.serviceInfo),
                        acquire: this.sessionId,
                        consistent: true
                    })
                        .then(setMasterResult => {
                            mainLogger("info", "consulServiceWorker->_runConsulWatcher->masterKeyWatcher->result of setting service as MASTER node: " + setMasterResult);
                            this.iMasterNode = setMasterResult;
                        })
                        .catch(setMasterNodeErr => {
                            mainLogger("error", "consulServiceWorker->_runConsulWatcher->masterKeyWatcher->setting service as MASTER node error: " + setMasterNodeErr.message);
                        })
                }, 3 * 1000);
            }
        });

        masterKeyWatcher.on('error', (err) => {
            mainLogger("error", "consulServiceWorker->_runConsulWatcher->masterKeyWatcher->error: " + err.message);
        });

        //Watch for health of services
        const healthCheckWatcher = consul.watch({
            method: consul.health.state,
            options: {
                state: 'critical',
                consistent: true
            }
        });

        healthCheckWatcher.on('change', (data) => {
            mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->change event: ${JSON.stringify(data)}`);
            for (let node of data) {
                //engine node in critucal state
                if (node.ServiceName === config.CONSUL_SERVICE_NAME
                    && node.ServiceID !== this.id
                    && node.Status === "critical" && node.Output === "TTL expired") {
                    mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->${node.ServiceName} ${node.ServiceID} TTL expired`);
                    if (this.iMasterNode) {
                        mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->i am master node, need derigister ${node.ServiceName} ${node.ServiceID} and up all data, that processed at that node`);
                        consul.agent.service.deregister({id: node.ServiceID})
                            .then(() => {
                                mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->${node.ServiceName} ${node.ServiceID} derigistered`);
                            })
                            .catch(err => {
                                mainLogger("error", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->${node.ServiceName} ${node.ServiceID} derigister error: ${err.message}`);
                            })
                            .then(() => {
                                mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->start to send data from fallen node(${node.ServiceName} ${node.ServiceID}) to healthy nodes...`);
                                const ctiticalNodeId = parseInt(node.ServiceID.split("-")[0]);
                                Promise.all(
                                    [
                                        workResumeCluster.loadOrders(ctiticalNodeId),
                                        workResumeCluster.loadWorkerUnblockTimers(ctiticalNodeId),
                                        workResumeCluster.loadWorkerShifts(ctiticalNodeId)
                                    ])
                                    .then(results => {
                                        mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->result of sending data from fallen node to cluster loader: ${results}`);
                                    })
                                    .catch(err => {
                                        mainLogger("error", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->error of sending data from fallen node to cluster loader: ${err.message}`);
                                    });
                            })
                    } else {
                        mainLogger("info", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->i not master node, dont need up data from ${node.ServiceName} ${node.ServiceID}`);
                    }
                }
            }
        });

        healthCheckWatcher.on('error', (err) => {
            mainLogger("error", `consulServiceWorker->_runConsulWatcher->healthCheckWatcher->error: ${err.message}`);
        });
    }

    /**
     * Deregister service from consul
     * @returns {Promise<boolean>}
     */
    deregister() {
        return new Promise((resolve, reject) => {
            if (!this.sessionId) {
                consul.agent.service.deregister(this.serviceInfo)
                    .then(() => {
                        mainLogger("info", `consulServiceWorker->deregister->service derigistred`);
                        return resolve(true);
                    })
                    .catch(err => {
                        mainLogger("info", `consulServiceWorker->deregister->deregister error: ${err.message}`);
                        return reject(err);
                    })
            } else {
                mainLogger("info", "consulServiceWorker->deregister->service session id: " + this.sessionId);
                mainLogger("info", "consulServiceWorker->deregister->start to release session...");
                consul.kv.set({
                    key: config.CONSUL_SERVICE_MASTER_KEY_NAME,
                    value: JSON.stringify(this.serviceInfo),
                    release: this.sessionId,
                    consistent: true
                })
                    .then(setResult => {
                        mainLogger("info", `consulServiceWorker->deregister->session release result: ${setResult}`);
                        return consul.session.destroy(this.sessionId);
                    })
                    .then(() => {
                        mainLogger("info", `consulServiceWorker->deregister->session released`);
                        return consul.agent.service.deregister(this.serviceInfo)
                    })
                    .then(() => {
                        mainLogger("info", `consulServiceWorker->deregister->service deregistered`);
                        return resolve(true);
                    })
                    .catch(err => {
                        mainLogger("error", `consulServiceWorker->deregister->error: ${err.message}`);
                        return reject(err);
                    })
            }
        });
    }

    /**
     * Get status: master/candidate
     * @returns {string}
     */
    get status() {
        return this.iMasterNode ? "master" : "candidate";
    }


}
module.exports = exports = new ConsulServiceWorker();