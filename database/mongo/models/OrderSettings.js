const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const OrderSettings = new mongoose.Schema({
    order_id: Number,
    tenant_id: Number,
    settings: Map

}, {collection: 'order_settings'});

OrderSettings.plugin(timestamps);

module.exports = exports = mongoose.model('OrderSettings', OrderSettings);