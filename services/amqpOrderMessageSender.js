"use strict";

const lib = require("./lib");
const config = lib.getConfig();
const amqp = require('amqplib/callback_api');
const mainLogger = require('./logger').mainLogger;


/**
 * amqpOrderMessageSender
 *  Amqp order message publisher
 * @param {Number} orderId
 * @param {String} messageString
 * @param {Function} callback
 */

function sendMessage(orderId, messageString, callback) {
    const queueLabel = `order_${orderId}`;
    amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
        if (err) {
            return callback(err);
        } else {
            conn.createChannel(function (err, ch) {
                if (err) {
                    return callback(err);
                } else {
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true
                        });

                        ch.sendToQueue(queueLabel, new Buffer(messageString), {
                            persistent: true
                        });
                        return callback(null, 'message sent');
                    } catch (err) {
                        return callback(`Err of sending message:${messageString} to queue:${queueLabel}`);
                    }
                }
            });
            setTimeout(function () {
                try {
                    conn.close();
                } catch (err) {
                    mainLogger('error', err);
                }
            }, 500);
        }
    });
}

exports.sendMessage = sendMessage;
