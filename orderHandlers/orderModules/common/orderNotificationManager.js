'use strict';

const pushApi = require("../../../services/externalWebServices/pushApi");
const serviceApi = require("../../../services/externalWebServices/serviceApi");
const phoneApi = require("../../../services/externalWebServices/phoneApi");
const sqlManager = require('../../../database/sqlDB/sqlManager');
const logger = require('../../../services/logger');
const orderLogger = logger.orderLogger;
const redisWorkerManager = require("../../../database/redisDB/redisWorkerManager");
const statusInfo = require("../../../services/orderStatusManager").statusInfo;
const positionManager = require("../../../services/workerPositionManager");
const async = require("async");
const orderDistributionManager = require("./orderDistributionManager");

/**
 * @class OrderNotificationManager
 */
class OrderNotificationManager {

    static get TYPE_GROUP_APP() {
        return 'APP';
    }

    static get TYPE_GROUP_CALL() {
        return 'CALL';
    }

    static get TYPE_GROUP_WEB() {
        return 'WEB';
    }

    static get TYPE_GROUP_BORDER() {
        return 'BORDER';
    }

    static get NOTICE_TYPE_SMS() {
        return 'sms';
    }

    static get NOTICE_TYPE_PUSH() {
        return 'push';
    }

    static get NOTICE_TYPE_AUTOCALL() {
        return 'autocall';
    }

    static get NOTICE_TYPE_NOTHING() {
        return 'nothing';
    }

    static get NOTICE_TYPE_SMS_PASSENGER() {
        return 'smsPassenger';
    }


    /**
     * @constructor
     */
    constructor() {

    }

    /**
     * Send change order status notification to client
     * @param {object} orderData
     * @param {object} options
     * @param {number} options.smsNotifyPointRecipientAboutOrderExecuting
     * @param {boolean} options.pointRecipientsNotifiedAboutOrderExecution
     * @returns {Promise}
     */
    sendChangeOrderStatusNotificationToClient(orderData, options = {}) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', "orderNotificationManager->sendChangeOrderStatusNotificationToClient called!");
                const orderId = orderData.order_id;
                const tenantId = orderData.tenant_id;
                const positionId = orderData.tariff.position_id;
                const cityId = orderData.city_id;
                let statusId = parseInt(orderData.status_id);
                const statusGroup = orderData.status.status_group;
                const typeDevice = orderData.device;
                const clientPhone = orderData.phone;
                const deviceToken = orderData.client_device_token;
                const lang = orderData.client.lang;
                const appId = orderData.app_id;
                const clientPassengerId = orderData.client_passenger_id;
                let typeGroup = null;
                let result = {
                    notify_client: false,
                    notify_point_recipient: false,
                    notify_client_passenger: false,
                };

                //Notify point recipient
                if (statusId === statusInfo.order_execution.status_id
                    && options.smsNotifyPointRecipientAboutOrderExecuting === 1
                    && !options.pointRecipientsNotifiedAboutOrderExecution) {
                    result.notify_point_recipient = true;

                    serviceApi.sendSmsNotificationForPointsRecipient(tenantId, orderId, statusId)
                        .then(result => {
                            orderLogger(orderId, 'info', `orderNotificationManager->serviceApi.sendSmsNotificationForPointsRecipient->result:${result}`);
                        })
                        .catch(err => {
                            orderLogger(orderId, 'error', `orderNotificationManager->serviceApi.sendSmsNotificationForPointsRecipient->error:${err.message}`);
                        })
                }

                switch (typeDevice) {
                    case "IOS":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_APP;
                        break;
                    case "ANDROID":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_APP;
                        break;
                    case "WINFON":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_APP;
                        break;
                    case "DISPATCHER":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_CALL;
                        break;
                    case "WEB":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_WEB;
                        break;
                    case "CABINET":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_WEB;
                        break;
                    case "WORKER":
                        typeGroup = OrderNotificationManager.TYPE_GROUP_BORDER;
                        break;
                    default :
                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->There are no type notification for device: ${typeDevice}`);
                        break;
                }

                if (!typeGroup) {
                    return resolve(result);
                }
                statusId = this._getOrderStatusIdForNotificationTemplate(statusId);
                orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->Type group of order notification: ${typeGroup}`);

                sqlManager.getOrderNoticeType({
                    tenantId: tenantId,
                    positionId: positionId,
                    cityId: cityId,
                    statusId: statusId,
                    typeGroup: typeGroup,
                }, (err, notice) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->getOrderNoticeType->Error: ${err.message}`);
                        return resolve(result);
                    }

                    if (!notice) {
                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->There are no notice for status_id: ${statusId}`);
                        return resolve(result);
                    }

                    orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->Type notice of order notification: ${notice}`);
                    const noticeList = notice.split(',');
                    if (notice.length === 1 && notice[0] === OrderNotificationManager.NOTICE_TYPE_NOTHING) {
                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->There are no notice for status_id: ${statusId}`);
                        return resolve(result);
                    }
                    result.notify_client = true;


                    for (let notice of noticeList) {
                        switch (notice) {
                            case OrderNotificationManager.NOTICE_TYPE_SMS:
                                serviceApi.sendSmsNotificationToClient({orderId, tenantId, statusId})
                                    .then(result => {
                                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->serviceApi.sendSmsNotificationToClient->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(orderId, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->serviceApi.sendSmsNotificationToClient->Error: ${err.message}`);
                                    });
                                break;
                            case OrderNotificationManager.NOTICE_TYPE_SMS_PASSENGER:
                                if (!clientPassengerId) {
                                    break;
                                }
                                result.notify_client_passenger = true;
                                serviceApi.sendSmsNotificationToClientPassenger({orderId, tenantId, statusId})
                                    .then(result => {
                                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->serviceApi.sendSmsNotificationToClientPassenger->Result: ${result}`);
                                    })
                                    .catch(err => {
                                        orderLogger(orderId, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->serviceApi.sendSmsNotificationToClientPassenger->Error: ${err.message}`);
                                    });
                                break;
                            case OrderNotificationManager.NOTICE_TYPE_PUSH:
                                const visible = true;
                                pushApi.sendPushNotificationToClient({
                                        tenantId,
                                        orderId,
                                        positionId,
                                        cityId,
                                        deviceToken,
                                        typeDevice,
                                        appId,
                                        statusGroup,
                                        statusId,
                                        lang,
                                        visible
                                    },
                                    (err, result) => {
                                        if (err) {
                                            orderLogger(orderId, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->pushApi.sendPushNotificationToClient->Error: ${err.message}`);
                                        } else {
                                            orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->pushApi.sendPushNotificationToClient->Result: ${result}`);
                                        }
                                    });
                                break;
                            case OrderNotificationManager.NOTICE_TYPE_AUTOCALL:
                                phoneApi.sendCallNotification(orderId, tenantId, clientPhone, statusId, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->sendCallNotification->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->sendCallNotification->Result: ${result}`);
                                    }
                                });
                                break;
                            default :
                                orderLogger(orderId, 'info', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->getOrderNoticeType->There are no notice for status_id: ${statusId}`);
                                break;
                        }
                    }
                    return resolve(result);
                });
            } catch (err) {
                orderLogger(orderData.order_id, 'error', `orderNotificationManager->sendChangeOrderStatusNotificationToClient->Error: ${err.message}`);
                return reject(err);
            }
        })
    }

    /**
     * Send push notification about updating free orders list to all relevant worker
     * @param {Object} orderData
     * @param {Object} options
     * @param {Number} options.sendPushFreeOrderForAll  - 1/0
     * @param {Number|null} options.distanceToFilterFreeOrders
     * @param {Array|null} options.dislikeWorkerIds
     * @param {Array|null} options.exceptCarModels
     */
    sendUpdateFreeOrdersListToWorkers(orderData, options) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers called!`);
                orderLogger(orderData.order_id, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers-> 
                sendPushFreeOrderForAll: ${options.sendPushFreeOrderForAll}. distanceToFilterFreeOrders: ${options.distanceToFilterFreeOrders}`);
                const orderId = parseInt(orderData.order_id);
                const orderCityId = parseInt(orderData.city_id);
                const statusId = parseInt(orderData.status_id);
                const tenantId = parseInt(orderData.tenant_id);
                const orderPositionId = parseInt(orderData.tariff.position_id);
                const orderTariffClassId = parseInt(orderData.tariff.class_id);
                let soundOn = 0;
                if (statusInfo.free_order.status_id === statusId) {
                    soundOn = 1;
                }
                orderLogger(orderId, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers->soundOn: ${soundOn}`);
                let hasCar = positionManager.positionHasCar(orderPositionId);
                orderLogger(orderId, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers->hasCar: ${hasCar}`);
                redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
                    if (err) {
                        return reject(err);
                    }
                    const goodWorkersCallsignArr = [];
                    const workerOnlineList = [];
                    async.each(workers, (workerItem, workerCb) => {
                        if (workerItem && workerItem.worker && workerItem.worker.callsign) {
                            let workerCallsign = parseInt(workerItem.worker.callsign);
                            workerOnlineList.push(workerCallsign);
                            orderDistributionManager.isGoodWorkerForOrder(
                                orderData,
                                workerItem,
                                {
                                    needCalcDistance: true,
                                    distanceForSearch: options.distanceToFilterFreeOrders,
                                    needCheckSocket: false,
                                    needCheckStatus: true,
                                    dislikeWorkerIds: options.dislikeWorkerIds,
                                    exceptCarModels: options.exceptCarModels
                                })
                                .then(result => {
                                    goodWorkersCallsignArr.push(workerCallsign);
                                    return workerCb(null, 1)
                                })
                                .catch(err => {
                                    return workerCb(null, 1)
                                });
                        } else {
                            return workerCb(null, 1)
                        }
                    }, (err) => {
                        orderLogger(orderId, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers->goodWorkersCallsignArr: ${goodWorkersCallsignArr}`);
                        orderLogger(orderId, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers->workerOnlineList: ${workerOnlineList}`);
                        if (err) {
                            return reject(err);
                        } else {
                            if (options.sendPushFreeOrderForAll === 0) {
                                if (goodWorkersCallsignArr.length > 0) {
                                    const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                    pushApi.sendFreeOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushFreeOrderForAll, (err, result) => {
                                        if (err) {
                                            return reject(err);
                                        } else {
                                            return resolve(1);
                                        }
                                    })
                                } else {
                                    return resolve(1);
                                }
                            } else if (options.sendPushFreeOrderForAll === 1) {
                                if (soundOn === 1) {
                                    sqlManager.getGoodWorkerArrForOrder(tenantId, orderCityId, orderPositionId, orderTariffClassId, hasCar, (err, workers) => {
                                        if (err) {
                                            return reject(err);
                                        }
                                        if (!Array.isArray(workers)) {
                                            return resolve(1);
                                        }
                                        async.each(workers, (workerItem, workerCb) => {
                                            let workerCallsign = parseInt(workerItem.callsign);
                                            if (workerOnlineList.indexOf(workerCallsign) === -1) {
                                                goodWorkersCallsignArr.push(workerCallsign);
                                            }
                                            return workerCb(null, 1)
                                        }, (err) => {
                                            orderLogger(orderId, 'info', `orderNotificationManager->sendUpdateFreeOrdersListToWorkers->goodWorkersCallsignArr: ${goodWorkersCallsignArr}`);
                                            if (err) {
                                                return reject(err);
                                            }
                                            if (goodWorkersCallsignArr.length > 0) {
                                                const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                                pushApi.sendFreeOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushFreeOrderForAll, (err, result) => {
                                                    if (err) {
                                                        return reject(err);
                                                    } else {
                                                        return resolve(1);
                                                    }
                                                })
                                            } else {
                                                return resolve(1);
                                            }
                                        });
                                    })
                                } else {
                                    if (goodWorkersCallsignArr.length > 0) {
                                        const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                        pushApi.sendFreeOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushFreeOrderForAll, (err, result) => {
                                            if (err) {
                                                return reject(err);
                                            } else {
                                                return resolve(1);
                                            }
                                        })
                                    } else {
                                        return resolve(1);
                                    }
                                }

                            } else {
                                return resolve(1);
                            }

                        }
                    })
                })
            } catch (err) {
                return reject(err);
            }
        })
    }

    /**
     * Send push notification about updating pre orders list to all relevant worker
     * @param {object} orderData
     * @param {object} options
     * @param {number} options.sendPushPreOrderForAll - 1
     * @param {Array|null} options.dislikeWorkerIds
     * @param {Array|null} options.exceptCarModels
     * @returns {Promise}
     */
    sendUpdatePreOrdersListToWorkers(orderData, options) {
        return new Promise((resolve, reject) => {
            try {
                orderLogger(orderData.order_id, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers called!`);
                orderLogger(orderData.order_id, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers->sendPushPreOrderForAll: ${options.sendPushPreOrderForAll}`);
                const orderId = parseInt(orderData.order_id);
                const orderCityId = parseInt(orderData.city_id);
                const statusId = parseInt(orderData.status_id);
                const tenantId = parseInt(orderData.tenant_id);
                const orderPositionId = parseInt(orderData.tariff.position_id);
                const orderTariffClassId = parseInt(orderData.tariff.class_id);
                let soundOn = 0;
                if (statusInfo.new_pre_order.status_id === statusId ||
                    statusInfo.new_pre_order_no_parking.status_id === statusId) {
                    soundOn = 1;
                }
                orderLogger(orderId, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers->soundOn: ${soundOn}`);
                let hasCar = positionManager.positionHasCar(orderPositionId);
                orderLogger(orderId, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers->hasCar: ${hasCar}`);
                redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
                    if (err) {
                        return reject(err);
                    }
                    const goodWorkersCallsignArr = [];
                    const workerOnlineList = [];


                    async.each(workers, (workerItem, workerCb) => {
                        if (workerItem && workerItem.worker && workerItem.worker.callsign) {
                            let workerCallsign = parseInt(workerItem.worker.callsign);
                            workerOnlineList.push(workerCallsign);
                            orderDistributionManager.isGoodWorkerForOrder(
                                orderData,
                                workerItem,
                                {
                                    needCalcDistance: false,
                                    distanceForSearch: null,
                                    needCheckSocket: false,
                                    needCheckStatus: true,
                                    dislikeWorkerIds: options.dislikeWorkerIds,
                                    exceptCarModels: options.exceptCarModels
                                })
                                .then(result => {
                                    goodWorkersCallsignArr.push(workerCallsign);
                                    return workerCb(null, 1)
                                })
                                .catch(err => {
                                    return workerCb(null, 1)
                                });
                        } else {
                            return workerCb(null, 1)
                        }
                    }, (err) => {
                        orderLogger(orderId, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers->goodWorkersCallsignArr: ${goodWorkersCallsignArr}`);
                        orderLogger(orderId, 'info', `orderNotificationManager->sendUpdatePreOrdersListToWorkers->workerOnlineList: ${workerOnlineList}`);
                        if (err) {
                            return reject(err);
                        } else {

                            if (!options.sendPushPreOrderForAll) {
                                if (goodWorkersCallsignArr.length > 0) {
                                    const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                    pushApi.sendPreOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushPreOrderForAll, (err, result) => {
                                        if (err) {
                                            return reject(err);
                                        } else {
                                            return resolve(1);
                                        }
                                    })
                                } else {
                                    return resolve(1);
                                }
                            } else {
                                if (soundOn === 1) {
                                    sqlManager.getGoodWorkerArrForOrder(tenantId, orderCityId, orderPositionId, orderTariffClassId, hasCar, (err, workers) => {
                                        if (err) {
                                            return reject(err);
                                        }
                                        if (!Array.isArray(workers)) {
                                            return resolve(1);
                                        }
                                        async.each(workers, (workerItem, workerCb) => {
                                            let workerCallsign = parseInt(workerItem.callsign);
                                            if (workerOnlineList.indexOf(workerCallsign) === -1) {
                                                goodWorkersCallsignArr.push(workerCallsign);
                                            }
                                            return workerCb(null, 1)
                                        }, (err) => {
                                            if (err) {
                                                return reject(err);
                                            }
                                            orderLogger(orderId, 'info', `orderNotificationManager->goodWorkersCallsignArr: ${goodWorkersCallsignArr}`);
                                            if (goodWorkersCallsignArr.length > 0) {
                                                const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                                pushApi.sendPreOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushPreOrderForAll, (err, result) => {
                                                    if (err) {
                                                        return reject(err);
                                                    } else {
                                                        return resolve(1);
                                                    }
                                                })
                                            } else {
                                                return resolve(1);
                                            }
                                        });
                                    })
                                } else {
                                    if (goodWorkersCallsignArr.length > 0) {
                                        const goodWorkersCallsignString = goodWorkersCallsignArr.join(',');
                                        pushApi.sendPreOrderForAll(tenantId, orderId, goodWorkersCallsignString, soundOn, options.sendPushPreOrderForAll, (err, result) => {
                                            if (err) {
                                                return reject(err);
                                            } else {
                                                return resolve(1);
                                            }
                                        })
                                    } else {
                                        return resolve(1);
                                    }
                                }
                            }
                        }

                    })
                });
            } catch (err) {
                return reject(err);
            }
        })
    }

    /**
     * Get order status id for notification template
     * @param {Number} orderStatusId
     * @returns {Number} orderStatusIdFixed
     * @private
     */
    _getOrderStatusIdForNotificationTemplate(orderStatusId) {
        let orderStatusIdFixed;
        switch (orderStatusId) {
            case statusInfo.new_order_no_parking.status_id:
                orderStatusIdFixed = statusInfo.new_order.status_id;
                break;
            case statusInfo.new_pre_order_no_parking.status_id:
                orderStatusIdFixed = statusInfo.new_pre_order.status_id;
                break;
            case statusInfo.pre_order_executing.status_id:
                orderStatusIdFixed = statusInfo.worker_accepted_order.status_id;
                break;
            default:
                orderStatusIdFixed = orderStatusId;
        }
        return orderStatusIdFixed;
    }

}

module.exports = exports = new OrderNotificationManager();
