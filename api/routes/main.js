'use strict';
const express = require('express');
const router = express.Router();
const check = require('check-types');
const lib = require("../../services/lib");
const config = lib.getConfig();
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const orderMainHandler = require('../../orderHandlers/orderMainHandler');
const workerMainHandler = require('../../workerShiftHandlers/workerMainHandler');

const watchWorkerShift = require('../../workerShiftHandlers/watchWorkerShift');
const closeWorkerShift = require('../../workerShiftHandlers/closeWorkerShift');
const blockWorkerOrder = require('../../workerBlockHandlers/blockWorkerOrder');
const unblockWorkerOrder = require('../../workerBlockHandlers/unblockWorkerOrder');
const blockWorkerPreOrder = require('../../workerBlockHandlers/blockWorkerPreOrder');
const unblockWorkerPreOrder = require('../../workerBlockHandlers/unblockWorkerPreOrder');
const unblockWorkerOrderTimer = require('../../workerBlockHandlers/unblockWorkerOrderTimer');
const unblockWorkerPreOrderTimer = require('../../workerBlockHandlers/unblockWorkerPreOrderTimer');
const orderDistributionManager = require('../../orderHandlers/orderModules/common/orderDistributionManager');
const sendRejectOrderToWorker = require('../../supportCommand/sendRejectOrderToWorker');
const changeWorkerStatus = require('../../supportCommand/changeWorkerStatus');
const serviceInfo = require('../../services/serviceInfo');
const DataProcessingService = require('../../services/dataProcessingService');
const Metric = require('../../services/Metric');



router.use(function (req, res, next) {
    mainLogger('info', 'Called method: ' + req.method + ' ' + req.url);
    mainLogger('info', 'Query params: ' + JSON.stringify(req.query));
    next();
});

/**
 * Add new order to service
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.order_id
 * @param req.query.is_from_loader
 */
router.get('/neworder_auto', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let orderId = parseInt(req.query.order_id);
    let isFromLoader = parseInt(req.query.is_from_loader);

    if (!check.integer(isFromLoader)) {
        isFromLoader = 0;
    }
    if (!check.integer(tenantId) || !check.integer(orderId) || !check.integer(isFromLoader)) {
        lib.badRequestApiAnswer(res);
    } else {
        orderMainHandler(tenantId, orderId, isFromLoader, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});

/**
 * Creating timer to watching for worker shift
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.tenant_login
 * @param req.query.worker_callsign
 * @param req.query.shift_life_time
 * @param req.query.shift_id
 */
router.get('/watch_worker_shift', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let tenantLogin = req.query.tenant_login;
    let workerCallsign = parseInt(req.query.worker_callsign);
    let shiftLifeTime = parseInt(req.query.shift_life_time);
    let shiftId = parseInt(req.query.shift_id);
    if (!check.integer(tenantId) || !check.string(tenantLogin) || !check.integer(workerCallsign) || !check.integer(shiftLifeTime) || !check.integer(shiftId)) {
        lib.badRequestApiAnswer(res);
    } else {
        workerMainHandler(tenantId, tenantLogin, workerCallsign, shiftLifeTime, shiftId, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });

       /* watchWorkerShift(tenantId, tenantLogin, workerCallsign, shiftLifeTime, shiftId, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });*/
    }

});


/**
 * Closing worker shift
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.tenant_login
 * @param req.query.worker_callsign
 * @param req.query.shift_id
 * @param req.query.shift_end_reason
 * @param req.query.shift_end_event_sender_type
 * @param req.query.shift_end_event_sender_id
 */
router.get('/close_worker_shift', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let tenantLogin = req.query.tenant_login;
    let workerCallsign = parseInt(req.query.worker_callsign);
    let shiftId = parseInt(req.query.shift_id);
    let shiftEndReason = req.query.shift_end_reason ? req.query.shift_end_reason : null;
    let shiftEndEventSenderType = req.query.shift_end_event_sender_type ? req.query.shift_end_event_sender_type : null;
    let shiftEndEventSenderId = req.query.shift_end_event_sender_id ? req.query.shift_end_event_sender_id : null;
    if (!check.integer(tenantId) || !check.string(tenantLogin) || !check.integer(workerCallsign) || !check.integer(shiftId)) {
        lib.badRequestApiAnswer(res);
    } else {
        closeWorkerShift(tenantId, tenantLogin, workerCallsign, shiftId, shiftEndReason, shiftEndEventSenderType, shiftEndEventSenderId, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});

/**
 * Block worker order
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 */
router.get('/block_worker_order', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    if (!check.integer(tenantId) || !check.integer(workerCallsign)) {
        lib.badRequestApiAnswer(res);
    } else {
        blockWorkerOrder(tenantId, workerCallsign, null, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});


/**
 * Unblock worker order
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 * @param req.query.block_id
 */
router.get('/unblock_worker_order', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    let blockId = parseInt(req.query.block_id);
    if (!check.integer(tenantId) || !check.integer(workerCallsign) || !check.integer(blockId)) {
        lib.badRequestApiAnswer(res);
    } else {
        unblockWorkerOrder(tenantId, workerCallsign, blockId, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }

});


/**
 * Block worker preorder
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 */
router.get('/block_worker_preorder', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    if (!check.integer(tenantId) || !check.integer(workerCallsign)) {
        lib.badRequestApiAnswer(res);
    } else {
        blockWorkerPreOrder(tenantId, workerCallsign, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});


/**
 * Unblock worker preorder
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 * @param req.query.block_id
 */
router.get('/unblock_worker_preorder', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    let blockId = parseInt(req.query.block_id);
    if (!check.integer(tenantId) || !check.integer(workerCallsign) || !check.integer(blockId)) {
        lib.badRequestApiAnswer(res);
    } else {
        unblockWorkerPreOrder(tenantId, workerCallsign, blockId, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});

/**
 * Create timer to unblock worker order
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 * @param req.query.block_id
 * @param req.query.shift_id
 * @param req.query.time_to_unblock
 */
router.get('/watch_block_order_timer', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    let shiftId = parseInt(req.query.shift_id);
    let blockId = parseInt(req.query.block_id);
    let timeToUnblock = parseInt(req.query.time_to_unblock);
    if (!check.integer(tenantId) || !check.integer(workerCallsign) || !check.integer(blockId) || !check.integer(timeToUnblock) || !check.integer(shiftId)) {
        lib.badRequestApiAnswer(res);
    } else {
        unblockWorkerOrderTimer(tenantId, workerCallsign, shiftId, blockId, timeToUnblock, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});

/**
 * Create timer to unblock worker preorder
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 * @param req.query.block_id
 * @param req.query.shift_id
 * @param req.query.time_to_unblock
 */
router.get('/watch_block_preorder_timer', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    let shiftId = parseInt(req.query.shift_id);
    let blockId = parseInt(req.query.block_id);
    let timeToUnblock = parseInt(req.query.time_to_unblock);
    if (!check.integer(tenantId) || !check.integer(workerCallsign) || !check.integer(blockId) || !check.integer(timeToUnblock) || !check.integer(shiftId)) {
        lib.badRequestApiAnswer(res);
    } else {
        unblockWorkerPreOrderTimer(tenantId, workerCallsign, shiftId, blockId, timeToUnblock, (err, result) => {
            lib.defaultApiAnswer(res, err);
        });
    }
});


/**
 * Is worker good for this order
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_id
 * @param req.query.worker_callsign
 * @param req.query.order_id
 */
router.get('/can_offer_order_for_worker', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantId = parseInt(req.query.tenant_id);
    let workerCallsign = parseInt(req.query.worker_callsign);
    let orderId = parseInt(req.query.order_id);
    let positionId = parseInt(req.query.position_id);
    let orderCity = parseInt(req.query.order_city);
    if (!check.integer(tenantId) || !check.integer(workerCallsign) || !check.integer(orderId)) {
        lib.badRequestApiAnswer(res);
    } else {
        orderDistributionManager.canOfferOrderForWorker(tenantId, orderId, workerCallsign, positionId,  orderCity)
            .then(result => {
                lib.defaultApiAnswer(res, null);
            })
            .catch(err => {
                lib.defaultApiAnswer(res, err);
            });
    }
});


/**
 * Get relevant workers callsign list to offer order
 * @param {Object} req
 * @param {Number} req.query.tenant_id
 * @param {Number} req.query.order_id
 * @param {Object} res
 * @param {function} next
 */
router.get('/get_relevant_workers', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let orderId = parseInt(req.query.order_id);
    let tenantId = parseInt(req.query.tenant_id);
    if (!check.integer(orderId) || !check.integer(tenantId)) {
        lib.badRequestApiAnswer(res);
    } else {
        orderDistributionManager.getRelevantWorkersList(tenantId, orderId)
            .then(list => {
                let responseData = {
                    "info": "ok",
                    "result": {
                        'workers': list
                    }
                };
                responseData = JSON.stringify(responseData);
                res.write(responseData);
                res.end();
            })
            .catch(err => {
                lib.defaultApiAnswer(res, err);
            })

    }

});


/**
 * Support command: send_reject_order_to_worker
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_domain
 * @param req.query.worker_callsign
 * @param req.query.order_number
 */
router.get('/send_reject_order_to_worker', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantDomain = req.query.tenant_domain;
    let workerCallsign = parseInt(req.query.worker_callsign);
    let orderNumber = parseInt(req.query.order_number);
    if (!check.string(tenantDomain) || !check.integer(workerCallsign) || !check.integer(orderNumber)) {
        lib.badRequestApiAnswer(res);
    } else {
        sendRejectOrderToWorker(tenantDomain, workerCallsign, orderNumber, (err, result) => {
            lib.defaultApiAnswer(res, err);
        })
    }
});

/**
 * Support command: change_worker_status
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 * @param req.query.tenant_domain
 * @param req.query.worker_callsign
 * @param req.query.worker_status
 */
router.get('/change_worker_status', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let tenantDomain = req.query.tenant_domain;
    let workerCallsign = parseInt(req.query.worker_callsign);
    let workerStatus = req.query.worker_status;
    if (!check.string(tenantDomain) || !check.integer(workerCallsign) || !check.string(workerStatus)) {
        lib.badRequestApiAnswer(res);
    } else {
        changeWorkerStatus(tenantDomain, workerCallsign, workerStatus)
            .then(result => {
                lib.defaultApiAnswer(res, null);
            })
            .catch(err => {
                lib.defaultApiAnswer(res, err);
            });

    }
});


/**
 * Ping
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/ping', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let responseData = {
        "result": "ok",
        "server_info": {
            MY_BIND_HOST: config.MY_BIND_HOST,
            MY_BIND_PORT: config.MY_BIND_PORT,
            MY_ID: config.MY_ID,
            MY_ENV: config.MY_ENV,
            MY_DEBUG: config.MY_DEBUG,

        }
    };
    responseData = JSON.stringify(responseData);
    res.write(responseData);
    res.end();
});

/**
 * Processed orders at this node
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/orders', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let responseData = {
        "info": "ok",
        "result": {
            "orders_count": DataProcessingService.activeOrdersListLength,
            "orders_ids": DataProcessingService.activeOrdersList,
        }
    };
    responseData = JSON.stringify(responseData);
    res.write(responseData);
    res.end();
});

/**
 * Processed worker shift at this node
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/worker_shifts', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let responseData = {
        "info": "ok",
        "result": {
            "worker_shifts_count": DataProcessingService.activeWorkerShiftsListLength,
            "worker_shifts_ids": DataProcessingService.activeWorkerShiftsList,
        }
    };
    responseData = JSON.stringify(responseData);
    res.write(responseData);
    res.end();
});


/**
 * processed worker blocks at this node
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/worker_blocks', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "application/json"});
    let responseData = {
        "info": "ok",
        "result": {
            "worker_blocks_count": DataProcessingService.activeWorkerBlocksListLength,
            "worker_blocks_ids": DataProcessingService.activeWorkerBlocksList,
        }
    };
    responseData = JSON.stringify(responseData);
    res.write(responseData);
    res.end();
});

router.get('/metrics', (req, res, next) => {
    res.writeHead(200, {"Content-Type": "text/plain"});
    const format = req.query.format;
    const metric = Metric.getMetric(format);
    res.write(metric);
    res.end();
});


/**
 * Get service version
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/version', serviceInfo.version);

/**
 * Get service  status
 * @param {Object} req
 * @param {Object} res
 * @param {function} next
 */
router.get('/status', serviceInfo.status);



module.exports = router;
