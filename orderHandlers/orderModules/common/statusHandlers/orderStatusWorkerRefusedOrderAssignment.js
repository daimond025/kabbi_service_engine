'use strict';
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderType = require("../../../orderEvent/orderDevice");
const redisHospitalWorkerOrderManager = require('../../../../database/redisDB/redisHospitalWorkerOrderManager');


function setStatusHospitalOrder(newStatusId, companyID){
    let newStatus = newStatusId;
    if(companyID != null){
        newStatus = statusInfo.hospital_free_order_company.status_id;
    }
    else{
        newStatus = statusInfo.hospital_free_order.status_id;
    }
    return newStatus;
}
/**
 * Order status worker refused order assigned
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {String} eventSenderService
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, eventSenderService) => {
    return new Promise((resolve, reject) => {
        try {
            if (orderInstance.statusChanged) {
                const orderId = orderData.order_id;
                const tenantId = orderData.tenant_id;
                orderLogger(orderId, 'info', 'OrderStatusWorkerRefusedOrderAssignment CALLED!');
                orderInstance.orderTimer.clearAll();
                const isWorkerEventOwner = eventSenderService === orderEventSenderService.workerService;

                let newStatusId = statusInfo.free_order.status_id;
                const typeOrder = orderData.device.toString();

                if((typeOrder == orderType.HOSPITAL)){

                    let companyID = null;
                    if((typeof orderData.worker_company == "object") && ( orderData.worker_company != null) && (typeof orderData.worker_company.tenant_company_id != "undefined")){
                        companyID = parseInt(orderData.worker_company.tenant_company_id );
                    }
                    newStatusId = setStatusHospitalOrder(newStatusId, companyID);

                    const workerCallsign = (orderData.worker && orderData.worker.callsign) ? parseInt(orderData.worker.callsign) : null;

                    if(workerCallsign){
                        redisHospitalWorkerOrderManager.delOrder(tenantId, workerCallsign, orderId, (err, result) => {
                            if(err){
                                orderLogger(orderId, 'error', `OrderStatusWorkerRefusedOrderAssignment->redisHospitalWorkerOrderManager->delOrder->Error: ${err.message}`);
                            }

                            orderLogger(orderId, 'info', `OrderStatusWorkerRefusedOrderAssignment->redisHospitalWorkerOrderManager->delOrder->Result: ${result}`);
                        });
                    }
                }

                orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                    isPreOrder: false,
                    isWorkerEventOwner,
                    newStatusId
                });
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    });
};
