'use strict';

const redisOrderManager = require('../../../database/redisDB/redisOrderManager');
const redisWorkerManager = require('../../../database/redisDB/redisWorkerManager');
const sqlManager = require('../../../database/sqlDB/sqlManager');
const config = require('../../../services/lib').getConfig();
const nodeId = config.MY_ID;

class OrderSettingsManager {
    constructor() {
        this.settings = [
            //1 - distribute order distance, 2- for parking
            'TYPE_OF_DISTRIBUTION_ORDER',
            // How long offer order to worker, at seconds
            'ORDER_OFFER_SEC',
            //queue - offer order in rotation one by one, batch - offer order to many workers at same time
            'OFFER_ORDER_TYPE',
            // if OFFER_ORDER_TYPE=batch, this setting show to how many workers need offer order
            'OFFER_ORDER_WORKER_BATCH_COUNT',
            //How many circles repeat in parking distribution process
            'CIRCLES_DISTRIBUTION_ORDER',
            //Check worker rating on distributing process(1/0)
            'CHECK_WORKER_RATING_TO_OFFER_ORDER',
            //Delay between distribution circles, in seconds
            'DELAY_CIRCLES_DISTRIBUTION_ORDER',
            //Set free status(1) or new(0) between cycles
            'FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES',
            //After how many seconds reject order, if there no workers
            'ORDER_REJECT_TIME',
            //Send push about free order to all workers(1) or only for workers at shift(0)
            'SEND_PUSH_FREE_ORDER_FOR_ALL',
            //distance to filter free orders, at metres
            'DISTANCE_TO_FILTER_FREE_ORDERS',
            //Send push about pre order to all workers(1) or only for workers at shift(0)
            'SEND_PUSH_PRE_ORDER_FOR_ALL',
            //Serialized array with info about when send notification to workers about his pre orders
            'WORKER_PRE_ORDER_REMINDER',
            //if pre order not taked before X min to order, send it to work
            'PRE_ORDER_WORKING',
            //Distance for search workers to distribute in 1 circle, in metres
            'CIRCLE_DISTANCE_1',
            //Count of repeats for search workers in 1 circle
            'CIRCLE_REPEAT_1',
            //Distance for search workers to distribute in 2 circle, in metres
            'CIRCLE_DISTANCE_2',
            //Count of repeats for search workers in 2 circle
            'CIRCLE_REPEAT_2',
            //Distance for search workers to distribute in 3 circle, in metres
            'CIRCLE_DISTANCE_3',
            //Count of repeats for search workers in 3 circle
            'CIRCLE_REPEAT_3',
            //Distance for search workers to distribute in 4 circle, in metres
            'CIRCLE_DISTANCE_4',
            //Count of repeats for search workers in 4 circle
            'CIRCLE_REPEAT_4',
            //Distance for search workers to distribute in 5 circle, in metres
            'CIRCLE_DISTANCE_5',
            //Count of repeats for search workers in 5 circle
            'CIRCLE_REPEAT_5',
            //restrict visibility of pre order by time
            'RESTRICT_VISIBILITY_OF_PRE_ORDER',
            //time of pre order visibility at hours
            'TIME_OF_PRE_ORDER_VISIBILITY',
            //automate send order to exchange
            'EXCHANGE_ENABLE_SALE_ORDERS',
            //after how many seconds send order to exchange after create order or sending pre order to work
            'EXCHANGE_HOW_WAIT_WORKER',
            //order form  - enable point phone field
            'ORDER_FORM_ADD_POINT_PHONE',
            //order form  - enable point comment field
            'ORDER_FORM_ADD_POINT_COMMENT',
            //notify point recipients by sms on order executing
            'SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING',
            //require point confirmation code
            'REQUIRE_POINT_CONFIRMATION_CODE',

            //require calculate time from order to worker
            'WORKER_TIME_CALCULATE',

            //  step of time  from order to worker
            'WORKER_TIME_CALCULATE_STEP',

            // count step of time  from order to worker
            'WORKER_TIME_CALCULATE_STEP_COUNT',

            // time for permit begin pre-order
            'START_TIME_BY_ORDER',

            // timer show offer to worker free order
            'FREE_ORDER_OFFER_SEC',

            // timer show offer to worker pre order
            'PRE_ORDER_OFFER_SEC',

            // timer to assign worker to order
            'NEW_ORDER_TIME_ASSIGN'

            // allow to sound text test order
          /*  TODO пока вводить параметр не нужно
           'WORKER_ORDER_SOUND',

            //TODO группа памаетров для преложения свободных заказов
            'OFFER_FREE_ORDER',  // вкл/выкл преложение свбодных заказов
            'OFFER_FREE_ORDER_DISTANCE',   // растояние от водитедтеля до заказа
            'OFFER_FREE_ORDER_COUNT',  // количество предложений - в цикле водителям
            'OFFER_FREE_ORDER_TIME_BETWEEN',  // время между предложениями
            'OFFER_FREE_ORDER_WORKER_ON_ORDER',  // предлагать водителя котоыре на заказе
            'OFFER_FREE_ORDER_WORKER_ON_BREAK',  // предлагать водителя котоыре на паузе
            */
        ];


        this.settings_workers = [
            'NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION',
            'WORKER_PARKING_POSITION_CHECK_DELAY_TIME',
            'WORKER_PARKING_POSITION_CHECK_TYPE',

            'NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME',
            'WORKER_POSITION_CHECK_DELAY_TIME',

        ];

    }

    /**
     * Get order settings key
     * @returns {Array}
     */
    get settingsKeys() {
        return this.settings;
    }


    /**
     * Get worker settings key
     * @returns {Array}
     */
    get settingsWorkerKeys() {
        return this.settings_workers;
    }

    /**
     * Get settings from db and persist to active order(redis)
     * @param tenantId
     * @param orderId
     * @returns {Promise}
     */
    persistSettings(tenantId, orderId) {
        return new Promise((resolve, reject) => {
            redisOrderManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    reject(err);
                } else {
                    const cityId = orderItem.city_id;
                    const positionId = orderItem.tariff.position_id;
                    sqlManager.getTenantSettingList(tenantId, cityId, positionId, this.settingsKeys, (err, settings) => {
                        if (err) {
                            reject(err);
                        } else {
                            let formattedSettings = {};
                            for (let key of this.settingsKeys) {
                                let keyIn = false;
                                for (let setting of settings) {
                                    if (setting.name === key) {
                                        keyIn = true;
                                        formattedSettings[key] = /^\d+$/.test(setting.value) ? parseInt(setting.value) : setting.value;
                                    }
                                }
                                if (!keyIn) {
                                    formattedSettings[key] = null;
                                }
                            }
                            orderItem.settings = formattedSettings;

                            redisOrderManager.saveOrder(orderItem, (err, result) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(formattedSettings);
                                }
                            });
                        }
                    })
                }
            });
        });
    }

    /**
     * Get settings from db and persist to worker (redis)
     * @param tenantId
     * @param workerCallsign
     * @returns {Promise}
     */
    persistWorkerSettings(tenantId, workerCallsign) {
        return new Promise((resolve, reject) => {
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                if (err) {
                    reject(err);
                } else {
                    const cityId = workerData.worker.city_id;
                    const positionId = workerData.position.position_id;
                    sqlManager.getTenantSettingList(tenantId, cityId, positionId, this.settingsWorkerKeys, (err, settings) => {
                        if (err) {
                            reject(err);
                        } else {
                            let formattedSettings = {};
                            for (let key of this.settingsWorkerKeys) {
                                let keyIn = false;
                                for (let setting of settings) {
                                    if (setting.name === key) {
                                        keyIn = true;
                                        formattedSettings[key] = /^\d+$/.test(setting.value) ? parseInt(setting.value) : setting.value;
                                    }
                                }
                                if (!keyIn) {
                                    formattedSettings[key] = null;
                                }
                            }
                            workerData.settings = formattedSettings;

                            redisWorkerManager.saveWorker(workerData, (err, result) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(formattedSettings);
                                }
                            });
                        }
                    })
                }
            });
        });
    }


    /**
     * Get active order settings
     * @param {Number} tenantId
     * @param {Number} orderId
     * @returns {Promise}
     */
    getActiveOrderSettings(tenantId, orderId) {
        return new Promise((resolve, reject) => {
            redisOrderManager.getOrder(tenantId, orderId, (err, order) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(order.settings);
                }
            });
        });
    }

    /**
     * Set system settings, like process_id and server_id to active order(redis)
     * @param tenantId
     * @param orderId
     * @returns {Promise}
     */
    setSystemSettings(tenantId, orderId) {
        return new Promise((resolve, reject) => {
            redisOrderManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    reject(err);
                } else {
                    orderItem.server_id = nodeId;
                    orderItem.process_id = process.pid;
                    redisOrderManager.saveOrder(orderItem, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }


    /**
     * Set system settings, like process_id and server_id to active worker(redis)
     * @param tenantId
     * @param workerCallsign
     * @returns {Promise}
     */
    setWorkerSystemSettings(tenantId, workerCallsign) {
        return new Promise((resolve, reject) => {
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                if (err) {
                    reject(err);
                } else {
                    workerData.server_id = nodeId;
                    workerData.process_id = process.pid;
                    redisWorkerManager.saveWorker(workerData, (err, result) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    }
}

module.exports = exports = new OrderSettingsManager();