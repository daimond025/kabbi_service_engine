'use strict';

const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const sqlManager = require("../../../../database/sqlDB/sqlManager");
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const preOrderWatcher = require('../../common/preOrderWatcher');

/**
 * Order status free hospital order for all
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {*}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'preOrderStatusHospitalForCompany called!');
            orderInstance.orderTimer.clearAll();
            preOrderWatcher.runWatcher(orderInstance, orderData);
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};

