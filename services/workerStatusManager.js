'use strict';

module.exports = {
    free: 'FREE',
    offerOrder: 'OFFER_ORDER',
    onOrder: 'ON_ORDER',
    blocked: 'BLOCKED',
    onBreak: 'ON_BREAK',
    refuseOrder: 'REFUSE_ORDER',
    removeFromOrder: 'REMOVE_FROM_ORDER',
};