'use strict';

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderActionManager = require('../orderActionManager');
const orderDistributionManager = require('../orderDistributionManager');

/**
 * Order status new order
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusNew called!');

            orderInstance.currentDistributionCircle = 1;
            orderInstance.currentDistributionCircleRepeat = 1;
            orderInstance.offeredInCircleRepeatWorkersList.clear();
            orderInstance.offeredInCircleWorkersList.clear();
            orderInstance.offeredInAllTimeWorkersList.clear();
            orderInstance.orderTimer.clearAll();
            orderInstance.workerLateTimestamp = null;
            orderInstance.startWaitingForPaymentTimestamp = null;


            orderActionManager.rejectOrderByTimeout(orderInstance, orderData)
                .then(result => {
                    orderLogger(orderInstance.orderId, 'info', `orderStatusNew->orderActionManager.rejectOrderByTimeout->Result: ${result}`);
                })
                .catch(err => {
                    orderLogger(orderInstance.orderId, 'info', `orderStatusNew->orderActionManager.rejectOrderByTimeout->Error: ${err.message}`);
                });
            if (orderInstance.statusChanged) {
                if (orderInstance.newStatusAlreadyWas === false) {
                    orderInstance.newStatusAlreadyWas = true;
                }

                if (orderData.worker_id && orderData.worker && orderData.worker.callsign) {
                    orderDistributionManager.runPersonalOffer(orderInstance, orderData, orderData.worker.callsign);
                } else {
                    orderDistributionManager.runDistributionProcess(orderInstance, orderData);
                }
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};