const redisOrderManager = require('../database/redisDB/redisOrderManager');
const redisWorkerManager = require('../database/redisDB/redisWorkerManager');
const sqlManager = require('../database/sqlDB/sqlManager');
const logger = require('../services/logger');
const orderLogger = logger.mainLogger;
const async = require('async');
const workerStatusManager = require("../services/workerStatusManager");
const redisWorkerReservedOrderListManager = require("../database/redisDB/redisWorkerReservedOrderListManager");
const orderEventWorkerMessage = require('../orderHandlers/orderEvent/orderEventWorkerMessage');
const orderEventWorkerCommand = require('../orderHandlers/orderEvent/orderEventWorkerCommand');
const amqpWorkerMessageSender = require("../services/amqpWorkerMessageSender");

/**
 *  Force change worker status
 * @param {String} tenantDomain
 * @param {Number} workerCallsign
 * @param {Number} workerStatus
 * @returns {Promise}
 */
module.exports = function (tenantDomain, workerCallsign, workerStatus) {
    return new Promise((resolve, reject)=> {
        workerStatus = workerStatus.toUpperCase();
        const workerStatusArr = ['FREE', 'ON_ORDER', 'BLOCKED', 'ON_BREAK'];
        if (workerStatusArr.indexOf(workerStatus) === -1) {
            reject('INVALID worker_status');
        }
        sqlManager.getTenantIdByDomain(tenantDomain, (err, tenantId)=> {
            if (err) {
                orderLogger('error', err);
                reject(err);
            }
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, worker)=> {
                if (err) {
                    if (err === "No worker at redisDB") {
                        err = "Сменить статус не удалось: исполнитель не на смене";
                    }
                    reject(err);
                }
                if (worker && worker.worker && worker.worker.status) {
                    let active_orders_real = [];
                    let active_orders_worker = typeof worker.worker.active_orders === "object" ?
                        Object.keys(worker.worker.active_orders).map(key => parseInt(worker.worker.active_orders[key])): [];


                    async.each(active_orders_worker, (order_id, callback) => {
                        let orderId = parseInt(order_id);
                        redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
                            if (err) {
                                // удаление заказа из связанных структур
                               redisWorkerReservedOrderListManager.delOrder(tenantId, workerCallsign, orderId, (err, result_save) => {
                                    if (err) {
                                        orderLogger(orderId, 'error', `changeWorkerStatus->redisWorkerReservedOrderListManager.delOrder->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `changeWorkerStatus->redisWorkerReservedOrderListManager.delOrder->result_save:${result_save}`);
                                    }
                                });

                                let messageString = orderEventWorkerMessage({
                                    command: orderEventWorkerCommand.rejectWorkerFromOrder,
                                    tenant_id: tenantId,
                                    tenant_login: tenantDomain,
                                    worker_callsign: workerCallsign,
                                    params: {
                                        order_id: orderId
                                    }
                                });
                                const messageTtl = 60 * 60 * 24 * 10; // 10 days
                                amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTtl, (err, result) => {
                                    if (err) {
                                        orderLogger(orderId, 'info', `changeWorkerStatus->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                                    } else {
                                        orderLogger(orderId, 'info', `changeWorkerStatus->amqpWorkerMessageSender.sendMessage->published command: ${orderEventWorkerCommand.rejectWorkerFromOrder} to worker ${workerCallsign}.Result: ${result}`);
                                    }
                                });
                            }else{
                                if (orderData && orderData.worker && orderData.worker.callsign) {
                                    active_orders_real.push(orderId);
                                }
                            }
                            return callback(null, 1);
                        });

                    }, (err) => {
                        if (err) {
                            reject(err);
                        }else{
                            worker.worker.active_orders = active_orders_real;
                            //orderLogger(orderId, 'info', `changeWorkerStatus->active_orders ${active_orders_real.join(',')} in workerCallsign: ${workerCallsign}`);

                            if(active_orders_real.length > 0 ){
                                worker.worker.status = workerStatusManager.onOrder;
                            }else{
                                worker.worker.status = workerStatus;
                            }
                            redisWorkerManager.saveWorker(worker, (err, result_save)=> {
                                if (err) {
                                    orderLogger(orderId, 'error', `changeWorkerStatus->saveWorkerRedis->Error: ${err.message}`);
                                    reject(err);
                                } else {
                                    resolve(1);
                                }
                            });
                        }

                    });
                } else {
                    reject("INVALID worker data at redis DB")
                }
            });
        })
    });
};