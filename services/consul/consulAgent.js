'use strict';

const config = require("../lib").getConfig();
const randomItem = require("../lib").randomItem;

const consul = require("consul")({
    host: config.CONSUL_MAIN_HOST,
    port: parseInt(config.CONSUL_MAIN_PORT),
    promisify: true
});

const getAgentInfo = () => {
    return new Promise((resolve, reject) => {
        consul.agent.self()
            .then(info => {
                return resolve(info);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

const getHealthOfServicesByName = serviceName => {
    return new Promise((resolve, reject) => {
        consul.health.service(serviceName)
            .then(services => {
                return resolve(services);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

const getHealthyServicesLocation = serviceName => {
    return new Promise((resolve, reject) => {
        const listofHealthyServicesLocation = [];
        getHealthOfServicesByName(serviceName)
            .then(services => {
                for (let service of services) {
                    const checksCount = service.Checks.length;
                    let checksGoodCount = 0;

                    for (let check of service.Checks) {
                        if (check.Status === "passing") {
                            checksGoodCount++;
                        }
                    }
                    if (checksGoodCount === checksCount) {
                        listofHealthyServicesLocation.push({
                            host: service.Service.Address,
                            port: service.Service.Port
                        })
                    }
                }
                resolve(listofHealthyServicesLocation);
            })
            .catch(err => {
                return reject(err);
            })
    });
};

const getRandomHealthyServiceLocation = serviceName => {
    return new Promise((resolve, reject) => {
        getHealthyServicesLocation(serviceName)
            .then(serviceLocations => {
                let serviceLocation = randomItem(serviceLocations);
                if (serviceLocation && serviceLocation.host) {
                    return resolve(serviceLocation);
                }
                return reject(new Error(`There no healthy services with name:${serviceName}`));
            })
            .catch(err => {
                return reject(err);
            })
    });
};


const getAllActiveServiceLocations = serviceName => {
    return new Promise((resolve, reject) => {
        consul.catalog.service.nodes(serviceName)
            .then(nodes => {
                const activeServiceAddresses = [];
                for (let node of nodes) {
                    activeServiceAddresses.push(
                        {
                            host: node.Address,
                            port: node.ServicePort
                        }
                    )
                }
                resolve(activeServiceAddresses);
            })
            .catch(err => {
                return reject(err);
            });
    });
};

/**
 * Get random active service location
 * @param serviceName
 * @return {Promise}
 */
const getRandomActiveServiceLocation = serviceName => {
    return new Promise((resolve, reject) => {
        this.getAllActiveServiceLocations(serviceName)
            .then(serviceLocations => {
                let service = randomItem(serviceLocations);
                return resolve(service);
            })
            .catch(err => {
                return reject(err);
            })
    });
};


module.exports = {
    consul,
    getAgentInfo,
    getAllActiveServiceLocations,
    getRandomActiveServiceLocation,
    getHealthOfServicesByName,
    getHealthyServicesLocation,
    getRandomHealthyServiceLocation

};