"use strict";

const request = require('request');
const config = require("../lib").getConfig();

/**
 * Send call notification to client
 * @param  {Number} orderId
 * @param  {Number} tenantId
 * @param  {String} phone
 * @param  {Number} statusId
 * @param  {Function} callback
 */
function sendCallNotificationToClient(orderId, tenantId, phone, statusId, callback) {


    const getString = "order_id=" + orderId + "&phone=" + phone + "&status_id=" + statusId + "&tenant_id=" + tenantId;
    const requesOptions = {
        url: `${config.API_PHONE_URL}call/notify_client?${getString}`
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requesOptions, resultData);

}

/**
 * Send call notification to с
 * @param  {Number} orderId
 * @param  {Number} tenantId
 * @param  {String} company_id
 * @param  {String} name
 * @param  {String} phone
 * @param  {String} user_id
 * @param  {Function} callback
 */
function sendCallNotificationToCompany(orderId, tenantId, company_id, callback) {

    const getString = "order_id=" + orderId +
        "&tenant_id=" + tenantId +
        "&company_id=" + company_id ;


    const requesOptions = {
        url: `${config.API_PHONE_URL}phone/notify_company?${getString}`
    };

    function resultData(error, response, body) {
        if (!error && parseInt(response.statusCode) === 200) {
            try {
                const info = JSON.parse(body);
                if (parseInt(info.result) === 1) {
                    return callback(null, 1);
                } else {
                    return callback(null, 0);
                }
            } catch (err) {
                return callback(err);
            }

        } else {
            return callback(error);
        }
    }

    request(requesOptions, resultData);

}

exports.sendCallNotification = sendCallNotificationToClient;
exports.sendCallNotificationCompany = sendCallNotificationToCompany;