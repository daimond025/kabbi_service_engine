'use strict';

class OrderDetailCostService {

    constructor(orderDetailCostInfo) {
        this.orderDetailCostInfo = orderDetailCostInfo;
    }

    get startPointLocationIsInCity() {
        return this.getValue('start_point_location') === 'in';
    }


    getValue(property) {
        return !isNaN(parseFloat(this.orderDetailCostInfo[property])) ? parseFloat(this.orderDetailCostInfo[property]) : 0;
    }

    get orderTime() {
        return this.getValue('summary_time');
    }

    get orderCost() {
        return this.getValue('summary_cost');
    }

    get supplyCost() {
        return this.getValue('planting_price');
    }

    get includedSupplyTime() {
        return this.getValue('planting_include_time');
    }

    get includedSupplyDistance() {
        return this.getValue('planting_include');
    }

    get includedSupplyCost() {
        return 0;
    }

    get supplyOutsideCityTariff() {
        return this.getValue('supply_price');
    }

    get supplyOutsideCityMeter() {
        return this.getValue('distance_for_plant');
    }

    get supplyOutsideCityCost() {
        return this.getValue('distance_for_plant_cost');
    }

    get beforeBoardingTariff() {
        return this.startPointLocationIsInCity ? this.getValue('city_wait_price') : this.getValue('out_wait_price');
    }

    get beforeBoardingMeter() {
        return this.getValue('before_time_wait') * 60;
    }

    get beforeBoardingCost() {
        return this.getValue('before_time_wait_cost');
    }

    get cityDistanceTariff() {
        return this.getValue('city_next_km_price');
    }

    get cityDistanceMeter() {
        return this.getValue('city_distance');
    }

    get cityDistanceCost() {
        return this.getValue('city_cost');
    }

    get cityTimeTariff() {
        return this.getValue('city_next_km_price_time');
    }

    get cityTimeMeter() {
        return this.getValue('city_time') * 60;
    }

    get cityTimeCost() {
        return this.getValue('city_cost_time');
    }

    get outsideCityDistanceTariff() {
        return this.getValue('out_next_km_price');
    }

    get outsideCityDistanceMeter() {
        return this.getValue('out_city_distance');
    }

    get outsideCityDistanceCost() {
        return this.getValue('out_city_cost');
    }

    get outsideCityTimeTariff() {
        return this.getValue('out_next_km_price_time');
    }

    get outsideCityTimeMeter() {
        return this.getValue('out_city_time') * 60;
    }

    get outsideCityTimeCost() {
        return this.getValue('out_city_cost_time');
    }


    get waitingCityTariff() {
        return this.getValue('city_wait_price');
    }


    get waitingCityMeter() {
        return this.getValue('city_time_wait') * 60;
    }

    get waitingCityCost() {
        return this.getValue('city_wait_cost');
    }


    get waitingOutsideCityTariff() {
        return this.getValue('out_wait_price');
    }

    get waitingOutsideCityMeter() {
        return this.getValue('out_time_wait') * 60;
    }

    get waitingOutsideCityCost() {
        return this.getValue('out_wait_cost');
    }

    get additionalCost() {
        return this.getValue('additional_cost');
    }

    get summaryDistance() {
        return this.getValue('summary_distance');
    }

    get summaryTime() {
        return this.getValue('summary_time') * 60;
    }

    get bonusReplenishment() {
        return this.getValue('bonus');
    }

    get promoBonusReplenishment() {
        return this.getValue('promo_discount_value');
    }

    get summaryCostNoDiscount() {
        return this.summaryCost + this.promoBonusReplenishment;
    }

    get summaryCost() {
        return this.getValue('summary_cost');
    }

    /**
     * Get  detailing order report
     * @returns {
     * {order_time: *,
     * order_cost: *,
     * supply_cost: *,
     * included_supply_time: *,
     * included_supply_distance: *,
     * included_supply_cost: number,
     * supply_outside_city_tariff: *,
     * supply_outside_city_meter: *,
     * supply_outside_city_cost: *,
     * before_boarding_tariff: *,
     * before_boarding_meter: *,
     * before_boarding_cost: *,
     * city_distance_tariff: *,
     * city_distance_meter: *,
     * city_distance_cost: *,
     * city_time_tariff: *,
     * city_time_meter: *,
     * city_time_cost: *,
     * outside_city_distance_tariff: *,
     * outside_city_distance_meter: *,
     * outside_city_distance_cost: *,
     * outside_city_time_tariff: *,
     * outside_city_time_meter: *,
     * outside_city_time_cost: *,
     * waiting_city_tariff: *,
     * waiting_city_meter: *,
     * waiting_city_cost: *,
     * waiting_outside_city_tariff: *,
     * waiting_outside_city_meter: *,
     * waiting_outside_city_cost: *,
     * additional_cost: *,
     * summary_distance: *,
     * summary_time: *,
     * bonus_replenishment: *,
     * promo_bonus_replenishment: *,
     * summary_cost_no_discount: *,
     * summary_cost: *
     * }}
     */
    getDetailingOrderReport() {
        return {
            'order_time': this.orderTime, // время поездки. ?? мин
            'order_cost': this.orderCost, // стоимость заказа

            'supply_cost': this.supplyCost, // подача авто. ?? руб

            'included_supply_time': this.includedSupplyTime, // включено в подачу. ?? мин
            'included_supply_distance': this.includedSupplyDistance, // включено в подачу. ?? км
            'included_supply_cost': this.includedSupplyCost, // включено в подачу авто. ?? руб

            'supply_outside_city_tariff': this.supplyOutsideCityTariff, //Стоимость подачи за город. ?? руб/мин.
            'supply_outside_city_meter': this.supplyOutsideCityMeter, //Стоимость подачи за город. ?? км
            'supply_outside_city_cost': this.supplyOutsideCityCost, //Стоимость подачи за город. ?? руб

            'before_boarding_tariff': this.beforeBoardingTariff, // Тариф время ожидания до посадки. ?? руб/мин
            'before_boarding_meter': this.beforeBoardingMeter, // Счетчик время ожидания до посадки. ?? сек
            'before_boarding_cost': this.beforeBoardingCost, // Стоимость время ожидания до посадки. ?? руб

            'city_distance_tariff': this.cityDistanceTariff, // Город. ?? руб/км
            'city_distance_meter': this.cityDistanceMeter, // Город. ?? км
            'city_distance_cost': this.cityDistanceCost, // Город. ?? руб

            'city_time_tariff': this.cityTimeTariff, // Город. ?? руб/мин
            'city_time_meter': this.cityTimeMeter, // город. ?? сек
            'city_time_cost': this.cityTimeCost, // город. ?? руб

            'outside_city_distance_tariff': this.outsideCityDistanceTariff, // загород. ?? руб/км
            'outside_city_distance_meter': this.outsideCityDistanceMeter, // загород. ?? км
            'outside_city_distance_cost': this.outsideCityDistanceCost, // загород. ?? руб

            'outside_city_time_tariff': this.outsideCityTimeTariff, // загород. ?? руб/мин
            'outside_city_time_meter': this.outsideCityTimeMeter, // загород ?? сек
            'outside_city_time_cost': this.outsideCityTimeCost, // загород. ?? руб

            'waiting_city_tariff': this.waitingCityTariff, // Ожидание в городе. ?? руб/мин
            'waiting_city_meter': this.waitingCityMeter, // Ожидание в городе. ?? сек
            'waiting_city_cost': this.waitingCityCost, //Ожидание в городе. ?? руб

            'waiting_outside_city_tariff': this.waitingOutsideCityTariff, // ожидание загородом. ?? руб/мин
            'waiting_outside_city_meter': this.waitingOutsideCityMeter, // ожидание загородом. ?? сек
            'waiting_outside_city_cost': this.waitingOutsideCityCost, // ожидание загородом. ?? руб

            'additional_cost': this.additionalCost, // доп опции. ?? руб
            'summary_distance': this.summaryDistance, // суммарное расстояние. ?? км
            'summary_time': this.summaryTime, // суммарное время. ?? сек.
            'bonus_replenishment': this.bonusReplenishment, // начисление бонусов. ?? Б(руб)
            'promo_bonus_replenishment': this.promoBonusReplenishment, // начисление промо бонусов. ?? Б(руб)
            'summary_cost_no_discount': this.summaryCostNoDiscount, // стоимость без скидки. ?? руб
            'summary_cost': this.summaryCost, // стоимость со скидкой. ?? руб
        }

    }

}

module.exports = OrderDetailCostService;


