'use strict';

class OrderSettingsRepository {

    create(orderSettings) {
        return new Promise((resolve, reject) => {
            orderSettings.save(err => {
                if (err) {
                    return reject(err);
                }
                return resolve(true);
            })
        })
    }
}

module.exports = exports = new OrderSettingsRepository();