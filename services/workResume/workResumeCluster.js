"use strict";
const WorkResumeBase = require("./workResumeBase");
const logger = require('../logger');
const mainLogger = logger.mainLogger;
const consul = require("../consul/consulAgent");
const config = require("../../services/lib").getConfig();
const request = require('request');
const async = require("async");

/**
 * @class WorkResumeCluster
 * Load to processing orders,worker shifts, unblock timers
 * to all active nodes at cluster
 */
class WorkResumeCluster extends WorkResumeBase {

    /**
     * Load orders which was processed at node with id= nodeId to all active nodes at cluster
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadOrders(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeCluster->loadOrders called");
            mainLogger("info", `WorkResumeCluster->loadOrders->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedOrders(nodeId)
                .then(orders => {
                    async.each(orders, (orderData, orderCb) => {
                        consul.getRandomHealthyServiceLocation(config.CONSUL_SERVICE_NAME)
                            .then(serviceLocation => {
                                return this._sendOrderToNode({
                                    host: serviceLocation.host,
                                    port: serviceLocation.port,
                                    tenantId: orderData.tenantId,
                                    orderId: orderData.orderId,
                                    isFromLoader: 1

                                })
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadOrders->getRandomActiveServiceLocation->Order id: ${orderData.orderId}. Error: ${err.message}`);
                                return orderCb(null, 1);
                            })
                            .then(orderSentResult => {
                                mainLogger("info", `WorkResumeCluster->loadOrders->Order id: ${orderData.orderId}. Send result: ${orderSentResult}`);
                                return orderCb(null, 1);
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadOrders->_sendOrderToNode->Order id: ${orderData.orderId}.Error: ${err.message}`);
                                return orderCb(null, 1);
                            })
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(1);
                    })
                })
                .catch(err => {
                    return reject(err);
                })
        })
    }

    /**
     * Load worker shifts which was processed at node with id= nodeId to all active nodes at cluster,
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadWorkerShifts(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeCluster->loadWorkerShifts called");
            mainLogger("info", `WorkResumeCluster->loadWorkerShifts->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedWorkerShifts(nodeId)
                .then(processedShifts => {
                    async.each(processedShifts, (shiftData, shiftCb) => {
                        consul.getRandomHealthyServiceLocation(config.CONSUL_SERVICE_NAME)
                            .then(serviceLocation => {
                                return this._sendWorkerShiftToNode({
                                    host: serviceLocation.host,
                                    port: serviceLocation.port,
                                    tenantId: shiftData.tenantId,
                                    tenantLogin: shiftData.tenantLogin,
                                    workerCallsign: shiftData.workerCallsign,
                                    shiftLifeTime: shiftData.shiftLifeTime,
                                    shiftId: shiftData.shiftId,
                                })
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadWorkerShifts->getRandomActiveServiceLocation->Shift id: ${shiftData.shiftId}. Error: ${err.message}`);
                                return shiftCb(null, 1);
                            })
                            .then(orderSentResult => {
                                mainLogger("info", `WorkResumeCluster->loadWorkerShifts->Shift id: ${shiftData.shiftId}. Send result: ${orderSentResult}`);
                                return shiftCb(null, 1);
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadWorkerShifts->_sendWorkerShiftToNode->Shift id: ${shiftData.shiftId}.Error: ${err.message}`);
                                return shiftCb(null, 1);
                            })
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(1);
                    })
                })
                .catch(err => {
                    return reject(err);
                })
        })
    }

    /**
     * Load worker unblock timers  which was processed at node with id= nodeId to all active nodes at cluster
     * @param {Number} nodeId
     * @returns {Promise}
     */
    loadWorkerUnblockTimers(nodeId) {
        return new Promise((resolve, reject) => {
            mainLogger("info", "WorkResumeCluster->loadWorkerUnblockTimers called");
            mainLogger("info", `WorkResumeCluster->loadWorkerUnblockTimers->nodeId:${nodeId}`);
            WorkResumeBase.getProcessedWorkerUnblockTimers(nodeId)
                .then(processedWorkerUnblockTimers => {
                    async.each(processedWorkerUnblockTimers, (timerData, timerCb) => {
                        consul.getRandomHealthyServiceLocation(config.CONSUL_SERVICE_NAME)
                            .then(serviceLocation => {
                                if (timerData.typeBlock === "order") {
                                    return this._sendWorkerOrderBlockToNode({
                                        host: serviceLocation.host,
                                        port: serviceLocation.port,
                                        tenantId: timerData.tenantId,
                                        workerCallsign: timerData.workerCallsign,
                                        shiftId: timerData.shiftId,
                                        blockId: timerData.blockId,
                                        timeToUnblock: timerData.timeToUnblock,

                                    })
                                } else {
                                    return this._sendWorkerPreOrderBlockToNode({
                                        host: serviceLocation.host,
                                        port: serviceLocation.port,
                                        tenantId: timerData.tenantId,
                                        workerCallsign: timerData.workerCallsign,
                                        shiftId: timerData.shiftId,
                                        blockId: timerData.blockId,
                                        timeToUnblock: timerData.timeToUnblock,
                                    })
                                }
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadWorkerUnblockTimers->getRandomActiveServiceLocation->Block id: ${timerData.blockId}. Error: ${err.message}`);
                                return timerCb(null, 1);
                            })
                            .then(orderSentResult => {
                                mainLogger("info", `WorkResumeCluster->loadWorkerUnblockTimers->Block id: ${timerData.blockId}. Send result: ${orderSentResult}`);
                                return timerCb(null, 1);
                            })
                            .catch(err => {
                                mainLogger("error", `WorkResumeCluster->loadWorkerUnblockTimers->_sendWorkerShiftToNode->Block id: ${timerData.blockId}.Error: ${err.message}`);
                                return timerCb(null, 1);
                            })
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(1);
                    });
                })
                .catch(err => {
                    return reject(err);
                })
        })

    }


    /**
     * Send order to node
     * @param {Object} options
     * @param {String} options.host
     * @param {Number} options.port
     * @param {Number} options.tenantId
     * @param {Number} options.orderId
     * @param {Number} options.isFromLoader
     * @return {Promise}
     * @private
     */
    _sendOrderToNode(options) {
        return new Promise((resolve, reject) => {
            const getString =
                "order_id=" + options.orderId +
                "&tenant_id=" + options.tenantId +
                "&is_from_loader=" + options.isFromLoader;

            const requestOptions = {
                url: "http://" + options.host + ":" + options.port + "/neworder_auto?" + getString
            };

            function resultData(error, response, body) {
                if (!error && parseInt(response.statusCode) === 200) {
                    try {
                        const info = JSON.parse(body);
                        if (parseInt(info.result) === 1) {
                            return resolve(1);
                        } else {
                            return resolve(0);
                        }
                    } catch (err) {
                        return reject(err);
                    }
                } else {
                    return reject(error);
                }
            }

            request(requestOptions, resultData);
        });
    }

    /**
     * Send worker shift to node
     * @param {Object} options
     * @param {String} options.host
     * @param {Number} options.port
     * @param {Number} options.tenantId
     * @param {Number} options.tenantLogin
     * @param {Number} options.workerCallsign
     * @param {Number} options.shiftLifeTime
     * @param {Number} options.shiftId
     * @return {Promise}
     * @private
     */
    _sendWorkerShiftToNode(options) {
        return new Promise((resolve, reject) => {
            const getString =
                "tenant_id=" + options.tenantId +
                "&tenant_login=" + options.tenantLogin +
                "&worker_callsign=" + options.workerCallsign +
                "&shift_life_time=" + options.shiftLifeTime +
                "&shift_id=" + options.shiftId;

            const requestOptions = {
                url: "http://" + options.host + ":" + options.port + "/watch_worker_shift?" + getString
            };

            function resultData(error, response, body) {
                if (!error && parseInt(response.statusCode) === 200) {
                    try {
                        const info = JSON.parse(body);
                        if (parseInt(info.result) === 1) {
                            return resolve(1);
                        } else {
                            return resolve(0);
                        }
                    } catch (err) {
                        return reject(err);
                    }
                } else {
                    return reject(error);
                }
            }

            request(requestOptions, resultData);
        });
    }

    /**
     * Send worker order block to create watching timer
     * @param {Object} options
     * @param {String} options.host
     * @param {Number} options.port
     * @param {Number} options.tenantId
     * @param {Number} options.workerCallsign
     * @param {Number} options.shiftId
     * @param {Number} options.blockId
     * @param {Number} options.timeToUnblock
     * @return {Promise}
     * @private
     */
    _sendWorkerOrderBlockToNode(options) {
        return new Promise((resolve, reject) => {
            const getString =
                "tenant_id=" + options.tenantId +
                "&worker_callsign=" + options.workerCallsign +
                "&shift_id=" + options.shiftId +
                "&block_id=" + options.blockId +
                "&time_to_unblock=" + options.timeToUnblock;

            const requestOptions = {
                url: "http://" + options.host + ":" + options.port + "/watch_block_order_timer?" + getString
            };

            function resultData(error, response, body) {
                if (!error && parseInt(response.statusCode) === 200) {
                    try {
                        const info = JSON.parse(body);
                        if (parseInt(info.result) === 1) {
                            return resolve(1);
                        } else {
                            return resolve(0);
                        }
                    } catch (err) {
                        return reject(err);
                    }
                } else {
                    return reject(error);
                }
            }

            request(requestOptions, resultData);
        });

    }

    /**
     * Send worker preorder block to create watching timer
     * @param {Object} options
     * @param {String} options.host
     * @param {Number} options.port
     * @param {Number} options.tenantId
     * @param {Number} options.workerCallsign
     * @param {Number} options.shiftId
     * @param {Number} options.blockId
     * @param {Number} options.timeToUnblock
     * @return {Promise}
     * @private
     */
    _sendWorkerPreOrderBlockToNode(options) {
        return new Promise((resolve, reject) => {
            const getString =
                "tenant_id=" + options.tenantId +
                "&worker_callsign=" + options.workerCallsign +
                "&shift_id=" + options.shiftId +
                "&block_id=" + options.blockId +
                "&time_to_unblock=" + options.timeToUnblock;

            const requestOptions = {
                url: "http://" + options.host + ":" + options.port + "/watch_block_preorder_timer?" + getString
            };

            function resultData(error, response, body) {
                if (!error && parseInt(response.statusCode) === 200) {
                    try {
                        const info = JSON.parse(body);
                        if (parseInt(info.result) === 1) {
                            return resolve(1);
                        } else {
                            return resolve(0);
                        }
                    } catch (err) {
                        return reject(err);
                    }
                } else {
                    return reject(error);
                }
            }

            request(requestOptions, resultData);
        });

    }
}

module.exports = exports = new WorkResumeCluster();