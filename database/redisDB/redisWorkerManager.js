"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const PHPUnserialize = require('php-unserialize');
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const redis = require("redis");
const async = require("async");
const retryStrategy = require("./redisRetryStrategy");

/**
 * Worker manager
 * @param {Number} port
 * @param {String} ip
 * @returns {RedisWorkerManager}
 */
function RedisWorkerManager(port, ip) {
    //Workers
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db0");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db0");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db0:" + err);
    });

    this.dbNumber = 0;


}


/**
 * Get all active workers by tenantId
 * @param  {Number} tenantId
 * @param  {Function} callback
 */
RedisWorkerManager.prototype.getAllWorkers = function (tenantId, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hvals(tenantId, (err, workers) => {
            if (err) {
                return callback(err);
            }
            const workersResultArr = [];
            async.each(workers, (workerItem, callback) => {
                try {
                    let worker = PHPUnserialize.unserialize(workerItem);
                    workersResultArr.push(worker);
                    callback();
                } catch (err) {
                    return callback();
                }
            }, function (err) {
                if (err) {
                    return callback(err);
                }
                callback(null, workersResultArr);
            });

        });
    });
};

/**
 * Get worker data by tenantId and his callsign
 * @param {Number} tenantId
 * @param {Number} callsign
 * @param {Function} callback
 */
RedisWorkerManager.prototype.getWorker = function (tenantId, callsign, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hget([tenantId, callsign], (err, worker) => {
            if (err) {
                return callback(err);
            }
            try {
                worker = PHPUnserialize.unserialize(worker);
                if (worker === null || typeof(worker) === "undefined") {
                    return callback(new Error("No worker at redisDB"));
                }
                if (!worker.worker || !worker.geo || !worker.position) {
                    return callback(new Error("Incorrect worker data at redisDB"));
                }
                callback(null, worker);
            } catch (err) {
                return callback(err);
            }
        });

    });

};


/**
 * Save worker data
 * @param  {Object} workerData
 * @param  {Function} callback
 */
RedisWorkerManager.prototype.saveWorker = function (workerData, callback) {
    const self = this;
    let tenantId, callsign, worker;
    if (workerData.worker && workerData.worker.tenant_id) {
        tenantId = workerData.worker.tenant_id;
    } else {
        return callback(new Error('Bad worker tenant id'));
    }
    if (workerData.worker && workerData.worker.callsign) {
        callsign = workerData.worker.callsign;
    } else {
        return callback(new Error('Bad worker callsign'));
    }
    try {
        worker = lib.serialize(workerData);
    } catch (err) {
        return callback(err);
    }
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hset(tenantId, callsign, worker, (err, hsetResult) => {
            if (err) {
                return callback(err);
            }

            if (hsetResult === 1 || hsetResult === 0) {
                return callback(null, 1);
            }
            return callback(new Error('Error - hset bad result. Hset result:' + hsetResult));
        });
    });
};

/**
 * Delete Worker from redis
 * @param {Number} tenantId
 * @param {Number} callsign
 * @param {Function} callback
 */
RedisWorkerManager.prototype.deleteWorker = function (tenantId, callsign, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hdel([tenantId, callsign], (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });

    });
};


module.exports = exports = new RedisWorkerManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);