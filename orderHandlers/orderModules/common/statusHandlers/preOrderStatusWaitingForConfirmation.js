'use strict';

const orderLogger = require('../../../../services/logger').orderLogger;
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");

/**
 * Order status waiting for confirmation preorder
 * @param {BaseGootaxOrder} orderInstance
 * @return {Promise}
 */
module.exports = (orderInstance) => {
    return new Promise((resolve, reject) => {
        const confirmTimeout = 125;
        try {
            orderLogger(orderInstance.orderId, 'info', 'preOrderStatusWaitingForConfirmation called!');
            const orderId = orderInstance.orderId;
            const tenantId = orderInstance.tenantId;
            orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.preOrderWaitingForConfirmLabel);
            orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.preOrderWaitingForConfirmLabel, () => {
                orderLogger(orderId, 'info', `preOrderStatusWaitingForConfirmation->${orderInstance.orderTimer.preOrderWaitingForConfirmLabel} called`);
                const messageString = orderEventOrderMessage({
                    command: orderEventOrderCommand.preOrderConfirmTimeout,
                    tenant_id: tenantId,
                    order_id: orderId,
                    params: {}
                });
                amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                    if (err) {
                        orderLogger(orderId, "error", `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderStatusOfferOrder->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.orderOfferTimeout} to order: ${orderId}. Result: ${result}`);
                    }
                })
            }, confirmTimeout * 1000);
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    })
};