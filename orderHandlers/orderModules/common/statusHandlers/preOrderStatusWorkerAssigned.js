'use strict';

const preOrderWatcher = require('../preOrderWatcher');
const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;

/**
 * PreOrder status worker assigned
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {string} eventSenderService
 * @param {Boolean} orderTimeIsChanged
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, eventSenderService, orderTimeIsChanged) => {
    return new Promise((resolve, reject) => {
        try {
            if (orderInstance.statusChanged) {
                const orderId = orderData.order_id;
                orderLogger(orderId, 'info', 'PreOrderStatusWorkerAssigned CALLED!');
                orderLogger(orderId, 'info', 'PreOrderStatusWorkerAssigned->status is changed');
                orderInstance.orderTimer.clearAll();
                preOrderWatcher.runWatcher(orderInstance, orderData);

                let showWorker = false;
                let isPreOrder = true;
                orderWorkerAssignManager.processAssignedOrder(orderData, eventSenderService, {
                    isPreOrder,
                    showWorker
                });
            } else if (orderTimeIsChanged) {
                const orderId = orderData.order_id;
                orderLogger(orderId, 'info', 'PreOrderStatusWorkerAssigned CALLED!');
                orderLogger(orderId, 'info', 'preOrderStatusWorkerAssigned->order time is changed');
                orderInstance.orderTimer.clearAll();
                preOrderWatcher.runWatcher(orderInstance, orderData);
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }

    });
};
