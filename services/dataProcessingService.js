'use strict';

/**
 * @class DataProcessingService
 */
class DataProcessingService {
    /**
     * @constructor
     */
    constructor() {
        this.activeOrders = new Set();
        this.activeWorkerShifts = new Set();
        this.activeWorkerBlocks = new Set();
    }

    get activeOrdersList() {
        return [...this.activeOrders];
    }

    get activeOrdersListLength() {
        return this.activeOrders.size;
    }

    get activeWorkerShiftsList() {
        return [...this.activeWorkerShifts];
    }

    get activeWorkerShiftsListLength() {
        return this.activeWorkerShifts.size;
    }

    get activeWorkerBlocksList() {
        return [...this.activeWorkerBlocks];
    }

    get activeWorkerBlocksListLength() {
        return this.activeWorkerBlocks.size;
    }

    addOrder(orderId) {
        this.activeOrders.add(orderId);
    }

    delOrder(orderId) {
        this.activeOrders.delete(orderId);
    }

    addWorkerShift(shiftId) {
        this.activeWorkerShifts.add(shiftId);
    }

    delWorkerShift(shiftId) {
        this.activeWorkerShifts.delete(shiftId);
    }

    addWorkerBlock(blockId) {
        this.activeWorkerBlocks.add(blockId);
    }

    delWorkerBlock(blockId) {
        this.activeWorkerBlocks.delete(blockId);
    }
}

module.exports = exports = new DataProcessingService();