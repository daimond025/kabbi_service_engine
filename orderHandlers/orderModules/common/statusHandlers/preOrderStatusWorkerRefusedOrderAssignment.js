'use strict';

const orderWorkerAssignManager = require('../orderWorkerAssignManager');
const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;
const orderEventSenderService = require('../../../orderEvent/orderEventSenderService');

/**
 * PreOrder status worker refused order assigned
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @param {String} eventSenderService
 * @return {Promise}
 */
module.exports = (orderInstance, orderData, eventSenderService) => {
    return new Promise((resolve, reject) => {
        try {
            if (orderInstance.statusChanged) {
                const orderId = orderInstance.orderId;
                orderLogger(orderId, 'info', 'PreOrderStatusWorkerRefusedOrderAssignment CALLED!');
                orderInstance.orderTimer.clearAll();
                const isWorkerEventOwner = eventSenderService === orderEventSenderService.workerService;
                let wasAssignedBySelf = true;
                if (orderInstance.statusArr.length >= 2 && orderInstance.statusArr[orderInstance.statusArr.length - 2]) {
                    if (orderInstance.statusArr[orderInstance.statusArr.length - 2] !== statusInfo.worker_accepted_preorder.status_id) {
                        wasAssignedBySelf = false;
                    }
                }
                let newStatusId;
                if (wasAssignedBySelf && isWorkerEventOwner) {
                    newStatusId = statusInfo.new_pre_order.status_id;
                } else {
                   // newStatusId = statusInfo.manual_mode.status_id;
                    newStatusId = statusInfo.new_pre_order.status_id;
                }
                orderWorkerAssignManager.processRefusedOrderAssignment(orderData, {
                    isPreOrder: true,
                    isWorkerEventOwner,
                    newStatusId
                });
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }
    });
};
