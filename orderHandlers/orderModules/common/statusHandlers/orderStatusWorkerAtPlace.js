"use strict";

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const redisWorkerManager = require("../../../../database/redisDB/redisWorkerManager");
const amqpOrderMessageSender = require("../../../../services/amqpOrderMessageSender");
const sqlManager = require('../../../../database/sqlDB/sqlManager');
const orderEventOrderMessage = require('../../../orderEvent/orderEventOrderMessage');
const orderEventWorkerMessage = require('../../../orderEvent/orderEventWorkerMessage');
const orderEventOrderCommand = require('../../../orderEvent/orderEventOrderCommand');
const statusInfo = require('../../../../services/orderStatusManager').statusInfo;


/**
 * Order status worker at place
 * @param {BaseGootaxOrder} orderInstance
 * @param {object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        try {
            orderLogger(orderInstance.orderId, 'info', 'orderStatusWorkerAtPlace called!');
            if (orderInstance.statusChanged) {
                const orderId = orderData.order_id;
                const statusId = orderData.status.status_id;
                const tenantId = orderData.tenant_id;
                const startPointLocation = orderData.costData.start_point_location;
                const isDay = parseInt(orderData.costData.tariffInfo.isDay);
                const workerCallsign = orderData.worker && orderData.worker.callsign ? orderData.worker.callsign : null;
                let freeWaitTime = 0;
                if (statusInfo.worker_arrived_order.status_id === statusId) {
                    orderInstance.orderTimer.clearServiceTimeout(orderInstance.orderTimer.workerLateTimeoutLabel);
                    if (startPointLocation === "in") {
                        if (isDay === 1) {
                            if (orderData.costData.tariffInfo.tariffDataCity.wait_time_day) {
                                freeWaitTime = orderData.costData.tariffInfo.tariffDataCity.wait_time_day;
                            }
                        }
                        if (isDay === 0) {
                            if (orderData.costData.tariffInfo.tariffDataCity.wait_time_night) {
                                freeWaitTime = orderData.costData.tariffInfo.tariffDataCity.wait_time_night;
                            }
                        }
                    }
                    if (startPointLocation === "out") {
                        if (isDay === 1) {
                            if (orderData.costData.tariffInfo.tariffDataTrack.wait_time_day) {
                                freeWaitTime = orderData.costData.tariffInfo.tariffDataTrack.wait_time_day;
                            }
                        }
                        if (isDay === 0) {
                            if (orderData.costData.tariffInfo.tariffDataTrack.wait_time_night) {
                                freeWaitTime = orderData.costData.tariffInfo.tariffDataTrack.wait_time_night;
                            }
                        }
                    }
                    freeWaitTime = parseInt(freeWaitTime) > 0 ? parseInt(freeWaitTime) : 0;
                    orderLogger(orderId, 'info', `orderStatusWorkerAtPlace->clientLateTimeout will be called after ${freeWaitTime} min`);
                    freeWaitTime = freeWaitTime * 60 * 1000;
                    try {
                        orderInstance.orderTimer.setServiceTimeout(orderInstance.orderTimer.clientLateTimeoutLabel, () => {
                            orderLogger(orderId, 'info', `orderStatusCarAtPlace->${orderInstance.orderTimer.clientLateTimeoutLabel} called!`);
                            const messageString = orderEventOrderMessage({
                                command: orderEventOrderCommand.clientLateTimeout,
                                tenant_id: tenantId,
                                order_id: orderId,
                                params: {}
                            });
                            amqpOrderMessageSender.sendMessage(orderId, messageString, (err, result) => {
                                if (err) {
                                    orderLogger(orderId, "error", `orderStatusCarAtPlace->amqpOrderMessageSender.sendMessage->Error: ${err.message}`);
                                } else {
                                    orderLogger(orderId, 'info', `orderStatusCarAtPlace->amqpOrderMessageSender.sendMessage->Published command: ${orderEventOrderCommand.clientLateTimeout} to order: ${orderId}. Result: ${result}`);

                                }
                            });
                        }, freeWaitTime, tenantId, orderId)
                    } catch (err) {
                        orderLogger(orderId, 'error', `orderStatusCarAtPlace->setServiceTimeout->Error: ${err.message}`);
                    }
                    if (orderInstance.workerLateTimestamp !== null) {
                        try {
                            const lateTime = Math.floor(Date.now() / 1000) - orderInstance.workerLateTimestamp;
                            orderInstance.workerLateTimestamp = null;
                            if(workerCallsign){
                                redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                                    if (err) {
                                        orderLogger(orderId, "error", `orderStatusCarAtPlace->redisWorkerManager.getWorker->Error: ${err.message}`);
                                    } else {
                                        const workerShiftId = workerData.worker.worker_shift_id;
                                        sqlManager.logWorkerLateTime(workerShiftId, lateTime, (err, result) => {
                                            if (err) {
                                                orderLogger(orderId, 'error', `orderStatusCarAtPlace->sqlManager.logWorkerLateTime->Error: ${err.message}`);
                                            } else {
                                                orderLogger(orderId, 'info', `orderStatusCarAtPlace->sqlManager.logWorkerLateTime->Result: ${result}`);
                                            }
                                        });
                                    }
                                })
                            }
                        } catch (err) {
                            orderLogger(orderId, "error", `orderStatusCarAtPlace->Error: ${err.message}`);
                        }
                    }
                }
            }
            return resolve(1);
        } catch (err) {
            return reject(err);
        }

    });
};