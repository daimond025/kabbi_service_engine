'use strict';

const logger = require('../../../../services/logger');
const orderLogger = logger.orderLogger;
const orderActionManager = require('../orderActionManager');

/**
 * Order status outdated
 * @param {BaseGootaxOrder} orderInstance
 * @param {Object} orderData
 * @return {Promise}
 */
module.exports = (orderInstance, orderData) => {
    return new Promise((resolve, reject) => {
        orderLogger(orderInstance.orderId, 'info', 'orderStatusOutdated called!');
        if (orderData['exchange_info'] && orderData['exchange_info']['exchange_order_id']) {
            orderLogger(orderInstance.orderId, 'info', `orderStatusOutdated->order already in exchange, do not need create timer`);
            return;
        }
        orderActionManager.sendOrderToExchangeByTimeout(orderInstance, orderData)
            .then(result => {
                orderLogger(orderInstance.orderId, 'info', `orderStatusOutdated->orderActionManager.sendOrderToExchangeByTimeout->Result: ${result}`);
                return resolve(1);
            })
            .catch(err => {
                orderLogger(orderInstance.orderId, 'info', `orderStatusOutdated->orderActionManager.sendOrderToExchangeByTimeout->Error: ${err.message}`);
                return reject(err);
            });
    })
};