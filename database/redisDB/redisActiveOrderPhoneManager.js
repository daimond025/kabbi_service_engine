"use strict";

const lib = require("../../services/lib");
const config = lib.getConfig();
const PHPUnserialize = require('php-unserialize');
const redis = require("redis");
const logger = require("../../services/logger");
const mainLogger = logger.mainLogger;
const retryStrategy = require("./redisRetryStrategy");


/**
 * Active order phone manager
 * @param {Number} port
 * @param  {String} ip
 * @returns {RedisActiveOrderPhoneManager}
 */
function RedisActiveOrderPhoneManager(port, ip) {
    //WorkerShifts
    this.redisClientDb = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb.on("connect", () => {
        mainLogger('info', "connected Redis db9");
    });
    this.redisClientDb.on("reconnecting", () => {
        mainLogger('info', "reconnecting Redis db9");
    });

    this.redisClientDb.on("error", err => {
        mainLogger('error', "Redis error in db9:" + err);
    });

    this.dbNumber = 9;

}

/**
 * Delete all orders of Phone Number
 * @param  {Number} tenantId
 * @param  {String} phoneValue
 * @param  {Function} callback
 */
RedisActiveOrderPhoneManager.prototype.deletePhoneOrders = function (tenantId, phoneValue, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hdel([tenantId, phoneValue], (err, result) => {
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};


/**
 * Ger Orders By Phone
 * @param  {Number} tenantId
 * @param  {String} phoneValue
 * @param  {Function} callback
 */
RedisActiveOrderPhoneManager.prototype.gerOrdersByPhone = function (tenantId, phoneValue, callback) {
    const self = this;
    self.redisClientDb.select(self.dbNumber, () => {
        self.redisClientDb.hget([tenantId, phoneValue], (err, ordersSer) => {
            if (err) {
                return callback(err);
            }
            try {
                const orders = PHPUnserialize.unserialize(ordersSer);
                if (orders === null || typeof(orders) === "undefined") {
                    return callback(null, null);
                }
                callback(null, orders);
            } catch (err) {
                return callback(err);
            }

        });
    });
};

/**
 * Set Orders By Phone
 * @param  {Number} tenantId
 * @param  {String} phoneValue
 * @param  {Object} orders
 * @param  {Function} callback
 */
RedisActiveOrderPhoneManager.prototype.setOrdersByPhone = function (tenantId, phoneValue, orders, callback) {
    const self = this;
    try {
        const orderArray = lib.serialize(orders);
        self.redisClientDb.select(self.dbNumber, () => {
            self.redisClientDb.hset([tenantId, phoneValue, orderArray], (err, hsetResult) => {
                if (err) {
                    return callback(err);
                }
                if (hsetResult || hsetResult === 0) {
                    return callback(null, 1);
                }
                callback(new Error('Error - hset bad result. Hset result:' + hsetResult));
            });
        });
    } catch (err) {
        return callback(err);
    }

};


module.exports = exports = new RedisActiveOrderPhoneManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);