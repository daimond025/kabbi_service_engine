'use strict';
const mainLogger = require('./logger').mainLogger;

/**
 * Timer service
 * @returns {serviceTimer}
 */
function ServiceTimer() {
    //Array of active timers
    this.activeTimers = [];
    //Array of active intervals
    this.activeIntervals = [];

    this.offerOrderTimeoutLabel = "offerOrderTimeout";
    this.preOrderWaitingForConfirmLabel = "preOrderWaitingForConfirmTimeout";
    this.workerLateTimeoutLabel = "workerLateTimeout";
    this.clientLateTimeoutLabel = "clientLateTimeout";
    this.orderLateTimeoutLabel = "orderLateTimeout";
    this.distributionCyclesTimeoutLabel = "distributionCyclesTimeout";
    this.rejectOrderByTimeoutLabel = "rejectOrderByTimeout";
    this.preOrderSenderToWorkLabel = "preOrderSenderToWork";
    this.preOrderFirstReminderLabel = "preOrderFirstReminder";
    this.preOrderSecondReminderLabel = "preOrderSecondReminder";
    this.preOrderThirdReminderLabel = "preOrderThirdReminder";
    this.preOrderUpdateListTimeoutLabel = "preOrderUpdateListTimeout";
    this.sendToExchangeTimeoutLabel = "sendToExchangeTimeoutLabel";

    this.orderComplateTimeoutLabel = "orderComplateTimeoutLabel";
    this.preOrderPermitStart = "orderComplateTimeoutLabel";



    // worker intervals
    this.workerParkingTimeoutLabel = "workerParkingTimeoutLabel";
    this.workerCoordsTimeoutLabel = "workerCoordsUpdateChecker";
    this.workerShiftTimer = "workerShiftTimer";
}

/**
 * Add timeout to service timers array
 * @param  {String} timerLabel
 * @param  {Object} timerObject
 */
ServiceTimer.prototype.pushTimeout = function (timerLabel, timerObject) {
    const self = this;
    self.activeTimers.push({'timerLabel': timerLabel, 'timer': timerObject});
};

ServiceTimer.prototype.pushInterval = function (intervalLabel, intervalObject) {
    const self = this;
    self.activeIntervals.push({'intervalLabel': intervalLabel, 'interval': intervalObject});

};

/**
 * Set service timeout
 * @param  {String} timerLabel
 * @param  {Function} callback
 * @param  {Number} ms
 */
ServiceTimer.prototype.setServiceTimeout = function (timerLabel, callback, ms) {
    const self = this;
    self.clearServiceTimeout(timerLabel);
    if (typeof callback !== 'function')
        throw new Error('Callback must be a function');
    ms = parseInt(ms);
    if (Number.isNaN(ms))
        throw new Error('Delay must be an integer');

    const args = Array.prototype.slice.call(arguments, 2);
    const cb = callback.bind.apply(callback, [this].concat(args));


    const longTimeout = {
        timer: null,
        clear: function () {
            if (this.timer)
                clearTimeout(this.timer);
        }
    };
    const max = 2147483647;
    if (ms <= max)
        longTimeout.timer = setTimeout(cb, ms);
    else {
        let count = Math.floor(ms / max); // the number of times we need to delay by max
        const rem = ms % max; // the length of the final delay
        (function delay() {
            if (count > 0) {
                count--;
                longTimeout.timer = setTimeout(delay, max);
            } else {
                longTimeout.timer = setTimeout(cb, rem);
            }
        })();
    }
    self.pushTimeout(timerLabel, longTimeout);
    mainLogger('info', 'Add new server timer: ' + timerLabel);
    return timerLabel;
};

ServiceTimer.prototype.setServiceInterval = function (intervalLabel, callback, ms) {
    const self = this;
    self.clearServiceInterval(intervalLabel);
    if (typeof callback !== 'function')
        throw new Error('Callback must be a function');
    ms = parseInt(ms);
    if (Number.isNaN(ms))
        throw new Error('Delay must be an integer');

    const args = Array.prototype.slice.call(arguments, 2);
    const cb = callback.bind.apply(callback, [this].concat(args));

    const interval = {
        interval: setInterval(cb, ms),
        clear: function () {
            if (this.interval)
                clearInterval(this.interval);
        }
    };
    self.pushInterval(intervalLabel, interval);
    console.log('Add new server interval: ' + intervalLabel);
    return intervalLabel;
};

/**
 * Clear service interval
 * @param   {String} intervalLabel
 * @returns {Boolean}
 */
ServiceTimer.prototype.clearServiceInterval = function (intervalLabel) {
    const self = this;
    for (let i = 0; i < self.activeIntervals.length; i++) {
        if (self.activeIntervals[i].intervalLabel === intervalLabel) {
            const intervalObj = self.activeIntervals[i].interval;
            if (intervalObj &&
                typeof intervalObj.clear === 'function') {
                intervalObj.clear();
                self.activeIntervals.splice(i, 1);
                mainLogger('info', 'Deleted interval ' + intervalLabel);
                return true;
            }
        }
    }
    return false;
};


/**
 * Clear service timeout
 * @param   {String} timerLabel
 * @returns {Boolean}
 */
ServiceTimer.prototype.clearServiceTimeout = function (timerLabel) {
    const self = this;
    for (let i = 0; i < self.activeTimers.length; i++) {
        if (self.activeTimers[i].timerLabel === timerLabel) {
            const timeoutObject = self.activeTimers[i].timer;
            if (timeoutObject &&
                typeof timeoutObject.clear === 'function') {
                timeoutObject.clear();
                self.activeTimers.splice(i, 1);
                mainLogger('info', 'Deleted timer ' + timerLabel);
                return true;
            }
        }
    }
    return false;
};


/**
 * Clear all service timeouts
 * @returns {Boolean}
 */
ServiceTimer.prototype.clearAll = function () {
    mainLogger('info', "Deleting all timers and intervals...");
    const self = this;
    let i, len;
    for (i = 0, len = self.activeTimers.length; i < len; ++i) {
        const timeoutObject = self.activeTimers[0].timer;
        const timerLabel = self.activeTimers[0].timerLabel;
        if (timeoutObject && typeof timeoutObject.clear === 'function') {
            timeoutObject.clear();
            self.activeTimers.splice(0, 1);
            mainLogger('info', 'Deleted timer: ' + timerLabel);
        }
    }
    for (i = 0, len = self.activeIntervals.length; i < len; ++i) {
        const intervalObject = self.activeIntervals[0].interval;
        const intervalLabel = self.activeIntervals[0].intervalLabel;
        if (intervalObject && typeof intervalObject.clear === 'function') {
            intervalObject.clear();
            self.activeIntervals.splice(0, 1);
            mainLogger('info', 'Deleted interval: ' + intervalLabel);
        }
    }
    return true;
};

/**
 * Clear all exept  this array of timers
 * @param {Array} timerObjLabelArr
 */
ServiceTimer.prototype.clearAllExeptThis = function (timerObjLabelArr) {
    if (!Array.isArray(timerObjLabelArr)) {
        throw new Error('TimerLabelArr must be array');
    }
    mainLogger('info', "Deleting all timers and intervals except:");
    mainLogger('info', timerObjLabelArr);
    const self = this;
    let i, len;
    for (i = 0, len = self.activeTimers.length; i < len; ++i) {
        const timeoutObject = self.activeTimers[0].timer;
        const timerLabel = self.activeTimers[0].timerLabel;
        if (timeoutObject && typeof timeoutObject.clear === 'function' && timerObjLabelArr.indexOf(timerLabel) === -1) {
            timeoutObject.clear();
            self.activeTimers.splice(0, 1);
            mainLogger('info', 'Deleted timer: ' + timerLabel);
        }
    }
    for (i = 0, len = self.activeIntervals.length; i < len; ++i) {
        const intervalObject = self.activeIntervals[0].interval;
        const intervalLabel = self.activeIntervals[0].intervalLabel;
        if (intervalObject && typeof intervalObject.clear === 'function' && timerObjLabelArr.indexOf(intervalLabel) === -1) {
            intervalObject.clear();
            self.activeIntervals.splice(0, 1);
            mainLogger('info', 'Deleted interval: ' + intervalLabel);
        }
    }
    return true;
};


module.exports = ServiceTimer;