'use strict';

const IOS = "IOS";
const ANDROID = "ANDROID";
const WINFON = "WINFON";
const DISPATCHER = "DISPATCHER";
const WEB = "WEB";
const CABINET = "CABINET";
const WORKER = "WORKER";
const HOSPITAL = "HOSPITAL";

module.exports = {
    IOS,
    ANDROID,
    WINFON,
    DISPATCHER,
    WEB,
    CABINET,
    WORKER,
    HOSPITAL,
};