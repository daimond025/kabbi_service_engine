const log4js = require('log4js');

log4js.configure({
    appenders: [
        {type: 'console'},
        {
            type: 'file',
            filename: 'logs/worker_shift.log',
            category: 'worker_shift',
            maxLogSize: 52428800,
            backups: 5

        },
        {
            type: 'file',
            filename: 'logs/order.log',
            category: 'order',
            maxLogSize: 52428800,
            backups: 5
        },
        {
            type: 'file',
            filename: 'logs/worker_block.log',
            category: 'worker_block',
            maxLogSize: 52428800,
            backups: 5

        }
    ]
});
const orderLog = log4js.getLogger('order');
const shiftLog = log4js.getLogger('worker_shift');
const blockLog = log4js.getLogger('worker_block');

/**
 * Order logger
 * @param  {Number} orderId
 * @param  {String} category
 * @param  {String} message
 */
function orderLogger(orderId, category, message) {
    const prefix = orderId + '  ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    message = prefix + message;
    switch (category) {
        case 'info':
            orderLog.info(message);
            break;
        case 'error':
            orderLog.error(message);
            break;
        default:
            orderLog.info(message);
    }
}

/**
 * Worker shift Logger
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {String} category
 * @param  {String} message
 */
function workerShiftLogger(tenantId, workerCallsign, category, message) {
    const prefix = tenantId + '_' + workerCallsign + '  ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    message = prefix + message;
    switch (category) {
        case 'info':
            shiftLog.info(message);
            break;
        case 'error':
            shiftLog.error(message);
            break;
        default:
            shiftLog.info(message);
    }
}

/**
 * Worker block Logger
 * @param  {Number} tenantId
 * @param  {Number} workerCallsign
 * @param  {String} category
 * @param  {String} message
 */
function workerBlockLogger(tenantId, workerCallsign, category, message) {
    const prefix = tenantId + '_' + workerCallsign + '  ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    message = prefix + message;
    switch (category) {
        case 'info':
            blockLog.info(message);
            break;
        case 'error':
            blockLog.error(message);
            break;
        default:
            blockLog.info(message);
    }
}


exports.orderLogger = orderLogger;
exports.workerShiftLogger = workerShiftLogger;
exports.workerBlockLogger = workerBlockLogger;

