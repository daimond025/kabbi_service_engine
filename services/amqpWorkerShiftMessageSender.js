"use strict";

const lib = require("./lib");
const config = lib.getConfig();
const amqp = require('amqplib/callback_api');
const mainLogger = require('./logger').mainLogger;

/**
 *  Amqp worker shift message publisher
 * @param {Number} tenantId
 * @param {Number} workerCallsign
 * @param {String} messageString
 * @param {Function} callback
 */

function sendMessage(tenantId, workerCallsign, messageString, callback) {
    const queueLabel = tenantId + "_worker_" + workerCallsign + '_shift';
    amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
        if (err) {
            return callback(err);
        } else {
            conn.createChannel(function (err, ch) {
                if (err) {
                    return callback(err);
                } else {
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true
                        });
                        ch.sendToQueue(queueLabel, new Buffer(messageString), {
                            persistent: true
                        });
                        return callback(null, 'message sent');
                    } catch (err) {
                        return callback(`Err of sending message:${messageString} to queue:${queueLabel}`);
                    }

                }
            });
            setTimeout(function () {
                try {
                    conn.close();
                } catch (err) {
                    mainLogger('error', err);
                }
            }, 500);
        }
    });
}

function existQueue(tenantId, workerCallsign, callback) {
    const queueLabel = tenantId + "_worker_" + workerCallsign + '_shift';
    amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
        if (err) {
            return callback(err);
        } else {
            conn.createChannel(function (err, ch) {
                if (err) {
                    return callback(err);
                } else {
                    try {
                        ch.assertQueue(queueLabel, {
                            durable: true
                        });
                        ch.checkQueue(queueLabel, function (err, ok) {
                            if(err !== null){
                                return callback(`Err of checkQueue :${queueLabel}`);

                            }else {
                                return callback(null, 1);
                            }
                        });
                    } catch (err) {
                        return callback(`Err of checkQueue :${queueLabel}`);
                    }

                }
            });
        }
    });
}

exports.sendMessage = sendMessage;
exports.existQueue = existQueue;
