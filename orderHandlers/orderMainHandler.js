'use strict';

const redisOrderManager = require('../database/redisDB/redisOrderManager');
const redisAsyncAnswerManager = require('../database/redisDB/redisAsyncAnswerManager');


const BaseGootaxOrder = require('./orderModules/baseGootaxOrder');
const logger = require('../services/logger');
const orderLogger = logger.orderLogger;
const config = require("../services/lib").getConfig();
const amqp = require('amqplib/callback_api');
const orderApi = require("../services/externalWebServices/orderApi");
const amqpWorkerMessageSender = require("../services/amqpWorkerMessageSender");
const getPositionById = require("../services/workerPositionManager").getPositionById;
const orderEventCommand = require('./orderEvent/orderEventOrderCommand');
const orderEventSenderService = require('./orderEvent/orderEventSenderService');
const DataProcessingService = require("../services/dataProcessingService");

/**
 * Order main handler
 * @param {Number}  tenantId
 * @param {Number}  orderId
 * @param {Number}  isFromLoader - (1/0) Was it order add after restart node process(isFromLoader = 1), or from rest api
 * @param {Function} callback
 */
module.exports = function (tenantId, orderId, isFromLoader, callback) {
    orderLogger(orderId, 'info', "OrderMainHandler CALLED!");
    redisOrderManager.getOrder(tenantId, orderId, (err, orderData) => {
        if (err) {
            orderLogger(orderId, 'error', `orderMainHandler->getOrder->Error: ${err.message}`);
            return callback(new Error('Error of getting order from redis'));
        }
        const orderContainer = {};
        const orderPositionId = parseInt(orderData.tariff.position_id);
        const orderPositionInfo = getPositionById(orderPositionId);

        if (orderPositionInfo) {
            orderLogger(orderId, 'info', `orderMainHandler->order has worker position_id:${orderPositionInfo.position_id}, name:${orderPositionInfo.name}`);

            orderContainer.instance = new BaseGootaxOrder(tenantId, orderId, isFromLoader);
        } else {
            orderLogger(orderId, 'error', `orderMainHandler->unsupported worker position (position_id:${orderPositionInfo.position_id}, name:${orderPositionInfo.name})`);
            return callback(new Error(`Unsupported order position: ${orderPositionId}`));
        }
        orderEventQueueListener(orderId, orderContainer, isFromLoader);
        return callback(null, 1);
    });
};


/**
 * Order event queue listener
 * @param orderId
 * @param orderContainer
 * @param isFromLoader
 */
function orderEventQueueListener(orderId, orderContainer, isFromLoader) {
    // Delay between order messages at order queue in seconds
    const messageWorkTime = 1.5;
    let connSelfClosed = false;
    let chanelSelfClosed = false;
    startListenQueue();

    /**
     * Order queue listener
     */
    function startListenQueue() {
        amqp.connect(`amqp://${config.RABBITMQ_MAIN_HOST}:${config.RABBITMQ_MAIN_PORT}`, (err, conn) => {
            if (err) {
                orderLogger(orderId, 'error', `orderMainHandler->queue->connect->Error: ${err.message}. Need to try reconnect...`);
                return setTimeout(startListenQueue, 2000);
            }
            conn.on("error", err => {
                orderLogger(orderId, 'error', err.message);
                if (err.message !== "Connection closing") {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->connection->Error ${err.message}`);
                }
            });
            conn.on("close", () => {
                if (!connSelfClosed) {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->connection closed, need to try reconnect...`);
                    return setTimeout(startListenQueue, 2000);
                }
            });
            orderLogger(orderId, 'info', `orderMainHandler->queue->connected`);
            DataProcessingService.addOrder(orderId);
            conn.createChannel((err, ch) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->create channel->Error: ${err.message}`);
                }
                ch.on("error", (err) => {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->chanel->Error: ${err.message}`);
                });
                ch.on("close", () => {
                    if (!chanelSelfClosed) {
                        orderLogger(orderId, 'error', `orderMainHandler->queue->channel closed`);
                    }
                });
                let delayToStartingConsumeQueue = 0;
                if (isFromLoader) {
                    delayToStartingConsumeQueue = 1.5;
                }
                orderLogger(orderId, 'info', `orderMainHandler->queue->channel created`);
                setTimeout(() => {
                    const q = `order_${orderId}`;
                    ch.assertQueue(q, {
                        durable: true
                    });
                    ch.prefetch(1);
                    orderLogger(orderId, 'info', `orderMainHandler->queue->waiting for task...`);
                    ch.consume(q, (message) => {
                        orderLogger(orderId, 'info', `orderMainHandler->queue->new task!`);
                        handleTask(message, q, conn, ch);
                    }, {noAck: false});
                }, delayToStartingConsumeQueue * 1000);

            });
        });
    }


    /**
     * Order task handler
     * @param {object} message
     * @param {string} queueLabel
     * @param {object} connection - queue connection
     * @param {object} channel - queue channel
     */
    function handleTask(message, queueLabel, connection, channel) {
        if (!message) {
            return;
        }
        let taskObject;
        try {
            taskObject = JSON.parse(message.content.toString());
        } catch (err) {
            orderLogger(orderId, 'info', `orderMainHandler->JSON.parse(message.content.toString())->Error: ${err.message}`);
            return;
        }
        orderLogger(orderId, 'info', `orderMainHandler->queue->original task:${JSON.stringify(taskObject)}`);
        //@hack  - modify some event to other events, for some cases
        taskObject = orderContainer.instance.modifyEvent(taskObject);
        orderLogger(orderId, 'info', `orderMainHandler->queue->modified task:${JSON.stringify(taskObject)}`);

        let command = taskObject.command;
        let senderService = taskObject.sender_service;

        if (orderEventCommand.destroyService === command) {
            return destroyQueue(connection, channel, queueLabel);
        }

        console.log("Новая задача");
        console.log(taskObject);


        //Событие нужно прогнать через api orderupdate_order
        if (orderEventCommand.isOrderEventMustBeProcessedByOrderService(command, senderService)) {
            orderApi.sendRequest(taskObject, (err, result) => {
                orderLogger(orderId, 'info', `Send worker->Result : ${message.content.toString()}   ${JSON.stringify(result)}`);
                orderLogger(orderId, 'info', result);

                console.log("Ответ от запроса ");
                console.log(result);

                if (err) {
                    orderLogger(orderId, 'error', `orderMainHandler->orderApi.sendRequest->Error: ${err.message}`);
                    result = orderApi.getFormattedErrorResult(taskObject.uuid, orderId, senderService);
                } else {
                    orderLogger(orderId, 'info', `orderMainHandler->orderApi.sendRequest->Result: ${JSON.stringify(result)}`);
                }


                if (orderEventSenderService.needAddAsyncAnswer(senderService)) {
                    let messageTTL = 900;
                    if (senderService === orderEventSenderService.workerService) {
                        messageTTL = 900;
                    }
                    // Запишем ответ в редис
                    redisAsyncAnswerManager.setAsyncAnswer(taskObject.uuid, result, messageTTL, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderMainHandler->orderApi.setAsyncAnswer->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderMainHandler->orderApi.setAsyncAnswer->Result: ${result}`);
                        }
                    });
                }
                //Если это было сообщение от водителя, то продублируем ответ в очередь
                if (senderService === orderEventSenderService.workerService) {
                    const result_answer = result;

                    // псевдо правильный ответ - его не надо обрабатывать
                    if((typeof(result_answer.code) !== undefined ) && (result_answer.code === 500)) {
                        result_answer.code = 0;
                        result_answer.info = "OK";
                        result_answer.result.set_result = 1;

                        orderLogger(orderId, 'info', `orderMainHandler->amqpWorkerMessageSender->RepeatStatus: ${result_answer}`);
                    }

                    let tenantId = taskObject.tenant_id;
                    let workerCallsign = taskObject.change_sender_id;
                    let messageString = JSON.stringify(result_answer);
                    let messageTTL = 300;

                    amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTTL, (err, result) => {
                        if (err) {
                            orderLogger(orderId, 'error', `orderMainHandler->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                        } else {
                            orderLogger(orderId, 'info', `orderMainHandler->amqpWorkerMessageSender->Result: ${result_answer}`);
                        }
                    });
                }


                const isGoodOrderAnswer = orderApi.isGoodResult(result, senderService);
                if (!isGoodOrderAnswer) {
                    ack(channel, command, message);
                    return;
                }

                orderContainer.instance.doEvent(taskObject)
                    .then(result => {
                        orderLogger(orderId, 'info', `orderMainHandler-> orderContainer.instance.doEvent->Result: ${result}`);
                        ack(channel, command, message);
                    })
                    .catch(err => {
                        orderLogger(orderId, 'error', `orderMainHandler-> orderContainer.instance.doEvent->Error: ${err.message}`);
                        ack(channel, command, message);
                    });
            });
        } else {
            // Если водитель отказался от заказа при массовом предложении заказа,
            // то сразу отдаем ему успешный ответ
            if (command === orderEventCommand.refuseOfferedOrderMultiple) {
                let result = orderApi.getFormattedGoodResult(taskObject.uuid, orderId, senderService);
                let messageTTL = 900;
                // Запишем ответ в редис
                redisAsyncAnswerManager.setAsyncAnswer(taskObject.uuid, result, messageTTL, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderMainHandler->redisAsyncAnswerManager.setAsyncAnswer->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderMainHandler->redisAsyncAnswerManager.setAsyncAnswer->Result: ${result}`);
                    }
                });
                let tenantId = taskObject.tenant_id;
                let workerCallsign = taskObject.change_sender_id;
                let messageString = JSON.stringify(result);
                messageTTL = 60;
                amqpWorkerMessageSender.sendMessage(tenantId, workerCallsign, messageString, messageTTL, (err, result) => {
                    if (err) {
                        orderLogger(orderId, 'error', `orderMainHandler->amqpWorkerMessageSender.sendMessage->Error: ${err.message}`);
                    } else {
                        orderLogger(orderId, 'info', `orderMainHandler->amqpWorkerMessageSender->Result: ${result}`);
                    }
                });
            }
            orderContainer.instance.doEvent(taskObject)
                .then(result => {
                    orderLogger(orderId, 'info', `orderMainHandler-> orderContainer.instance.doEvent->Result: ${result}`);
                    ack(channel, command, message);
                })
                .catch(err => {
                    orderLogger(orderId, 'error', `orderMainHandler-> orderContainer.instance.doEvent->Error: ${err.message}`);
                    ack(channel, command, message);
                });

        }
    }

    /**
     * Acknowledge  message in queue
     * @param channel
     * @param command
     * @param message
     */
    function ack(channel, command, message) {
        if (orderEventCommand.isReturnRealResult(command)) {
            orderLogger(orderId, 'info', `orderMainHandler->queue->task done`);
            try {
                channel.ack(message);
            } catch (err) {
                orderLogger(orderId, 'error', `orderMainHandler->queue->channel.ack(message)->Error: ${err.message}`);
            }
        } else {
            setTimeout(() => {
                orderLogger(orderId, 'info', `orderMainHandler->queue->task done`);
                try {
                    channel.ack(message);
                } catch (err) {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->channel.ack(message)->Error: ${err.message}`);
                }
            }, messageWorkTime * 1000);
        }
    }

    /**
     * Destroy order queue
     * @param connection
     * @param channel
     * @param queueLabel
     */
    function destroyQueue(connection, channel, queueLabel) {
        try {
            channel.deleteQueue(queueLabel, {ifUnused: false, ifEmpty: false}, (err, ok) => {
                if (err) {
                    orderLogger(orderId, 'error', `orderMainHandler->queue->deleteQueue->Error: ${err.message}`);
                } else {
                    orderLogger(orderId, 'info', "orderMainHandler->queue->order queue deleted");
                    try {
                        channel.close();
                        chanelSelfClosed = true;
                        orderLogger(orderId, 'info', "orderMainHandler->queue->channel closed");
                    } catch (err) {
                        orderLogger(orderId, 'error', `orderMainHandler->queue->channel closing->Error: ${err.message}`);
                    }
                    try {
                        connection.close();
                        connSelfClosed = true;
                        orderLogger(orderId, 'info', "orderMainHandler->queue->connection closed");
                    } catch (err) {
                        orderLogger(orderId, 'error', `orderMainHandler->queue->connection closing->Error: ${err.message}`);
                    }
                    delete orderContainer.instance;
                    DataProcessingService.delOrder(orderId);
                    orderLogger(orderId, 'info', "orderMainHandler->orderContainer deleted");
                }
            });
        } catch (err) {
            orderLogger(orderId, 'error', `orderMainHandler->queue->deleteQueue->Error: ${err.message}`);
        }
    }


}